import gulp from "gulp";
import gulpMjml from "gulp-mjml";
import mjml from "mjml";

gulp.task("default", () => {
  return gulp
    .src("./src/*.mjml")
    .pipe(gulpMjml(mjml, { minfy: false, validationLevel: "strict" }))
    .on("error", function (error) {
      console.error(error.toString());
      this.emit("end");
    })
    .pipe(gulp.dest("./out"));
});
