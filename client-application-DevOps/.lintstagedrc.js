module.exports = {
  "*/src/**/*.{js,jsx,ts,tsx}": (filenames) =>
    filenames.length > 10
      ? "eslint . --cache --cache-location ./eslint-cache --fix"
      : `eslint ${filenames.join(" ")} --cache --cache-location .eslint-cache --fix`
};
