module.exports = {
  i18n: {
    defaultLocale: "en",
    locales: ["en", "de"],
  },
  serializeConfig: false,
  reloadOnPrerender: process.env.NODE_ENV !== "production",
};
