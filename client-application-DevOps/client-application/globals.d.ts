/// <reference types="jquery" />
import JitsiMeetJS from "@together/lib-jitsi-meet";

declare global {
  interface Window {
    // eslint-disable-next-line no-undef
    $?: JQueryStatic;
    JitsiMeetJS?: JitsiMeetJS;
  }
}
