declare module "colyseus-schema" {
  import { MapSchema, Schema, type } from "@colyseus/schema";
  class RoomParticipantQuickReactionSchema extends Schema {
    @type("string") type: string;
    @type("string") value: string;
    @type("number") createdOn?: number;
  }
  export class RoomParticipantSchema extends Schema {
    @type("string") userId!: string;
    @type("string") conferenceUserId!: string;
    @type("string") nickname!: string;
    @type("string") userColor!: string;
    @type("string") userRoleId!: string;
    @type("string") userRoleName!: string;
    @type("number") sortIndex!: number;
    @type("boolean") isOnStage: boolean;
    @type("boolean") isVideoMirrored: boolean;
    @type("boolean") isAudioMuted: boolean;
    @type("boolean") isVideoMuted: boolean;
    @type(RoomParticipantQuickReactionSchema)
    quickReaction?: RoomParticipantQuickReactionSchema;
  }

  export class RoomStageItemPayloadSchema extends Schema {
    // participant
    @type("string") userId!: string;
    @type("string") conferenceUserId!: string;
    @type("string") nickname!: string;
    @type("string") userColor!: string;
    // content
    @type("string") fileId!: string;
  }

  export class RoomStageItemSchema extends Schema {
    @type("string") id!: string;
    @type("string") title!: string;
    @type("string") type!: string;
    @type("string") ownerId!: string;
    @type("number") sortIndex!: number;
    @type(RoomStageItemPayloadSchema)
    payload: RoomStageItemPayloadSchema;
  }
  export class RoomSchema extends Schema {
    @type("string") roomId!: string;
    @type("string") roomName!: string;
    @type({ map: RoomStageItemSchema })
    stage: MapSchema<RoomStageItemSchema>;
    @type({ map: RoomParticipantSchema })
    participants: MapSchema<RoomParticipantSchema>;
    @type({ map: "string" })
    clientUserIds: MapSchema<string>;
  }
}
