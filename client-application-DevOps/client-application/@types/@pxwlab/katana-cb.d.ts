import "@pxwlab/katana-cb";

declare module "@pxwlab/katana-cb" {
  interface VideoThumbActionsProps {
    toggleLabel?: string;
    toggleIcon?: string;
  }

  interface VideoThumbActionsItemProps {
    label?: string;
  }
}
