declare module "client-application" {
  type Nullable<T> = T | null;

  interface Model {
    id: string;
    date_created: string;
    date_updated: string;
  }

  export interface PageIndexFeature extends Model {
    title?: Nullable<string>;
    description?: Nullable<string>;
  }

  export interface PageIndex extends Model {
    logo?: Nullable<string>;
    hero_title?: Nullable<string>;
    hero_description?: Nullable<string>;
    hero_image?: Nullable<string>;
    features: PageIndexFeature[];
    features_title?: Nullable<string>;
    features_image?: Nullable<string>;
  }

  export type ContentTypeMap = {
    page_index_features: PageIndexFeature;
    page_index: PageIndex;
  };
}
