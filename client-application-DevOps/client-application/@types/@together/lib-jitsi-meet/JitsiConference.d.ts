/* eslint-disable eslint-comments/disable-enable-pair */

/* eslint-disable unicorn/prevent-abbreviations */
declare module "@together/lib-jitsi-meet/JitsiConference" {
  import EventEmitter from "events";
  import JitsiConnection from "@together/lib-jitsi-meet/JitsiConnection";
  import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
  import JitsiTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiTrack";
  import RTC from "@together/lib-jitsi-meet/modules/RTC/RTC";
  import * as MediaType from "@together/lib-jitsi-meet/service/RTC/MediaType";
  import JitsiParticipant from "./JitsiParticipant";

  /*
  import ConnectionQuality from "./modules/connectivity/ConnectionQuality";
  import ParticipantConnectionStatusHandler from "./modules/connectivity/ParticipantConnectionStatus";
  import P2PDominantSpeakerDetection from "./modules/detection/P2PDominantSpeakerDetection";
  import E2ePing from "./modules/e2eping/e2eping";
  import Jvb121EventGenerator from "./modules/event/Jvb121EventGenerator";
  import { ReceiveVideoController } from "./modules/qualitycontrol/ReceiveVideoController";
  import { SendVideoController } from "./modules/qualitycontrol/SendVideoController";
  import RecordingManager from "./modules/recording/RecordingManager";
  import { CodecSelection } from "./modules/RTC/CodecSelection";
  import AvgRTPStatsReporter from "./modules/statistics/AvgRTPStatsReporter";
  import SpeakerStatsCollector from "./modules/statistics/SpeakerStatsCollector";
  import Statistics from "./modules/statistics/statistics";
  import VideoSIPGW from "./modules/videosipgw/VideoSIPGW";
  import JingleSessionPC from "./modules/xmpp/JingleSessionPC";
  import XMPP from "./modules/xmpp/xmpp";
  import Transcriber from "./_unsorted/module/transcription/transcriber";
  import JitsiVideoSIPGWSession from "./_unsorted/module/videosipgw/JitsiVideoSIPGWSession";
  */

  type ConnectionQuality = unknown;
  type ParticipantConnectionStatusHandler = unknown;
  type P2PDominantSpeakerDetection = unknown;
  type E2ePing = unknown;
  type Jvb121EventGenerator = unknown;
  type ReceiveVideoController = unknown;
  type SendVideoController = unknown;
  type RecordingManager = unknown;
  type CodecSelection = unknown;
  type AvgRTPStatsReporter = unknown;
  type SpeakerStatsCollector = unknown;
  type Statistics = unknown;
  type VideoSIPGW = unknown;
  type JingleSessionPC = unknown;
  type XMPP = unknown;
  type Transcriber = unknown;
  type JitsiVideoSIPGWSession = unknown;

  export type JitsiConferenceOptionConfig = {
    /**
     * how many samples are to be
     * collected by {@link AvgRTPStatsReporter}, before arithmetic mean is
     * calculated and submitted to the analytics module.
     * @default 15
     */
    avgRtpStatsN?: number;
    /**
     * enables the ICE restart logic.
     * @default false
     */
    enableIceRestart?: boolean;
    /**
     * The requested amount of videos are going to be delivered after the
     * value is in effect. Set to -1 for unlimited or all available videos.
     * @default -1
     */
    channelLastN?: number;
    /**
     * "Math.random() < forceJVB121Ratio" will determine whether a 2 people
     * conference should be moved to the JVB instead of P2P. The decision is made on
     * the responder side, after ICE succeeds on the P2P connection.
     */
    forceJVB121Ratio?: number;
    p2p?: {
      /**
       * when set to <tt>true</tt>
       * the peer to peer mode will be enabled. It means that when there are only 2
       * participants in the conference an attempt to make direct connection will be
       * made. If the connection succeeds the conference will stop sending data
       * through the JVB connection and will use the direct one instead.
       */
      enabled?: boolean;
      /**
       * a delay given in
       * seconds, before the conference switches back to P2P, after the 3rd
       * participant has left the room.
       */
      backToP2PDelay?: number;
    };
  };

  export type JitsiConferenceOptions = {
    /**
     * the name of the conference
     */
    name: string;
    /**
     * the JitsiConnection object for this JitsiConference.
     */
    connection: JitsiConnection;
    /**
     * properties / settings related to the conference that will be created.
     */
    config?: JitsiConferenceOptionConfig;
  };

  declare class JitsiConference {
    /**
     * Create a resource for the a jid. We use the room nickname (the resource part
     * of the occupant JID, see XEP-0045) as the endpoint ID in colibri. We require
     * endpoint IDs to be 8 hex digits because in some cases they get serialized
     * into a 32bit field.
     *
     * @param {string} jid - The id set onto the XMPP connection.
     * @param {boolean} isAuthenticatedUser - Whether or not the user has connected
     * to the XMPP service with a password.
     * @returns {string}
     * @static
     */
    static resourceCreator(jid: string, isAuthenticatedUser: boolean): string;

    /**
     * Creates a JitsiConference object with the given name and properties.
     *
     * Note: this constructor is not a part of the public API (objects should be
     * created using JitsiConnection.createConference).
     * @private
     * */
    constructor(options: JitsiConferenceOptions);

    eventEmitter: EventEmitter;

    /**
     * The object which monitors local and remote connection statistics (e.g.
     * sending bitrate) and calculates a number which represents the connection
     * quality.
     */
    connectionQuality: ConnectionQuality;
    /**
     * Reports average RTP statistics to the analytics module.
     * @type {AvgRTPStatsReporter}
     */
    avgRtpStatsReporter: AvgRTPStatsReporter;
    /**
     * Indicates whether the connection is interrupted or not.
     */
    isJvbConnectionInterrupted: boolean;
    /**
     * The object which tracks active speaker times
     */
    speakerStatsCollector: SpeakerStatsCollector;
    /**
     * Stores reference to deferred start P2P task. It's created when 3rd
     * participant leaves the room in order to avoid ping pong effect (it
     * could be just a page reload).
     * @type {number|null}
     */
    deferredStartP2PTask: number | null;
    /**
     * A delay given in seconds, before the conference switches back to P2P
     * after the 3rd participant has left.
     * @type {number}
     */
    backToP2PDelay: number;
    /**
     * If set to <tt>true</tt> it means the P2P ICE is no longer connected.
     * When <tt>false</tt> it means that P2P ICE (media) connection is up
     * and running.
     * @type {boolean}
     */
    isP2PConnectionInterrupted: boolean;
    /**
     * Flag set to <tt>true</tt> when P2P session has been established
     * (ICE has been connected) and this conference is currently in the peer to
     * peer mode (P2P connection is the active one).
     * @type {boolean}
     */
    p2p: boolean;
    /**
     * A JingleSession for the direct peer to peer connection.
     * @type {JingleSessionPC}
     */
    p2pJingleSession: JingleSessionPC;
    videoSIPGWHandler: VideoSIPGW;
    recordingManager: RecordingManager;
    connection: JitsiConnection;
    xmpp: XMPP;
    codecSelection: CodecSelection;
    room: unknown;
    e2eping: E2ePing;
    rtc: RTC;
    receiveVideoController: ReceiveVideoController;
    sendVideoController: SendVideoController;
    participantConnectionStatus: ParticipantConnectionStatusHandler;
    statistics: Statistics;
    /**
     * Emits {@link JitsiConferenceEvents.JVB121_STATUS}.
     * @type {Jvb121EventGenerator}
     */
    jvb121Status: Jvb121EventGenerator;
    p2pDominantSpeakerDetection: P2PDominantSpeakerDetection;
    /**
     * Joins the conference.
     * @param password {string} the password
     */
    join(password: string): void;
    /**
     * Authenticates and upgrades the role of the local participant/user.
     *
     * @returns {Object} A <tt>thenable</tt> which (1) settles when the process of
     * authenticating and upgrading the role of the local participant/user finishes
     * and (2) has a <tt>cancel</tt> method that allows the caller to interrupt the
     * process.
     */
    authenticateAndUpgradeRole(options: {
      id: string;
      password: string;
      roomPassword: string;
      onLoginSuccessful?: () => void;
    }): Promise<void> & { cancel: () => void };

    /**
     * Check if joined to the conference.
     */
    isJoined(): boolean;

    /**
     * Tells whether or not the P2P mode is enabled in the configuration.
     * @return {boolean}
     */
    isP2PEnabled(): boolean;

    /**
     * When in P2P test mode, the conference will not automatically switch to P2P
     * when there 2 participants.
     * @return {boolean}
     */
    isP2PTestModeEnabled(): boolean;

    /**
     * Leaves the conference.
     * @returns {Promise}
     */
    leave(): Promise<void>;

    /**
     * Returns name of this conference.
     */
    getName(): string;

    /**
     * Returns the {@link JitsiConnection} used by this this conference.
     */
    getConnection(): JitsiConnection;

    /**
     * Check if authentication is enabled for this conference.
     */
    isAuthEnabled(): boolean;

    /**
     * Check if user is logged in.
     */
    isLoggedIn(): boolean;

    /**
     * Get authorized login.
     */
    getAuthLogin(): unknown;

    /**
     * Check if external authentication is enabled for this conference.
     */
    isExternalAuthEnabled(): boolean;

    /**
     * Get url for external authentication.
     * @param {boolean} [urlForPopup] if true then return url for login popup,
     *                                else url of login page.
     * @returns {Promise}
     */
    getExternalAuthUrl(urlForPopup?: boolean): Promise<unknown>;

    /**
     * Returns the local tracks of the given media type, or all local tracks if no
     * specific type is given.
     * @param {MediaType} [mediaType] Optional media type (audio or video).
     */
    getLocalTracks(mediaType?: typeof MediaType): JitsiLocalTrack[];

    /**
     * Obtains local audio track.
     * @return {JitsiLocalTrack|null}
     */
    getLocalAudioTrack(): JitsiLocalTrack | null;

    /**
     * Obtains local video track.
     * @return {JitsiLocalTrack|null}
     */
    getLocalVideoTrack(): JitsiLocalTrack | null;

    /**
     * Obtains the performance statistics.
     * @returns {Object|null}
     */
    getPerformanceStats(): unknown | null;

    /**
     * Attaches a handler for events(For example - "participant joined".) in the
     * conference. All possible event are defined in JitsiConferenceEvents.
     * @param eventId the event ID.
     * @param handler handler for the event.
     *
     * Note: consider adding eventing functionality by extending an EventEmitter
     * impl, instead of rolling ourselves
     */
    addEventListener(eventId: string, handler: (...arguments: unknown[] | undefined) => void): void;
    /**
     * Removes event listener
     * @param eventId the event ID.
     * @param [handler] optional, the specific handler to unbind
     *
     * Note: consider adding eventing functionality by extending an EventEmitter
     * impl, instead of rolling ourselves
     */
    removeEventListener(
      eventId: string,
      handler: (...arguments: unknown[] | undefined) => void
    ): void;

    /**
     * Receives notifications from other participants about commands / custom events
     * (sent by sendCommand or sendCommandOnce methods).
     * @param command {String} the name of the command
     * @param handler {Function} handler for the command
     */
    addCommandListener(
      command: string,
      handler: (...arguments: unknown[] | undefined) => void
    ): void;
    /**
     * Removes command  listener
     * @param command {String} the name of the command
     * @param handler {Function} handler to remove for the command
     */
    removeCommandListener(
      command: string,
      handler: (...arguments: unknown[] | undefined) => void
    ): void;

    /**
     * Sends text message to the other participants in the conference
     * @param message the text message.
     * @param elementName the element name to encapsulate the message.
     * @deprecated Use 'sendMessage' instead. TODO: this should be private.
     */
    sendTextMessage(message: string, elementName?: string): void;

    /**
     * Send private text message to another participant of the conference
     * @param id the id of the participant to send a private message.
     * @param message the text message.
     * @param elementName the element name to encapsulate the message.
     * @deprecated Use 'sendMessage' instead. TODO: this should be private.
     */
    sendPrivateTextMessage(id: string, message: string, elementName?: string): void;

    /**
     * Send presence command.
     * @param name {String} the name of the command.
     * @param values {Object} with keys and values that will be sent.
     */
    sendCommand(name: string, values: Record<string, string | number | unknown>): void;

    /**
     * Send presence command one time.
     * @param name {String} the name of the command.
     * @param values {Object} with keys and values that will be sent.
     */
    sendCommandOnce(name: string, values: Record<string, string | number | unknown>): void;

    /**
     * Removes presence command.
     * @param name {String} the name of the command.
     */
    removeCommand(name: string): void;

    /**
     * Sets the display name for this conference.
     * @param name the display name to set
     */
    setDisplayName(name: string): void;

    /**
     * Set new subject for this conference. (available only for moderator)
     * @param {string} subject new subject
     */
    setSubject(subject: string): void;

    /**
     * Get a transcriber object for all current participants in this conference
     * @return {Transcriber} the transcriber object
     */
    getTranscriber(): Transcriber;

    /**
     * Returns the transcription status.
     *
     * @returns {String} "on" or "off".
     */
    getTranscriptionStatus(): string;

    /**
     * Adds JitsiLocalTrack object to the conference.
     * @param {JitsiLocalTrack} track the JitsiLocalTrack object.
     * @returns {Promise<JitsiLocalTrack>}
     * @throws {Error} if the specified track is a video track and there is already
     * another video track in the conference.
     */
    addTrack(track: JitsiLocalTrack): Promise<JitsiLocalTrack>;

    /**
     * Clear JitsiLocalTrack properties and listeners.
     * @param track the JitsiLocalTrack object.
     */
    onLocalTrackRemoved(track: JitsiLocalTrack): void;

    /**
     * Removes JitsiLocalTrack from the conference and performs
     * a new offer/answer cycle.
     * @param {JitsiLocalTrack} track
     * @returns {Promise}
     */
    removeTrack(track: JitsiLocalTrack): Promise<void>;

    /**
     * Replaces oldTrack with newTrack and performs a single offer/answer
     * cycle after both operations are done. Either oldTrack or newTrack
     * can be null; replacing a valid 'oldTrack' with a null 'newTrack'
     * effectively just removes 'oldTrack'
     * @param {JitsiLocalTrack} oldTrack the current stream in use to be replaced
     * @param {JitsiLocalTrack} newTrack the new stream to use
     * @returns {Promise} resolves when the replacement is finished
     */
    replaceTrack(oldTrack: JitsiLocalTrack | null, newTrack: JitsiLocalTrack | null): Promise<void>;

    /**
     * Get role of the local user.
     * @returns {string} user role: 'moderator' or 'none'
     */
    getRole(): string;

    /**
     * Returns whether or not the current conference has been joined as a hidden
     * user.
     *
     * @returns {boolean|null} True if hidden, false otherwise. Will return null if
     * no connection is active.
     */
    isHidden(): boolean | null;

    /**
     * Check if local user is moderator.
     * @returns {boolean|null} true if local user is moderator, false otherwise. If
     * we're no longer in the conference room then <tt>null</tt> is returned.
     */
    isModerator(): boolean | null;

    /**
     * Set password for the room.
     * @param {string} password new password for the room.
     * @returns {Promise}
     */
    lock(password: string): Promise<void>;

    /**
     * Remove password from the room.
     * @returns {Promise}
     */
    unlock(): Promise<void>;

    /**
     * Elects the participant with the given id to be the selected participant in
     * order to receive higher video quality (if simulcast is enabled).
     * Or cache it if channel is not created and send it once channel is available.
     * @param participantId the identifier of the participant
     * @throws NetworkError or InvalidStateError or Error if the operation fails.
     * @returns {void}
     */
    selectParticipant(participantId: string): void;

    /*
     * Elects participants with given ids to be the selected participants in order
     * to receive higher video quality (if simulcast is enabled). The argument
     * should be an array of participant id strings or an empty array; an error will
     * be thrown if a non-array is passed in. The error is thrown as a layer of
     * protection against passing an invalid argument, as the error will happen in
     * the bridge and may not be visible in the client.
     *
     * @param {Array<strings>} participantIds - An array of identifiers for
     * participants.
     * @returns {void}
     */
    selectParticipants(participantIds: string[]): void;

    /**
     * Obtains the current value for "lastN". See {@link setLastN} for more info.
     * The number of videos requested from the bridge, -1 represents unlimited or all available videos.
     * @returns {number}
     */
    getLastN(): number;

    /**
     * Selects a new value for "lastN". The requested amount of videos are going
     * to be delivered after the value is in effect. Set to -1 for unlimited or
     * all available videos.
     * @param lastN the new number of videos the user would like to receive.
     * @throws Error or RangeError if the given value is not a number or is smaller
     * than -1.
     */
    setLastN(lastN: number): void;

    /**
     * Checks if the participant given by participantId is currently included in
     * the last N.
     * @param {string} participantId the identifier of the participant we would
     * like to check.
     * @return {boolean} true if the participant with id is in the last N set or
     * if there's no last N set, false otherwise.
     * @deprecated this method should never be used to figure out the UI, but
     * {@link ParticipantConnectionStatus} should be used instead.
     */
    isInLastN(participantId: string): boolean;

    /**
     * @return Array<JitsiParticipant> an array of all participants in this
     * conference.
     */
    getParticipants(): JitsiParticipant[];

    /**
     * Returns the number of participants in the conference, including the local
     * participant.
     * @param countHidden {boolean} Whether or not to include hidden participants
     * in the count. Default: false.
     */
    getParticipantCount(countHidden?: boolean): number;

    /**
     * @returns {JitsiParticipant} the participant in this conference with the
     * specified id (or undefined if there isn't one).
     * @param id the id of the participant.
     */
    getParticipantById(id: string): JitsiParticipant | undefined;

    /**
     * Grant owner rights to the participant.
     * @param {string} id id of the participant to grant owner rights to.
     */
    grantOwner(id: string): void;

    /**
     * Revoke owner rights to the participant or local Participant as
     * the user might want to refuse to be a moderator.
     * @param {string} id id of the participant to revoke owner rights to.
     */
    revokeOwner(id: string): void;

    /**
     * Kick participant from this conference.
     * @param {string} id id of the participant to kick
     * @param {string} reason reason of the participant to kick
     */
    kickParticipant(id: string, reason: string): void;

    /**
     * Mutes a participant.
     * @param {string} id The id of the participant to mute.
     */
    muteParticipant(id: string, mediaType: string): void;

    /**
     * Allows to check if there is at least one user in the conference
     * that supports DTMF.
     * @returns {boolean} true if somebody supports DTMF, false otherwise
     */
    isDTMFSupported(): boolean;

    /**
     * Returns the local user's ID
     * @return {string} local user's ID
     */
    myUserId(): string;

    /**
     * Sends DTMF tones if possible.
     *
     * @param {string} tones - The DTMF tones string as defined by {@code RTCDTMFSender.insertDTMF}, 'tones' argument.
     * @param {number} duration - The amount of time in milliseconds that each DTMF should last. It's 200ms by default.
     * @param {number} interToneGap - The length of time in miliseconds to wait between tones. It's 200ms by default.
     *
     * @returns {void}
     */
    sendTones(tones: string, duration: number, pause: number): void;

    /**
     * Starts recording the current conference.
     *
     * @param {Object} options - Configuration for the recording. See
     * {@link Chatroom#startRecording} for more info.
     * @returns {Promise} See {@link Chatroom#startRecording} for more info.
     */
    startRecording(options: unknown): Promise<unknown>;

    /**
     * Stop a recording session.
     *
     * @param {string} sessionID - The ID of the recording session that
     * should be stopped.
     * @returns {Promise} See {@link Chatroom#stopRecording} for more info.
     */
    stopRecording(sessionID: string): Promise<unknown>;
    /**
     * Returns true if the SIP calls are supported and false otherwise
     */
    isSIPCallingSupported(): boolean;
    /**
     * Dials a number.
     * @param number the number
     */
    dial(number: number): Promise<unknown>;

    /**
     * Hangup an existing call
     */
    hangup(): Promise<unknown>;

    /**
     * Starts the transcription service.
     */
    startTranscriber(): Promise<unknown>;

    /**
     * Stops the transcription service.
     */
    stopTranscriber(): Promise<unknown>;
    /**
     * Returns the phone number for joining the conference.
     */
    getPhoneNumber(): string;

    /**
     * Returns the pin for joining the conference with phone.
     */
    getPhonePin(): string;

    /**
     * Returns the meeting unique ID if any.
     *
     * @returns {string|undefined}
     */
    getMeetingUniqueId(): string | undefined;

    /**
     * Will return P2P or JVB <tt>TraceablePeerConnection</tt> depending on
     * which connection is currently active.
     *
     * @return {TraceablePeerConnection|null} null if there isn't any active
     * <tt>TraceablePeerConnection</tt> currently available.
     * @public (FIXME how to make package local ?)
     */
    getConnectionState(): string | null;

    /**
     * Make all new participants mute their audio/video on join.
     * @param policy {Object} object with 2 boolean properties for video and audio:
     * @param {boolean} audio if audio should be muted.
     * @param {boolean} video if video should be muted.
     */
    setStartMutedPolicy(policy: { audio: boolean; video: boolean }): void;

    /**
     * Returns current start muted policy
     * @returns {Object} with 2 properties - audio and video.
     */
    getStartMutedPolicy(): boolean;

    /**
     * Check if audio is muted on join.
     */
    isStartAudioMuted(): boolean;

    /**
     * Check if video is muted on join.
     */
    isStartVideoMuted(): boolean;

    /**
     * Returns measured connectionTimes.
     */
    getConnectionTimes(): unknown;
    /**
     * Sets a property for the local participant.
     */
    setLocalParticipantProperty(name: string, value: string | number | boolean): void;
    /**
     *  Removes a property for the local participant and sends the updated presence.
     */
    removeLocalParticipantProperty(name: string): void;

    /**
     * Gets a local participant property.
     *
     * @return value of the local participant property if the tagName exists in the
     * list of properties, otherwise returns undefined.
     */
    getLocalParticipantProperty(name: string): string | number | boolean | undefined;

    /**
     * Sends the given feedback through CallStats if enabled.
     *
     * @param overallFeedback an integer between 1 and 5 indicating the
     * user feedback
     * @param detailedFeedback detailed feedback from the user. Not yet used
     * @returns {Promise} Resolves if feedback is submitted successfully.
     */
    sendFeedback(overallFeedback: number, detailedFeedback: string): Promise<void>;

    /**
     * Returns true if the callstats integration is enabled, otherwise returns
     * false.
     *
     * @returns true if the callstats integration is enabled, otherwise returns
     * false.
     */
    isCallstatsEnabled(): boolean;

    /**
     * Finds the SSRC of a given track
     *
     * @param track
     * @returns {number|undefined} the SSRC of the specificed track, otherwise undefined.
     */
    getSsrcByTrack(track: JitsiTrack): number | undefined;

    /**
     * Logs an "application log" message.
     * @param message {string} The message to log. Note that while this can be a
     * generic string, the convention used by lib-jitsi-meet and jitsi-meet is to
     * log valid JSON strings, with an "id" field used for distinguishing between
     * message types. E.g.: {id: "recorder_status", status: "off"}
     */
    sendApplicationLog(message: string): void;

    /**
     * Sends local stats via the bridge channel which then forwards to other endpoints selectively.
     * @param {Object} payload The payload of the message.
     * @throws NetworkError/InvalidStateError/Error if the operation fails or if there is no data channel created.
     */
    sendEndpointStatsMessage(payload: unknown): void;
    /**
     * Sends a message to a given endpoint (if 'to' is a non-empty string), or
     * broadcasts it to all endpoints in the conference.
     * @param {string} to The ID of the endpoint/participant which is to receive
     * the message, or '' to broadcast the message to all endpoints in the
     * conference.
     * @param {string|object} message the message to send. If this is of type
     * 'string' it will be sent as a chat message. If it is of type 'object', it
     * will be encapsulated in a format recognized by jitsi-meet and converted to
     * JSON before being sent.
     * @param {boolean} sendThroughVideobridge Whether to send the message through
     * jitsi-videobridge (via the COLIBRI data channel or web socket), or through
     * the XMPP MUC. Currently only objects can be sent through jitsi-videobridge.
     */
    sendMessage(message: string | unknown, to?: string, sendThroughVideobridge?: boolean): void;

    isConnectionInterrupted(): boolean;

    /**
     * Gets a conference property with a given key.
     *
     * @param {string} key - The key.
     * @returns {*} The value
     */
    getProperty(key: string): string | number | boolean | undefined;

    /**
     * Checks whether or not the conference is currently in the peer to peer mode.
     * Being in peer to peer mode means that the direct connection has been
     * established and the P2P connection is being used for media transmission.
     * @return {boolean} <tt>true</tt> if in P2P mode or <tt>false</tt> otherwise.
     */
    isP2PActive(): boolean;

    /**
     * Returns the current ICE state of the P2P connection.
     * NOTE: method is used by the jitsi-meet-torture tests.
     * @return {string|null} an ICE state or <tt>null</tt> if there's currently
     * no P2P connection.
     */
    getP2PConnectionState(): string | null;

    /**
     * Manually starts new P2P session (should be used only in the tests).
     */
    startP2PSession(): void;

    /**
     * Manually stops the current P2P session (should be used only in the tests)
     */
    stopP2PSession(): void;

    /**
     * Get a summary of how long current participants have been the dominant speaker
     * @returns {object}
     */
    getSpeakerStats(): unknown;

    /**
     * Sets the constraints for the video that is requested from the bridge.
     *
     * @param {Object} videoConstraints The constraints which are specified in the
     * following format. The message updates the fields that are present and leaves the
     * rest unchanged on the bridge. Therefore, any field that is not applicable anymore
     * should be cleared by passing an empty object or list (whatever is applicable).
     * {
     *      'lastN': 20,
     *      'selectedEndpoints': ['A', 'B', 'C'],
     *      'onStageEndpoints': ['A'],
     *      'defaultConstraints': { 'maxHeight': 180 },
     *      'constraints': {
     *          'A': { 'maxHeight': 720 }
     *      }
     * }
     */
    setReceiverConstraints(videoConstraints: unknown): void;

    /**
     * Sets the maximum video size the local participant should receive from remote
     * participants.
     *
     * @param {number} maxFrameHeight - the maximum frame height, in pixels,
     * this receiver is willing to receive.
     * @returns {void}
     */
    setReceiverVideoConstraint(maxFrameHeight: number): void;

    /**
     * Sets the maximum video size the local participant should send to remote
     * participants.
     * @param {number} maxFrameHeight - The user preferred max frame height.
     * @returns {Promise} promise that will be resolved when the operation is
     * successful and rejected otherwise.
     */
    setSenderVideoConstraint(maxFrameHeight: number): Promise<void>;

    /**
     * Creates a video SIP GW session and returns it if service is enabled. Before
     * creating a session one need to check whether video SIP GW service is
     * available in the system {@link JitsiConference.isVideoSIPGWAvailable}. Even
     * if there are available nodes to serve this request, after creating the
     * session those nodes can be taken and the request about using the
     * created session can fail.
     *
     * @param {string} sipAddress - The sip address to be used.
     * @param {string} displayName - The display name to be used for this session.
     * @returns {JitsiVideoSIPGWSession|Error} Returns null if conference is not
     * initialised and there is no room.
     */
    createVideoSIPGWSession(
      sipAddress: string,
      displayName: string
    ): JitsiVideoSIPGWSession | Error;

    /**
     * Returns whether End-To-End encryption is enabled.
     *
     * @returns {boolean}
     */
    isE2EEEnabled(): boolean;

    /**
     * Returns whether End-To-End encryption is supported. Note that not all participants
     * in the conference may support it.
     *
     * @returns {boolean}
     */
    isE2EESupported(): boolean;

    /**
     * Enables / disables End-to-End encryption.
     *
     * @param {boolean} enabled whether to enable E2EE or not.
     * @returns {void}
     */
    toggleE2EE(enabled: boolean): void;

    /**
     * Returns <tt>true</tt> if lobby support is enabled in the backend.
     *
     * @returns {boolean} whether lobby is supported in the backend.
     */
    isLobbySupported(): boolean;

    /**
     * Returns <tt>true</tt> if the room has members only enabled.
     *
     * @returns {boolean} whether conference room is members only.
     */
    isMembersOnly(): boolean;

    /**
     * Enables lobby by moderators
     *
     * @returns {Promise} resolves when lobby room is joined or rejects with the error.
     */
    enableLobby(): Promise<void>;

    /**
     * Disabled lobby by moderators
     *
     * @returns {void}
     */
    disableLobby(): void;

    /**
     * Joins the lobby room with display name and optional email or with a shared password to skip waiting.
     *
     * @param {string} displayName Display name should be set to show it to moderators.
     * @param {string} email Optional email is used to present avatar to the moderator.
     * @returns {Promise<never>}
     */
    joinLobby(displayName: string, email: string): Promise<never>;

    /**
     * Denies an occupant in the lobby room access to the conference.
     * @param {string} id The participant id.
     */
    lobbyDenyAccess(id: string): void;

    /**
     * Approves the request to join the conference to a participant waiting in the lobby.
     *
     * @param {string} id The participant id.
     */
    lobbyApproveAccess(id: string): void;
  }

  export default JitsiConference;
}
