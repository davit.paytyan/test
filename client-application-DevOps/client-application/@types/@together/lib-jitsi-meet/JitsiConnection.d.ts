/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable unicorn/prevent-abbreviations */

declare module "@together/lib-jitsi-meet/JitsiConnection" {
  import JitsiConference, {
    JitsiConferenceOptionConfig,
  } from "@together/lib-jitsi-meet/JitsiConference";

  export type JitsiConnectionOptions = {
    /**
     * URL passed to the XMPP client which will be used to establish XMPP
     */
    serviceUrl: string;
    /**
     * Enables XEP-0198 stream management which will make the XMPP
     * module try to resume the session in case the Websocket connection breaks.
     */
    enableWebsocketResume?: boolean;
    /**
     * The websocket keep alive interval.
     * See {@link XmppConnection} constructor for more details.
     */
    websocketKeepAlive?: number;
    /**
     * The websocket keep alive url.
     * See {@link XmppConnection} constructor for more details.
     */
    websocketKeepAliveUrl?: string;
    /**
     * The xmpp ping settings.
     */
    xmppPink?: unknown;
    /**
     * see {@link JingleConnectionPlugin} for more details.
     */
    p2pStunServers?: unknown;
    hosts: {
      domain: string;
      muc: string;
    };
  };

  type LogsObject = {
    metadata: {
      time: number;
      url: string;
      ua: string;
      xmpp?: unknown;
    };
    [key: string]: unknown;
  };

  /**
   * Creates a new connection object for the Jitsi Meet server side video
   * conferencing service. Provides access to the JitsiConference interface.
   * @param appID identification for the provider of Jitsi Meet video conferencing
   * services.
   * @param token the JWT token used to authenticate with the server(optional)
   * @param options Object with properties / settings related to connection with
   * the server.
   * @constructor
   */
  export default class JitsiConnection {
    /**
     * Creates a new connection object for the Jitsi Meet server side video
     * conferencing service. Provides access to the JitsiConference interface.
     * @param appID identification for the provider of Jitsi Meet video conferencing
     * services.
     * @param token the JWT token used to authenticate with the server(optional)
     * @param options Object with properties / settings related to connection with
     * the server.
     * @constructor
     */
    constructor(
      appID: string | undefined,
      token: string | undefined,
      options: Partial<JitsiConnectionOptions>
    );

    appID?: string;
    token?: string;
    options: Partial<JitsiConnectionOptions>;
    /**
     * Connect the client with the server.
     * @param options {object} connecting options (for example authentications parameters).
     */
    connect(options?: { jid?: string; password?: string }): void;
    /**
     * Attach to existing connection. Can be used for optimizations. For example:
     * if the connection is created on the server we can attach to it and start
     * using it.
     *
     * @param options {object} connecting options - rid, sid and jid.
     */
    attach(options: { rid?: string; sid?: string; jid?: string; password?: string }): void;
    /**
     * Disconnects this from the XMPP server (if this is connected).
     *
     * @param {Object} event - Optionally, the event which triggered the necessity to
     * disconnect from the XMPP server (e.g. beforeunload, unload).
     * @returns {Promise} - Resolves when the disconnect process is finished or rejects with an error.
     */
    disconnect(event?: Event): Promise<void>;
    /**
     * Returns the jid of the participant associated with the Strophe connection.
     * @returns {string} The jid of the participant.
     */
    getJid(): string;
    /**
     * This method allows renewal of the tokens if they are expiring.
     * @param token the new token.
     */
    setToken(token: string): void;
    /**
     * Creates and joins new conference.
     * @param {string} name the name of the conference; if null - a generated name will be
     * provided from the api
     * @param {JitsiConferenceOptionConfig} [options] Object with properties / settings related to the conference
     * that will be created.
     * @returns {JitsiConference} returns the new conference object.
     */
    initJitsiConference(
      name: string,
      options?: Partial<JitsiConferenceOptionConfig>
    ): JitsiConference;
    /**
     * Subscribes the passed listener to the event.
     * @param event {JitsiConnectionEvents} the connection event.
     * @param listener {Function} the function that will receive the event
     *
     * Indicates that the connection has been disconnected.
     */
    addEventListener(
      event: "connection.connectionDisconnected",
      /**
       * @param msg {string} a message associated with the disconnect such as the last (known) error message
       */
      listener: (msg: string) => void
    ): void;
    /**
     * Subscribes the passed listener to the event.
     * @param event {JitsiConnectionEvents} the connection event.
     * @param listener {Function} the function that will receive the event
     *
     * Indicates that the connection has been established.
     */
    addEventListener(
      event: "connection.connectionEstablished",
      /**
       * @param id the ID of the local endpoint/participant/peer (within
       * the context of the established connection)
       */
      listener: (id: string) => void
    ): void;
    /**
     * Subscribes the passed listener to the event.
     * @param event {JitsiConnectionEvents} the connection event.
     * @param listener {Function} the function that will receive the event
     *
     * Indicates that the connection has been failed for some reason. The event
     * provides the following parameters to its listeners:
     * */
    addEventListener(
      event: "connection.connectionFailed",
      /**
       * @param errType {JitsiConnectionErrors} the type of error associated with
       * the failure
       * @param errReason {string} the error (message) associated with the failure
       * @param credentials {object} the credentials used to connect (if any)
       * @param errReasonDetails {object} an optional object with details about
       * the error, like shard moving, suspending. Used for analytics purposes.
       */
      listener: (
        errType: string,
        errReason: string,
        credentials?: { jid: string; password?: string },
        errReasonDetails?: Record<string, unknown>
      ) => void
    ): void;
    /**
     * Subscribes the passed listener to the event.
     * @param event {JitsiConnectionEvents} the connection event.
     * @param listener {Function} the function that will receive the event
     *
     * Indicates that the display name is required over this connection and need to be supplied when
     * joining the room.
     * There are cases like lobby room where display name is required.
     */
    addEventListener(event: connectionEventTypes, listener: () => void): void;
    /**
     * Unsubscribes the passed handler.
     * @param event {JitsiConnectionEvents} the connection event.
     * @param listener {Function} the function that will receive the event
     */
    removeEventListener(event: connectionEventTypes, listener: (...args: unknown[]) => void): void;
    /**
     * Returns measured connectionTimes.
     */
    getConnectionTimes(): Record<string, string | number>;
    /**
     * Adds new feature to the list of supported features for the local
     * participant.
     * @param {String} feature the name of the feature.
     * @param {boolean} submit if true - the new list of features will be
     * immediately submitted to the others.
     */
    addFeature(feature: string, submit?: boolean): void;
    /**
     * Removes a feature from the list of supported features for the local
     * participant
     * @param {String} feature the name of the feature.
     * @param {boolean} submit if true - the new list of features will be
     * immediately submitted to the others.
     */
    removeFeature(feature: string, submit?: boolean): void;
    /**
     * Get object with internal logs.
     */
    getLogs(): LogsObject;
  }
}

type eventTypes =
  | "display_name_required"
  | "wrongState"
  | "connectionFailed"
  | "connectionEstablished"
  | "connectionDisconnected";

type connectionEventTypes = `connection.${eventTypes}`;
