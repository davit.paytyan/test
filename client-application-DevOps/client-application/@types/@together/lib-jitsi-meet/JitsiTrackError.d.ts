declare module "@together/lib-jitsi-meet/JitsiTrackError" {
  declare class JitsiTrackError extends Error {
    /**
     *
     * Represents an error that occurred to a JitsiTrack. Can represent various
     * types of errors. For error descriptions (@see JitsiTrackErrors).
     * @param {Object|string} error - error object or error name
     * @param {Object|string} (options) - getUserMedia constraints object or error message
     * @param {('audio'|'video'|'desktop'|'screen'|'audiooutput')[]} (devices) - list of getUserMedia requested devices
     */
    constructor(
      error: unknown | string,
      options: unknown | string,
      devices: Array<"audio" | "video" | "desktop" | "screen" | "audiooutput">
    );
    /**
     * Additional information about original getUserMedia error
     * and constraints.
     */
    gum: {
      error: Error;
      constraints: MediaStreamConstraints;
      devices: Array<"audio" | "video" | "desktop" | "screen" | "audiooutput">;
    };
    name: string;
    message: string;
    stack?: string;
  }

  export default JitsiTrackError;
}
