declare module "@together/lib-jitsi-meet/modules/util/Listenable" {
  import EventEmitter from "events";
  declare type Listener = (...arguments: unknown[] | undefined) => void;
  declare type Unsubscriber = (...arguments: unknown[] | undefined) => void;

  /**
   * The class implements basic event operations - add/remove listener.
   * NOTE: The purpose of the class is to be extended in order to add
   * this functionality to other classes.
   */
  export default class Listenable {
    /**
     * Creates new instance.
     * @param {EventEmitter} eventEmitter
     * @constructor
     */
    constructor(eventEmitter?: EventEmitter);
    eventEmitter: EventEmitter;
    addEventListener: (eventName: string, listener: Listener) => Unsubscriber;
    on: (eventName: string, listener: Listener) => Unsubscriber;
    removeEventListener: (eventName: string, listener: Listener) => void;
    off: (eventName: string, listener: Listener) => void;
    /**
     * Adds new listener.
     * @param {String} eventName the name of the event
     * @param {Function} listener the listener.
     * @returns {Function} - The unsubscribe function.
     */
    addListener(
      eventName: string,
      listener: Listener
    ): (...arguments: unknown[] | undefined) => void;
    /**
     * Removes listener.
     * @param {String} eventName the name of the event that triggers the
     * listener
     * @param {Function} listener the listener.
     */
    removeListener(eventName: string, listener: Listener): void;
  }
}
