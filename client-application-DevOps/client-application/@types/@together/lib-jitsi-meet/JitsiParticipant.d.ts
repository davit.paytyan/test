declare module "@together/lib-jitsi-meet/JitsiParticipant" {
  import JitsiConference from "@together/lib-jitsi-meet/JitsiConference";
  import JitsiRemoteTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiRemoteTrack";
  import * as MediaType from "@together/lib-jitsi-meet/service/RTC/MediaType";
  /**
   * Represents a participant in (i.e. a member of) a conference.
   */
  export default class JitsiParticipant {
    /**
     * Initializes a new JitsiParticipant instance.
     *
     * @constructor
     * @param jid the conference XMPP jid
     * @param conference
     * @param displayName
     * @param {Boolean} hidden - True if the new JitsiParticipant instance is to
     * represent a hidden participant; otherwise, false.
     * @param {string} statsID - optional participant statsID
     * @param {string} status - the initial status if any.
     * @param {object} identity - the xmpp identity
     */
    constructor(
      jid: string,
      conference: JitsiConference,
      displayName: string,
      hidden: boolean,
      statsID: string,
      status: string,
      identity: unknown
    );

    /**
     * @returns {JitsiConference} The conference that this participant belongs
     * to.
     */
    getConference(): JitsiConference;

    /**
     * Gets the value of a property of this participant.
     */
    getProperty(name: string): string | number | boolean | undefined;

    /**
     * Checks whether this <tt>JitsiParticipant</tt> has any video tracks which
     * are muted according to their underlying WebRTC <tt>MediaStreamTrack</tt>
     * muted status.
     * @return {boolean} <tt>true</tt> if this <tt>participant</tt> contains any
     * video <tt>JitsiTrack</tt>s which are muted as defined in
     * {@link JitsiTrack.isWebRTCTrackMuted}.
     */
    hasAnyVideoTrackWebRTCMuted(): boolean;

    /**
     * Return participant's connectivity status.
     *
     * @returns {string} the connection status
     * <tt>ParticipantConnectionStatus</tt> of the user.
     * {@link ParticipantConnectionStatus}.
     */
    getConnectionStatus(): string;

    /**
     * Sets the value of a property of this participant, and fires an event if
     * the value has changed.
     * @name the name of the property.
     * @value the value to set.
     */
    setProperty(name: string, value: string | number | boolean): void;

    /**
     * @returns {Array.<JitsiTrack>} The list of media tracks for this
     * participant.
     */
    getTracks(): Array<JitsiRemoteTrack>;

    /**
     * @param {MediaType} mediaType
     * @returns {Array.<JitsiTrack>} an array of media tracks for this
     * participant, for given media type.
     */
    getTracksByMediaType(mediaType: typeof MediaType): Array<JitsiRemoteTrack>;

    /**
     * @returns {String} The ID of this participant.
     */
    getId(): string;

    /**
     * @returns {String} The JID of this participant.
     */
    getJid(): string;

    /**
     * @returns {String} The human-readable display name of this participant.
     */
    getDisplayName(): string;

    /**
     * @returns {String} The stats ID of this participant.
     */
    getStatsID(): string;

    /**
     * @returns {String} The status of the participant.
     */
    getStatus(): string;

    /**
     * @returns {Boolean} Whether this participant is a moderator or not.
     */
    isModerator(): boolean;

    /**
     * @returns {Boolean} Whether this participant is a hidden participant. Some
     * special system participants may want to join hidden (like for example the
     * recorder).
     */
    isHidden(): boolean;

    /**
     * @returns {Boolean} Whether this participant has muted their audio.
     */
    isAudioMuted(): boolean;

    /**
     * @returns {Boolean} Whether this participant has muted their video.
     */
    isVideoMuted(): boolean;

    /**
     * @returns {String} The role of this participant.
     */
    getRole(): string;

    /**
     * Sets a new participant role.
     * @param {String} newRole - the new role.
     */
    setRole(newRole: string): void;

    /**
     *
     */
    supportsDTMF(): boolean;

    /**
     * Returns a set with the features for the participant.
     * @returns {Promise<Set<String>, Error>}
     */
    getFeatures(): Promise<Set<string>, Error>;

    /**
     * Checks current set features.
     * @param {String} feature - the feature to check.
     * @return {boolean} <tt>true</tt> if this <tt>participant</tt> contains the
     * <tt>feature</tt>.
     */
    hasFeature(feature: string): boolean;

    /**
     * Set new features.
     * @param {Set<String>|undefined} newFeatures - Sets new features.
     */
    setFeatures(newFeatures: Set<string> | undefined): void;

    /**
     * Returns the bot type for the participant.
     *
     * @returns {string|undefined} - The bot type of the participant.
     */
    getBotType(): string | undefined;

    /**
     * Sets the bot type for the participant.
     * @param {String} newBotType - The new bot type to set.
     */
    setBotType(newBotType: string): void;
  }
}
