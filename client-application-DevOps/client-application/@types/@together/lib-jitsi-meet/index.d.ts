declare module "@together/lib-jitsi-meet" {
  import JitsiConnection from "@together/lib-jitsi-meet/JitsiConnection";
  import ProxyConnectionService from "@together/lib-jitsi-meet/modules/proxyconnection/ProxyConnectionService";
  import JitsiTrackError from "@together/lib-jitsi-meet/JitsiTrackError";
  import JitsiMediaDevices from "@together/lib-jitsi-meet/JitsiMediaDevices";
  import AnalyticsAdapter from "@together/lib-jitsi-meet/modules/statistics/AnalyticsAdapter";

  import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
  import TrackVADEmitter from "@together/lib-jitsi-meet/modules/detection/TrackVADEmitter";
  import AudioMixer from "@together/lib-jitsi-meet/modules/webaudio/AudioMixer";
  import JitsiTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiTrack";
  import PrecallTest from "@together/lib-jitsi-meet/modules/statistics/PrecallTest";
  import BrowserCapabilities from "@together/lib-jitsi-meet/modules/browser/BrowserCapabilities";

  interface InitOptions {
    useIPv6?: boolean;
    disableAudioLevels?: boolean;
    disableSimulcast?: boolean;
    enableWindowOnErrorHandler?: boolean;
    disableThirdPartyRequests?: boolean;
    enableAnalyticsLogging?: boolean;
    externalStorage?: Storage;
    callStatsCustomScriptUrl?: string;
    disableRtx?: boolean;
    disabledCodec?: string;
    preferredCodec?: string;
    useTurnUdp?: boolean;
  }

  type JitsiMeetJS = Readonly<{
    version: string;
    JitsiConnection: typeof JitsiConnection;
    ProxyConnectionService: typeof ProxyConnectionService;
    constants: {
      participantConnectionStatus: {
        /**
         * Status indicating that connection is currently active.
         */
        ACTIVE: "active";

        /**
         * Status indicating that connection is currently inactive.
         * Inactive means the connection was stopped on purpose from the bridge,
         * like exiting lastN or adaptivity decided to drop video because of not
         * enough bandwidth.
         */
        INACTIVE: "inactive";

        /**
         * Status indicating that connection is currently interrupted.
         */
        INTERRUPTED: "interrupted";

        /**
         * Status indicating that connection is currently restoring.
         */
        RESTORING: "restoring";
      };
      recording: {
        error: {
          BUSY: "busy";
          ERROR: "error";
          RESOURCE_CONSTRAINT: "resource-constraint";
          SERVICE_UNAVAILABLE: "service-unavailable";
        };
        mode: {
          FILE: "file";
          STREAM: "stream";
        };
        status: {
          OFF: "off";
          ON: "on";
          PENDING: "pending";
        };
      };
      sipVideoGW: {
        /**
         * Status that video SIP GW service is available.
         * @type {string}
         */
        STATUS_AVAILABLE: "available";

        /**
         * Status that video SIP GW service is not available.
         * @type {string}
         */
        STATUS_UNDEFINED: "undefined";

        /**
         * Status that video SIP GW service is available but there are no free nodes
         * at the moment to serve new requests.
         * @type {string}
         */
        STATUS_BUSY: "busy";

        /**
         * Video SIP GW session state, currently running.
         * @type {string}
         */
        STATE_ON: "on";

        /**
         * Video SIP GW session state, currently stopped and not running.
         * @type {string}
         */
        STATE_OFF: "off";

        /**
         * Video SIP GW session state, currently is starting.
         * @type {string}
         */
        STATE_PENDING: "pending";

        /**
         * Video SIP GW session state, has observed some issues and is retrying at the
         * moment.
         * @type {string}
         */
        STATE_RETRYING: "retrying";

        /**
         * Video SIP GW session state, tried to start but it failed.
         * @type {string}
         */
        STATE_FAILED: "failed";

        /**
         * Error on trying to create video SIP GW session in conference where
         * there is no room connection (hasn't joined or has left the room).
         * @type {string}
         */
        ERROR_NO_CONNECTION: "error_no_connection";

        /**
         * Error on trying to create video SIP GW session with address for which
         * there is an already created session.
         * @type {string}
         */
        ERROR_SESSION_EXISTS: "error_session_already_exists";
      };
      transcriptionStatus: {
        /**
         * The transciption is on.
         * @type {String}
         */
        ON: "on";

        /**
         * The transciption is off.
         * @type {String}
         */
        OFF: "off";
      };
    };
    events: {
      /**
       * The events for the conference.
       */
      conference: {
        /**
         * Event indicates that the current conference audio input switched between audio
         * input states,i.e. with or without audio input.
         */
        AUDIO_INPUT_STATE_CHANGE: "conference.audio_input_state_changed";
        /**
         * Indicates that authentication status changed.
         */
        AUTH_STATUS_CHANGED: "conference.auth_status_changed";
        /**
         * Fired just before the statistics module is disposed and it's the last chance
         * to submit some logs to the statistics service (ex. CallStats if enabled),
         * before it's disconnected.
         */
        BEFORE_STATISTICS_DISPOSED: "conference.beforeStatisticsDisposed";
        /**
         * Indicates that an error occured.
         */
        CONFERENCE_ERROR: "conference.error";
        /**
         * Indicates that conference failed.
         */
        CONFERENCE_FAILED: "conference.failed";
        /**
         * Indicates that conference has been joined. The event does NOT provide any
         * parameters to its listeners.
         */
        CONFERENCE_JOINED: "conference.joined";
        /**
         * Indicates that conference has been left.
         */
        CONFERENCE_LEFT: "conference.left";
        /**
         * Indicates that the conference unique identifier has been set.
         */
        CONFERENCE_UNIQUE_ID_SET: "conference.unique_id_set";
        /**
         * Indicates that the connection to the conference has been established
         * XXX This is currently fired whenVthe *ICE* connection enters 'connected'
         * state for the first time.
         */
        CONNECTION_ESTABLISHED: "conference.connectionEstablished";
        /**
         * Indicates that the connection to the conference has been interrupted for some
         * reason.
         * XXX This is currently fired when the *ICE* connection is interrupted.
         */
        CONNECTION_INTERRUPTED: "conference.connectionInterrupted";
        /**
         * Indicates that the connection to the conference has been restored.
         * XXX This is currently fired when the *ICE* connection is restored.
         */
        CONNECTION_RESTORED: "conference.connectionRestored";
        /**
         * A connection to the video bridge's data channel has been established.
         */
        DATA_CHANNEL_OPENED: "conference.dataChannelOpened";
        /**
         * A user has changed it display name
         */
        DISPLAY_NAME_CHANGED: "conference.displayNameChanged";
        /**
         * The dominant speaker was changed.
         */
        DOMINANT_SPEAKER_CHANGED: "conference.dominantSpeaker";
        /**
         * UTC conference timestamp when first participant joined.
         */
        CONFERENCE_CREATED_TIMESTAMP: "conference.createdTimestamp";
        /**
         * Indicates that DTMF support changed.
         */
        DTMF_SUPPORT_CHANGED: "conference.dtmfSupportChanged";
        /**
         * Indicates that a message from another participant is received on data
         * channel.
         */
        ENDPOINT_MESSAGE_RECEIVED: "conference.endpoint_message_received";
        /**
         * Indicates that a message for the remote endpoint statistics has been received on the bridge channel.
         */
        ENDPOINT_STATS_RECEIVED: "conference.endpoint_stats_received";
        /**
         * NOTE This is lib-jitsi-meet internal event and can be removed at any time !
         *
         * Event emitted when conference transits, between one to one and multiparty JVB
         * conference. If the conference switches to P2P it's neither one to one nor
         * a multiparty JVB conference, but P2P (the status argument of this event will
         * be <tt>false</tt>).
         *
         * The first argument is a boolean which carries the previous value and
         * the seconds argument is a boolean with the new status. The event is emitted
         * only if the previous and the new values are different.
         *
         * @type {string}
         */
        JVB121_STATUS: string;
        /**
         * You are kicked from the conference.
         * @param {JitsiParticipant} the participant that initiated the kick.
         */
        KICKED: "conference.kicked";
        /**
         * Participant was kicked from the conference.
         * @event PARTICIPANT_KICKED
         * @param {JitsiParticipant} initiated participant that initiated the kick.
         * @param {JitsiParticipant} kicked participant that was kicked.
         */
        PARTICIPANT_KICKED: "conference.participant_kicked";
        /**
         * The Last N set is changed.
         *
         * @param {Array<string>|null} leavingEndpointIds the ids of all the endpoints
         * which are leaving Last N
         * @param {Array<string>|null} enteringEndpointIds the ids of all the endpoints
         * which are entering Last N
         */
        LAST_N_ENDPOINTS_CHANGED: "conference.lastNEndpointsChanged";
        /**
         * Indicates that the room has been locked or unlocked.
         */
        LOCK_STATE_CHANGED: "conference.lock_state_changed";
        /**
         * Indicates that the region of the media server (jitsi-videobridge) that we
         * are connected to changed (or was initially set).
         * @type {string} the region.
         */
        SERVER_REGION_CHANGED: string;
        /**
         * An event(library-private) fired when a new media session is added to the conference.
         * @type {string}
         * @private
         */
        _MEDIA_SESSION_STARTED: string;
        /**
         * An event(library-private) fired when the conference switches the currently active media session.
         * @type {string}
         * @private
         */
        _MEDIA_SESSION_ACTIVE_CHANGED: string;
        /**
         * Indicates that the conference had changed to members only enabled/disabled.
         * The first argument of this event is a <tt>boolean</tt> which when set to
         * <tt>true</tt> means that the conference is running in members only mode.
         * You may need to use Lobby if supported to ask for permissions to enter the conference.
         */
        MEMBERS_ONLY_CHANGED: "conference.membersOnlyChanged";
        /**
         * New text message was received.
         */
        MESSAGE_RECEIVED: "conference.messageReceived";
        /**
         * Event indicates that the current selected input device has no signal
         */
        NO_AUDIO_INPUT: "conference.no_audio_input";
        /**
         * Event indicates that the current microphone used by the conference is noisy.
         */
        NOISY_MIC: "conference.noisy_mic";
        /**
         * New private text message was received.
         */
        PRIVATE_MESSAGE_RECEIVED: "conference.privateMessageReceived";
        /**
         * Event fired when JVB sends notification about interrupted/restored user's
         * ICE connection status or we detect local problem with the video track.
         * First argument is the ID of the participant and
         * the seconds is a string indicating if the connection is currently
         * - active - the connection is active
         * - inactive - the connection is inactive, was intentionally interrupted by
         * the bridge
         * - interrupted - a network problem occurred
         * - restoring - the connection was inactive and is restoring now
         *
         * The current status value can be obtained by calling
         * JitsiParticipant.getConnectionStatus().
         */
        PARTICIPANT_CONN_STATUS_CHANGED: "conference.participant_conn_status_changed";
        /**
         * Indicates that the features of the participant has been changed.
         */
        PARTCIPANT_FEATURES_CHANGED: "conference.partcipant_features_changed";
        /**
         * Indicates that a the value of a specific property of a specific participant
         * has changed.
         */
        PARTICIPANT_PROPERTY_CHANGED: "conference.participant_property_changed";
        /**
         * Indicates that the conference has switched between JVB and P2P connections.
         * The first argument of this event is a <tt>boolean</tt> which when set to
         * <tt>true</tt> means that the conference is running on the P2P connection.
         */
        P2P_STATUS: "conference.p2pStatus";
        /**
         * Indicates that phone number changed.
         */
        PHONE_NUMBER_CHANGED: "conference.phoneNumberChanged";
        /**
         * The conference properties changed.
         * @type {string}
         */
        PROPERTIES_CHANGED: string;
        /**
         * Indicates that recording state changed.
         */
        RECORDER_STATE_CHANGED: "conference.recorderStateChanged";
        /**
         * Indicates that video SIP GW state changed.
         * @param {VideoSIPGWConstants} status.
         */
        VIDEO_SIP_GW_AVAILABILITY_CHANGED: "conference.videoSIPGWAvailabilityChanged";
        /**
         * Indicates that video SIP GW Session state changed.
         * @param {options} event - {
         *     {string} address,
         *     {VideoSIPGWConstants} oldState,
         *     {VideoSIPGWConstants} newState,
         *     {string} displayName}
         * }.
         */
        VIDEO_SIP_GW_SESSION_STATE_CHANGED: "conference.videoSIPGWSessionStateChanged";
        /**
         * Indicates that start muted settings changed.
         */
        START_MUTED_POLICY_CHANGED: "conference.start_muted_policy_changed";
        /**
         * Indicates that the local user has started muted.
         */
        STARTED_MUTED: "conference.started_muted";
        /**
         * Indicates that subject of the conference has changed.
         */
        SUBJECT_CHANGED: "conference.subjectChanged";
        /**
         * Indicates that DTMF support changed.
         */
        SUSPEND_DETECTED: "conference.suspendDetected";
        /**
         * Event indicates that local user is talking while he muted himself
         */
        TALK_WHILE_MUTED: "conference.talk_while_muted";
        /**
         * A new media track was added to the conference. The event provides the
         * following parameters to its listeners:
         *
         * @param {JitsiTrack} track the added JitsiTrack
         */
        TRACK_ADDED: "conference.trackAdded";
        /**
         * Audio levels of a media track ( attached to the conference) was changed.
         */
        TRACK_AUDIO_LEVEL_CHANGED: "conference.audioLevelsChanged";
        /**
         * A media track ( attached to the conference) mute status was changed.
         * @param {JitsiParticipant|null} the participant that initiated the mute
         * if it is a remote mute.
         */
        TRACK_MUTE_CHANGED: "conference.trackMuteChanged";
        /**
         * The media track was removed from the conference. The event provides the
         * following parameters to its listeners:
         *
         * @param {JitsiTrack} track the removed JitsiTrack
         */
        TRACK_REMOVED: "conference.trackRemoved";
        /**
         * Notifies for transcription status changes. The event provides the
         * following parameters to its listeners:
         *
         * @param {String} status - The new status.
         */
        TRANSCRIPTION_STATUS_CHANGED: "conference.transcriptionStatusChanged";
        /**
         * A new user joined the conference.
         */
        USER_JOINED: "conference.userJoined";
        /**
         * A user has left the conference.
         */
        USER_LEFT: "conference.userLeft";
        /**
         * User role changed.
         */
        USER_ROLE_CHANGED: "conference.roleChanged";
        /**
         * User status changed.
         */
        USER_STATUS_CHANGED: "conference.statusChanged";
        /**
         * Event indicates that the bot participant type changed.
         */
        BOT_TYPE_CHANGED: "conference.bot_type_changed";
        /**
         * A new user joined the lobby room.
         */
        LOBBY_USER_JOINED: "conference.lobby.userJoined";
        /**
         * A user from the lobby room has been update.
         */
        LOBBY_USER_UPDATED: "conference.lobby.userUpdated";
        /**
         * A user left the lobby room.
         */
        LOBBY_USER_LEFT: "conference.lobby.userLeft";
      };
      /**
       * The events for the connection.
       */
      connection: {
        /**
         * Indicates that the connection has been disconnected. The event provides
         * the following parameters to its listeners:
         *
         * @param msg {string} a message associated with the disconnect such as the
         * last (known) error message
         */
        CONNECTION_DISCONNECTED: "connection.connectionDisconnected";
        /**
         * Indicates that the connection has been established. The event provides
         * the following parameters to its listeners:
         *
         * @param id {string} the ID of the local endpoint/participant/peer (within
         * the context of the established connection)
         */
        CONNECTION_ESTABLISHED: "connection.connectionEstablished";
        /**
         * Indicates that the connection has been failed for some reason. The event
         * provides the following parameters to its listeners:
         *
         * @param errType {JitsiConnectionErrors} the type of error associated with
         * the failure
         * @param errReason {string} the error (message) associated with the failure
         * @param credentials {object} the credentials used to connect (if any)
         * @param errReasonDetails {object} an optional object with details about
         * the error, like shard moving, suspending. Used for analytics purposes.
         */
        CONNECTION_FAILED: "connection.connectionFailed";
        /**
         * Indicates that the performed action cannot be executed because the
         * connection is not in the correct state(connected, disconnected, etc.)
         */
        WRONG_STATE: "connection.wrongState";
        /**
         * Indicates that the display name is required over this connection and need to be supplied when
         * joining the room.
         * There are cases like lobby room where display name is required.
         */
        DISPLAY_NAME_REQUIRED: "connection.display_name_required";
      };
      detection: {
        /**
         * Event triggered by a audio detector indicating that its active state has changed from active to inactive or vice
         * versa.
         * @event
         * @type {boolean} - true when service has changed to active false otherwise.
         */
        DETECTOR_STATE_CHANGE: "detector_state_change";

        /** Event triggered by {@link NoAudioSignalDetector} when the local audio device associated with a JitsiConference
         * starts receiving audio levels with the value of 0 meaning no audio is being captured on that device, or when
         * it starts receiving audio levels !== 0 after being in a state of no audio.
         * @event
         * @type {boolean} - true when the current conference audio track has audio input false otherwise.
         */
        AUDIO_INPUT_STATE_CHANGE: "audio_input_state_changed";

        /** Event triggered by NoAudioSignalDetector when the local audio device associated with a JitsiConference goes silent
         * for a period of time, meaning that the device is either broken or hardware/software muted.
         * @event
         * @type {void}
         */
        NO_AUDIO_INPUT: "no_audio_input_detected";

        /**
         *  Event generated by {@link VADNoiseDetection} when the tracked device is considered noisy.
         *  @event
         *  @type {Object}
         */
        VAD_NOISY_DEVICE: "detection.vad_noise_device";

        /**
         * Event generated by VADReportingService when if finishes creating a VAD report for the monitored devices.
         * The generated objects are of type Array<Object>, one score for each monitored device.
         * @event VAD_REPORT_PUBLISHED
         * @type Array<Object> with the following structure:
         * @property {Date} timestamp - Timestamp at which the compute took place.
         * @property {number} avgVAD - Average VAD score over monitored period of time.
         * @property {string} deviceId - Associate local audio device ID.
         */
        VAD_REPORT_PUBLISHED: "vad-report-published";

        /**
         * Event generated by {@link TrackVADEmitter} when PCM sample VAD score is available.
         *
         * @event
         * @type {Object}
         * @property {Date}   timestamp - Exact time at which processed PCM sample was generated.
         * @property {number} score - VAD score on a scale from 0 to 1 (i.e. 0.7)
         * @property {Float32Array} pcmData - Raw PCM data with which the VAD score was calculated.
         * @property {string} deviceId - Device id of the associated track.
         */
        VAD_SCORE_PUBLISHED: "detection.vad_score_published";

        /**
         *  Event generated by {@link VADTalkMutedDetection} when a user is talking while the mic is muted.
         *
         *  @event
         *  @type {Object}
         */
        VAD_TALK_WHILE_MUTED: "detection.vad_talk_while_muted";
      };
      track: {
        /**
         * The media track was removed to the conference.
         */
        LOCAL_TRACK_STOPPED: "track.stopped";

        /**
         * Audio levels of a this track was changed.
         * The first argument is a number with audio level value in range [0, 1].
         * The second argument is a <tt>TraceablePeerConnection</tt> which is the peer
         * connection which measured the audio level (one audio track can be added
         * to multiple peer connection at the same time). This argument is optional for
         * local tracks for which we can measure audio level without the peer
         * connection (the value will be <tt>undefined</tt>).
         *
         * NOTE The second argument should be treated as library internal and can be
         * removed at any time.
         */
        TRACK_AUDIO_LEVEL_CHANGED: "track.audioLevelsChanged";

        /**
         * The audio output of the track was changed.
         */
        TRACK_AUDIO_OUTPUT_CHANGED: "track.audioOutputChanged";

        /**
         * A media track mute status was changed.
         */
        TRACK_MUTE_CHANGED: "track.trackMuteChanged";

        /**
         * The video type("camera" or "desktop") of the track was changed.
         */
        TRACK_VIDEOTYPE_CHANGED: "track.videoTypeChanged";

        /**
         * Indicates that the track is not receiving any data even though we expect it
         * to receive data (i.e. the stream is not stopped).
         */
        NO_DATA_FROM_SOURCE: "track.no_data_from_source";

        /**
         * Indicates that the local audio track is not receiving any audio input from
         * the microphone that is currently selected.
         */
        NO_AUDIO_INPUT: "track.no_audio_input";
      };
      /**
       * The events for the media devices.
       */
      mediaDevices: {
        /**
         * Indicates that the list of available media devices has been changed. The
         * event provides the following parameters to its listeners:
         *
         * @param {MediaDeviceInfo[]} devices - array of MediaDeviceInfo or
         *  MediaDeviceInfo-like objects that are currently connected.
         *  @see https://developer.mozilla.org/en-US/docs/Web/API/MediaDeviceInfo
         */
        DEVICE_LIST_CHANGED: "mediaDevices.devicechange";

        /**
         * Event emitted when the user granted/blocked a permission for the camera / mic.
         * Used to keep track of the granted permissions on browsers which don't
         * support the Permissions API.
         */
        PERMISSIONS_CHANGED: "rtc.permissions_changed";

        /**
         * Indicates that the environment is currently showing permission prompt to
         * access camera and/or microphone. The event provides the following
         * parameters to its listeners:
         *
         * @param {'chrome'|'opera'|'firefox'|'safari'|'nwjs'
         *  |'react-native'|'android'} environmentType - type of browser or
         *  other execution environment.
         */
        PERMISSION_PROMPT_IS_SHOWN: "mediaDevices.permissionPromptIsShown";

        SLOW_GET_USER_MEDIA: "mediaDevices.slowGetUserMedia";
      };
      connectionQuality: {
        /**
         * Indicates that the local connection statistics were updated.
         */
        LOCAL_STATS_UPDATED: "cq.local_stats_updated";
        /**
         * Indicates that the connection statistics for a particular remote participant
         * were updated.
         */
        REMOTE_STATS_UPDATED: "cq.remote_stats_updated";
      };
      e2eping: {
        /**
         * Indicates that the end-to-end round-trip-time for a participant has changed.
         */
        E2E_RTT_CHANGED: "e2eping.e2e_rtt_changed";
      };
    };
    errors: {
      /**
       * The errors for the conference.
       */
      conference: {
        /**
         * The errors for the conference.
         */
        /**
         * Indicates that client must be authenticated to create the conference.
         */
        AUTHENTICATION_REQUIRED: "conference.authenticationRequired";
        /**
         * Indicates that chat error occurred.
         */
        CHAT_ERROR: "conference.chatError";
        /**
         * Indicates that conference has been destroyed.
         */
        CONFERENCE_DESTROYED: "conference.destroyed";
        /**
         * Indicates that max users limit has been reached.
         */
        CONFERENCE_MAX_USERS: "conference.max_users";
        /**
         * Indicates that a connection error occurred when trying to join a conference.
         */
        CONNECTION_ERROR: "conference.connectionError";
        /**
         * Indicates that the client has been forced to restart by jicofo when the
         * conference was migrated from one bridge to another.
         */
        CONFERENCE_RESTARTED: "conference.restarted";
        /**
         * Indicates that a connection error is due to not allowed,
         * occurred when trying to join a conference.
         */
        NOT_ALLOWED_ERROR: "conference.connectionError.notAllowed";
        /**
         * Indicates that a connection error is due to not allowed,
         * occurred when trying to join a conference, only approved members are allowed to join.
         */
        MEMBERS_ONLY_ERROR: "conference.connectionError.membersOnly";
        /**
         * Indicates that a connection error is due to denied access to the room,
         * occurred after joining a lobby room and access is denied by the room moderators.
         */
        CONFERENCE_ACCESS_DENIED: "conference.connectionError.accessDenied";
        /**
         * Indicates that focus error happened.
         */
        FOCUS_DISCONNECTED: "conference.focusDisconnected";
        /**
         * Indicates that focus left the conference.
         */
        FOCUS_LEFT: "conference.focusLeft";
        /**
         * Indicates that graceful shutdown happened.
         */
        GRACEFUL_SHUTDOWN: "conference.gracefulShutdown";
        /**
         * Indicates that the media connection has failed.
         */
        ICE_FAILED: "conference.iceFailed";
        /**
         * Indicates that the versions of the server side components are incompatible
         * with the client side.
         */
        INCOMPATIBLE_SERVER_VERSIONS: "conference.incompatible_server_versions";
        /**
         * Indicates that offer/answer had failed.
         */
        OFFER_ANSWER_FAILED: "conference.offerAnswerFailed";
        /**
         * Indicates that password cannot be set for this conference.
         */
        PASSWORD_NOT_SUPPORTED: "conference.passwordNotSupported";
        /**
         * Indicates that a password is required in order to join the conference.
         */
        PASSWORD_REQUIRED: "conference.passwordRequired";
        /**
         * Indicates that reservation system returned error.
         */
        RESERVATION_ERROR: "conference.reservationError";
        /**
         * Indicates that there is no available videobridge.
         */
        VIDEOBRIDGE_NOT_AVAILABLE: "conference.videobridgeNotAvailable";
      };
      /**
       * The errors for the connection.
       */
      connection: {
        /**
         * Indicates that the connection was dropped with an error which was most likely
         * caused by some networking issues. The dropped term in this context means that
         * the connection was closed unexpectedly (not on user's request).
         *
         * One example is 'item-not-found' error thrown by Prosody when the BOSH session
         * times out after 60 seconds of inactivity. On the other hand 'item-not-found'
         * could also happen when BOSH request is sent to the server with the session-id
         * that is not know to the server. But this should not happen in lib-jitsi-meet
         * case as long as the service is configured correctly (there is no bug).
         */
        CONNECTION_DROPPED_ERROR: "connection.droppedError";

        /**
         * Not specified errors.
         */
        OTHER_ERROR: "connection.otherError";

        /**
         * Indicates that a password is required in order to join the conference.
         */
        PASSWORD_REQUIRED: "connection.passwordRequired";

        /**
         * Indicates that the connection was dropped, because of too many 5xx HTTP
         * errors on BOSH requests.
         */
        SERVER_ERROR: "connection.serverError";
      };
      /**
       * The errors for the JitsiTrack objects.
       */
      track: {
        /**
         * An error which indicates that some of requested constraints in
         * getUserMedia call were not satisfied.
         */
        CONSTRAINT_FAILED: "gum.constraint_failed";
        /**
         * A generic error which indicates an error occurred while selecting
         * a DesktopCapturerSource from the electron app.
         */
        ELECTRON_DESKTOP_PICKER_ERROR: "gum.electron_desktop_picker_error";
        /**
         * An error which indicates a custom desktop picker could not be detected
         * for the electron app.
         */
        ELECTRON_DESKTOP_PICKER_NOT_FOUND: "gum.electron_desktop_picker_not_found";
        /**
         * Generic getUserMedia error.
         */
        GENERAL: "gum.general";
        /**
         * An error which indicates that requested device was not found.
         */
        NOT_FOUND: "gum.not_found";
        /**
         * An error which indicates that user denied permission to share requested
         * device.
         */
        PERMISSION_DENIED: "gum.permission_denied";
        /**
         * Generic error for screensharing failure.
         */
        SCREENSHARING_GENERIC_ERROR: "gum.screensharing_generic_error";
        /**
         * An error which indicates that user canceled screen sharing window
         * selection dialog.
         */
        SCREENSHARING_USER_CANCELED: "gum.screensharing_user_canceled";
        /**
         * Indicates that the timeout passed to the obtainAudioAndVideoPermissions has expired without GUM resolving.
         */
        TIMEOUT: "gum.timeout";
        /**
         * An error which indicates that track has been already disposed and cannot
         * be longer used.
         */
        TRACK_IS_DISPOSED: "track.track_is_disposed";
        /**
         * An error which indicates that track has no MediaStream associated.
         */
        TRACK_NO_STREAM_FOUND: "track.no_stream_found";
        /**
         * An error which indicates that requested video resolution is not supported
         * by a webcam.
         */
        UNSUPPORTED_RESOLUTION: "gum.unsupported_resolution";
      };
    };
    errorTypes: {
      JitsiTrackError: typeof JitsiTrackError;
    };
    logLevels: {
      trace: 0;
      debug: 1;
      info: 2;
      log: 3;
      warn: 4;
      error: 5;
    };
    mediaDevices: JitsiMediaDevices;
    analytics: AnalyticsAdapter;

    init(options?: InitOptions): void;

    /**
     * Returns whether the desktop sharing is enabled or not.
     *
     * @returns {boolean}
     */
    isDesktopSharingEnabled(): boolean;

    /**
     * Returns whether the current execution environment supports WebRTC (for
     * use within this library).
     *
     * @returns {boolean} {@code true} if WebRTC is supported in the current
     * execution environment (for use within this library); {@code false},
     * otherwise.
     */
    isWebRtcSupported(): boolean;

    setLogLevel(level: string): void;

    /**
     * Sets the log level to the <tt>Logger</tt> instance with given id.
     *
     * @param {Logger.levels} level the logging level to be set
     * @param {string} id the logger id to which new logging level will be set.
     * Usually it's the name of the JavaScript source file including the path
     * ex. "modules/xmpp/ChatRoom.js"
     */
    setLogLevelById(level: string, id: string): void;

    /**
     * Registers new global logger transport to the library logging framework.
     *
     * @param globalTransport
     * @see {@link https://github.com/jitsi/jitsi-meet-logger/blob/master/lib/Logger.js#L46}
     */
    addGlobalLogTransport(globalTransport: unknown): void;

    /**
     * Removes global logging transport from the library logging framework.
     *
     * @param globalTransport
     * @see {@link https://github.com/jitsi/jitsi-meet-logger/blob/master/lib/Logger.js#L57}
     */
    removeGlobalLogTransport(globalTransport: unknown): void;

    /**
     * Sets global options which will be used by all loggers. Changing these
     * works even after other loggers are created.
     *
     * @param options
     * @see {@link https://github.com/jitsi/jitsi-meet-logger/blob/master/lib/Logger.js#L73}
     */
    setGlobalLogOptions(options: unknown): void;

    /**
     * Creates the media tracks and returns them trough the callback.
     *
     * @param options Object with properties / settings specifying the tracks
     * which should be created. should be created or some additional
     * configurations about resolution for example.
     * @param {Array} options.effects optional effects array for the track
     * @param {Array} options.devices the devices that will be requested
     * @param {string} options.resolution resolution constraints
     * @param {string} options.cameraDeviceId
     * @param {string} options.micDeviceId
     * @param {intiger} interval - the interval (in ms) for
     * checking whether the desktop sharing extension is installed or not
     * @param {Function} checkAgain - returns boolean. While checkAgain()==true
     * createLocalTracks will wait and check on every "interval" ms for the
     * extension. If the desktop extension is not install and checkAgain()==true
     * createLocalTracks will finish with rejected Promise.
     * @param {Function} listener - The listener will be called to notify the
     * user of lib-jitsi-meet that createLocalTracks is starting external
     * extension installation process.
     * NOTE: If the inline installation process is not possible and external
     * installation is enabled the listener property will be called to notify
     * the start of external installation process. After that createLocalTracks
     * will start to check for the extension on every interval ms until the
     * plugin is installed or until checkAgain return false. If the extension
     * is found createLocalTracks will try to get the desktop sharing track and
     * will finish the execution. If checkAgain returns false, createLocalTracks
     * will finish the execution with rejected Promise.
     *
     * @param {boolean} (firePermissionPromptIsShownEvent) - if event
     * JitsiMediaDevicesEvents.PERMISSION_PROMPT_IS_SHOWN should be fired
     * @param originalOptions - internal use only, to be able to store the
     * originally requested options.
     * @returns A promise that returns an array of created JitsiTracks if resolved, or a JitsiConferenceError if rejected.
     * @throws {JitsiConferenceError}
     */
    createLocalTracks(
      options?: {
        devices?: Array<"desktop" | "audio" | "video">;
        resolution?: unknown;
        constraints?: unknown;
        cameraDeviceId?: string;
        minFps?: number;
        maxFps?: number;
        desktopSharingFrameRate?: {
          min?: number;
          max?: number;
        };
        desktopSharingSourceDevice?: string;
        facingMode?: "environment" | "user";
        effects?: unknown;
        micDeviceId?: string;
        interval?: number;
        checkAgain?: (...arguments_: unknown[]) => boolean;
        listener?: (...arguments_: unknown[]) => void;
      },
      firePermissionPromptIsShownEvent?: boolean
    ): Promise<JitsiLocalTrack[]>;

    /**
     * Create a TrackVADEmitter service that connects an audio track to an VAD (voice activity detection) processor in
     * order to obtain VAD scores for individual PCM audio samples.
     * @param {string} localAudioDeviceId - The target local audio device.
     * @param {number} sampleRate - Sample rate at which the emitter will operate. Possible values  256, 512, 1024,
     * 4096, 8192, 16384. Passing other values will default to closes neighbor.
     * I.e. Providing a value of 4096 means that the emitter will process 4096 PCM samples at a time, higher values mean
     * longer calls, lowers values mean more calls but shorter.
     * @param {Object} vadProcessor - VAD Processors that does the actual compute on a PCM sample.The processor needs
     * to implement the following functions:
     * - <tt>getSampleLength()</tt> - Returns the sample size accepted by calculateAudioFrameVAD.
     * - <tt>getRequiredPCMFrequency()</tt> - Returns the PCM frequency at which the processor operates.
     * i.e. (16KHz, 44.1 KHz etc.)
     * - <tt>calculateAudioFrameVAD(pcmSample)</tt> - Process a 32 float pcm sample of getSampleLength size.
     */
    createTrackVADEmitter(
      localAudioDeviceId: string,
      sampleRate: 256 | 512 | 1024 | 4096 | 8192 | 16_384,
      vadProcessor: {
        getRequiredPCMFrequency: () => number;
        getSampleLength: () => number;
        calculateAudioFrameVAD: (pcmSample: number[]) => number;
      }
    ): Promise<TrackVADEmitter>;

    /**
     * Create AudioMixer, which is essentially a wrapper over web audio ChannelMergerNode. It essentially allows the
     * user to mix multiple MediaStreams into a single one.
     */
    createAudioMixer(): AudioMixer;

    /**
     * Go through all audio devices on the system and return one that is active, i.e. has audio signal.
     *
     * @returns Promise<Object> - Object containing information about the found device.
     */
    getActiveAudioDevice(): Promise<JitsiTrack | undefined>;

    /**
     * Checks if the current environment supports having multiple audio
     * input devices in use simultaneously.
     *
     * @returns {boolean} True if multiple audio input devices can be used.
     */
    isMultipleAudioInputSupported(): boolean;

    /**
     * Checks if local tracks can collect stats and collection is enabled.
     *
     * @param {boolean} True if stats are being collected for local tracks.
     */
    isCollectingLocalStats(): boolean;

    /**
     * Executes callback with list of media devices connected.
     *
     * @param {function} callback
     * @deprecated use JitsiMeetJS.mediaDevices.enumerateDevices instead
     */
    enumerateDevices(callback): void;

    /**
     * function that can be used to be attached to window.onerror and
     * if options.enableWindowOnErrorHandler is enabled returns
     * the function used by the lib.
     */
    getGlobalOnErrorHandler(
      message: string,
      source: string,
      lineno: number,
      colno: number,
      error: Error
    ): void;

    /**
     * Informs lib-jitsi-meet about the current network status.
     */
    setNetworkInfo({ isOnline }: { isOnline: boolean }): void;

    /**
     * Set the contentHint on the transmitted stream track to indicate
     * charaterstics in the video stream, which informs PeerConnection
     * on how to encode the track (to prefer motion or individual frame detail)
     * @param {MediaStreamTrack} track - the track that is transmitted
     * @param {String} hint - contentHint value that needs to be set on the track
     */
    setVideoTrackContentHints(track: MediaStreamTrack, hint: string): void;

    precallTest: PrecallTest;

    /**
     * Represents a hub/namespace for utility functionality which may be of
     * interest to lib-jitsi-meet clients.
     */
    util: {
      AuthUtil: {
        /**
         * Creates the URL pointing to JWT token authentication service. It is
         * formatted from the 'urlPattern' argument which can contain the following
         * constants:
         * '{room}' - name of the conference room passed as <tt>roomName</tt>
         * argument to this method.
         * '{roleUpgrade}' - will contain 'true' if the URL will be used for
         * the role upgrade scenario, where user connects from anonymous domain and
         * then gets upgraded to the moderator by logging-in from the popup window.
         *
         * @param urlPattern a URL pattern pointing to the login service
         * @param roomName the name of the conference room for which the user will
         * be authenticated
         * @param {bool} roleUpgrade <tt>true</tt> if the URL will be used for role
         * upgrade scenario, where the user logs-in from the popup window in order
         * to have the moderator rights granted
         *
         * @returns {string|null} the URL pointing to JWT login service or
         * <tt>null</tt> if 'urlPattern' is not a string and the URL can not be
         * constructed.
         */
        getTokenAuthUrl(urlPattern: string, roomName: string, roleUpgrade: boolean): string;
      };
      ScriptUtil: {
        /**
         * Loads a script from a specific source.
         *
         * @param source the source from the which the script is to be (down)loaded
         * @param async true to asynchronously load the script or false to
         * synchronously load the script
         * @param prepend true to schedule the loading of the script as soon as
         * possible or false to schedule the loading of the script at the end of the
         * scripts known at the time
         * @param relativeURL whether we need load the library from url relative
         * to the url that lib-jitsi-meet was loaded. Useful when sourcing the
         * library from different location than the app that is using it
         * @param loadCallback on load callback function
         * @param errorCallback callback to be called on error loading the script
         */
        loadScript(
          source: string,
          async: boolean,
          prepend: boolean,
          relativeURL: boolean,
          loadCallback?: (event: Event) => void,
          errorCallback?: (event: Event) => void
        ): void;
      };
      browser: BrowserCapabilities;
    };
  }>;

  export default JitsiMeetJS;
}
