declare module "client-application/actionbox-sdk/models" {
  import { Directus, ID, TransportError } from "@directus/sdk";

  type Nullable<T> = T | null;

  export type RoomState =
    | "created"
    | "active"
    | "active:open"
    | "active:locked"
    | "active:password-protected"
    | "active:password-protected:locked"
    | "read-only:open"
    | "read-only:password-protected"
    | "archived"
    | "blocked";

  type Model = {
    id: ID;
    date_created: string;
    date_updated: string;
  };

  export interface Room extends Model {
    name?: Nullable<string>;
    state?: Nullable<RoomState>;
    owner?: Nullable<string> | Nullable<User>;
    password?: Nullable<string>;
    description?: Nullable<string>;
    roles?: Nullable<Array<{ name: string; id: string }>>;
    room_users?: Nullable<Array<{ user: string; nickname: string; id: string }>>;
    invitations?: Nullable<string[]>;
  }

  export interface RoomUser extends Model {
    user?: Nullable<string>;
    role?: Nullable<string>;
    room?: Nullable<string>;
    nickname?: Nullable<string>;
  }

  export interface PermissionGroup {
    name?: Nullable<string>;
    permissions?: Nullable<string[]>;
  }

  export interface Permission extends Model {
    title?: Nullable<string>;
    key?: Nullable<string>;
    group?: Nullable<string>;
  }

  export interface Role extends Model {
    name?: Nullable<string>;
    room?: Nullable<string>;
    permissions?: Partial<RolesPermissions>[] | string[] | null;
  }

  export interface Invitation extends Model {
    room: string;
    email: Nullable<string>;
  }

  export interface User {
    id: string;
    first_name?: Nullable<string>;
    last_name?: Nullable<string>;
    email: string;
    password?: Nullable<string>;
    location?: Nullable<string>;
    title?: Nullable<string>;
    description?: Nullable<string>;
    tags?: Nullable<string>;
    avatar?: Nullable<string>;
    language?: Nullable<string>;
    theme?: Nullable<string>;
    tfa_secret?: Nullable<string>;
    status?: Nullable<string>;
    role?: Nullable<string>;
    token?: Nullable<string>;
    last_access?: Nullable<string>;
    last_page?: Nullable<string>;
  }

  export interface RolesPermissions extends Model {
    room: string;
    roles_id: string;
    permissions_id: string;
  }

  export interface RoomContent extends Model {
    room: string | Room;
    file: string;
    title: string;
    type: string;
    payload: string;
    sortOrder: number;
    createOptions?: Record<string, string | number | boolean>;
  }

  export type DataModel = {
    rooms: Room;
    room_users: RoomUser;
    permission_groups: PermissionGroup;
    roles_permissions: RolesPermissions;
    permissions: Permission;
    roles: Role;
    invitations: Invitation;
    users: User;
    directus_users: User;
    room_content: Partial<RoomContent>;
  };

  export type TypedDirectus<T = Record<string, unknown>> = Directus<DataModel & T>;

  export type NotFoundError = TransportError;
}
