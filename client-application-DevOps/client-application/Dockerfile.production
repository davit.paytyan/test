# Install dependencies only when needed
FROM node:alpine AS deps

# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm ci

# Rebuild the source code only when needed
FROM node:alpine AS builder

ARG NODE_ENV=production
ARG NEXT_PUBLIC_LIB_JITSI_MEET_SRC
ARG NEXT_PUBLIC_XMPP_SERVICE_URL
ARG NEXT_PUBLIC_XMPP_HOST_DOMAIN
ARG NEXT_PUBLIC_XMPP_HOST_MUC
ARG NEXT_PUBLIC_URL
ARG NEXT_PUBLIC_API_URL
ARG NEXT_PUBLIC_REALTIME_URL
ARG NEXT_PUBLIC_TEXTPAD_URL
ARG NEXT_PUBLIC_LOGIN_URL
ARG NEXT_PUBLIC_ACCEPT_INVITATION_URL
ARG NEXT_PUBLIC_PASSWORT_RESET_URL
ARG NEXT_PUBLIC_SENTRY_ENVIRONMENT
ARG NEXT_PUBLIC_SENTRY_RELEASE

WORKDIR /app

COPY . .
COPY --from=deps /app/node_modules ./node_modules

RUN npm run build

# Production image, copy all the files and run next
FROM node:alpine AS runner

ARG NODE_ENV=production
ENV NODE_ENV=production

WORKDIR /app

COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/next-i18next.config.js ./
COPY --from=builder /app/sentry.client.config.js ./
COPY --from=builder /app/sentry.server.config.js ./
COPY --from=builder /app/sentry.properties ./
COPY --from=builder /app/public ./public
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json

RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001
RUN chown -R nextjs:nodejs /app/.next

USER nextjs

EXPOSE 8080

# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry.
RUN npx next telemetry disable

CMD ["npm", "start"]