// This file sets a custom webpack configuration to use your Next.js app
// with Sentry.
// https://nextjs.org/docs/api-reference/next.config.js/introduction
// https://docs.sentry.io/platforms/javascript/guides/nextjs/
const { withSentryConfig } = require("@sentry/nextjs");
const withTM = require("next-transpile-modules")(["@colyseus/schema"]);
const { i18n } = require("./next-i18next.config");

const moduleExports = {
  publicRuntimeConfig: {
    audioOutputDisabled: process.env.AUDIO_OUTPUT_DISABLED,
    libJitsiMeetSrc: process.env.NEXT_PUBLIC_LIB_JITSI_MEET_SRC,
    publicUrl: process.env.NEXT_PUBLIC_URL,
    publicApiUrl: process.env.NEXT_PUBLIC_API_URL,
    realtimeUrl: process.env.NEXT_PUBLIC_REALTIME_URL,
    publicTextpadUrl: process.env.NEXT_PUBLIC_TEXTPAD_URL,
    xmppServiceUrl: process.env.NEXT_PUBLIC_XMPP_SERVICE_URL,
    xmppHostDomain: process.env.NEXT_PUBLIC_XMPP_HOST_DOMAIN,
    xmppHostMuc: process.env.NEXT_PUBLIC_XMPP_HOST_MUC,
    loginUrl: process.env.NEXT_PUBLIC_LOGIN_URL,
    invitationUrl: process.env.NEXT_PUBLIC_ACCEPT_INVITATION_URL,
    passwordResetUrl: process.env.NEXT_PUBLIC_PASSWORT_RESET_URL,
    sentryEnvironment: process.env.NEXT_PUBLIC_SENTRY_ENVIRONMENT,
    sentryRelease: process.env.NEXT_PUBLIC_SENTRY_RELEASE,
  },
  env: {
    NEXT_PUBLIC_LIB_JITSI_MEET_SRC: process.env.NEXT_PUBLIC_LIB_JITSI_MEET_SRC,
    NEXT_PUBLIC_XMPP_SERVICE_URL: process.env.NEXT_PUBLIC_XMPP_SERVICE_URL,
    NEXT_PUBLIC_XMPP_HOST_DOMAIN: process.env.NEXT_PUBLIC_XMPP_HOST_DOMAIN,
    NEXT_PUBLIC_XMPP_HOST_MUC: process.env.NEXT_PUBLIC_XMPP_HOST_MUC,
    NEXT_PUBLIC_URL: process.env.NEXT_PUBLIC_URL,
    NEXT_PUBLIC_API_URL: process.env.NEXT_PUBLIC_API_URL,
    NEXT_PUBLIC_REALTIME_URL: process.env.NEXT_PUBLIC_REALTIME_URL,
    NEXT_PUBLIC_TEXTPAD_URL: process.env.NEXT_PUBLIC_TEXTPAD_URL,
    NEXT_PUBLIC_LOGIN_URL: process.env.NEXT_PUBLIC_LOGIN_URL,
    NEXT_PUBLIC_ACCEPT_INVITATION_URL: process.env.NEXT_PUBLIC_ACCEPT_INVITATION_URL,
    NEXT_PUBLIC_PASSWORT_RESET_URL: process.env.NEXT_PUBLIC_PASSWORT_RESET_URL,
    NEXT_PUBLIC_SENTRY_ENVIRONMENT: process.env.NEXT_PUBLIC_SENTRY_ENVIRONMENT,
    NEXT_PUBLIC_SENTRY_RELEASE: process.env.NEXT_PUBLIC_SENTRY_RELEASE,
  },
  images: {
    domains: [process.env.NEXT_PUBLIC_API_URL],
  },
  i18n,
  webpack: (config) => {
    config.module.rules.push({
      test: /\.svg$/,
      use: [
        {
          loader: "@svgr/webpack",
          options: {
            svgo: false,
          },
        },
      ],
    });

    return config;
  },
};

if (process.env.NODE_ENV !== "production") {
  module.exports = withTM(moduleExports);
} else {
  const SentryWebpackPluginOptions = {
    // Additional config options for the Sentry Webpack plugin. Keep in mind that
    // the following options are set automatically, and overriding them is not
    // recommended:
    //   release, url, org, project, authToken, configFile, stripPrefix,
    //   urlPrefix, include, ignore
    // For all available options, see:
    // https://github.com/getsentry/sentry-webpack-plugin#options.
  };
  // Make sure adding Sentry options is the last code to run before exporting, to
  // ensure that your source maps include changes from all other Webpack plugins
  module.exports = withSentryConfig(withTM(moduleExports), SentryWebpackPluginOptions);
}
