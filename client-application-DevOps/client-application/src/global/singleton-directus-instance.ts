import { Directus } from "@directus/sdk";
import { TypedDirectus } from "client-application/actionbox-sdk/models";

const url = process.env.NEXT_PUBLIC_API_URL;

if (!url) throw new Error("NEXT_PUBLIC_API_URL is undefined!");

export const directus: TypedDirectus = new Directus(url);
