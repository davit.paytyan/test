import React from "react";
import { FullScreenLoader } from "../components/pages/common/full-screen-loader";
import { ConferenceContextProvider } from "./container/conference-context";
import { ConferenceStateRouter } from "./container/conference-state-router";
import { JoinRoomView } from "./views/join-room-view";
import { RoomAccessDeniedVied } from "./views/room-access-denied-view";
import { RoomJoinedView } from "./views/room-joined-view";
import { RoomNotFoundView } from "./views/room-not-found-view";

export function Room({ roomId }: { roomId: string }) {
  return (
    <ConferenceContextProvider roomId={roomId}>
      <ConferenceStateRouter
        renderWhenRoomAccessDenied={() => <RoomAccessDeniedVied />}
        renderWhenRoomNotFound={() => <RoomNotFoundView />}
        renderWhenWaitingRoom={() => <JoinRoomView />}
        renderWhenRoomJoined={() => <RoomJoinedView />}
      >
        <FullScreenLoader />
      </ConferenceStateRouter>
    </ConferenceContextProvider>
  );
}
