import { MediaTrack } from "library/utils/create-media-track";

export function useIsJitsiConferenceVideoTrackEnabled(track?: MediaTrack) {
  return track && !track.isMuted;
}
