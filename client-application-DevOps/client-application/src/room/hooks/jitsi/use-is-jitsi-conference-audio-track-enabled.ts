import { MediaTrack } from "library/utils/create-media-track";

export function useIsJitsiConferenceAudioTrackEnabled(track?: MediaTrack) {
  return track && !track.isMuted;
}
