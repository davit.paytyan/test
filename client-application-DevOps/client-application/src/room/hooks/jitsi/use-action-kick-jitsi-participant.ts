import { useCallback } from "react";
import { useJitsiService } from "./use-jitsi-service";

export function useActionKickJitsiParticipant() {
  const service = useJitsiService();
  return useCallback(
    (participantId: string, reason: string) => {
      service.send({ type: "kickParticipant", participantId, reason });
    },
    [service]
  );
}
