import { useSelector } from "@xstate/react";
import { useDataParticipant } from "../room-participants/use-data-participant";
import { useJitsiService } from "./use-jitsi-service";

export function useJitsiConferenceAudioTrack(userId: string) {
  const jitsiService = useJitsiService();
  const participant = useDataParticipant(userId);

  const track = useSelector(jitsiService, (state) => {
    if (!participant?.conferenceUserId) return;
    return state.context.audioTracks.get(participant?.conferenceUserId);
  });

  return track;
}
