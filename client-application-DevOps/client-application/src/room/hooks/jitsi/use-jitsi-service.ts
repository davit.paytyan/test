import { useSelector } from "@xstate/react";
import { useConferenceContext } from "../../container/conference-context";

export function useJitsiService() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    const child = state.context.refs.jitsi;
    if (!child) throw new Error("Access to jitsi is only possible after initialization");
    return child;
  });
}
