import { useSelector } from "@xstate/react";
import { useJitsiService } from "./use-jitsi-service";

export function useJitsiConferenceAudioTracks() {
  const jitsiService = useJitsiService();

  return useSelector(jitsiService, (state) => {
    return state.context.audioTracks;
  });
}
