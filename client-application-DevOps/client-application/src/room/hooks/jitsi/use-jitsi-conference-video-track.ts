import { useSelector } from "@xstate/react";
import { useDataParticipant } from "../room-participants/use-data-participant";
import { useJitsiService } from "./use-jitsi-service";

export function useJitsiConferenceVideoTrack(userId: string) {
  const jitsiService = useJitsiService();
  const participant = useDataParticipant(userId);

  const track = useSelector(jitsiService, (state) => {
    return state.context.videoTracks.get(participant?.conferenceUserId ?? userId);
  });

  return track;
}
