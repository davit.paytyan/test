import { useSelector } from "@xstate/react";
import { useJitsiService } from "./use-jitsi-service";

export function useJitsiConferenceVideoTracks() {
  const jitsiService = useJitsiService();

  return useSelector(jitsiService, (state) => {
    return state.context.videoTracks;
  });
}
