import { useCallback } from "react";
import { useLocalTracksService } from "../local-tracks/use-local-tracks-service";

export function useActionChangeAudioInputDevice() {
  const service = useLocalTracksService();
  return useCallback(
    (deviceId: string) => {
      service.send({ type: "changeLocalAudioTrack", deviceId, track: undefined });
    },
    [service]
  );
}
