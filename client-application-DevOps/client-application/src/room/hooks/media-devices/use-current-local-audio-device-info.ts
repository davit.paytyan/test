import { useMemo } from "react";
import { useAvailableMediaDevices } from "./use-available-media-devices";
import { useCurrentLocalAudioTrack } from "../local-tracks/use-current-local-audio-track";

export function useCurrentLocalAudioDeviceInfo() {
  const devices = useAvailableMediaDevices();
  const audioTrack = useCurrentLocalAudioTrack();

  return useMemo(() => {
    const selectedDeviceId = audioTrack?.getDeviceId();
    const deviceInfo = devices?.find((device) => {
      return device.deviceId === selectedDeviceId;
    });
    return deviceInfo;
  }, [devices, audioTrack]);
}
