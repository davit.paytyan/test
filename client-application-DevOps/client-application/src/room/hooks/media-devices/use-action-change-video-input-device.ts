import { useCallback } from "react";
import { useLocalTracksService } from "../local-tracks/use-local-tracks-service";

export function useActionChangeVideoInputDevice() {
  const service = useLocalTracksService();
  return useCallback(
    (deviceId: string) =>
      service.send({ type: "changeLocalVideoTrack", deviceId, track: undefined }),
    [service]
  );
}
