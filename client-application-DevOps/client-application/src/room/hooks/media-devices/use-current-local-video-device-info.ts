import { useMemo } from "react";
import { useAvailableMediaDevices } from "./use-available-media-devices";
import { useCurrentLocalVideoTrack } from "../local-tracks/use-current-local-video-track";

export function useCurrentLocalVideoDeviceInfo() {
  const devices = useAvailableMediaDevices();
  const videoTrack = useCurrentLocalVideoTrack();

  return useMemo(() => {
    const selectedDeviceId = videoTrack?.getDeviceId();
    const deviceInfo = devices?.find((device) => {
      return device.deviceId === selectedDeviceId;
    });
    return deviceInfo;
  }, [devices, videoTrack]);
}
