import { useSelector } from "@xstate/react";
import { useConferenceContext } from "../../container/conference-context";

export function useAvailableMediaDevices() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.context.devices;
  });
}
