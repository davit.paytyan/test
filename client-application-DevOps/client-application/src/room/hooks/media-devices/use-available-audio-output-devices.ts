import { useMemo } from "react";
import { useAvailableMediaDevices } from "./use-available-media-devices";

export function useAvailableAudioOutputDevices() {
  const devices = useAvailableMediaDevices();
  return useMemo(() => {
    return devices?.filter((device) => device.kind === "audiooutput");
  }, [devices]);
}
