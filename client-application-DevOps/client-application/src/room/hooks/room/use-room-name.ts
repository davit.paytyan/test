import { useSelector } from "@xstate/react";
import { useConferenceContext } from "../../container/conference-context";

export function useRoomName() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.context.room?.name;
  });
}
