import { useCallback } from "react";
import { useConferenceContext } from "../../container/conference-context";

export function useActionLeaveRoom() {
  const context = useConferenceContext();
  return useCallback(() => {
    context.conferenceService.send({ type: "leaveRoom" });
  }, [context.conferenceService]);
}
