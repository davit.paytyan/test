import { useCallback } from "react";
import { useRoomNameService } from "./use-room-name-service";

export function useActionSetRoomName() {
  const service = useRoomNameService();
  return useCallback(
    (value: string) => service.send({ type: "setRoomName", name: value }),
    [service]
  );
}
