import { useSelector } from "@xstate/react";
import { InterpreterFrom } from "xstate";
import { useConferenceContext } from "../../container/conference-context";
import { ColyseusRoomNameMachine } from "../../machines/colyseus-room-name-machine";

export function useRoomNameService() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.children["colyseus-room-name"] as InterpreterFrom<typeof ColyseusRoomNameMachine>;
  });
}
