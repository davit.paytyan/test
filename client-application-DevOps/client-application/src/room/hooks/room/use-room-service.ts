import { useSelector } from "@xstate/react";
import { InterpreterFrom } from "xstate";
import { useConferenceContext } from "../../container/conference-context";
import { ColyseusRoomMachine } from "../../machines/colyseus-room-machine";

export function useRoomService() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.children["colyseus-room"] as InterpreterFrom<typeof ColyseusRoomMachine>;
  });
}
