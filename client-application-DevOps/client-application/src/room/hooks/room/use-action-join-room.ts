import { useCallback } from "react";
import { useConferenceContext } from "../../container/conference-context";

export function useActionJoinRoom() {
  const context = useConferenceContext();
  return useCallback(
    (nickname: string) => {
      context.conferenceService.send({ type: "joinRoom", nickname });
    },
    [context.conferenceService]
  );
}
