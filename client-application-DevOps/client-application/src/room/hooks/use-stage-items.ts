import { useMemo } from "react";
import { MediaTrack } from "../../library/utils/create-media-track";
import { RoomStageItem } from "../machines/actors/room-stage-actor";
import { useJitsiConferenceAudioTracks } from "./jitsi/use-jitsi-conference-audio-tracks";
import { useJitsiConferenceVideoTracks } from "./jitsi/use-jitsi-conference-video-tracks";
import { useRoomStageItems } from "./room-stage/use-room-stage-items";

export type StageItem = RoomStageItem & {
  payload: RoomStageItem["payload"] & {
    videoTrack?: MediaTrack;
    audioTrack?: MediaTrack;
  };
};

export function useStageItems() {
  const videoTracks = useJitsiConferenceVideoTracks();
  const audioTracks = useJitsiConferenceAudioTracks();
  const roomStageItems = useRoomStageItems();

  return useMemo(() => {
    const collection: StageItem[] = [];

    for (const roomStageItem of roomStageItems) {
      if (roomStageItem.type !== "participant") {
        collection.push(roomStageItem);
        continue;
      }

      if (!roomStageItem.payload.conferenceUserId) continue;

      const videoTrack = videoTracks.get(roomStageItem.payload.conferenceUserId);
      const audioTrack = audioTracks.get(roomStageItem.payload.conferenceUserId);
      const stageItem: StageItem = {
        ...roomStageItem,
        payload: {
          ...roomStageItem.payload,
          videoTrack,
          audioTrack,
        },
      };
      collection.push(stageItem);
    }

    return collection;
  }, [videoTracks, audioTracks, roomStageItems]);
}
