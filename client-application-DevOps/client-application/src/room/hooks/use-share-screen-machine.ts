import getConfig from "next/config";
import { useCallback, useMemo, useRef, useState } from "react";
import { useActionPutItemOnStage } from "./room-stage/use-action-put-item-on-stage";
import { useActionRemoveItemFromStage } from "./room-stage/use-action-remove-item-from-stage";
import { useRoomId } from "./room/use-room-id";

const { publicRuntimeConfig } = getConfig();

export type useScreenSharingResult = {
  isLoading: boolean;
  isActive: boolean;
  startScreenSharing: () => void;
  stopScreenSharing: () => Promise<void>;
};

export function useScreenSharing(): useScreenSharingResult {
  const putItemOnStage = useActionPutItemOnStage();
  const removeItemFromStage = useActionRemoveItemFromStage();
  const roomId = useRoomId();
  const [isLoading, setIsLoading] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const stopScreenSharing = useRef<() => Promise<void>>(() => Promise.resolve());

  const startScreenSharing = useCallback(async () => {
    setIsLoading(true);

    const iframe = document.createElement("iframe") as HTMLIFrameElement;
    let conferenceUserId;

    iframe.src = `${publicRuntimeConfig.publicUrl}/room/screen-share?roomId=${roomId}`;
    iframe.width = "0px";
    iframe.height = "0px";
    iframe.style.display = "none";

    window.document.addEventListener("startScreenShare", (event: CustomEvent) => {
      conferenceUserId = event.detail;
      putItemOnStage({ id: event.detail as string, type: "shared_screen" });
      setIsLoading(false);
      setIsActive(true);
    });

    window.document.addEventListener("endScreenShare", (event: CustomEvent) => {
      removeItemFromStage(event.detail);
      iframe.remove();
      setIsActive(false);
      setIsLoading(false);
    });

    async function _stopScreenSharing() {
      removeItemFromStage(conferenceUserId);
      iframe.remove();
      setIsActive(false);
      setIsLoading(false);
    }

    document.body.append(iframe);

    stopScreenSharing.current = _stopScreenSharing;
  }, [roomId, putItemOnStage, removeItemFromStage]);

  return useMemo(
    () => ({
      isLoading,
      isActive,
      startScreenSharing,
      stopScreenSharing: async () => {
        await stopScreenSharing.current();
      },
    }),
    [isLoading, isActive, startScreenSharing, stopScreenSharing]
  );
}
