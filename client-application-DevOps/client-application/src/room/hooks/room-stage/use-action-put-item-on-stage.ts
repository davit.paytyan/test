import { useCallback } from "react";
import { NewRoomStageItem } from "../../machines/actors/room-stage-actor";
import { useRoomStageService } from "./use-room-stage-service";

export function useActionPutItemOnStage() {
  const service = useRoomStageService();
  return useCallback(
    (item: NewRoomStageItem) => service.send({ type: "putItemOnStage", item }),
    [service]
  );
}
