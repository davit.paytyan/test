import { useCallback } from "react";
import { useRoomStageService } from "./use-room-stage-service";

export function useActionRenameItemOnStage() {
  const service = useRoomStageService();
  return useCallback(
    (id: string, title: string, type: string) =>
      service.send({ type: "renameItemOnStage", item: { id, title, type } }),
    [service]
  );
}
