import { useRoomStageItems } from "./use-room-stage-items";

export function useStateIsStageFull() {
  const stageItems = useRoomStageItems();
  return stageItems.length > 3;
}
