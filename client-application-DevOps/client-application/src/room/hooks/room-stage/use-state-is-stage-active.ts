import { useRoomStageItems } from "./use-room-stage-items";

export function useStateIsStageActive() {
  const stageItems = useRoomStageItems();
  return stageItems.length > 0;
}
