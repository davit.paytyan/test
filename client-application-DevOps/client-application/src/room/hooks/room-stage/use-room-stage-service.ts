import { useSelector } from "@xstate/react";
import { InterpreterFrom } from "xstate";
import { useConferenceContext } from "../../container/conference-context";
import { ColyseusRoomStageMachine } from "../../machines/colyseus-room-stage-machine";

type Actor = InterpreterFrom<typeof ColyseusRoomStageMachine>;

export function useRoomStageService() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.children["colyseus-room-stage"] as Actor;
  });
}
