import { useCallback } from "react";
import { Participant } from "../../machines/actors/room-participants-actor";
import { useRoomUserIsOwnerOrModerator } from "../room-user/use-room-user-is-owner-or-moderator";
import { useActionPutItemOnStage } from "./use-action-put-item-on-stage";
import { useActionRemoveItemFromStage } from "./use-action-remove-item-from-stage";

export function useActionToggleRoomAudienceItemOnStage() {
  const isOwnerOrModerator = useRoomUserIsOwnerOrModerator();
  const putItemOnStage = useActionPutItemOnStage();
  const removeItemFromStage = useActionRemoveItemFromStage();

  return useCallback(
    (item: Pick<Participant, "isOnStage" | "userId" | "nickname">) => {
      if (!isOwnerOrModerator) return;
      if (item.isOnStage) {
        removeItemFromStage(item.userId);
      } else {
        putItemOnStage({ id: item.userId, type: "participant", title: item.nickname });
      }
    },
    [isOwnerOrModerator, putItemOnStage, removeItemFromStage]
  );
}
