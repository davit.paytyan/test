import { useSelector } from "@xstate/react";
import { useRoomStageService } from "./use-room-stage-service";

export function useRoomStageItems() {
  const service = useRoomStageService();
  return useSelector(service, (state) => state.context.stageItems);
}
