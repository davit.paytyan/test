import { useCallback } from "react";
import { useRoomStageService } from "./use-room-stage-service";

export function useActionRemoveItemFromStage() {
  const service = useRoomStageService();
  return useCallback((id: string) => service.send({ type: "removeItemFromStage", id }), [service]);
}
