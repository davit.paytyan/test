import { useCallback } from "react";
import { useActionCloseParticipantQuickReaction } from "./use-action-close-participant-quick-reaction";
import { useActionOpenParticipantQuickReaction } from "./use-action-open-participant-quick-reaction";
import { useStateIsParticipantQuickReactionOpen } from "./use-state-is-participant-quick-reaction-open";

export function useActionToggleQuickReaction(userId: string) {
  const isOpen = useStateIsParticipantQuickReactionOpen(userId);
  const open = useActionOpenParticipantQuickReaction(userId);
  const close = useActionCloseParticipantQuickReaction(userId);

  return useCallback(() => (isOpen ? close() : open()), [isOpen, open, close]);
}
