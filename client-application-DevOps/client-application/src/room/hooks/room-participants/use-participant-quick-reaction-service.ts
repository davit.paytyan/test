import { useActor } from "@xstate/react";
import { StateFrom } from "xstate";
import { NullActor } from "../../../library/null-actor";
import { ColyseusRoomParticipantsMachine } from "../../machines/colyseus-room-participants-machine";
import { useRoomParticipantsService } from "./use-room-participants-service";

type State = StateFrom<typeof ColyseusRoomParticipantsMachine>;

export function useParticipantQuickReactionService(userId: string) {
  const service = useRoomParticipantsService();
  const [state] = useActor(service ?? NullActor);
  return (state as State | undefined)?.context._quickReactionMachineRefs.get(userId);
}
