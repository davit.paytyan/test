import { useSelector } from "@xstate/react";
import { useRoomParticipantsService } from "./use-room-participants-service";

export function useStateIsParticipantQuickReactionOpen(userId: string) {
  const service = useRoomParticipantsService();
  return useSelector(service, (state) => {
    const child = state.context._quickReactionMachineRefs.get(userId);
    const snapshot = child?.getSnapshot();
    if (!snapshot) return false;
    return snapshot.matches("newReaction") || snapshot.matches("idle.open");
  });
}
