import { useSelector } from "@xstate/react";
import { useRoomParticipantsService } from "./use-room-participants-service";

export function useDataParticipants() {
  const service = useRoomParticipantsService();
  return useSelector(service, (state) => {
    return state.context.participants;
  });
}
