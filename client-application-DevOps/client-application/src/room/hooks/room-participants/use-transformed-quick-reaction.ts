import { useMemo } from "react";
import { QUICK_REACTIONS } from "library/constants/quick-reactions";
import { useDataParticipantQuickReaction } from "./use-data-participant-quick-reaction";

export function useTransformedQuickReaction(userId: string) {
  const quickReaction = useDataParticipantQuickReaction(userId);
  return useMemo(() => {
    if (!quickReaction) return;

    switch (quickReaction.type) {
      case "mood":
      case "bool":
      case "number":
        return {
          type: "reaction",
          value: QUICK_REACTIONS.get(`${quickReaction.type}-${quickReaction.value}`) as string,
        };
      case "text":
        return {
          type: "text",
          value: QUICK_REACTIONS.get(`${quickReaction.type}-${quickReaction.value}`) as string,
        };
      case "free-text":
        return {
          type: "text",
          value: quickReaction.value,
        };
        break;
    }
  }, [quickReaction]);
}
