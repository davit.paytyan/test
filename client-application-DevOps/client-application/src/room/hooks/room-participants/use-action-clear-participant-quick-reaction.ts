import { useCallback } from "react";
import { useRoomParticipantsService } from "./use-room-participants-service";

export function useActionClearParticipantQuickReaction() {
  const service = useRoomParticipantsService();
  return useCallback(() => service.send({ type: "clearParticipantQuickReaction" }), [service]);
}
