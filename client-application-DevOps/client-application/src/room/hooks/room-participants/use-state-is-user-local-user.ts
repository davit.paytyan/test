import { useMemo } from "react";
import { useRoomUserUserId } from "../room-user/use-room-user-user-id";

export function useIsUserLocalUser(userId: string) {
  const roomUserUserId = useRoomUserUserId();
  return useMemo(() => roomUserUserId === userId, [roomUserUserId, userId]);
}
