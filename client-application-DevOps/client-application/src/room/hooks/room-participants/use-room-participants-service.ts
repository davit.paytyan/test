import { useSelector } from "@xstate/react";
import { InterpreterFrom } from "xstate";
import { useConferenceContext } from "../../container/conference-context";
import { ColyseusRoomParticipantsMachine } from "../../machines/colyseus-room-participants-machine";

export type RoomParticipantsService = InterpreterFrom<typeof ColyseusRoomParticipantsMachine>;

export function useRoomParticipantsService() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    const child = state.context.colyseusParticipantsRef;
    if (!child)
      throw new Error("Access to colyseus-participants is only possible after initialization");
    return child;
  });
}
