import { useSelector } from "@xstate/react";
import { useRoomParticipantsService } from "./use-room-participants-service";

export function useDataParticipantQuickReaction(userId: string) {
  const service = useRoomParticipantsService();
  return useSelector(service, (state) => {
    const child = state.context._quickReactionMachineRefs.get(userId);
    const snapshot = child?.getSnapshot();
    if (!snapshot) return;
    return snapshot.context.quickReaction;
  });
}
