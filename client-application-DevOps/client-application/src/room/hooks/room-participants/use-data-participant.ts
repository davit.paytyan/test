import { useMemo } from "react";
import { useDataParticipants } from "./use-data-participants";

export function useDataParticipant(userId: string) {
  const participants = useDataParticipants();
  return useMemo(() => {
    return participants.find((participant) => participant.userId === userId);
  }, [userId, participants]);
}
