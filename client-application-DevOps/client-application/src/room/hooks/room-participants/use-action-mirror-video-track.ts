import { useCallback } from "react";
import { useRoomParticipantsService } from "./use-room-participants-service";

export function useActionMirrorVideoTrack() {
  const participantsService = useRoomParticipantsService();
  return useCallback(
    (mirrored: boolean) => {
      participantsService.send({ type: "mirrorVideoTrack", mirrored });
    },
    [participantsService]
  );
}
