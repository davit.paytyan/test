import { useSelector } from "@xstate/react";
import { useCallback } from "react";
import { useRoomParticipantsService } from "./use-room-participants-service";

export function useActionCloseParticipantQuickReaction(userId: string) {
  const service = useRoomParticipantsService();
  const bubbleService = useSelector(service, (state) => {
    return state.context._quickReactionMachineRefs.get(userId);
  });
  return useCallback(() => {
    bubbleService?.send({ type: "closeBubble" });
  }, [bubbleService]);
}
