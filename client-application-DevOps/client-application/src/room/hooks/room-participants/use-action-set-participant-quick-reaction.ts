import { useCallback } from "react";
import { useRoomParticipantsService } from "./use-room-participants-service";

export function useActionSetParticipantQuickReaction() {
  const service = useRoomParticipantsService();
  return useCallback(
    (status: Payload) => service.send({ type: "setParticipantQuickReaction", status }),
    [service]
  );
}

type Payload = { type: string; value: string };
