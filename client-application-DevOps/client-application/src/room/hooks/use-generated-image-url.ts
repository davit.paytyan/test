import { directus } from "global/singleton-directus-instance";
import getConfig from "next/config";

type ImageConstraints = {
  fit?: string;
  width?: number;
  height?: number;
};

function serializeConstraints(constraints?: ImageConstraints) {
  if (!constraints) return "";
  let string = "";
  for (const [key, value] of Object.entries(constraints)) {
    if (!value) continue;
    string = `${string}&${key}=${value}`;
  }
  return string !== "" ? `&${string}` : string;
}

export function useGeneratedImageUrl(fileId?: string, constraints?: ImageConstraints) {
  if (!fileId) return;
  const { token } = directus.auth ?? "";
  const { publicRuntimeConfig } = getConfig();
  const { publicApiUrl } = publicRuntimeConfig;
  const serializedConstraints = serializeConstraints(constraints);
  return `${publicApiUrl}/assets/${fileId}?access_token=${token}${serializedConstraints}`;
}
