import { useMemo } from "react";
import { useDataContentPanelListItems } from "./use-data-content-panel-list-items";
import { useDataContentPanelSelectedItems } from "./use-data-content-panel-selected-items";

export function useStateAreAllContentPanelItemsSelected() {
  const items = useDataContentPanelListItems();
  const selectedItems = useDataContentPanelSelectedItems();
  return useMemo(
    () => Boolean(items && items.length > 0 && items.length === selectedItems.size),
    [items, selectedItems]
  );
}
