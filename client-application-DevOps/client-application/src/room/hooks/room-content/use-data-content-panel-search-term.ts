import { useSelector } from "@xstate/react";
import { useRoomContentService } from "./use-room-content-service";

export function useDataContentPanelSearchTerm() {
  const service = useRoomContentService();
  return useSelector(service, (state) => state.context.searchTerm ?? "");
}
