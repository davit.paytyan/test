import { useSelector } from "@xstate/react";
import { useRoomContentService } from "./use-room-content-service";

export function useDataContentPanelSelectedItems() {
  const service = useRoomContentService();
  return useSelector(service, (state) => state.context.selectedItems);
}
