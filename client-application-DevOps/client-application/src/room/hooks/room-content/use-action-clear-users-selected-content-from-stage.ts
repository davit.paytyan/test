import { useCallback } from "react";
import { useActionRemoveItemFromStage } from "../room-stage/use-action-remove-item-from-stage";
import { useRoomStageItems } from "../room-stage/use-room-stage-items";
import { useDataContentPanelSelectedItems } from "./use-data-content-panel-selected-items";

export function useActionClearUsersSelectedContentFromStage() {
  const stageItems = useRoomStageItems();
  const removeItemFromStage = useActionRemoveItemFromStage();
  const selectedItems = useDataContentPanelSelectedItems();

  return useCallback(() => {
    if (selectedItems.size === 0) return;
    const stageItemIds = new Set(stageItems.map((item) => item.id));
    for (const item of selectedItems) {
      if (stageItemIds.has(item)) removeItemFromStage(item);
    }
  }, [stageItems, selectedItems, removeItemFromStage]);
}
