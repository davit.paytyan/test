import { useCallback } from "react";
import { useRoomContentService } from "./use-room-content-service";

export function useActionOpenContentPanelActions() {
  const service = useRoomContentService();
  return useCallback(() => service.send({ type: "openPanelActions" }), [service]);
}
