import { useCallback } from "react";
import { RoomContentMachineModel } from "../../machines/room-content-machine";
import { useRoomContentService } from "./use-room-content-service";

export function useActionRenameContentPanelItem() {
  const service = useRoomContentService();
  return useCallback(
    (itemId: string, title: string) =>
      service.send(RoomContentMachineModel.events.renameItem(itemId, title)),
    [service]
  );
}
