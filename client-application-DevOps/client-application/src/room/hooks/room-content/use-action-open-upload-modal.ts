import { useContentUploadContext } from "../../../content-upload/components/content-upload-context";

export function useActionOpenUploadModal() {
  const { openUploadModal } = useContentUploadContext();
  return openUploadModal;
}
