import { useCallback } from "react";
import { useActionRemoveItemFromStage } from "../room-stage/use-action-remove-item-from-stage";
import { useRoomStageItems } from "../room-stage/use-room-stage-items";
import { useDataContentPanelListItems } from "./use-data-content-panel-list-items";

export function useActionClearUsersContentFromStage() {
  const stageItems = useRoomStageItems();
  const contentPanelItems = useDataContentPanelListItems();
  const removeItemFromStage = useActionRemoveItemFromStage();

  return useCallback(() => {
    if (!contentPanelItems) return;
    const stageItemIds = new Set(stageItems.map((item) => item.id));
    for (const item of contentPanelItems) {
      if (stageItemIds.has(item)) removeItemFromStage(item);
    }
  }, [stageItems, removeItemFromStage, contentPanelItems]);
}
