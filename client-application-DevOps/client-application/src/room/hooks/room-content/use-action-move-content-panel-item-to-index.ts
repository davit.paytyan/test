import { useCallback } from "react";
import { useRoomContentService } from "./use-room-content-service";

export function useActionMoveContentPanelItemToIndex() {
  const service = useRoomContentService();
  return useCallback(
    (itemId: string, newIndex: number, save = false) => {
      service.send({ type: "moveItemToIndex", itemId, newIndex, save });
    },
    [service]
  );
}
