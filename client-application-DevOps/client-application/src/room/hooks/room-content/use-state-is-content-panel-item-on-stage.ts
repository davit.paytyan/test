import { useMemo } from "react";
import { useRoomStageItems } from "../room-stage/use-room-stage-items";

export function useStateIsContentPanelItemOnStage(itemId: string) {
  const stageItems = useRoomStageItems();
  return useMemo(() => {
    return stageItems.some((item) => item.id === itemId);
  }, [stageItems, itemId]);
}
