import { useSelector } from "@xstate/react";
import { useRoomContentService } from "./use-room-content-service";

export function useDataContentPanelListItem(itemId: string) {
  const service = useRoomContentService();
  return useSelector(service, (state) => {
    return state.context.items.find((item) => item.id === itemId);
  });
}
