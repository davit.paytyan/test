import { useCallback } from "react";
import { RoomContentMachineModel } from "../../machines/room-content-machine";
import { useRoomContentService } from "./use-room-content-service";

export function useActionCreateContentItem() {
  const service = useRoomContentService();
  return useCallback(
    (title: string, options?: Record<string, string | number | boolean>) =>
      service.send(RoomContentMachineModel.events.createItem(title, options)),
    [service]
  );
}
