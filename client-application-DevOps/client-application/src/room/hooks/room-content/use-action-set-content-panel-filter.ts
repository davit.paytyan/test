import { useCallback } from "react";
import { RoomContentMachineModel } from "../../machines/room-content-machine";
import { useRoomContentService } from "./use-room-content-service";

export function useActionSetContentPanelSearchTerm() {
  const service = useRoomContentService();
  return useCallback(
    (value?: string) => service.send(RoomContentMachineModel.events.setSearchTerm(value)),
    [service]
  );
}
