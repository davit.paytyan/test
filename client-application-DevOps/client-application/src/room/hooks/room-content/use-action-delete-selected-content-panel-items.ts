import { useCallback } from "react";
import { RoomContentMachineModel } from "../../machines/room-content-machine";
import { useActionClearUsersSelectedContentFromStage } from "./use-action-clear-users-selected-content-from-stage";
import { useRoomContentService } from "./use-room-content-service";

export function useActionDeleteSelectedContentPanelItems() {
  const service = useRoomContentService();
  const clearItemsFromStage = useActionClearUsersSelectedContentFromStage();

  return useCallback(() => {
    clearItemsFromStage();
    service.send(RoomContentMachineModel.events.deleteSelectedItems());
  }, [service, clearItemsFromStage]);
}
