import { useCallback } from "react";
import { useRoomContentService } from "./use-room-content-service";

export function useActionRefreshRoomContent() {
  const service = useRoomContentService();
  return useCallback(() => service.send({ type: "refreshRoomContent" }), [service]);
}
