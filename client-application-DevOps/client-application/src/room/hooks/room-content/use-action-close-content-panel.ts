import { useCallback } from "react";
import { useRoomContentService } from "./use-room-content-service";

export function useActionCloseContentPanel() {
  const service = useRoomContentService();
  return useCallback(() => service.send({ type: "closePanel" }), [service]);
}
