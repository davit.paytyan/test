import { useMemo } from "react";
import { useRoomStageItems } from "../room-stage/use-room-stage-items";

export function useStateIsTextpadActive() {
  const stageItems = useRoomStageItems();
  return useMemo(() => {
    return stageItems.some((item) => item.id === "textpad");
  }, [stageItems]);
}
