import { useSelector } from "@xstate/react";
import { useRoomContentService } from "./use-room-content-service";

export function useStateAreContentPanelActionsOpen() {
  const service = useRoomContentService();
  return useSelector(service, (state) => {
    return state.matches("actions.opened") ?? false;
  });
}
