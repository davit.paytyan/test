import { useContentUploadContext } from "../../../content-upload/components/content-upload-context";

export function useActionCloseUploadModal() {
  const { closeUploadModal } = useContentUploadContext();
  return closeUploadModal;
}
