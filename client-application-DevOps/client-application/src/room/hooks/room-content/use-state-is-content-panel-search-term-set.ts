import { useSelector } from "@xstate/react";
import { useRoomContentService } from "./use-room-content-service";

export function useStateIsContentPanelSearchTermSet() {
  const service = useRoomContentService();
  return useSelector(service, (state) => {
    return state.matches("collection.searchTerm.set") ?? false;
  });
}
