import { useSelector } from "@xstate/react";
import { useRoomContentService } from "./use-room-content-service";

export function useStateIsContentPanelOpen() {
  const service = useRoomContentService();
  return useSelector(service, (state) => {
    return state.matches("panel.opened") ?? false;
  });
}
