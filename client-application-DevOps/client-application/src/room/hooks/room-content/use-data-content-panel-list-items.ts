import { useSelector } from "@xstate/react";
import { useMemo } from "react";
import { useDataContentPanelSearchTerm } from "./use-data-content-panel-search-term";
import { useRoomContentService } from "./use-room-content-service";
import { useStateIsContentPanelSearchTermSet } from "./use-state-is-content-panel-search-term-set";

export function useDataContentPanelListItems() {
  const service = useRoomContentService();
  const searchTerm = useDataContentPanelSearchTerm();
  const isSearchTermSet = useStateIsContentPanelSearchTermSet();
  const rawItems = useSelector(service, (state) => {
    return state.context.items;
  });

  return useMemo(() => {
    if (!isSearchTermSet) return rawItems.map((item) => item.id);
    const filteredItems = rawItems.filter((item) =>
      item.title.toLowerCase().includes(searchTerm.toLowerCase())
    );
    return filteredItems.map((item) => item.id);
  }, [searchTerm, isSearchTermSet, rawItems]);
}
