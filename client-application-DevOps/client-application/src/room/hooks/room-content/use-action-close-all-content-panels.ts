import { useCallback } from "react";
import { useContentPanelWrapperService } from "../../container/content-panel-wrapper-context";

export function useActionCloseAllContentPanels() {
  const service = useContentPanelWrapperService();
  return useCallback(() => service.send({ type: "closeAllContentPanels" }), [service]);
}
