import { useCallback } from "react";
import { RoomContentMachineModel } from "../../machines/room-content-machine";
import { useRoomContentService } from "./use-room-content-service";

export function useActionDeselectAllContentPanelItem() {
  const service = useRoomContentService();
  return useCallback(
    () => service.send(RoomContentMachineModel.events.deselectAllItems()),
    [service]
  );
}
