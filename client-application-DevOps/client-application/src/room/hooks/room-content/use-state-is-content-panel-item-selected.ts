import { useMemo } from "react";
import { useDataContentPanelSelectedItems } from "./use-data-content-panel-selected-items";

export function useStateIsContentPanelItemSelected(itemId: string) {
  const selectedItems = useDataContentPanelSelectedItems();
  return useMemo(() => selectedItems.has(itemId), [selectedItems, itemId]);
}
