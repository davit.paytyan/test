import { useCallback } from "react";
import { useRoomContentService } from "./use-room-content-service";

export function useActionOpenContentPanel() {
  const service = useRoomContentService();
  return useCallback(() => service.send({ type: "openPanel" }), [service]);
}
