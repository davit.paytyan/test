import { ActorRefFrom } from "xstate";
import { useContentPanelService } from "../../container/content-panel-context";
import { RoomContentMachine } from "../../machines/room-content-machine";

export type RoomContentServiceRef = ActorRefFrom<typeof RoomContentMachine>;

export function useRoomContentService() {
  return useContentPanelService();
}
