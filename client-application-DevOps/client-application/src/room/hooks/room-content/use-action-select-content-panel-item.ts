import { useCallback } from "react";
import { RoomContentMachineModel } from "../../machines/room-content-machine";
import { useRoomContentService } from "./use-room-content-service";

export function useActionSelectContentPanelItem() {
  const service = useRoomContentService();
  return useCallback(
    (itemId: string) => service.send(RoomContentMachineModel.events.selectItems([itemId])),
    [service]
  );
}
