import { useCallback } from "react";
import { useRoomContentService } from "./use-room-content-service";

export function useActionCloseContentPanelActions() {
  const service = useRoomContentService();
  return useCallback(() => service.send({ type: "closePanelActions" }), [service]);
}
