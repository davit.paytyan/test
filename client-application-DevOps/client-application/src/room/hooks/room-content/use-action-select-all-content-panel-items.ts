import { useCallback } from "react";
import { RoomContentMachineModel } from "../../machines/room-content-machine";
import { useRoomContentService } from "./use-room-content-service";

export function useActionSelectAllContentPanelItems() {
  const service = useRoomContentService();
  return useCallback(
    () => service.send(RoomContentMachineModel.events.selectAllItems()),
    [service]
  );
}
