import { useSelector } from "@xstate/react";
import { useConferenceContext } from "../../container/conference-context";

export function useRoomUserIsOwnerOrModerator() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    const roleName = state.context.roomUser?.role.name;
    return roleName === "Owner" || roleName === "Moderator";
  });
}
