import { useSelector } from "@xstate/react";
import { useConferenceContext } from "../../container/conference-context";

export function useRoomUserUserId() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.context.roomUser?.userId ?? "";
  });
}
