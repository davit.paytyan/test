import { useSelector } from "@xstate/react";
import { useConferenceContext } from "../../container/conference-context";

export function useRoomUserNickname() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.context.roomUserNickname ?? "";
  });
}
