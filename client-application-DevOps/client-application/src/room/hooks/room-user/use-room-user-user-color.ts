import { useSelector } from "@xstate/react";
import { useConferenceContext } from "../../container/conference-context";

export function useRoomUserUserColor() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.context.roomUser?.userColor ?? "";
  });
}
