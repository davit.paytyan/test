import { useActor } from "@xstate/react";
import { NullActor } from "library/null-actor";
import { useCallback } from "react";
import { useRoomModalsService } from "./use-room-modals-service";

export function useStateCheckIsModalOpen() {
  const service = useRoomModalsService();

  const [state] = useActor(service ?? NullActor);
  return useCallback(
    (modalId: string) => {
      return typeof state !== "undefined" && state.matches(modalId);
    },
    [state]
  );
}
