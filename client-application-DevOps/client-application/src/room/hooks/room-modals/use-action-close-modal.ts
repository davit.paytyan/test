import { useActor } from "@xstate/react";
import { NullActor } from "library/null-actor";
import { useCallback } from "react";
import { useRoomModalsService } from "./use-room-modals-service";

export function useActionCloseModal() {
  const service = useRoomModalsService();

  const [, send] = useActor(service ?? NullActor);
  return useCallback(() => send({ type: "closeModal" }), [send]);
}
