import { useActor } from "@xstate/react";
import { NullActor } from "library/null-actor";
import { useCallback } from "react";
import { useRoomModalsService } from "./use-room-modals-service";

export function useActionOpenModal() {
  const service = useRoomModalsService();

  const [, send] = useActor(service ?? NullActor);
  return useCallback((modalId: string) => send({ type: "openModal", id: modalId }), [send]);
}
