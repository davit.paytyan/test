import { useSelector } from "@xstate/react";
import { InterpreterFrom } from "xstate";
import { useConferenceContext } from "../../container/conference-context";
import { RoomModalsMachine } from "../../machines/room-modals-machine";

export function useRoomModalsService() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.children["room-modals"] as InterpreterFrom<typeof RoomModalsMachine> | undefined;
  });
}
