import { useMemo } from "react";
import { MediaTrack } from "../../library/utils/create-media-track";
import { Participant } from "../machines/actors/room-participants-actor";
import { useJitsiConferenceAudioTracks } from "./jitsi/use-jitsi-conference-audio-tracks";
import { useJitsiConferenceVideoTracks } from "./jitsi/use-jitsi-conference-video-tracks";
import { useDataParticipants } from "./room-participants/use-data-participants";

export type AudienceItem = Participant & {
  videoTrack: MediaTrack | undefined;
  audioTrack: MediaTrack | undefined;
};

export function useAudienceItems() {
  const videoTracks = useJitsiConferenceVideoTracks();
  const audioTracks = useJitsiConferenceAudioTracks();
  const participants = useDataParticipants();

  return useMemo(() => {
    const collection: AudienceItem[] = [];

    for (const participant of participants) {
      if (!participant.conferenceUserId) continue;
      const videoTrack = videoTracks.get(participant.conferenceUserId);
      const audioTrack = audioTracks.get(participant.conferenceUserId);
      const audienceItem = { ...participant, videoTrack, audioTrack };
      collection.push(audienceItem);
    }

    return collection;
  }, [videoTracks, audioTracks, participants]);
}
