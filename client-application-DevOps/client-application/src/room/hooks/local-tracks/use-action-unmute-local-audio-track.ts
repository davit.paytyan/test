import { useCallback } from "react";
import { useLocalTracksService } from "./use-local-tracks-service";

export function useActionUnmuteLocalAudioTrack() {
  const service = useLocalTracksService();
  return useCallback(() => service.send("unmuteLocalAudioTrack"), [service]);
}
