import { useSelector } from "@xstate/react";
import { useConferenceContext } from "../../container/conference-context";

export function useCurrentLocalAudioTrack() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    return state.context.audioTrack;
  });
}
