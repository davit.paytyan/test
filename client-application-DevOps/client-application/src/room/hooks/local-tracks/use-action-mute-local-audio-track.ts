import { useCallback } from "react";
import { useLocalTracksService } from "./use-local-tracks-service";

export function useActionMuteLocalAudioTrack() {
  const service = useLocalTracksService();
  return useCallback(() => service.send("muteLocalAudioTrack"), [service]);
}
