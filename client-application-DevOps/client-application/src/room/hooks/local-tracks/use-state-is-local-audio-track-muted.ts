import { useSelector } from "@xstate/react";
import { useLocalTracksService } from "./use-local-tracks-service";

export function useStateIsLocalAudioTrackMuted() {
  const localTracksService = useLocalTracksService();
  return useSelector(localTracksService, (state) => {
    return state.matches("audioTrack.idle.muted");
  });
}
