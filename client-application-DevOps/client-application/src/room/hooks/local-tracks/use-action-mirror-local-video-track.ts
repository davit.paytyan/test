import { useCallback } from "react";
import { useLocalTracksService } from "./use-local-tracks-service";

export function useActionMirrorLocalVideoTrack() {
  const localTracksService = useLocalTracksService();
  return useCallback(
    (mirrored: boolean) => {
      localTracksService.send({ type: "mirrorLocalVideoTrack", mirrored });
    },
    [localTracksService]
  );
}
