import { useSelector } from "@xstate/react";
import { useLocalTracksService } from "./use-local-tracks-service";

export function useStateIsLocalVideoTrackMirrored() {
  const localTracksService = useLocalTracksService();
  return useSelector(localTracksService, (state) => {
    return state.context.isVideoMirrored ?? false;
  });
}
