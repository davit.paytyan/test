import { useCallback } from "react";
import { useLocalTracksService } from "./use-local-tracks-service";

export function useActionMuteLocalVideoTrack() {
  const service = useLocalTracksService();
  return useCallback(() => service.send("muteLocalVideoTrack"), [service]);
}
