import { useCallback } from "react";
import { useLocalTracksService } from "./use-local-tracks-service";

export function useActionUnmuteLocalVideoTrack() {
  const service = useLocalTracksService();
  return useCallback(() => service.send("unmuteLocalVideoTrack"), [service]);
}
