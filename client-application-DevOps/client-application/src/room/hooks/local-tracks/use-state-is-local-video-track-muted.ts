import { useSelector } from "@xstate/react";
import { useLocalTracksService } from "./use-local-tracks-service";

export function useStateIsLocalVideoTrackMuted() {
  const localTracksService = useLocalTracksService();
  return useSelector(localTracksService, (state) => {
    return state.matches("videoTrack.idle.muted");
  });
}
