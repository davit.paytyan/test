import { useSelector } from "@xstate/react";
import { useConferenceContext } from "../../container/conference-context";

export function useLocalTracksService() {
  const context = useConferenceContext();
  return useSelector(context.conferenceService, (state) => {
    const child = state.context.refs.localTracks;
    if (!child) throw new Error("Access to local-tracks is only possible after initialization");
    return child;
  });
}
