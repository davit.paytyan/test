import { createModel } from "xstate/lib/model";

const initialContext = {};

export const RoomModalsMachineModel = createModel(initialContext, {
  events: {
    openModal: (id: string) => ({ id }),
    closeModal: () => ({}),
  },
});

export const RoomModalsMachine = RoomModalsMachineModel.createMachine({
  id: "room-modals",
  context: RoomModalsMachineModel.initialContext,
  initial: "none",
  states: {
    none: {},
    invite_participants: {},
  },
  on: {
    closeModal: ".none",
    openModal: [
      {
        cond: (context, event) => event.id === "invite_participants",
        target: ".invite_participants",
      },
    ],
  },
});
