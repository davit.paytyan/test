import { assign } from "@xstate/immer";
import { RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { ActorRefFrom, forwardTo, InvokeCreator, spawn } from "xstate";
import { stop } from "xstate/lib/actions";
import { createModel } from "xstate/lib/model";
import { ModelEventsFrom } from "xstate/lib/model.types";
import { Logger } from "../../global/logger";
import {
  Participant,
  ParticipantQuickReaction as QuickReaction,
  RoomParticipantsActor,
} from "./actors/room-participants-actor";
import { ParticipantQuickReactionMachine } from "./participant-quick-reaction-machine";

type MachineContext = {
  participants: Participant[];
  colyseusRoom?: Room<RoomSchema>;
  _quickReactionMachineRefs: Map<string, ActorRefFrom<typeof ParticipantQuickReactionMachine>>;
};

const initialContext: MachineContext = {
  participants: [],
  _quickReactionMachineRefs: new Map(),
};

const ColyseusRoomParticipantsMachineModel = createModel(initialContext, {
  events: {
    // action events
    setColyseusRoom: (room: Room<RoomSchema>) => ({ room }),
    mirrorVideoTrack: (mirrored: boolean) => ({ mirrored }),
    setParticipantConferenceUserId: (conferenceUserId: string) => ({ conferenceUserId }),
    setParticipantQuickReaction: (status: QuickReaction) => ({ status }),
    clearParticipantQuickReaction: () => ({}),
    // participant callbacks
    onUpdateParticipants: (participants: Participant[]) => ({ participants }),
    onParticipantAdded: (participantId: string) => ({ participantId }),
    onParticipantRemoved: (participantId: string) => ({ participantId }),
    onParticipantChanged: (participantId: string) => ({ participantId }),
    // quick reaction callback
    onSetQuickReaction: (participantId: string, quickReaction: QuickReaction) => ({
      participantId,
      quickReaction,
    }),
    onClearQuickReaction: (participantId: string) => ({ participantId }),
  },
});

type Events = ModelEventsFrom<typeof ColyseusRoomParticipantsMachineModel>;

const actorCallback: InvokeCreator<MachineContext, Events> = (context) => {
  let roomSet = false;

  const actor = new RoomParticipantsActor("room-participants");

  function eventListener(event: Events) {
    if (event.type === "setColyseusRoom") {
      if (roomSet) return Logger.warn("colyseus room was already set!");
      actor.send({ type: "onJoinRoomSuccessful", room: event.room });
      roomSet = true;
    }

    if (event.type === "mirrorVideoTrack") {
      actor.send(event);
    }

    if (event.type === "setParticipantConferenceUserId") {
      actor.send(event);
    }

    if (event.type === "setParticipantQuickReaction") {
      actor.send(event);
    }

    if (event.type === "clearParticipantQuickReaction") {
      actor.send(event);
    }
  }

  return (callback, onEvent) => {
    if (context.colyseusRoom) {
      actor.send({ type: "onJoinRoomSuccessful", room: context.colyseusRoom });
    }

    onEvent(eventListener);
    actor.subscribe(callback);

    return () => {
      actor.stop();
    };
  };
};

export const ColyseusRoomParticipantsMachine = ColyseusRoomParticipantsMachineModel.createMachine({
  id: "colyseus-room-participants",
  context: ColyseusRoomParticipantsMachineModel.initialContext,
  invoke: {
    id: "room-participants-actor-callback",
    src: actorCallback,
  },
  on: {
    setColyseusRoom: {
      actions: [
        forwardTo("room-participants-actor-callback"),
        assign((context, event) => (context.colyseusRoom = event.room)),
      ],
    },
    mirrorVideoTrack: {
      actions: forwardTo("room-participants-actor-callback"),
    },
    setParticipantQuickReaction: {
      actions: forwardTo("room-participants-actor-callback"),
    },
    setParticipantConferenceUserId: {
      actions: forwardTo("room-participants-actor-callback"),
    },
    clearParticipantQuickReaction: {
      actions: forwardTo("room-participants-actor-callback"),
    },
    onUpdateParticipants: {
      actions: assign((context, event) => {
        context.participants = event.participants;
      }),
    },
    onParticipantAdded: {
      actions: assign((context, event) => {
        const id = `quickReactionMachine_${event.participantId}`;
        const actor = spawn(ParticipantQuickReactionMachine, { name: id, sync: true });
        context._quickReactionMachineRefs.set(event.participantId, actor);
      }),
    },
    onParticipantRemoved: {
      actions: [
        stop((context, event) => {
          return `quickReactionMachine_${event.participantId}`;
        }),
        assign((context, event) => {
          context._quickReactionMachineRefs.delete(event.participantId);
        }),
      ],
    },
    onSetQuickReaction: {
      actions: (context, event) => {
        context._quickReactionMachineRefs
          .get(event.participantId)
          ?.send({ type: event.type, quickReaction: event.quickReaction });
      },
    },
    onClearQuickReaction: {
      actions: (context, event) => {
        context._quickReactionMachineRefs.get(event.participantId)?.send({ type: event.type });
      },
    },
  },
});
