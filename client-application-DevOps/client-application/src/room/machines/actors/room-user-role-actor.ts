import { RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { AbstractActor } from "library/abstract-actor";
import { Actor } from "xstate";

type UpdateRoomUserRolePayload = { id: string; name: string };

type DispatchedEvents = { type: "onUpdateRoomUserRole"; id: string; name: string };
type Events = { type: "onJoinRoomSuccessful"; room: Room<RoomSchema> };

export class RoomUserRoleActor
  extends AbstractActor<DispatchedEvents, Events>
  implements Actor<DispatchedEvents, Events>
{
  private _room: Room<RoomSchema>;

  protected _send(event: Events) {
    if (event.type === "onJoinRoomSuccessful") {
      this._room = event.room;
      this._room.onMessage("updateRoomUserRole", (payload: UpdateRoomUserRolePayload) => {
        this._notifySubscribers({ type: "onUpdateRoomUserRole", ...payload });
      });
    }
  }
}
