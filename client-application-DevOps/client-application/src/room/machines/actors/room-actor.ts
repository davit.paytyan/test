import { RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { AbstractActor } from "library/abstract-actor";
import { Actor } from "xstate";
import { ServerError } from "../../../library/server-error";

type DispatchedEvents =
  | { type: "onRoomLeft" }
  | { type: "onRoomError"; error: Error }
  | { type: "onRefreshTextboards" };

type Events =
  | { type: "onJoinRoomSuccessful"; room: Room<RoomSchema> }
  | { type: "leaveRoom" }
  | { type: "refreshTextboards" };

export class RoomActor
  extends AbstractActor<DispatchedEvents, Events>
  implements Actor<DispatchedEvents, Events>
{
  private _room: Room<RoomSchema>;

  protected _send(event: Events) {
    if (event.type === "onJoinRoomSuccessful") {
      this._room = event.room;
      this._room.onError((code, message) => this._onError(code, message));
      this._room.onLeave(() => this._onLeave());
      this._room.onMessage("onRefreshTextboards", () => {
        this._notifySubscribers({ type: "onRefreshTextboards" });
      });
    }
    if (event.type === "leaveRoom") this._room.leave(true);

    if (event.type === "refreshTextboards") this._room.send("refreshTextboards");
  }

  private _onError(code, message) {
    const error = new ServerError(code, message);
    error.name = `ERR(${code})`;
    this._notifySubscribers({ type: "onRoomError", error });
    this._sendErrorToSubscribers(error);
  }

  private _onLeave() {
    this._notifySubscribers({ type: "onRoomLeft" });
    this._sendCompleteToSubscribers();
    this._room.removeAllListeners();
  }
}
