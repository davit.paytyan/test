import { RoomSchema, RoomStageItemSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { AbstractActor } from "library/abstract-actor";
import Logger from "loglevel";
import { Actor } from "xstate";

export type RoomStageItem = {
  id: string;
  title?: string;
  type: string;
  ownerId: string;
  sortIndex: number;
  payload: {
    userId?: string;
    conferenceUserId?: string;
    nickname?: string;
    userColor?: string;
    fileId?: string;
  };
};

export type NewRoomStageItem = { id: string; type: string; title?: string; payload?: string };

export type DispatchedEvents =
  | { type: "onUpdateStageItems"; items: RoomStageItem[] }
  | { type: "onStageItemAdded"; itemId: string }
  | { type: "onStageItemRemoved"; itemId: string }
  | { type: "onStageItemChanged"; itemId: string }
  | { type: "onPutItemOnStageCompleted"; itemId: string }
  | { type: "onPutItemOnStageFailed"; itemId: string; reason: string }
  | { type: "onRemoveItemFromStageCompleted"; itemId: string }
  | { type: "onRemoveItemFromStageFailed"; itemId: string; reason: string }
  | { type: "onRenameItemOnStageCompleted"; itemId: string; itemType: string; newItemName: string }
  | { type: "onRenameItemOnStageFailed"; itemId: string; reason: string };

type Events =
  | { type: "onJoinRoomSuccessful"; room: Room<RoomSchema> }
  | { type: "putItemOnStage"; item: NewRoomStageItem }
  | { type: "removeItemFromStage"; id: string }
  | { type: "renameItemOnStage"; item: { id: string; title: string; type: string } };

export class RoomStageActor
  extends AbstractActor<DispatchedEvents, Events>
  implements Actor<DispatchedEvents, Events>
{
  private _room: Room<RoomSchema>;

  public get state() {
    const items: RoomStageItem[] = [];
    for (const item of this._room.state.stage.values()) {
      items.push({
        id: item.id,
        title: item.title,
        type: item.type,
        ownerId: item.ownerId,
        sortIndex: item.sortIndex,
        payload: {
          userId: item.payload.userId,
          conferenceUserId: item.payload.conferenceUserId,
          nickname: item.payload.nickname,
          userColor: item.payload.userColor,
          fileId: item.payload.fileId,
        },
      });
    }

    items.sort((a, b) => {
      if (a.sortIndex > b.sortIndex) return 1;
      if (a.sortIndex < b.sortIndex) return -1;
      return 0;
    });

    return { items };
  }

  public stop() {
    super.stop();
    this._room.state.stage.onAdd = undefined;
    this._room.state.stage.onRemove = undefined;
    for (const item of this._room.state.stage.values()) {
      item.onChange = undefined;
    }
  }

  protected _send(event: Events) {
    if (event.type === "onJoinRoomSuccessful") {
      this._room = event.room;
      this._room.state.stage.onAdd = this._onItemAdded.bind(this);
      this._room.state.stage.onRemove = this._onItemRemoved.bind(this);

      this._room.onMessage("putItemOnStageCompleted", (payload: { id: string }) => {
        this._onPutItemOnStageCompleted(payload.id);
      });

      this._room.onMessage("putItemOnStageFailed", (payload: { id: string; reason: string }) => {
        this._onPutItemOnStageFailed(payload.id, payload.reason);
      });

      this._room.onMessage("removeItemFromStageCompleted", (payload: { id: string }) => {
        this._onRemoveItemFromStageCompleted(payload.id);
      });

      this._room.onMessage(
        "removeItemFromStageFailed",
        (payload: { id: string; reason: string }) => {
          this._onRemoveItemFromStageFailed(payload.id, payload.reason);
        }
      );

      this._room.onMessage(
        "renameItemOnStageCompleted",
        (payload: { id: string; itemType: string; newItemName: string }) => {
          this._onRenameItemOnStageCompleted(payload.id, payload.itemType, payload.newItemName);
        }
      );

      this._room.onMessage("renameItemOnStageFailed", (payload: { id: string; reason: string }) => {
        this._onRenameItemOnStageFailed(payload.id, payload.reason);
      });
    }

    if (event.type === "putItemOnStage") {
      const { type, item } = event;
      Logger.info(`Putting item ${item.id} of type ${item.type} on stage.`);
      this._room.send(type, { ...item });
    }

    if (event.type === "removeItemFromStage") {
      const { type, id } = event;
      Logger.info(`Removing item ${id} from stage.`);
      this._room.send(type, { id });
    }

    if (event.type === "renameItemOnStage") {
      const { type, item } = event;
      Logger.info(`Renaming item ${item.id} on stage.`);
      this._room.send(type, { ...item });
    }
  }

  private _onItemAdded(item: RoomStageItemSchema) {
    // each item will always receive changes from colyseus after they have been added
    item.onChange = (/* changes */) => this._onItemChanged(/* item, changes */);
    // the onStageItemAdded Event is currently nowhere else processed
    // this._notifySubscribers({ type: "onStageItemAdded", itemId: item.id });
  }

  private _onItemRemoved(item: RoomStageItemSchema) {
    item.onChange = undefined;
    this._notifySubscribers({ type: "onUpdateStageItems", items: this.state.items });
    // the onStageItemRemoved Event is currently nowhere else processed
    // this._notifySubscribers({ type: "onStageItemRemoved", itemId: item.id });
  }

  private _onItemChanged(/* item: RoomStageItemSchema, changes: DataChange<RoomStageItemSchema>[] */) {
    this._notifySubscribers({ type: "onUpdateStageItems", items: this.state.items });
    // the onStageItemChanged Event is currently nowhere else processed
    // this._notifySubscribers({ type: "onStageItemChanged", itemId: item.id });
  }

  private _onPutItemOnStageCompleted(itemId: string) {
    Logger.info(`Put item ${itemId} on stage completed.`);
    this._notifySubscribers({ type: "onPutItemOnStageCompleted", itemId });
  }

  private _onPutItemOnStageFailed(itemId: string, reason: string) {
    Logger.warn(`Put item ${itemId} on stage failed. (Reason: ${reason})`);
    this._notifySubscribers({ type: "onPutItemOnStageFailed", itemId, reason });
  }

  private _onRemoveItemFromStageCompleted(itemId: string) {
    Logger.info(`Remove item ${itemId} from stage completed.`);
    this._notifySubscribers({ type: "onRemoveItemFromStageCompleted", itemId });
  }

  private _onRemoveItemFromStageFailed(itemId: string, reason: string) {
    Logger.warn(`Remove item ${itemId} from stage failed. (Reason: ${reason})`);
    this._notifySubscribers({ type: "onRemoveItemFromStageFailed", itemId, reason });
  }

  private _onRenameItemOnStageCompleted(itemId: string, itemType: string, newItemName: string) {
    Logger.info(`Renamed item ${itemId} on stage completed.`);
    this._notifySubscribers({
      type: "onRenameItemOnStageCompleted",
      itemId,
      itemType,
      newItemName,
    });
  }

  private _onRenameItemOnStageFailed(itemId: string, reason: string) {
    Logger.warn(`Renaming item ${itemId} on stage failed. (Reason: ${reason})`);
    this._notifySubscribers({ type: "onRenameItemOnStageFailed", itemId, reason });
  }
}
