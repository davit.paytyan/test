import JitsiConference from "@together/lib-jitsi-meet/JitsiConference";
import JitsiParticipant from "@together/lib-jitsi-meet/JitsiParticipant";
import JitsiRemoteTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiRemoteTrack";
import { AbstractActor } from "library/abstract-actor";
import { createMediaTrack, MediaTrack } from "library/utils/create-media-track";
import Logger from "loglevel";
import { Actor } from "xstate";

type DispatchedEvents =
  | {
      type: "onTrackAdded";
      trackId: string;
      participantId: string;
      track: MediaTrack;
    }
  | {
      type: "onTrackRemoved";
      trackId: string;
      participantId: string;
      trackType: "audio" | "video";
    }
  | {
      type: "onTrackMuteChanged";
      trackId: string;
      participantId: string;
      trackType: "audio" | "video";
      trackMuted: boolean;
    };

type Events = { type: "setConference"; conference: JitsiConference };

export class JitsiConferenceTracksActor
  extends AbstractActor<DispatchedEvents, Events>
  implements Actor<DispatchedEvents, Events>
{
  public get state() {
    const tracks = {};

    for (const participant of this._conference.getParticipants() as JitsiParticipant[]) {
      const tracks = participant.getTracks();
      const audio = tracks.find((track) => track.getType() === "audio");
      const video = tracks.find((track) => track.getType() === "video");
      tracks[participant.getId()] = { audio, video };
    }

    return { tracks };
  }

  private _conference: JitsiConference;

  constructor(id: string, conference: JitsiConference) {
    super(id);

    this._conference = conference;

    this._onTrackAdded = this._onTrackAdded.bind(this);
    this._onTrackRemoved = this._onTrackRemoved.bind(this);
    this._onTrackMuteChanged = this._onTrackMuteChanged.bind(this);

    this._addConferenceEventListener();
  }

  public stop() {
    this._removeConferenceEventListener();
    super.stop();
  }

  public toJSON() {
    const json = { id: this.id, tracks: {} };

    for (const participant of this._conference.getParticipants() as JitsiParticipant[]) {
      const tracks = participant.getTracks();
      const audio = tracks.find((track) => track.getType() === "audio");
      const video = tracks.find((track) => track.getType() === "video");
      json.tracks[participant.getId()] = {
        audio: { id: audio?.getId() },
        video: { id: video?.getId() },
      };
    }

    return json;
  }

  private _addConferenceEventListener() {
    this._conference.addEventListener("conference.trackAdded", this._onTrackAdded);
    this._conference.addEventListener("conference.trackRemoved", this._onTrackRemoved);
    this._conference.addEventListener("conference.trackMuteChanged", this._onTrackMuteChanged);
  }

  private _removeConferenceEventListener() {
    this._conference.removeEventListener("conference.trackAdded", this._onTrackAdded);
    this._conference.removeEventListener("conference.trackRemoved", this._onTrackRemoved);
    this._conference.removeEventListener("conference.trackMuteChanged", this._onTrackMuteChanged);
  }

  private _onTrackAdded(track: JitsiRemoteTrack) {
    Logger.info(`Track (${track.getType()}) ${track.getId()} added.`);
    const mediaTrack = createMediaTrack(track);
    this._notifySubscribers({
      type: "onTrackAdded",
      trackId: track.getId() ?? "fake-track",
      participantId: track.getParticipantId(),
      track: mediaTrack,
    });
  }

  private _onTrackRemoved(track: JitsiRemoteTrack) {
    Logger.info(`Track (${track.getType()}) ${track.getId()} removed.`);
    this._notifySubscribers({
      type: "onTrackRemoved",
      trackId: track.getId() ?? "fake-track",
      participantId: track.getParticipantId(),
      trackType: track.getType() as "audio" | "video",
    });
  }

  private _onTrackMuteChanged(track: JitsiRemoteTrack) {
    Logger.info(`Track (${track.getType()}) ${track.getId()} was muted/unmuted.`);
    this._notifySubscribers({
      type: "onTrackMuteChanged",
      trackId: track.getId() ?? "fake-track",
      participantId: track.getParticipantId(),
      trackType: track.getType() as "audio" | "video",
      trackMuted: track.isMuted(),
    });
  }
}
