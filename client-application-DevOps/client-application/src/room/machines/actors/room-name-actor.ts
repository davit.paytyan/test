import { RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { AbstractActor } from "library/abstract-actor";
import Logger from "loglevel";
import { Actor } from "xstate";

export type DispatchedEvents =
  | { type: "onUpdateRoomName"; name: string }
  | { type: "onSetRoomNameCompleted" }
  | { type: "onSetRoomNameFailed"; reason: string };

type Events =
  | { type: "onJoinRoomSuccessful"; room: Room<RoomSchema> }
  | { type: "setRoomName"; name: string };

export class RoomNameActor
  extends AbstractActor<DispatchedEvents, Events>
  implements Actor<DispatchedEvents, Events>
{
  private _room: Room<RoomSchema>;

  protected _send(event: Events) {
    if (event.type === "onJoinRoomSuccessful") {
      this._room = event.room;
      this._room.state.listen("roomName", (name: string) => {
        this._notifySubscribers({ type: "onUpdateRoomName", name });
      });

      this._room.onMessage("setRoomNameCompleted", () => {
        this._onSetRoomNameCompleted();
      });

      this._room.onMessage("setRoomNameFailed", (payload: { reason: string }) => {
        this._onSetRoomNameFailed(payload.reason);
      });
    }
    if (event.type === "setRoomName") {
      Logger.info(`Changing room stage name to ${event.name}.`);
      this._room.send(event.type, { name: event.name ?? "" });
    }
  }

  public get state() {
    return { title: this._room.state.roomName };
  }

  private _onSetRoomNameCompleted() {
    Logger.info(`Change room title completed.`);
    this._notifySubscribers({ type: "onSetRoomNameCompleted" });
  }

  private _onSetRoomNameFailed(reason: string) {
    Logger.warn(`Change room title failed. (Reason: ${reason})`);
    this._notifySubscribers({ type: "onSetRoomNameFailed", reason });
  }
}
