import { RoomParticipantSchema, RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { AbstractActor } from "library/abstract-actor";
import Logger from "loglevel";
import { Actor } from "xstate";

export type ParticipantQuickReaction = {
  type: string;
  value: string;
  createdOn?: number;
};

export type Participant = {
  userId: string;
  conferenceUserId?: string;
  nickname: string;
  userColor: string;
  userRoleId: string;
  userRoleName: string;
  sortIndex: number;
  isOnStage: boolean;
  isVideoMirrored: boolean;
  quickReaction?: ParticipantQuickReaction;
};

export type DispatchedEvents =
  | { type: "onUpdateParticipants"; participants: Participant[] }
  | { type: "onParticipantAdded"; participantId: string }
  | { type: "onParticipantRemoved"; participantId: string }
  | { type: "onParticipantChanged"; participantId: string }
  | {
      type: "onSetQuickReaction";
      participantId: string;
      quickReaction: ParticipantQuickReaction;
    }
  | { type: "onClearQuickReaction"; participantId: string };

type Events =
  | { type: "onJoinRoomSuccessful"; room: Room<RoomSchema> }
  | { type: "mirrorVideoTrack"; mirrored: boolean }
  | { type: "setParticipantQuickReaction"; status: { type: string; value: string } }
  | { type: "setParticipantConferenceUserId"; conferenceUserId: string }
  | { type: "clearParticipantQuickReaction" };

export class RoomParticipantsActor
  extends AbstractActor<DispatchedEvents, Events>
  implements Actor<DispatchedEvents, Events>
{
  private _room: Room<RoomSchema>;

  constructor(id: string) {
    super(id);
    this._onParticipantJoined = this._onParticipantJoined.bind(this);
    this._onParticipantLeft = this._onParticipantLeft.bind(this);
    this._onQuickReactionChanged = this._onQuickReactionChanged.bind(this);
  }

  protected _send(event: Events) {
    if (event.type === "onJoinRoomSuccessful") {
      this._room = event.room;
      this._room.state.participants.onAdd = this._onItemAdded.bind(this);
      this._room.state.participants.onRemove = this._onItemRemoved.bind(this);
      this._room.onMessage("participantJoined", this._onParticipantJoined);
      this._room.onMessage("participantLeft", this._onParticipantLeft);
    }

    if (event.type === "setParticipantQuickReaction") {
      this._room.send("setParticipantQuickReaction", event.status);
    }

    if (event.type === "clearParticipantQuickReaction") {
      this._room.send("clearParticipantQuickReaction");
    }

    if (event.type === "setParticipantConferenceUserId") {
      this._room.send("setParticipantConferenceUserId", {
        conferenceUserId: event.conferenceUserId,
      });
    }

    if (event.type === "mirrorVideoTrack") {
      this._room.send("mirrorVideoTrack", { mirrored: event.mirrored });
    }
  }

  public stop() {
    super.stop();
    this._room.state.participants.onAdd = undefined;
    this._room.state.participants.onRemove = undefined;
    for (const item of this._room.state.participants.values()) {
      item.onChange = undefined;
    }
  }

  public get state() {
    const participants: Participant[] = [];
    for (const participant of this._room.state.participants.values()) {
      participants.push(getParticipantObject(participant));
    }

    participants.sort((a, b) => {
      if (a.sortIndex > b.sortIndex) return 1;
      if (a.sortIndex < b.sortIndex) return -1;
      return 0;
    });

    return { participants };
  }

  private _onItemAdded(participant: RoomParticipantSchema) {
    participant.onChange = () => this._onItemChanged(participant);
    participant.listen("quickReaction", () => this._onQuickReactionChanged(participant));
    participant.listen("conferenceUserId", () => {
      Logger.log("conference user id changed to", participant.conferenceUserId);
    });
    this._notifySubscribers({ type: "onParticipantAdded", participantId: participant.userId });
    this._notifySubscribers({
      type: "onUpdateParticipants",
      participants: this.state.participants,
    });
  }

  private _onItemRemoved(participant: RoomParticipantSchema) {
    participant.onChange = undefined;
    this._notifySubscribers({ type: "onParticipantRemoved", participantId: participant.userId });
    this._notifySubscribers({
      type: "onUpdateParticipants",
      participants: this.state.participants,
    });
  }

  private _onItemChanged(participant: RoomParticipantSchema) {
    this._notifySubscribers({ type: "onParticipantChanged", participantId: participant.userId });
    this._notifySubscribers({
      type: "onUpdateParticipants",
      participants: this.state.participants,
    });
  }

  private _onParticipantJoined({ userId }: { userId: string }) {
    Logger.info(`Participant ${userId} joined.`);
  }

  private _onParticipantLeft({ userId }: { userId: string }) {
    Logger.info(`Participant ${userId} left.`);
  }

  private _onQuickReactionChanged(participant: RoomParticipantSchema) {
    const quickReaction = getParticipantQuickReactionObject(participant);
    const { userId: participantId } = participant;
    if (quickReaction) {
      this._notifySubscribers({ type: "onSetQuickReaction", participantId, quickReaction });
    } else {
      this._notifySubscribers({ type: "onClearQuickReaction", participantId });
    }
  }
}

function getParticipantQuickReactionObject(participant: RoomParticipantSchema) {
  return !participant.quickReaction
    ? undefined
    : {
        type: participant.quickReaction.type,
        value: participant.quickReaction.value,
        createdOn: participant.quickReaction.createdOn,
      };
}

function getParticipantObject(participant: RoomParticipantSchema): Participant {
  return {
    userId: participant.userId,
    conferenceUserId: participant.conferenceUserId,
    nickname: participant.nickname,
    userColor: participant.userColor,
    userRoleId: participant.userRoleId,
    userRoleName: participant.userRoleName,
    isOnStage: participant.isOnStage,
    isVideoMirrored: participant.isVideoMirrored,
    sortIndex: participant.sortIndex,
    quickReaction: getParticipantQuickReactionObject(participant),
  };
}
