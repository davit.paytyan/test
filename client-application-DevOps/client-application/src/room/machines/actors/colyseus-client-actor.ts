import { Client, Room } from "colyseus.js";
import { Actor } from "xstate";
import { AbstractActor } from "library/abstract-actor";
import Logger from "loglevel";
import { RoomSchema } from "colyseus-schema";
import { ServerError } from "library/server-error";

export type DispatchedEvents =
  | { type: "onJoinRoomSuccessful"; room: Room<RoomSchema> }
  | { type: "onJoinRoomFailed"; error: Error | ServerError | Response };

type Events = { type: "joinRoom"; endpoint: string; roomId: string; userId: string };
export class ColyseusClientActor
  extends AbstractActor<DispatchedEvents, Events>
  implements Actor<DispatchedEvents, Events>
{
  protected async _send(event: Events) {
    if (event.type === "joinRoom") {
      const [room, error] = await this._joinRoom(event.endpoint, event.roomId, event.userId);
      if (room) this._notifySubscribers({ type: "onJoinRoomSuccessful", room });
      if (error) this._notifySubscribers({ type: "onJoinRoomFailed", error });
    }
  }

  private async _joinRoom(
    endpoint: string,
    roomId: string,
    userId: string
  ): Promise<[Room<RoomSchema> | null, Error | null]> {
    const client = new Client(endpoint);
    let room: Room<RoomSchema> | null = null;
    let error: Error | null = null;
    try {
      room = await client.joinById(roomId, { userId });
      if (!room) throw new Error("Missing Room!");
      return [room, null];
    } catch (_error) {
      error = _error;
      Logger.warn(`Room ${roomId} is not available.`);
    }

    try {
      room = await client.create("together_room", { roomId, userId });
      if (!room) throw new Error("Missing Room!");
      Logger.info(`Created and joined room ${roomId}.`);
      return [room, null];
    } catch (_error) {
      error = _error;
      Logger.warn(`Could not create and join room ${roomId}!`);
    }
    return [null, error];
  }
}
