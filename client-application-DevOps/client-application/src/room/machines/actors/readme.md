# What are actors?

Actors could be seen as redundant because they can be implemented as invoked callbacks as well.
But they are a good way to encapsulate complex behavior into a streamlined and observable
interface which then could be easily used inside the invoked callbacks. 

Since they are independent from the xstate machines, they can also be used by themselves to build
complex application structures.

https://xstate.js.org/docs/guides/actors.html#actor-api