import JitsiConnection from "@together/lib-jitsi-meet/JitsiConnection";
import { AbstractActor } from "library/abstract-actor";
import Logger from "loglevel";
import { Actor } from "xstate";

export type JitsiConnectionCredentials = {
  jid: string;
  password?: string | undefined;
};

export type DispatchedEvents =
  | { type: "onConnectionEstablished"; connectionId: string; connection: JitsiConnection }
  | { type: "onConnectionDisconnected"; message: string }
  | {
      type: "onConnectionFailed";
      errorType: string;
      errorReason: string;
      // credentials?: JitsiConnectionCredentials;
      // errorReasonDetails?: Record<string, unknown> | undefined;
    }
  | { type: "onConnectionDisplayNameRequired" }
  | { type: "onConnectionWrongState" };

type Events = { type: "connect"; password: string } | { type: "disconnect" };

export class JitsiConnectionActor
  extends AbstractActor<DispatchedEvents, Events>
  implements Actor<DispatchedEvents, Events>
{
  private _connection: JitsiConnection;

  public get state() {
    return {
      appId: this._connection.appID,
      token: this._connection.token,
      options: this._connection.options,
      jid: this._connection.getJid(),
      connection: this._connection,
    };
  }

  constructor(id: string, connection: JitsiConnection) {
    super(id);

    this._connection = connection;

    this._onConnectionEstablished = this._onConnectionEstablished.bind(this);
    this._onConnectionDisconnected = this._onConnectionDisconnected.bind(this);
    this._onConnectionFailed = this._onConnectionFailed.bind(this);
    this._onConnectionDisplayNameRequired = this._onConnectionDisplayNameRequired.bind(this);
    this._onConnectionWrongState = this._onConnectionWrongState.bind(this);

    this._addConnectionEventHandler();
  }

  protected async _send(event: Events) {
    switch (event.type) {
      case "connect":
        this._connection.connect({ password: event.password ?? "" });
        break;
      case "disconnect":
        this._connection.disconnect();
        break;
    }
  }

  public stop() {
    this._removeConnectionEventHandler();
    super.stop();
  }

  public toJSON() {
    return {
      id: this.id,
      appId: this._connection.appID,
      token: this._connection.token,
      options: this._connection.options,
      jid: this._connection.getJid(),
    };
  }

  private _onConnectionEstablished(connectionId: string) {
    Logger.info(`Connected to Server. (ConenctionId: ${connectionId})`);
    this._notifySubscribers({
      type: "onConnectionEstablished",
      connectionId,
      connection: this._connection,
    });
  }

  private _onConnectionDisconnected(message = "") {
    Logger.info(`Disconnected from Server. (Reason: ${message})`);
    this._notifySubscribers({ type: "onConnectionDisconnected", message });
  }

  private _onConnectionFailed(
    errorType: string,
    errorReason: string
    // credentials?: JitsiConnectionCredentials,
    // errorReasonDetails?: Record<string, unknown> | undefined
  ) {
    Logger.warn(`Connection to Server failed. (${errorType}: ${errorReason})`);
    this._notifySubscribers({
      type: "onConnectionFailed",
      errorType,
      errorReason,
      // credentials,
      // errorReasonDetails,
    });
  }

  private _onConnectionDisplayNameRequired() {
    Logger.warn(`Connection requires display name!`);
    this._notifySubscribers({ type: "onConnectionDisplayNameRequired" });
  }

  private _onConnectionWrongState() {
    Logger.warn(`Connection in wrong state!`);
    this._notifySubscribers({ type: "onConnectionWrongState" });
  }

  private _addConnectionEventHandler() {
    // rooms don't have a function to unsubscribe to particular events
    this._connection.addEventListener(
      "connection.connectionEstablished",
      this._onConnectionEstablished
    );
    this._connection.addEventListener(
      "connection.connectionDisconnected",
      this._onConnectionDisconnected
    );
    this._connection.addEventListener("connection.connectionFailed", this._onConnectionFailed);
    this._connection.addEventListener(
      "connection.display_name_required",
      this._onConnectionDisplayNameRequired
    );
    this._connection.addEventListener("connection.wrongState", this._onConnectionWrongState);
  }

  private _removeConnectionEventHandler() {
    this._connection.removeEventListener(
      "connection.connectionEstablished",
      this._onConnectionEstablished
    );
    this._connection.removeEventListener(
      "connection.connectionDisconnected",
      this._onConnectionDisconnected
    );
    this._connection.removeEventListener("connection.connectionFailed", this._onConnectionFailed);
    this._connection.removeEventListener(
      "connection.display_name_required",
      this._onConnectionDisplayNameRequired
    );
    this._connection.removeEventListener("connection.wrongState", this._onConnectionWrongState);
  }
}
