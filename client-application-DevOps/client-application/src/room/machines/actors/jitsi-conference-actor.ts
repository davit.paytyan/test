import JitsiMeetJS from "@together/lib-jitsi-meet";
import JitsiConference from "@together/lib-jitsi-meet/JitsiConference";
import { AbstractActor } from "library/abstract-actor";
import Logger from "loglevel";
import { Actor, ActorRef } from "xstate";

export type ConferenceErrorReason = JitsiMeetJS["errors"]["conference"]["CHAT_ERROR"];
export type ConferenceFailedReason =
  JitsiMeetJS["errors"]["conference"][keyof JitsiMeetJS["errors"]["conference"]];

type DispatchedEvents =
  | { type: "onConferenceJoined"; conference: JitsiConference }
  | { type: "onConferenceLeft"; conference: JitsiConference }
  | { type: "onConferenceKicked"; conference: JitsiConference }
  | { type: "onConferenceError"; conference: JitsiConference; reason: ConferenceErrorReason }
  | { type: "onConferenceFailed"; conference: JitsiConference; reason: ConferenceFailedReason };

type Events =
  | { type: "joinConference"; password?: string }
  | { type: "leaveConference" }
  | {
      type: "kickParticipant";
      participantId: string;
      reason?: string;
    };

export type JitsiConferenceActorRef = ActorRef<Events, DispatchedEvents>;

export class JitsiConferenceActor
  extends AbstractActor<DispatchedEvents, Events>
  implements Actor<DispatchedEvents, Events>
{
  public get state() {
    return {
      conference: this._conference,
    };
  }

  private _conference: JitsiConference;

  constructor(id: string, conference: JitsiConference) {
    super(id);

    this._conference = conference;

    this._onConferenceJoined = this._onConferenceJoined.bind(this);
    this._onConferenceLeft = this._onConferenceLeft.bind(this);
    this._onConferenceKicked = this._onConferenceKicked.bind(this);
    this._onConferenceError = this._onConferenceError.bind(this);
    this._onConferenceFailed = this._onConferenceFailed.bind(this);
    this._onWindowUnload = this._onWindowUnload.bind(this);

    this._addConferenceEventListener();
  }

  protected _send(event: Events) {
    // noop
    switch (event.type) {
      case "joinConference":
        this._conference.join(event.password ?? "");
        break;

      case "kickParticipant":
        this._conference.kickParticipant(event.participantId, event.reason ?? "unspecified");
        break;

      case "leaveConference":
        this._conference.leave();
        break;
    }
  }

  public stop() {
    this._removeConferenceEventListener();
    super.stop();
  }

  private _addConferenceEventListener() {
    this._conference.addEventListener("conference.joined", this._onConferenceJoined);
    this._conference.addEventListener("conference.left", this._onConferenceLeft);
    this._conference.addEventListener("conference.kicked", this._onConferenceKicked);
    this._conference.addEventListener("conference.error", this._onConferenceError);
    this._conference.addEventListener("conference.failed", this._onConferenceFailed);
    window.addEventListener("unload", this._onWindowUnload);
  }

  private _removeConferenceEventListener() {
    this._conference.removeEventListener("conference.joined", this._onConferenceJoined);
    this._conference.removeEventListener("conference.left", this._onConferenceLeft);
    this._conference.removeEventListener("conference.kicked", this._onConferenceKicked);
    this._conference.removeEventListener("conference.error", this._onConferenceError);
    this._conference.removeEventListener("conference.failed", this._onConferenceFailed);
    window.removeEventListener("unload", this._onWindowUnload);
  }

  private _onConferenceJoined() {
    Logger.info(`Conference ${this._conference.getName()} joined.`);
    this._notifySubscribers({ type: "onConferenceJoined", conference: this._conference });
  }

  private _onConferenceLeft() {
    Logger.info(`Conference ${this._conference.getName()} left.`);
    this._notifySubscribers({ type: "onConferenceLeft", conference: this._conference });
  }

  private _onConferenceKicked() {
    Logger.info(`Kicked out of conference ${this._conference.getName()}.`);
    this._notifySubscribers({ type: "onConferenceKicked", conference: this._conference });
  }

  private _onConferenceError(reason: ConferenceErrorReason) {
    Logger.warn(`Conference: Error with reason "${reason}".`);
    this._notifySubscribers({ type: "onConferenceError", conference: this._conference, reason });
  }

  private _onConferenceFailed(reason: ConferenceErrorReason) {
    Logger.warn(`Conference: Failed with reason "${reason}".`);
    this._notifySubscribers({ type: "onConferenceFailed", conference: this._conference, reason });
  }

  private _onWindowUnload() {
    // TODO: put in central place
    this.send({ type: "leaveConference" });
  }
}
