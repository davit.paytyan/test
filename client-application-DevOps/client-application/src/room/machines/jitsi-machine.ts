import JitsiConference, {
  JitsiConferenceOptionConfig,
} from "@together/lib-jitsi-meet/JitsiConference";
import JitsiConnection, { JitsiConnectionOptions } from "@together/lib-jitsi-meet/JitsiConnection";
import { assign } from "@xstate/immer";
import { WritableDraft } from "immer/dist/internal";
import { ActorRefFrom, forwardTo, send, sendParent, spawn } from "xstate";
import { createModel } from "xstate/lib/model";
import { MediaTrack } from "../../library/utils/create-media-track";
import {
  JitsiConferenceMachine,
  JitsiConferenceMachineModelCreator,
} from "./jitsi-conference-machine";
import { JitsiConferenceReceiverConstraintsMachine } from "./jitsi-conference-receiver-constraints-machine";
import {
  JitsiConnectionMachine,
  JitsiConnectionMachineModelCreator,
} from "./jitsi-connection-machine";
import {
  JitsiConferenceTracksMachine,
  JitsiConferenceTracksMachineModelCreator,
} from "./jitsi-tracks-machine";
import { CreateJitsiConferenceMachine } from "./tasks/create-jitsi-conference-machine";
import { CreateJitsiConnectionMachine } from "./tasks/create-jitsi-connection-machine";

type MachineContext = {
  roomId: string;
  connectionConfig: {
    appId?: string;
    token?: string;
    options: Partial<JitsiConnectionOptions>;
  };
  conferenceConfig: {
    options: JitsiConferenceOptionConfig;
  };
  connection?: JitsiConnection;
  connectionRef?: ActorRefFrom<typeof JitsiConnectionMachine>;
  conference?: JitsiConference;
  conferenceRef?: ActorRefFrom<typeof JitsiConferenceMachine>;
  conferenceTracksRef?: ActorRefFrom<typeof JitsiConferenceTracksMachine>;
  conferenceReceiverQualityRef?: ActorRefFrom<typeof JitsiConferenceReceiverConstraintsMachine>;
  audioTracks: Map<string, MediaTrack>;
  videoTracks: Map<string, MediaTrack>;
  error?: Error;
};

const initialContext: MachineContext = {
  roomId: "",
  connectionConfig: { options: {} },
  conferenceConfig: { options: {} },
  audioTracks: new Map(),
  videoTracks: new Map(),
};

export const JitsiMachineModel = createModel(initialContext, {
  events: {
    ...JitsiConnectionMachineModelCreator.events,
    ...JitsiConferenceMachineModelCreator.events,
    ...JitsiConferenceTracksMachineModelCreator.events,
    updateParticipantsOnStage: (participantIds: string[], itemsOnStage: number) => ({
      participantIds,
      itemsOnStage,
    }),
  },
});

export const JitsiMachine = JitsiMachineModel.createMachine({
  id: "jitsi-conference",
  context: JitsiMachineModel.initialContext,
  initial: "initializeConnection",
  states: {
    initializeConnection: {
      invoke: {
        id: "create-jitsi-connection",
        src: CreateJitsiConnectionMachine,
        data: {
          appId: (context) => context.connectionConfig.appId,
          token: (context) => context.connectionConfig.token,
          options: (context) => context.connectionConfig.options,
        },
        onDone: {
          target: "connecting",
          // actions: assign({
          //   connection: (context, event) => event.data.jitsiConnection,
          //   connectionRef: (context, event) => {
          //     const machineConfig = JitsiConnectionMachine.withContext({
          //       connection: event.data.jitsiConnection,
          //     });
          //     return spawn(machineConfig, "jitsi-connection");
          //   },
          // }),
          actions: assign((context, event) => {
            context.connection = event.data.jitsiConnection;
            const machineConfig = JitsiConnectionMachine.withContext({
              connection: event.data.jitsiConnection,
            });
            context.connectionRef = spawn(machineConfig, "jitsi-connection");
          }),
        },
        onError: {
          target: "error",
          // actions: assign({
          //   error: (context, event) => event.data.error,
          // }),
          actions: assign((context, event) => {
            context.error = event.data.error;
          }),
        },
      },
    },
    connecting: {
      entry: (context) => context.connectionRef?.send({ type: "connect", password: "" }),
      invoke: {
        id: "create-jitsi-conference",
        src: CreateJitsiConferenceMachine,
        data: {
          roomName: (context: MachineContext) => context.roomId,
          options: (context: MachineContext) => context.conferenceConfig.options,
        },
        onDone: {
          target: "connected",
          // actions: assign({
          //   conference: (context, event) => event.data.jitsiConference,
          //   conferenceRef: (context, event) => {
          //     const machineConfig = JitsiConferenceMachine.withContext({
          //       conference: event.data.jitsiConference,
          //     });
          //     return spawn(machineConfig, "jitsi-conference");
          //   },
          //   conferenceTracksRef: (context, event) => {
          //     const machineConfig = JitsiConferenceTracksMachine.withContext({
          //       conference: event.data.jitsiConference,
          //     });
          //     return spawn(machineConfig, "jitsi-conference-tracks");
          //   },
          // }),
          actions: assign((context, event) => {
            context.conference = event.data.jitsiConference;
            const conferenceMachineConfig = JitsiConferenceMachine.withContext({
              conference: event.data.jitsiConference,
            });
            const tracksMachineConfig = JitsiConferenceTracksMachine.withContext({
              conference: event.data.jitsiConference,
            });
            context.conferenceRef = spawn(conferenceMachineConfig, "jitsi-conference");
            context.conferenceTracksRef = spawn(tracksMachineConfig, "jitsi-conference-tracks");
          }),
        },
      },
      on: {
        onConnectionEstablished: {
          actions: send((context) => ({ type: "setConnection", connection: context.connection }), {
            to: "create-jitsi-conference",
          }),
        },
        onConnectionFailed: {
          target: "error",
          // actions: assign({
          //   error: (context, event) => new Error(event.errorReason),
          // }),
          actions: assign((context, event) => {
            context.error = new Error(event.errorReason);
          }),
        },
      },
    },
    connected: {
      initial: "idle",
      states: {
        idle: {
          on: {
            joinConference: {
              target: "joiningConference",
              actions: forwardTo("jitsi-conference"),
            },
          },
        },
        joiningConference: {
          on: {
            onConferenceJoined: {
              target: "joinedConference",
              actions: sendParent((context, event) => {
                return event;
              }),
            },
          },
        },
        joinedConference: {
          entry: [
            // assign({
            //   conferenceReceiverQualityRef: (context) => {
            //     const machineConfig = JitsiConferenceReceiverConstraintsMachine.withContext({
            //       ...JitsiConferenceReceiverConstraintsMachine.context,
            //       conference: context.conference,
            //     });
            //     return spawn(machineConfig, "receiver-constraints");
            //   },
            // }),
            assign((context) => {
              const machineConfig = JitsiConferenceReceiverConstraintsMachine.withContext({
                ...JitsiConferenceReceiverConstraintsMachine.context,
                conference: context.conference,
              });
              context.conferenceReceiverQualityRef = spawn(machineConfig, "receiver-constraints");
            }),
          ],
          on: {
            kickParticipant: {
              actions: forwardTo("jitsi-conference"),
            },
            updateParticipantsOnStage: {
              actions: (context, event) => {
                context.conferenceReceiverQualityRef?.send(event);
              },
            },
            onTrackAdded: {
              actions: assign((context, event) => {
                switch (event.track.type) {
                  case "audio":
                    context.audioTracks.set(event.participantId, event.track);
                    break;
                  case "video":
                    context.videoTracks.set(event.participantId, event.track);
                    break;
                }
                return context;
              }),
            },
            onTrackRemoved: {
              actions: assign((context, event) => {
                switch (event.trackType) {
                  case "audio":
                    context.audioTracks.delete(event.participantId);
                    break;
                  case "video":
                    context.videoTracks.delete(event.participantId);
                    break;
                }
                return context;
              }),
            },
            onTrackMuteChanged: {
              actions: assign((context, event) => {
                let track: WritableDraft<MediaTrack> | undefined;
                switch (event.trackType) {
                  case "audio":
                    track = context.audioTracks.get(event.participantId);
                    break;
                  case "video":
                    track = context.videoTracks.get(event.participantId);
                    break;
                }
                if (track) track.isMuted = event.trackMuted;
                return context;
              }),
            },
          },
        },
        leavingConference: {},
        leftConference: {
          type: "final",
        },
      },
      on: {
        onConferenceLeft: ".leftConference",
        onConferenceFailed: {
          // actions: assign({
          //   error: (context, event) => new Error(event.reason),
          // }),
          actions: assign((context, event) => {
            context.error = new Error(event.reason);
          }),
        },
        onConferenceError: {
          // actions: assign({
          //   error: (context, event) => new Error(event.reason),
          // }),
          actions: assign((context, event) => {
            context.error = new Error(event.reason);
          }),
        },
      },
    },
    error: {
      type: "final",
      data: {
        error: (context) => context.error,
      },
    },
  },
});
