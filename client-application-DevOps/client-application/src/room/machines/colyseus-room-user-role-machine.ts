import { assign } from "@xstate/immer";
import { RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { forwardTo, InvokeCreator, sendParent } from "xstate";
import { createModel } from "xstate/lib/model";
import { ModelEventsFrom } from "xstate/lib/model.types";
import { Logger } from "../../global/logger";
import { RoomUserRoleActor } from "./actors/room-user-role-actor";

type MachineContext = {
  id: string;
  name: string;
  colyseusRoom?: Room<RoomSchema>;
};

const ColyseusRoomUserRoleMachineModel = createModel(
  {
    id: "",
    name: "",
  } as MachineContext,
  {
    events: {
      setColyseusRoom: (room: Room<RoomSchema>) => ({ room }),
      onUpdateRoomUserRole: (id: string, name: string) => ({ id, name }),
    },
  }
);

type Events = ModelEventsFrom<typeof ColyseusRoomUserRoleMachineModel>;

const actorCallback: InvokeCreator<MachineContext, Events> = (context) => {
  const actor = new RoomUserRoleActor("room-user-role");
  let roomSet = false;
  function eventListener(event: Events) {
    if (event.type === "setColyseusRoom") {
      if (roomSet) return Logger.warn("colyseus room was already set!");
      actor.send({ type: "onJoinRoomSuccessful", room: event.room });
      roomSet = true;
    }
  }

  return (callback, onEvent) => {
    if (context.colyseusRoom) {
      actor.send({ type: "onJoinRoomSuccessful", room: context.colyseusRoom });
    } else {
      onEvent(eventListener);
    }

    actor.subscribe(callback);

    return () => {
      actor.stop();
    };
  };
};

export const ColyseusRoomUserRoleMachine = ColyseusRoomUserRoleMachineModel.createMachine({
  id: "colyseus-room-user-role",
  context: ColyseusRoomUserRoleMachineModel.initialContext,
  initial: "_checkRole",
  invoke: {
    id: "room-user-role-actor-callback",
    src: actorCallback,
  },
  states: {
    _checkRole: {
      always: [
        {
          cond: (context) => context.name.toLowerCase() === "owner",
          target: "owner",
        },
        {
          cond: (context) => context.name.toLowerCase() === "moderator",
          target: "owner",
        },
        {
          target: "participant",
        },
      ],
    },
    owner: {},
    moderator: {},
    participant: {},
  },
  on: {
    setColyseusRoom: {
      actions: [
        forwardTo("room-user-role-actor-callback"),
        assign((context, event) => (context.colyseusRoom = event.room)),
      ],
    },
    onUpdateRoomUserRole: {
      target: "_checkRole",
      internal: true,
      actions: [
        sendParent((context, event) => event),
        assign((context, event) => {
          context.id = event.id;
          context.name = event.name;
        }),
      ],
    },
  },
});
