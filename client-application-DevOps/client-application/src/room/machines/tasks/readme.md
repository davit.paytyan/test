# What are tasks?

Tasks are state machines that will run automatically until they will come to a final state.
Each task has a success and an error state.

The results are provided through the `data` attribute on the final states, so parten machines will
get those data through `onDone` events.

Any occuring error during a task, that can't be handeled must result in the termination of the task
in the final error state. The error is provided via the data attribute on the error state as well.

## Example

```js
const machine = createMachine({
  id: "example-task",
  context: {},
  initial: "exectuingTask",
  states: {
    executingTask: {
      invoke: {
        src: (context, event) => {
          const number = Math.random();
          if (number < .5) return Promise.resolve(number)
          return Promise.reject(number);
        },
        onDone: {
          target: "success",
          actions: /** @xstate/immer */assign((context, event) => {
            context.number = event.data;
          })
        },
        onError: {
          target: "failed",
          actions: /** @xstate/immer */assign((context, event) => {
            context.error = new Error("number is greater than .5");
          })
        }
      }
    },
    success: {
      type: "final",
      data: {
        number: (context) => context.number
      }
    },
    failed: {
      type: "final",
      data: {
        error: (context) => context.error
      }
    }
  }
});
```