import JitsiConference from "@together/lib-jitsi-meet/JitsiConference";
import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
import { createMachine, assign } from "xstate";

type MachineContext = {
  conference: JitsiConference;
  track: JitsiLocalTrack;
  error?: Error;
};

export const ChangeLocalTrackMachine = createMachine<MachineContext>({
  id: "change-local-track",
  initial: "changing",
  states: {
    changing: {
      invoke: {
        src: (context) => {
          const changeTrack = async () => {
            if (context.track.getType() === "audio") {
              const oldTrack = context.conference.getLocalAudioTrack();
              if (oldTrack) await context.conference.removeTrack(oldTrack);
            } else if (context.track.getType() === "video") {
              const oldTrack = context.conference.getLocalVideoTrack();
              if (oldTrack) await context.conference.removeTrack(oldTrack);
            }
            await context.conference.addTrack(context.track);
          };
          return changeTrack();
        },
        onDone: {
          target: "complete",
        },
        onError: {
          target: "failed",
          actions: assign({
            error: (context, event) => event.data,
          }),
        },
      },
    },
    complete: {
      type: "final",
      data: {
        track: (context) => context.track,
      },
    },
    failed: {
      type: "final",
      data: {
        error: (context) => context.error,
      },
    },
  },
});
