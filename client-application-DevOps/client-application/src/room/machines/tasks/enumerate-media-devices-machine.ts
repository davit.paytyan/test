import { assign } from "@xstate/immer";
import { createMachine } from "xstate";

type MachineContext = {
  devices: MediaDeviceInfo[];
  error?: Error;
};

export const EnumerateMediaDevicesMachine = createMachine<MachineContext>({
  id: "enumerate-media-devices",
  initial: "enumerating",
  context: {
    devices: [],
  },
  states: {
    enumerating: {
      invoke: {
        src: () => {
          return new Promise((resolve) => {
            if (!window.JitsiMeetJS) throw new Error("LibJitsiMeet is required!");
            window.JitsiMeetJS.mediaDevices.enumerateDevices(resolve);
          });
        },
        onDone: {
          target: "complete",
          actions: assign((context, event) => {
            context.devices = event.data;
          }),
        },
        onError: {
          target: "failed",
          actions: assign((context, event) => {
            context.error = event.data;
          }),
        },
      },
    },
    complete: {
      type: "final",
      data: {
        devices: (context) => context.devices,
      },
    },
    failed: {
      type: "final",
      data: {
        error: (context) => context.error,
      },
    },
  },
});
