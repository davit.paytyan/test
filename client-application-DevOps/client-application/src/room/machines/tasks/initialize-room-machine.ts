import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
import { assign } from "@xstate/immer";
import { createMachine } from "xstate";
import { CreateLocalTracksMachine } from "./create-local-tracks-machine";
import { EnumerateMediaDevicesMachine } from "./enumerate-media-devices-machine";

type MachineContext = {
  devices?: MediaDeviceInfo[];
  audioTrack?: JitsiLocalTrack;
  videoTrack?: JitsiLocalTrack;
  error?: Error;
};

export const InitializeRoomMachine = createMachine<MachineContext>({
  id: "initialize-room",
  initial: "initializeLocalTracks",
  context: {},
  states: {
    initializeLocalTracks: {
      invoke: {
        src: CreateLocalTracksMachine,
        onDone: [
          {
            cond: (context, event) => typeof event.data.error !== "undefined",
            target: "initialized",
            actions: assign((context, event) => {
              context.error = event.data.error;
            }),
          },
          {
            target: "initializeMediaDevices",
            actions: assign((context, event) => {
              context.audioTrack = event.data.audioTrack;
              context.videoTrack = event.data.videoTrack;
            }),
          },
        ],
      },
    },
    initializeMediaDevices: {
      invoke: {
        src: EnumerateMediaDevicesMachine,
        onDone: [
          {
            cond: (context, event) => typeof event.data.error !== "undefined",
            target: "initialized",
            actions: assign((context, event) => {
              context.error = event.data.error;
            }),
          },
          {
            target: "initialized",
            actions: (context, event) => {
              context.devices = event.data.devices;
            },
          },
        ],
      },
    },
    initialized: {
      type: "final",
      data: {
        audioTrack: (context) => context.audioTrack,
        videoTrack: (context) => context.videoTrack,
        devices: (context) => context.devices,
      },
    },
  },
});
