import JitsiConnection, { JitsiConnectionOptions } from "@together/lib-jitsi-meet/JitsiConnection";
import { createMachine, assign } from "xstate";

type MachineContext = {
  options: Partial<JitsiConnectionOptions>;
  appId?: string;
  token?: string;
  jitsiConnection?: JitsiConnection;
  error?: Error;
};

export const CreateJitsiConnectionMachine = createMachine<MachineContext>({
  id: "create-jitsi-connection",
  initial: "creating",
  states: {
    creating: {
      invoke: {
        id: "create-connection",
        src: (context) => {
          if (!window.JitsiMeetJS) return Promise.reject(new Error("LibJitsiMeet is required!"));
          const connection = new window.JitsiMeetJS.JitsiConnection(context.appId, context.token, {
            ...context.options,
            hosts: {
              domain: context.options.hosts?.domain ?? "",
              muc: context.options.hosts?.muc ?? "",
            },
          });
          return Promise.resolve(connection);
        },
        onDone: {
          target: "created",
          actions: assign((context, event) => {
            context.jitsiConnection = event.data;
            return context;
          }),
        },
        onError: {
          target: "failed",
          actions: assign((context, event) => {
            context.error = event.data;
            return context;
          }),
        },
      },
    },
    created: {
      type: "final",
      data: {
        jitsiConnection: (context) => context.jitsiConnection,
      },
    },
    failed: {
      type: "final",
      data: {
        error: (context) => context.error,
      },
    },
  },
});
