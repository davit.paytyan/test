import JitsiConference, {
  JitsiConferenceOptionConfig,
} from "@together/lib-jitsi-meet/JitsiConference";
import JitsiConnection from "@together/lib-jitsi-meet/JitsiConnection";
import { assign } from "@xstate/immer";
import { createMachine } from "xstate";

type MachineContext = {
  roomName: string;
  jitsiConnection?: JitsiConnection;
  jitsiConference?: JitsiConference;
  options: JitsiConferenceOptionConfig;
  error?: Error;
};

export const CreateJitsiConferenceMachine = createMachine<MachineContext>({
  id: "create-jitsi-conference",
  initial: "connectionMissing",
  states: {
    connectionMissing: {
      on: {
        setConnection: {
          target: "creating",
          actions: assign((context, event) => {
            context.jitsiConnection = event.connection;
          }),
        },
      },
    },
    creating: {
      invoke: {
        src: (context) => {
          if (!context.jitsiConnection)
            return Promise.reject(new Error("JitsiConnection is required!"));

          const conference = context.jitsiConnection.initJitsiConference(
            context.roomName,
            context.options
          );

          return Promise.resolve(conference);
        },
        onDone: {
          target: "created",
          actions: assign((context, event) => {
            context.jitsiConference = event.data;
          }),
        },
        onError: {
          target: "failed",
          actions: assign((context, event) => {
            context.error = event.data;
          }),
        },
      },
    },
    created: {
      type: "final",
      data: {
        jitsiConference: (context) => context.jitsiConference,
      },
    },
    failed: {
      type: "final",
      data: {
        error: (context) => context.error,
      },
    },
  },
});
