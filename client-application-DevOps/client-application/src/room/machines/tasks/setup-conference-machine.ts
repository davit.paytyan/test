import { assign } from "xstate";
import { createModel } from "xstate/lib/model";
import { getRoom, getRoomUser } from "../../../actionbox-sdk";
import { directus } from "../../../global/singleton-directus-instance";
import { LoadingDependenciesMachine } from "./loading-dependencies-machine";

type MachineContext = {
  roomId: string;
  room?: {
    name: string;
    description?: string;
  };
  roomUser?: {
    userId: string;
    nickname: string;
    role: {
      id: string;
      name: string;
    };
    userColor: string;
  };
  error?: Error;
};

const initialContext: MachineContext = {
  roomId: "",
};

export const SetupConferenceMachineModel = createModel(initialContext, {
  events: {},
});

export const SetupConferenceMachine = SetupConferenceMachineModel.createMachine({
  id: "setupConference",
  initial: "setup",
  states: {
    setup: {
      type: "parallel",
      states: {
        loadRoomData: {
          initial: "loading",
          states: {
            loading: {
              invoke: {
                id: "loadRoomData",
                src: (context) => getRoom(directus, context.roomId),
                onDone: {
                  target: "loadingComplete",
                  actions: assign((context, event) => {
                    context.room = {
                      name: event.data.name,
                      description: event.data.description,
                    };
                    return context;
                  }),
                },
                onError: {
                  target: "loadingComplete",
                  actions: assign((context, event) => {
                    context.error = event.data;
                    return context;
                  }),
                },
              },
            },
            loadingComplete: {
              type: "final",
            },
          },
        },
        loadRoomUser: {
          initial: "loading",
          states: {
            loading: {
              invoke: {
                id: "loadRoomUser",
                src: (context) => getRoomUser(directus, context.roomId),
                onDone: {
                  target: "loadingComplete",
                  actions: assign((context, event) => {
                    context.roomUser = {
                      userId: event.data.user,
                      nickname: event.data.nickname ?? "",
                      role: event.data.role,
                      userColor: event.data.user_colors.userColor,
                    };
                    return context;
                  }),
                },
                onError: {
                  target: "loadingComplete",
                  actions: assign((context, event) => {
                    context.error = event.data;
                    return context;
                  }),
                },
              },
            },
            loadingComplete: {
              type: "final",
            },
          },
        },
        loadDependencies: {
          initial: "loading",
          states: {
            loading: {
              invoke: {
                src: LoadingDependenciesMachine,
                onDone: "loadingComplete",
              },
            },
            loadingComplete: { type: "final" },
          },
        },
      },
      onDone: [
        {
          cond: (context) => typeof context.error !== "undefined",
          target: "failed",
        },
        { target: "completed" },
      ],
    },
    completed: {
      type: "final",
      data: {
        room: (context) => context.room,
        roomUser: (context) => context.roomUser,
      },
    },
    failed: {
      type: "final",
      data: {
        error: (context) => {
          return context.error;
        },
      },
    },
  },
});
