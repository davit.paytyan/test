import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
import { createMachine, assign } from "xstate";

type MachineContext = {
  audioTrack?: JitsiLocalTrack;
  videoTrack?: JitsiLocalTrack;
  desktopTrack?: JitsiLocalTrack;
  error?: Error;
  devices: Array<"audio" | "video" | "desktop">;
  preselectedCameraDeviceId?: string;
  preselectedMicDeviceId?: string;
};

export const CreateLocalTracksMachine = createMachine<MachineContext>({
  id: "create-local-tracks",
  initial: "creating",
  context: {
    devices: ["audio", "video"],
  },
  states: {
    creating: {
      invoke: {
        src: (context) => {
          if (!window.JitsiMeetJS) throw new Error("LibJitsiMeet is required!");
          return window.JitsiMeetJS.createLocalTracks({
            devices: context.devices,
            cameraDeviceId: context.preselectedCameraDeviceId,
            micDeviceId: context.preselectedMicDeviceId,
          });
        },
        onDone: {
          target: "complete",
          actions: assign((context, event) => {
            for (const track of event.data as JitsiLocalTrack[]) {
              if (track.isAudioTrack()) {
                context.audioTrack = track;
                continue;
              }

              if (track.isVideoTrack()) {
                context.videoTrack = track;
                continue;
              }

              if (track.isScreenSharing()) {
                context.desktopTrack = track;
                continue;
              }
            }
            return context;
          }),
        },
        onError: {
          target: "failed",
          actions: assign((context, event) => {
            context.error = event.data;
            return context;
          }),
        },
      },
    },
    complete: {
      type: "final",
      data: {
        audioTrack: (context) => context.audioTrack,
        videoTrack: (context) => context.videoTrack,
        desktopTrack: (context) => context.desktopTrack,
      },
    },
    failed: {
      type: "final",
      data: {
        error: (context) => context.error,
      },
    },
  },
});
