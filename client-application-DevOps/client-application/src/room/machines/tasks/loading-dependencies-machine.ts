import JitsiMeetJS from "@together/lib-jitsi-meet";
import getConfig from "next/config";
import { createMachine } from "xstate";

async function loadLibraryJitsiMeet() {
  if (window.JitsiMeetJS) return window.JitsiMeetJS;

  const { publicRuntimeConfig } = getConfig();

  const scriptTag = document.createElement("script");
  scriptTag.async = true;
  scriptTag.id = "jitsi-lib";
  scriptTag.src = publicRuntimeConfig.libJitsiMeetSrc;

  function load() {
    return new Promise<JitsiMeetJS>((resolve, reject) => {
      function onLoad() {
        if (!window.JitsiMeetJS) throw new Error("JitsiMeetJS is missing!");
        window.JitsiMeetJS.init({
          disableAudioLevels: true,
          disableThirdPartyRequests: true,
          preferredCodec: "VP9",
        });
        window.JitsiMeetJS.setLogLevel("warn");
        scriptTag.removeEventListener("load", onLoad);
        scriptTag.removeEventListener("error", onError);
        scriptTag.remove();
        resolve(window.JitsiMeetJS as JitsiMeetJS);
      }

      function onError(error) {
        reject(error);
      }

      scriptTag.addEventListener("load", onLoad);
      scriptTag.addEventListener("error", onError);

      document.body.append(scriptTag);
    });
  }

  const library = await load();
  if (!library) throw new Error("JitsiMeetJS could not be loaded!");
  return library;
}

async function loadJQuery() {
  if (typeof window === "undefined") return;
  if (window.$) return;
  await import("jquery/dist/jquery.slim").then((module) => {
    window.$ = module.default;
  });
}

export const LoadingDependenciesMachine = createMachine({
  id: "loadingDependencies",
  initial: "loading",
  states: {
    loading: {
      type: "parallel",
      states: {
        jitsi: {
          invoke: {
            src: () => loadLibraryJitsiMeet(),
            onDone: ".complete",
            onError: ".failed",
          },
          initial: "loading",
          states: {
            loading: {},
            complete: {
              type: "final",
            },
            failed: {
              type: "final",
            },
          },
        },
        jquery: {
          invoke: {
            src: () => loadJQuery(),
            onDone: ".complete",
            onError: ".failed",
          },
          initial: "loading",
          states: {
            loading: {},
            complete: {
              type: "final",
            },
            failed: {
              type: "final",
            },
          },
        },
      },
      onDone: "loadingComplete",
    },
    loadingComplete: {
      type: "final",
    },
  },
});
