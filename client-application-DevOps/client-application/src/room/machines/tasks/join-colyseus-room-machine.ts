import { assign } from "@xstate/immer";
import { RoomSchema } from "colyseus-schema";
import { Client, Room } from "colyseus.js";
import Logger from "loglevel";
import { createMachine } from "xstate";

type MachineContext = {
  roomId: string;
  userId: string;
  endpoint: string;
  nickname?: string;
  isVideoMirrored?: boolean;
  room?: Room<RoomSchema>;
  error?: Error;
};

async function joinColyseusRoom(
  endpoint: string,
  roomId: string,
  userId: string,
  nickname?: string,
  isVideoMirrored?: boolean
): Promise<[Room<RoomSchema> | null, Error | null]> {
  const client = new Client(endpoint);
  let room: Room<RoomSchema> | null = null;
  let error: Error | null = null;

  try {
    room = await client.joinById(roomId, { userId, nickname, isVideoMirrored });
    if (!room) throw new Error("Missing Room!");
    return [room, null];
  } catch (_error) {
    error = _error;
    Logger.warn(`Room ${roomId} is not available.`);
  }

  try {
    room = await client.create("together_room", { roomId, userId, nickname, isVideoMirrored });
    if (!room) throw new Error("Missing Room!");
    Logger.info(`Created and joined room ${roomId}.`);
    return [room, null];
  } catch (_error) {
    error = _error;
    Logger.warn(`Could not create and join room ${roomId}!`);
  }
  return [null, error];
}

export const JoinColyseusRoomMachine = createMachine<MachineContext>({
  id: "join-colyseus-room",
  initial: "joining",
  states: {
    joining: {
      invoke: {
        src: (context) =>
          joinColyseusRoom(
            context.endpoint,
            context.roomId,
            context.userId,
            context.nickname,
            context.isVideoMirrored
          ),
        onDone: [
          {
            cond: (context, event) => {
              const [, error] = event.data;
              return Boolean(error);
            },
            target: "failed",
            actions: assign((context, event) => {
              const [, error] = event.data;
              context.error = error;
            }),
          },
          {
            target: "joined",
            actions: assign((context, event) => {
              const [room] = event.data;
              context.room = room;
            }),
          },
        ],
      },
    },
    joined: {
      type: "final",
      data: {
        room: (context) => context.room,
      },
    },
    failed: {
      type: "final",
      data: {
        error: (context) => context.error,
      },
    },
  },
});
