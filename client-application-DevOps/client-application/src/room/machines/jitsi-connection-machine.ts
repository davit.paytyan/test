import JitsiConnection from "@together/lib-jitsi-meet/JitsiConnection";
import { forwardTo, InvokeCreator, sendParent } from "xstate";
import { createModel } from "xstate/lib/model";
import { ModelEventsFrom } from "xstate/lib/model.types";
import { JitsiConnectionActor } from "./actors/jitsi-connection-actor";

type MachineContext = {
  connection: JitsiConnection;
};

const initialContext: MachineContext = {
  // you *have* to provide the connection via `Machine.withContext()` method
  connection: undefined as unknown as JitsiConnection,
};

export const JitsiConnectionMachineModelCreator = {
  events: {
    connect: (password: string) => ({ password }),
    disconnect: () => ({}),
    // callbacks
    onConnectionEstablished: (connectionId: string, connection: JitsiConnection) => ({
      connectionId,
      connection,
    }),
    onConnectionDisconnected: (message: string) => ({ message }),
    onConnectionFailed: (errorType: string, errorReason: string) => ({
      errorType,
      errorReason,
    }),
    onConnectionDisplayNameRequired: () => ({}),
    onConnectionWrongState: () => ({}),
  },
};

export const JitsiConnectionMachineModel = createModel(
  initialContext,
  JitsiConnectionMachineModelCreator
);

type Events = ModelEventsFrom<typeof JitsiConnectionMachineModel>;

const actorCallback: InvokeCreator<MachineContext, Events> = (context) => {
  const actor = new JitsiConnectionActor("jitsi-connection", context.connection);

  function eventListener(event: Events) {
    if (event.type === "connect") actor.send(event);
    if (event.type === "disconnect") actor.send(event);
  }

  return (callback, onEvent) => {
    onEvent(eventListener);
    actor.subscribe(callback);

    return () => {
      actor.stop();
    };
  };
};

export const JitsiConnectionMachine = JitsiConnectionMachineModel.createMachine({
  id: "jitsi-connection",
  context: JitsiConnectionMachineModel.initialContext,
  invoke: {
    id: "jitsi-connection-actor-callback",
    src: actorCallback,
  },
  on: {
    connect: {
      actions: forwardTo("jitsi-connection-actor-callback"),
    },
    disconnect: {
      actions: forwardTo("jitsi-connection-actor-callback"),
    },
    "*": {
      actions: sendParent((context, event) => event),
    },
  },
});
