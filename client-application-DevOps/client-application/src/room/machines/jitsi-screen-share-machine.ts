import JitsiConference, {
  JitsiConferenceOptionConfig,
} from "@together/lib-jitsi-meet/JitsiConference";
import JitsiConnection, { JitsiConnectionOptions } from "@together/lib-jitsi-meet/JitsiConnection";
import { assign } from "@xstate/immer";
import { ActorRefFrom, forwardTo, send, sendParent, spawn } from "xstate";
import { createModel } from "xstate/lib/model";
import {
  JitsiConferenceMachine,
  JitsiConferenceMachineModelCreator,
} from "./jitsi-conference-machine";
import {
  JitsiConnectionMachine,
  JitsiConnectionMachineModelCreator,
} from "./jitsi-connection-machine";
import { CreateJitsiConferenceMachine } from "./tasks/create-jitsi-conference-machine";
import { CreateJitsiConnectionMachine } from "./tasks/create-jitsi-connection-machine";

type MachineContext = {
  roomId: string;
  connectionConfig: {
    appId?: string;
    token?: string;
    options: Partial<JitsiConnectionOptions>;
  };
  conferenceConfig: {
    password: string;
    options: JitsiConferenceOptionConfig;
  };
  connection?: JitsiConnection;
  connectionRef?: ActorRefFrom<typeof JitsiConnectionMachine>;
  conference?: JitsiConference;
  conferenceRef?: ActorRefFrom<typeof JitsiConferenceMachine>;
  error?: Error;
};

const initialContext: MachineContext = {
  roomId: "",
  connectionConfig: { options: {} },
  conferenceConfig: { password: "", options: {} },
};

export const JitsiScreenShareMachineModel = createModel(initialContext, {
  events: {
    ...JitsiConnectionMachineModelCreator.events,
    ...JitsiConferenceMachineModelCreator.events,
  },
});

export const JitsiScreenShareMachine = JitsiScreenShareMachineModel.createMachine({
  id: "jitsi-screen-share",
  context: JitsiScreenShareMachineModel.initialContext,
  initial: "initializeConnection",
  states: {
    initializeConnection: {
      invoke: {
        id: "create-jitsi-connection",
        src: (context) =>
          CreateJitsiConnectionMachine.withContext({
            appId: context.connectionConfig.appId,
            token: context.connectionConfig.token,
            options: context.connectionConfig.options,
          }),
        onDone: {
          target: "connecting",
          actions: assign((context, event) => {
            const machineConfig = JitsiConnectionMachine.withContext({
              connection: event.data.jitsiConnection,
            });
            context.connectionRef = spawn(machineConfig);
            context.connection = event.data.jitsiConnection;
          }),
        },
        onError: {
          target: "error",
          actions: assign((context, event) => {
            context.error = event.data.error;
          }),
        },
      },
    },
    connecting: {
      entry: (context) => context.connectionRef?.send({ type: "connect", password: "" }),
      invoke: {
        id: "create-jitsi-conference",
        src: CreateJitsiConferenceMachine,
        data: {
          roomName: (context: MachineContext) => context.roomId,
          options: (context: MachineContext) => context.conferenceConfig.options,
        },
        onDone: {
          target: "connected",
          actions: assign((context, event) => {
            const conferenceMachineConfig = JitsiConferenceMachine.withContext({
              conference: event.data.jitsiConference,
            });
            const conferenceRef = spawn(conferenceMachineConfig, "jitsi-conference");
            context.conference = event.data.jitsiConference;
            context.conferenceRef = conferenceRef;
          }),
        },
      },
      on: {
        onConnectionEstablished: {
          actions: send((context) => ({ type: "setConnection", connection: context.connection }), {
            to: "create-jitsi-conference",
          }),
        },
        onConferenceJoined: {
          target: "sharingScreen",
          actions: sendParent((context, event) => {
            return event;
          }),
        },
        onConnectionFailed: {
          actions: assign((context, event) => {
            context.error = new Error(event.errorReason);
          }),
        },
      },
    },
    connected: {
      initial: "idle",
      states: {
        idle: {
          on: {
            joinConference: {
              target: "joiningConference",
              actions: forwardTo("jitsi-conference"),
            },
          },
        },
        joiningConference: {
          on: {
            onConferenceJoined: {
              target: "joinedConference",
              actions: sendParent((context, event) => {
                return event;
              }),
            },
          },
        },
        joinedConference: {
          on: {
            kickParticipant: {
              actions: forwardTo("jitsi-conference"),
            },
          },
        },
        leavingConference: {},
        leftConference: {
          type: "final",
        },
      },
      on: {
        onConferenceLeft: ".leftConference",
        onConferenceFailed: {
          actions: assign((context, event) => {
            context.error = new Error(event.reason);
          }),
        },
        onConferenceError: {
          actions: assign((context, event) => {
            context.error = new Error(event.reason);
          }),
        },
      },
    },
    error: {
      type: "final",
      data: {
        error: (context) => context.error,
      },
    },
  },
});
