import JitsiConference, {
  JitsiConferenceOptionConfig,
} from "@together/lib-jitsi-meet/JitsiConference";
import JitsiConnection, { JitsiConnectionOptions } from "@together/lib-jitsi-meet/JitsiConnection";
import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
import { ActorRefFrom, assign, createMachine, send, spawn } from "xstate";
import { ServerError } from "../../library/server-error";
import { JitsiConferenceMachine } from "./jitsi-conference-machine";
import { JitsiConnectionMachine } from "./jitsi-connection-machine";
import { JitsiMachine } from "./jitsi-machine";
import { CreateJitsiConferenceMachine } from "./tasks/create-jitsi-conference-machine";
import { CreateJitsiConnectionMachine } from "./tasks/create-jitsi-connection-machine";
import { CreateLocalTracksMachine } from "./tasks/create-local-tracks-machine";
import { SetupConferenceMachine } from "./tasks/setup-conference-machine";

type MachineContext = {
  roomId: string;
  jitsiConnectionConfig: {
    appId?: string;
    token?: string;
    options: Partial<JitsiConnectionOptions>;
  };
  jitsiConferenceConfig: Partial<{
    options: JitsiConferenceOptionConfig;
  }>;
  jitsiConnection?: JitsiConnection;
  jitsiConnectionRef?: ActorRefFrom<typeof JitsiConnectionMachine>;
  jitsiConference?: JitsiConference;
  jitsiConferenceRef?: ActorRefFrom<typeof JitsiConferenceMachine>;
  refs: {
    jitsi?: ActorRefFrom<typeof JitsiMachine>;
  };
  track?: JitsiLocalTrack;
  error?: ServerError;
};

const initialContext: MachineContext = {
  roomId: "",
  jitsiConnectionConfig: { options: {} },
  jitsiConferenceConfig: {
    options: {
      p2p: {
        enabled: false,
      },
    },
  },
  refs: {},
};

export const ScreenShareMachine = createMachine<MachineContext>({
  id: "screen-share-machine",
  context: initialContext,
  initial: "setup",
  states: {
    setup: {
      invoke: {
        id: "setupRoom",
        src: (context) => SetupConferenceMachine.withContext({ roomId: context.roomId }),
        onDone: [
          {
            cond: (context, event) => {
              const { error } = event.data;
              return typeof error !== "undefined";
            },
            target: "error",
          },
          {
            target: "initializeDesktopTrack",
          },
        ],
      },
    },
    initializeDesktopTrack: {
      invoke: {
        src: CreateLocalTracksMachine,
        data: {
          devices: ["desktop"],
        },
        onDone: [
          {
            cond: (context, event) => typeof event.data.error !== "undefined",
            target: "error",
            actions: assign((context, event) => {
              context.error = event.data.error;
              return context;
            }),
          },
          {
            target: "initializeConnection",
            actions: assign((context, event) => {
              context.track = event.data.videoTrack;
              return context;
            }),
          },
        ],
      },
    },
    initializeConnection: {
      invoke: {
        id: "create-jitsi-connection",
        src: (context) =>
          CreateJitsiConnectionMachine.withContext({
            appId: context.jitsiConnectionConfig.appId,
            token: context.jitsiConnectionConfig.token,
            options: context.jitsiConnectionConfig.options,
          }),
        onDone: {
          target: "connecting",
          actions: assign({
            jitsiConnection: (context, event) => event.data.jitsiConnection,
            jitsiConnectionRef: (context, event) => {
              const machineConfig = JitsiConnectionMachine.withContext({
                connection: event.data.jitsiConnection,
              });
              return spawn(machineConfig);
            },
          }),
        },
        onError: {
          target: "error",
          actions: assign({
            error: (context, event) => event.data.error,
          }),
        },
      },
    },
    connecting: {
      entry: (context) => context.jitsiConnectionRef?.send({ type: "connect", password: "" }),
      invoke: {
        id: "create-jitsi-conference",
        src: CreateJitsiConferenceMachine,
        data: {
          roomName: (context: MachineContext) => context.roomId,
          options: (context: MachineContext) => context.jitsiConferenceConfig.options,
        },
        onDone: {
          target: "initialized",
          actions: assign({
            jitsiConference: (context, event) => event.data.jitsiConference,
            jitsiConferenceRef: (context, event) => {
              const conferenceMachineConfig = JitsiConferenceMachine.withContext({
                conference: event.data.jitsiConference,
              });
              return spawn(conferenceMachineConfig, "jitsi-conference");
            },
          }),
        },
      },
      on: {
        onConnectionEstablished: {
          actions: send(
            (context) => ({ type: "setConnection", connection: context.jitsiConnection }),
            { to: "create-jitsi-conference" }
          ),
        },
        onConnectionFailed: {
          target: "error",
          actions: assign({
            error: (context, event) => new ServerError(500, event.errorReason),
          }),
        },
      },
    },
    initialized: {
      entry: [
        (context) => {
          context.jitsiConferenceRef?.send({ type: "joinConference", password: "" });
        },
      ],
      on: {
        onConferenceJoined: {
          actions: [
            (context) => {
              if (context.track) {
                context.jitsiConference?.addTrack(context.track);
                context.jitsiConference?.setReceiverConstraints({
                  defaultConstraints: { maxHeight: 0 },
                });
                context.track.getTrack().addEventListener("ended", () => {
                  context.jitsiConference?.leave();
                });
              }
            },
          ],
        },
        onConferenceLeft: "terminated",
        onConferenceKicked: "terminated",
        onConferenceError: "error",
        onConferenceFailed: "error",
      },
    },
    error: {
      type: "final",
    },
    terminated: {
      type: "final",
    },
  },
});
