import JitsiConference from "@together/lib-jitsi-meet/JitsiConference";
import { getLogger } from "loglevel";
import { createModel } from "xstate/lib/model";

const Logger = getLogger("ReceiverConstraints");

Logger.setLevel("DEBUG");

//
// MACHINE INITIAL CONTEXT
//

type ReceiverConstraintsInitialContext = {
  conference?: JitsiConference;
  participantsOnStage: string[];
  itemsOnStage: number;
};

const initialContext: ReceiverConstraintsInitialContext = {
  participantsOnStage: [],
  itemsOnStage: 0,
};

//
// MACHINE MODEL
//

export const JitsiConferenceReceiverConstraintsModel = createModel(initialContext, {
  events: {
    setConference: (conference: JitsiConference) => {
      return { conference };
    },
    updateParticipantsOnStage: (participantIds: string[], itemsOnStage: number) => {
      return { participantIds, itemsOnStage };
    },
    // updateStageCardSize: (width: number, height: number) => {
    //   return { width, height };
    // },
  },
});

//
// MODEL ASSIGN ACTIONS
// they need to be outside of the machines config
// see: https://xstate.js.org/docs/guides/models.html#narrowing-assign-event-types
//

const assignConference = JitsiConferenceReceiverConstraintsModel.assign(
  {
    conference: (_, event) => event.conference,
  },
  "setConference"
);

const assignParticipantsOnStage = JitsiConferenceReceiverConstraintsModel.assign(
  {
    participantsOnStage: (context, event) => {
      return [...event.participantIds];
    },
    itemsOnStage: (context, event) => {
      return event.itemsOnStage;
    },
  },
  "updateParticipantsOnStage"
);

//
// MACHINE CONFIGURATION
//

export const JitsiConferenceReceiverConstraintsMachine =
  JitsiConferenceReceiverConstraintsModel.createMachine({
    id: "jitsi-conference-receiver-constraints",
    context: JitsiConferenceReceiverConstraintsModel.initialContext,
    initial: "waiting_for_conference",
    states: {
      waiting_for_conference: {
        always: {
          cond: (context) => typeof context.conference !== "undefined",
          target: "managing_video_quality",
        },
        on: {
          setConference: {
            actions: [assignConference],
          },
        },
      },
      managing_video_quality: {
        on: {
          updateParticipantsOnStage: {
            actions: [
              assignParticipantsOnStage,
              (context) => {
                if (!context.conference) return null;

                const { conference, participantsOnStage, itemsOnStage } = context;
                const constraints: Record<
                  string,
                  {
                    maxHeight: number;
                    minHeight: number;
                    maxFrameRate: number;
                    minFrameRate: number;
                  }
                > = {};

                let maxHeight;

                if (itemsOnStage <= 2) {
                  maxHeight = 1080;
                } else if (itemsOnStage <= 4) {
                  maxHeight = 720;
                } else if (itemsOnStage <= 6) {
                  maxHeight = 640;
                } else {
                  maxHeight = 480;
                }

                Logger.debug("Participants on stage:", participantsOnStage.length);
                Logger.debug("Total items on stage:", itemsOnStage);
                Logger.debug("Max height constraint set to:", maxHeight);

                for (const participant of participantsOnStage) {
                  constraints[participant] = {
                    maxFrameRate: 30,
                    minFrameRate: 15,
                    maxHeight,
                    minHeight: 360,
                  };
                }

                conference.setReceiverConstraints({
                  defaultConstraints: { maxHeight: 180 },
                  onStageEndpoints: participantsOnStage,
                  constraints,
                });

                Logger.debug("SetReceiverConstraints to", {
                  defaultConstraints: { maxHeight: 180 },
                  onStageEndpoints: participantsOnStage,
                  constraints,
                });
              },
            ],
          },
        },
      },
    },
  });
