import { assign } from "@xstate/immer";
import { createModel } from "xstate/lib/model";

type QuickReaction = {
  type: string;
  value: string;
};

type Context = {
  quickReaction?: QuickReaction;
};

const initialState: Context = {};

const ParticipantQuickReactionModel = createModel(initialState, {
  events: {
    onSetQuickReaction: (quickReaction: QuickReaction) => ({ quickReaction }),
    onClearQuickReaction: () => ({}),
    openBubble: () => ({}),
    closeBubble: () => ({}),
  },
});

export const ParticipantQuickReactionMachine = ParticipantQuickReactionModel.createMachine(
  {
    context: ParticipantQuickReactionModel.initialContext,
    initial: "unset",
    states: {
      unset: {},
      newReaction: {
        after: {
          20_000: {
            target: "idle.closed",
          },
        },
        on: {
          closeBubble: "idle.closed",
        },
      },
      idle: {
        states: {
          open: {
            on: {
              closeBubble: "closed",
            },
          },
          closed: {
            on: {
              openBubble: "open",
            },
          },
        },
      },
    },
    on: {
      onClearQuickReaction: {
        target: "unset",
        actions: "unassignQuickReaction",
      },
      onSetQuickReaction: {
        target: "newReaction",
        actions: "assignQuickReaction",
      },
    },
  },
  {
    actions: {
      assignQuickReaction: assign((context, event) => {
        if (event.type !== "onSetQuickReaction") return;
        context.quickReaction = event.quickReaction;
      }),
      unassignQuickReaction: assign((context) => {
        context.quickReaction = undefined;
      }),
    },
  }
);
