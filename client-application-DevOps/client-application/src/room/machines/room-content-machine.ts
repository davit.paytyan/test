import { assign } from "@xstate/immer";
import { send } from "xstate";
import { createModel } from "xstate/lib/model";
import { directus } from "../../global/singleton-directus-instance";

export const RoomContentMachineModel = createModel(
  {
    searchTerm: "",
    roomId: "",
    type: "directus_files",
    items: [] as RoomContentCollectionItem[],
    selectedItems: new Set<string>(),
  },
  {
    events: {
      openPanel: () => ({}),
      closePanel: () => ({}),
      openPanelActions: () => ({}),
      closePanelActions: () => ({}),
      setSearchTerm: (value?: string) => ({ value: value?.trim() ?? "" }),
      loadRoomContent: () => ({}),
      refreshRoomContent: () => ({}),
      itemsLoaded: () => ({}),
      selectItems: (items: string[]) => ({ items }),
      selectAllItems: () => ({}),
      deselectItems: (items: string[]) => ({ items }),
      deselectAllItems: () => ({}),
      deleteSelectedItems: () => ({}),
      moveItemToIndex: (itemId: string, newIndex: number, save: boolean) => ({
        itemId,
        newIndex,
        save,
      }),
      deleteItems: (items: string[]) => ({ items }),
      createItem: (title: string, options?: Record<string, string | number | boolean>) => ({
        title: title.trim(),
        options,
      }),
      renameItem: (itemId: string, title: string) => ({ itemId, title: title.trim() }),
      sortItems: () => ({}),
    },
  }
);

export const RoomContentMachine = RoomContentMachineModel.createMachine({
  id: "room-content",
  context: RoomContentMachineModel.initialContext,
  type: "parallel",
  states: {
    panel: {
      initial: "closed",
      states: {
        opened: {},
        closed: {},
      },
      on: {
        closePanel: ".closed",
        openPanel: ".opened",
      },
    },
    actions: {
      initial: "closed",
      states: {
        opened: {},
        closed: {},
      },
      on: {
        closePanelActions: ".closed",
        openPanelActions: ".opened",
      },
    },
    collection: {
      type: "parallel",
      states: {
        searchTerm: {
          initial: "unset",
          states: {
            unset: {},
            set: {},
          },
          on: {
            setSearchTerm: [
              {
                cond: (context, event) => event.value.length > 2,
                target: ".set",
                actions: assign((context, event) => {
                  context.searchTerm = event.value;
                }),
              },
              {
                target: ".unset",
                actions: assign((context, event) => {
                  context.searchTerm = event.value;
                }),
              },
            ],
          },
        },
        filter: {},
        items: {
          initial: "empty",
          states: {
            empty: {},
            filled: {
              on: {
                selectItems: {
                  actions: assign((context, event) => {
                    for (const itemId of event.items) context.selectedItems.add(itemId);
                  }),
                },
                selectAllItems: {
                  actions: assign((context) => {
                    for (const item of context.items) context.selectedItems.add(item.id);
                  }),
                },
                deselectItems: {
                  actions: assign((context, event) => {
                    for (const itemId of event.items) context.selectedItems.delete(itemId);
                  }),
                },
                deselectAllItems: {
                  actions: assign((context) => {
                    context.selectedItems.clear();
                  }),
                },
                moveItemToIndex: [
                  {
                    cond: (context, event) => {
                      return event.save;
                    },
                    actions: [
                      assign((context, event) => {
                        const itemIndex = context.items.findIndex((item) => {
                          return item.id === event.itemId;
                        });
                        if (itemIndex === -1) return;
                        const item = context.items[itemIndex];
                        context.items.splice(itemIndex, 1);
                        context.items.splice(event.newIndex, 0, item);
                        let index = 0;
                        for (const item of context.items) item.index = index++;
                      }),
                      send("sortItems"),
                    ],
                  },
                  {
                    actions: [
                      assign((context, event) => {
                        const itemIndex = context.items.findIndex((item) => {
                          return item.id === event.itemId;
                        });
                        if (itemIndex === -1) return;
                        const item = context.items[itemIndex];
                        context.items.splice(itemIndex, 1);
                        context.items.splice(event.newIndex, 0, item);
                        let index = 0;
                        for (const item of context.items) item.index = index++;
                      }),
                    ],
                  },
                ],
              },
            },
          },
        },
        source: {
          initial: "loading",
          states: {
            loading: {
              invoke: {
                src: (context) => {
                  const url = `/custom/room-contents/${context.roomId}/${context.type}`;
                  return directus.transport
                    .get<RoomContentCollectionItem[]>(url)
                    .then((response) => {
                      return response.data?.map((item, index) => {
                        item.index = index;
                        return item;
                      });
                    });
                },
                onDone: {
                  target: "loaded",
                  actions: assign((context, event) => {
                    context.items = event.data;
                  }),
                },
                onError: {
                  target: "error",
                },
              },
            },
            creating: {
              invoke: {
                id: "create-item",
                src: (context, event) => {
                  if (event.type !== "createItem") throw new Error("wrong event!");
                  const safeTitle = event.title === "" ? `new ${context.type}` : event.title;
                  return directus.items("room_content").createOne(
                    {
                      type: context.type,
                      room: context.roomId,
                      title: safeTitle,
                      payload: event.options ? JSON.stringify(event.options) : undefined,
                    },
                    { fields: ["id", "title", "type", "sortOrder", "payload", "file"] }
                  );
                },
                onDone: {
                  target: "sorting",
                  actions: [
                    assign((context, event) => {
                      context.items.push(event.data);
                      let index = 0;
                      for (const item of context.items) item.index = index++;
                    }),
                    send("onCreatedItem"),
                  ],
                },
                onError: {
                  target: "error",
                },
              },
            },
            deleting: {
              // eager deletion
              entry: assign((context, event) => {
                if (event.type !== "deleteItems") throw new Error("wrong event!");
                const { items } = event;

                context.items = context.items.filter((item) => {
                  if (items.includes(item.id)) {
                    context.selectedItems.delete(item.id);
                    return false;
                  }
                  return true;
                });

                let index = 0;
                for (const item of context.items) item.index = index++;
              }),
              invoke: {
                src: async (context, event) => {
                  if (event.type !== "deleteItems") throw new Error("wrong event!");
                  const { items } = event;
                  return directus.items("room_content").deleteMany(items);
                },
                onDone: {
                  target: "sorting",
                  actions: send("onDeletedItems"),
                },
                onError: {
                  target: "error",
                },
              },
            },
            renaming: {
              invoke: {
                src: (context, event) => {
                  if (event.type !== "renameItem") throw new Error("wrong event!");
                  return directus
                    .items("room_content")
                    .updateOne(event.itemId, { title: event.title });
                },
                onDone: {
                  target: "loaded",
                  actions: [
                    assign((context, event) => {
                      const updatedItem = event.data;
                      for (const item of context.items) {
                        if (item.id === updatedItem.id) item.title = updatedItem.title;
                      }
                      context.items = [...context.items];
                    }),
                    send("onRenamedItem"),
                  ],
                },
                onError: {
                  target: "error",
                },
              },
            },
            sorting: {
              invoke: {
                src: (context) => {
                  const url = `/custom/room-contents/${context.roomId}/sort`;
                  const ids = [] as string[];
                  for (const item of context.items) ids.push(item.id);
                  return directus.transport.patch<RoomContentCollectionItem[]>(url, ids);
                },
                onDone: {
                  target: "loading",
                  actions: send("onSortedItems"),
                },
                onError: {
                  target: "error",
                },
              },
            },
            loaded: {
              entry: send("itemsLoaded"),
              on: {
                refreshRoomContent: "loading",
                createItem: "creating",
                renameItem: "renaming",
                deleteItems: "deleting",
                sortItems: "sorting",
                deleteSelectedItems: {
                  cond: (context) => context.selectedItems.size > 0,
                  actions: send((context) => {
                    return RoomContentMachineModel.events.deleteItems([
                      ...context.selectedItems.values(),
                    ]);
                  }),
                },
              },
            },
            error: {
              on: {
                refreshRoomContent: "loading",
              },
            },
          },
        },
      },
      on: {
        itemsLoaded: [
          {
            cond: (context) => context.items.length > 0,
            target: ".items.filled",
          },
          {
            cond: (context) => context.items.length === 0,
            target: ".items.empty",
          },
        ],
      },
    },
  },
});

//
// TYPES
//
interface AbstractRoomContentCollectionItem {
  id: string;
  title: string;
  type: "textpad" | "directus_files";
  sortOrder: number;
  index: number;
}

interface RoomContentCollectionTextpadItem extends AbstractRoomContentCollectionItem {
  type: "textpad";
  payload: string;
  file: never;
}

interface RoomContentCollectionFileItem extends AbstractRoomContentCollectionItem {
  type: "directus_files";
  payload: never;
  file: {
    id: string;
    width: number;
    height: number;
    type: string;
    filesize: number;
  };
}

export type RoomContentCollectionItem =
  | RoomContentCollectionTextpadItem
  | RoomContentCollectionFileItem;
