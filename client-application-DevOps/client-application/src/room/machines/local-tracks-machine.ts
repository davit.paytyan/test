import JitsiConference from "@together/lib-jitsi-meet/JitsiConference";
import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
import { AnyEventObject, assign, createMachine, send, sendParent } from "xstate";
import { ChangeLocalTrackMachine } from "./tasks/change-local-track-machine";
import { CreateLocalTracksMachine } from "./tasks/create-local-tracks-machine";

type MachineContext = {
  conference?: JitsiConference;
  audioTrack?: JitsiLocalTrack;
  videoTrack?: JitsiLocalTrack;
  isVideoMirrored?: boolean;
};

export type MuteLocalTrackEvents = {
  type: "muteLocalAudioTrack" | "muteLocalVideoTrack";
};

export type MirrorLocalTrackEvent = {
  type: "mirrorLocalVideoTrack" | "onMirrorLocalVideoTrack";
  mirrored: boolean;
};

export type UnmuteLocalTrackEvents = {
  type: "unmuteLocalAudioTrack" | "unmuteLocalVideoTrack";
};

type ChangeLocalTrackEventTypes = "changeLocalAudioTrack" | "changeLocalVideoTrack";

export type ChangeLocalTrackEvents =
  | {
      type: ChangeLocalTrackEventTypes;
      deviceId: string;
      track?: never;
    }
  | {
      type: ChangeLocalTrackEventTypes;
      deviceId?: never;
      track: JitsiLocalTrack;
    };

export type ChangeLocalTrackDoneEvents = {
  type: "onChangeLocalAudioTrackDone" | "onChangeLocalVideoTrackDone";
  track: JitsiLocalTrack;
};

export type MachineEvents =
  | MirrorLocalTrackEvent
  | MuteLocalTrackEvents
  | UnmuteLocalTrackEvents
  | ChangeLocalTrackEvents
  | { type: "setConference"; conference: JitsiConference };

const createChangeTrackMachine = (context, event: ChangeLocalTrackEvents) => {
  return ChangeLocalTrackMachine.withContext({
    conference: context.conference,
    track: event.track as JitsiLocalTrack,
  });
};

const createStateNodes = (type: "audio" | "video") => {
  const deviceType = type === "audio" ? "preselectedMicDeviceId" : "preselectedCameraDeviceId";

  const uppercaseType = type === "audio" ? "Audio" : "Video";
  const eventType = `changeLocal${uppercaseType}Track`;
  const doneEventType = `onChangeLocal${uppercaseType}TrackDone`;

  const moveToNextStepAction = send(
    (context, event: AnyEventObject) => {
      const nextEvent = {
        type: eventType,
        track: event.data[`${type}Track`],
        deviceId: undefined,
      };
      return nextEvent;
    },
    { delay: 100 }
  );

  const notifiyParentAction = sendParent((context, event: AnyEventObject) => ({
    type: doneEventType,
    track: event.data.track ?? event.data[`${type}Track`],
  }));

  const assignTrackAction = assign((context: MachineContext, event: AnyEventObject) => {
    if (context[`${type}Track`]) context[`${type}Track`].dispose();
    context[`${type}Track`] = event.data.track ?? event.data[`${type}Track`];
    return context;
  });

  const onDoneTransition = {
    target: "idle",
    actions: [assignTrackAction, notifiyParentAction],
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const states: Record<string, any> = {
    idle: {
      initial: "_pre",
      states: {
        _pre: {
          always: [
            {
              cond: (context) => typeof context[`${type}Track`] === undefined,
              target: "notSet",
            },
            {
              cond: (context) => (context[`${type}Track`] as JitsiLocalTrack)?.isMuted(),
              target: "muted",
            },
            {
              target: "unmuted",
            },
          ],
        },
        notSet: {},
        muted: {},
        unmuted: {},
      },
      on: {
        [eventType]: [
          {
            cond: (context, event) => typeof event.deviceId !== "undefined",
            target: "createTrack",
          },
          {
            cond: (context, event) => typeof event.track !== "undefined",
            target: "changeTrack",
          },
        ],
        [`muteLocal${uppercaseType}Track`]: {
          cond: (context) => typeof context[`${type}Track`] !== undefined,
          target: "muteTrack",
        },
        [`unmuteLocal${uppercaseType}Track`]: {
          cond: (context) => typeof context[`${type}Track`] !== undefined,
          target: "unmuteTrack",
        },
      },
    },
    createTrack: {
      invoke: {
        src: (context, event) => {
          return CreateLocalTracksMachine.withContext({
            devices: [type],
            [deviceType]: event.deviceId,
          });
        },
        onDone: [
          {
            cond: (context) => typeof context.conference !== "undefined",
            actions: moveToNextStepAction,
          },
          onDoneTransition,
        ],
      },
      on: {
        [eventType]: [
          {
            cond: (context, event) => typeof event.track !== "undefined",
            target: "changeTrack",
          },
        ],
      },
    },
    changeTrack: {
      invoke: {
        id: `change-local-${type}-track`,
        src: createChangeTrackMachine,
        onDone: onDoneTransition,
      },
    },
    muteTrack: {
      invoke: {
        src: (context: MachineContext) => {
          const track: JitsiLocalTrack = context[`${type}Track`];
          return track.mute();
        },
        onDone: "idle",
      },
    },
    unmuteTrack: {
      invoke: {
        src: (context: MachineContext) => {
          const track: JitsiLocalTrack = context[`${type}Track`];
          return track.unmute();
        },
        onDone: "idle",
      },
    },
  };

  return states;
};

export const LocalTracksMachine = createMachine<MachineContext>({
  id: "local-tracks",
  type: "parallel",
  context: {},
  states: {
    audioTrack: {
      initial: "idle",
      states: createStateNodes("audio"),
    },
    videoTrack: {
      initial: "idle",
      states: {
        ...createStateNodes("video"),
        mirrorTrack: {
          always: {
            target: "idle",
          },
        },
      },
      on: {
        mirrorLocalVideoTrack: {
          target: ".mirrorTrack",
          actions: [
            assign({ isVideoMirrored: (context, event: MirrorLocalTrackEvent) => event.mirrored }),
            sendParent((context, event: MirrorLocalTrackEvent) => ({
              type: "onMirrorLocalVideoTrack",
              mirrored: event.mirrored,
            })),
          ],
        },
      },
    },
  },
  on: {
    setConference: {
      actions: [
        assign({
          conference: (
            context: MachineContext,
            event: { type: "setConference"; conference: JitsiConference }
          ) => event.conference,
        }),
        send((context: MachineContext) => ({
          type: "changeLocalAudioTrack",
          deviceId: context.audioTrack?.getDeviceId(),
        })),
        send((context: MachineContext) => ({
          type: "changeLocalVideoTrack",
          deviceId: context.videoTrack?.getDeviceId(),
        })),
      ],
    },
  },
});
