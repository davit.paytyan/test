import { InvokeCreator, sendParent } from "xstate";
import { createModel } from "xstate/lib/model";
import { ModelEventsFrom } from "xstate/lib/model.types";

export const LocalDeviceWatcherMachineModelCreator = {
  events: {
    onMediaDeviceListChanged: (devices: MediaDeviceInfo[]) => ({ devices }),
    onMediaDevicePermissionsChanged: () => ({}),
  },
};

export const LocalDeviceWatcherMachineModel = createModel(
  null,
  LocalDeviceWatcherMachineModelCreator
);

type Events = ModelEventsFrom<typeof LocalDeviceWatcherMachineModel>;

const callback: InvokeCreator<null, Events> = () => {
  return (callback) => {
    if (!window.JitsiMeetJS) {
      throw new Error("JitsiMeetJS is required!");
    }

    const sdk = window.JitsiMeetJS;
    const events = window.JitsiMeetJS.events.mediaDevices;

    function onDeviceListChanged(devices: MediaDeviceInfo[]) {
      callback({ type: "onMediaDeviceListChanged", devices });
    }

    function onPermissionChanged(event) {
      console.log("onPermissionChanged", event);
      callback({ type: "onMediaDevicePermissionsChanged" });
    }

    sdk.mediaDevices.addEventListener(events.DEVICE_LIST_CHANGED, onDeviceListChanged);
    sdk.mediaDevices.addEventListener(events.PERMISSIONS_CHANGED, onPermissionChanged);

    return () => {
      sdk.mediaDevices.removeEventListener(events.DEVICE_LIST_CHANGED, onDeviceListChanged);
      sdk.mediaDevices.removeEventListener(events.PERMISSIONS_CHANGED, onPermissionChanged);
    };
  };
};

export const LocalDeviceWatcherMachine = LocalDeviceWatcherMachineModel.createMachine({
  id: "jitsi-conference-tracks",
  invoke: {
    id: "local-media-device-watcher-callback",
    src: callback,
  },
  on: {
    "*": {
      actions: sendParent((context, event) => event),
    },
  },
});
