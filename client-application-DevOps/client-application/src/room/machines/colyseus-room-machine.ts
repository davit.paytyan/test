import { assign } from "@xstate/immer";
import { RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { forwardTo, InvokeCreator, sendParent } from "xstate";
import { createModel } from "xstate/lib/model";
import { ModelEventsFrom } from "xstate/lib/model.types";
import { Logger } from "../../global/logger";
import { RoomActor } from "./actors/room-actor";

type MachineContext = {
  colyseusRoom?: Room<RoomSchema>;
};

const initialContext: MachineContext = {};

const ColyseusRoomMachineModel = createModel(initialContext, {
  events: {
    setColyseusRoom: (room: Room<RoomSchema>) => ({ room }),
    leaveRoom: () => ({}),
    refreshTextboards: () => ({}),
    // callbacks
    onRoomLeft: () => ({}),
    onRoomError: (error: Error) => ({ error }),
    onRefreshTextboards: () => ({}),
  },
});

type Events = ModelEventsFrom<typeof ColyseusRoomMachineModel>;

const actorCallback: InvokeCreator<MachineContext, Events> = (context) => {
  let roomSet = false;

  const actor = new RoomActor("room");

  function eventListener(event: Events) {
    if (event.type === "setColyseusRoom") {
      if (roomSet) return Logger.warn("colyseus room was already set!");
      actor.send({ type: "onJoinRoomSuccessful", room: event.room });
      roomSet = true;
    }
    if (event.type === "leaveRoom") {
      actor.send(event);
    }
    if (event.type === "refreshTextboards") {
      actor.send(event);
    }
  }

  return (callback, onEvent) => {
    if (context.colyseusRoom) {
      actor.send({ type: "onJoinRoomSuccessful", room: context.colyseusRoom });
    }

    onEvent(eventListener);
    actor.subscribe(callback);

    return () => {
      actor.stop();
    };
  };
};

export const ColyseusRoomMachine = ColyseusRoomMachineModel.createMachine({
  id: "colyseus-room",
  context: ColyseusRoomMachineModel.initialContext,
  invoke: {
    id: "room-actor-callback",
    src: actorCallback,
  },
  on: {
    setColyseusRoom: {
      actions: [
        forwardTo("room-actor-callback"),
        assign((context, event) => (context.colyseusRoom = event.room)),
      ],
    },
    leaveRoom: {
      actions: forwardTo("room-actor-callback"),
    },
    refreshTextboards: {
      actions: forwardTo("room-actor-callback"),
    },
    onRoomLeft: {
      actions: sendParent((context, event) => event),
    },
    onRoomError: {
      actions: sendParent((context, event) => event),
    },
    onRefreshTextboards: {
      actions: sendParent((context, event) => event),
    },
  },
});
