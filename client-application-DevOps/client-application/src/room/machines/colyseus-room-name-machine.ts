import { assign } from "@xstate/immer";
import { RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { forwardTo, InvokeCreator, sendParent } from "xstate";
import { createModel } from "xstate/lib/model";
import { ModelEventsFrom } from "xstate/lib/model.types";
import { Logger } from "../../global/logger";
import { RoomNameActor } from "./actors/room-name-actor";

type MachineContext = {
  roomName: string;
  colyseusRoom?: Room<RoomSchema>;
};

const ColyseusRoomNameMachineModel = createModel({ roomName: "" } as MachineContext, {
  events: {
    setColyseusRoom: (room: Room<RoomSchema>) => ({ room }),
    setRoomName: (name: string) => ({ name }),
    // room name
    onUpdateRoomName: (name: string) => ({ name }),
    onSetRoomNameCompleted: () => ({}),
    onSetRoomNameFailed: (reason: string) => ({ reason }),
  },
});

type Events = ModelEventsFrom<typeof ColyseusRoomNameMachineModel>;

const actorCallback: InvokeCreator<MachineContext, Events> = (context) => {
  let roomSet = false;

  const actor = new RoomNameActor("room-name");

  function eventListener(event: Events) {
    if (event.type === "setColyseusRoom") {
      if (roomSet) return Logger.warn("colyseus room was already set!");
      actor.send({ type: "onJoinRoomSuccessful", room: event.room });
      roomSet = true;
    }
    if (event.type === "setRoomName") {
      actor.send(event);
    }
  }

  return (callback, onEvent) => {
    if (context.colyseusRoom) {
      actor.send({ type: "onJoinRoomSuccessful", room: context.colyseusRoom });
    }

    onEvent(eventListener);
    actor.subscribe(callback);

    return () => {
      actor.stop();
    };
  };
};

export const ColyseusRoomNameMachine = ColyseusRoomNameMachineModel.createMachine({
  id: "colyseus-room-name",
  context: ColyseusRoomNameMachineModel.initialContext,
  invoke: {
    id: "room-name-actor-callback",
    src: actorCallback,
  },
  on: {
    setColyseusRoom: {
      actions: [
        forwardTo("room-name-actor-callback"),
        assign((context, event) => (context.colyseusRoom = event.room)),
      ],
    },
    setRoomName: {
      actions: forwardTo("room-name-actor-callback"),
    },
    onUpdateRoomName: {
      actions: [
        sendParent((context, event) => event),
        assign((context, event) => {
          context.roomName = event.name;
        }),
      ],
    },
  },
});
