import JitsiConference, {
  JitsiConferenceOptionConfig,
} from "@together/lib-jitsi-meet/JitsiConference";
import { JitsiConnectionOptions } from "@together/lib-jitsi-meet/JitsiConnection";
import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
import { RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import Logger from "loglevel";
import { ActorRefFrom, assign, forwardTo, send, spawn } from "xstate";
import { createModel } from "xstate/lib/model";
import { ServerError } from "../../library/server-error";
import { ColyseusRoomMachine } from "./colyseus-room-machine";
import { ColyseusRoomNameMachine } from "./colyseus-room-name-machine";
import { ColyseusRoomParticipantsMachine } from "./colyseus-room-participants-machine";
import { ColyseusRoomStageMachine } from "./colyseus-room-stage-machine";
import { ColyseusRoomUserRoleMachine } from "./colyseus-room-user-role-machine";
import { JitsiMachine } from "./jitsi-machine";
import { LocalDeviceWatcherMachine } from "./local-device-watcher-machine";
import { LocalTracksMachine } from "./local-tracks-machine";
import { RoomModalsMachine } from "./room-modals-machine";
import { InitializeRoomMachine } from "./tasks/initialize-room-machine";
import { JoinColyseusRoomMachine } from "./tasks/join-colyseus-room-machine";
import { SetupConferenceMachine } from "./tasks/setup-conference-machine";

type MachineContext = {
  roomId: string;
  // TODO: flatten this data
  room?: {
    name: string;
    description?: string;
  };
  // TODO: flatten this data
  roomUser?: {
    userId: string;
    nickname: string;
    role: {
      id: string;
      name: string;
    };
    userColor: string;
  };
  roomUserNickname?: string;
  colyseusEndpoint: string;
  jitsiConnectionConfig: {
    appId?: string;
    token?: string;
    options: Partial<JitsiConnectionOptions>;
  };
  jitsiConferenceConfig: {
    options: JitsiConferenceOptionConfig;
  };
  devices?: MediaDeviceInfo[];
  audioTrack?: JitsiLocalTrack;
  videoTrack?: JitsiLocalTrack;
  colyseusRoom?: Room<RoomSchema>;
  jitsiConference?: JitsiConference;
  refs: {
    localTracks?: ActorRefFrom<typeof LocalTracksMachine>;
    jitsi?: ActorRefFrom<typeof JitsiMachine>;
  };
  localDeviceWatcherRef?: ActorRefFrom<typeof LocalDeviceWatcherMachine>;
  colyseusStageRef?: ActorRefFrom<typeof ColyseusRoomStageMachine>;
  colyseusParticipantsRef?: ActorRefFrom<typeof ColyseusRoomParticipantsMachine>;
  error?: ServerError;
};

const initialContext: MachineContext = {
  roomId: "",
  colyseusEndpoint: "",
  jitsiConnectionConfig: { options: {} },
  jitsiConferenceConfig: { options: {} },
  refs: {},
};

export const ConferenceMachineModel = createModel(initialContext, {
  events: {
    joinRoom: (nickname: string) => ({ nickname }),
    leaveRoom: () => ({}),
    // local tracks callbacks
    onChangeLocalAudioTrackDone: (track: JitsiLocalTrack) => ({ track }),
    onChangeLocalVideoTrackDone: (track: JitsiLocalTrack) => ({ track }),
    // local device watcher callback
    onMediaDeviceListChanged: (devices: MediaDeviceInfo[]) => ({ devices }),
    // room name callbacks
    onUpdateRoomName: (name: string) => ({ name }),
    // room user role callbacks
    onUpdateRoomUserRole: (id: string, name: string) => ({ id, name }),
    onRoomLeft: () => ({}),
    onRoomError: (error: ServerError) => ({ error }),
    // jitsi conference callbacks
    onConferenceJoined: (conference: JitsiConference) => ({ conference }),
  },
});

export const ConferenceMachine = ConferenceMachineModel.createMachine({
  id: "conference-machine",
  initial: "setup",
  states: {
    setup: {
      invoke: {
        id: "setupRoom",
        src: (context) => SetupConferenceMachine.withContext({ roomId: context.roomId }),
        onDone: [
          {
            cond: (context, event) => {
              const { error } = event.data;
              return typeof error !== "undefined";
            },
            target: "_redirectError",
            actions: assign({
              error: (context, event) => event.data.error,
            }),
          },
          {
            target: "initialize",
            actions: [
              assign((context, event) => {
                context.room = event.data.room;
                context.roomUser = event.data.roomUser;
                return context;
              }),
              assign({
                roomUserNickname: (context, event) => event.data.roomUser.nickname,
              }),
            ],
          },
        ],
      },
    },
    initialize: {
      invoke: {
        id: "initializeRroom",
        src: InitializeRoomMachine,
        onDone: [
          {
            cond: (context, event) => typeof event.data.error !== "undefined",
            target: "error",
          },
          {
            target: "initialized",
            actions: assign((context, event) => {
              context.devices = event.data.devices;
              context.audioTrack = event.data.audioTrack;
              context.videoTrack = event.data.videoTrack;
              return context;
            }),
          },
        ],
      },
    },
    initialized: {
      initial: "waitingRoom",
      entry: [
        assign((context) => {
          const jitsiMachineConfig = JitsiMachine.withContext({
            ...JitsiMachine.context,
            roomId: context.roomId,
            connectionConfig: context.jitsiConnectionConfig,
            conferenceConfig: context.jitsiConferenceConfig,
          });
          const jitsiRef = spawn(jitsiMachineConfig, "jitsi");
          context.refs.jitsi = jitsiRef;
          return context;
        }),
        assign((context) => {
          const localTracksMachineConfig = LocalTracksMachine.withContext({
            audioTrack: context.audioTrack,
            videoTrack: context.videoTrack,
          });
          const localTracksRef = spawn(localTracksMachineConfig, "local-tracks");
          context.refs.localTracks = localTracksRef;
          return context;
        }),
        assign({
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          localDeviceWatcherRef: (_context) => {
            return spawn(LocalDeviceWatcherMachine, "local-device-watcher");
          },
        }),
      ],
      states: {
        waitingRoom: {
          on: {
            joinRoom: {
              target: "joiningRoom",
              actions: assign({
                roomUserNickname: (context, event) => event.nickname,
              }),
            },
          },
        },
        joiningRoom: {
          entry: send({ type: "joinConference", password: "" }, { to: "jitsi" }),
          invoke: {
            id: "join-coylseus-room",
            src: (context) => {
              return JoinColyseusRoomMachine.withContext({
                roomId: context.roomId,
                userId: context.roomUser?.userId as string,
                nickname: context.roomUserNickname,
                isVideoMirrored: context.refs.localTracks?.getSnapshot()?.context.isVideoMirrored,
                endpoint: context.colyseusEndpoint,
              });
            },
            onDone: [
              {
                cond: (context, event) => typeof event.data.error !== "undefined",
                target: "roomError",
                actions: assign((context, event) => {
                  context.error = event.data.error;
                  return context;
                }),
              },
              {
                target: "roomJoined",
                actions: assign({
                  colyseusRoom: (context, event) => event.data.room,
                }),
              },
            ],
          },
        },
        roomJoined: {
          entry: [
            assign({
              colyseusStageRef: (context) => {
                const machineConfig = ColyseusRoomStageMachine.withContext({
                  ...ColyseusRoomStageMachine.context,
                  colyseusRoom: context.colyseusRoom,
                });

                // todo: unsubscribe from this and despawn
                const actorRef = spawn(machineConfig, "colyseus-room-stage");

                actorRef.subscribe((state) => {
                  // TODO: rework this into a single function combined with the one of onUpdateParticipants
                  if (state.changed && state.event.type === "onUpdateStageItems") {
                    const itemsOnStage = state.context.stageItems.length;

                    // if no items are on stage, wild view is active
                    if (itemsOnStage === 0 && context.colyseusRoom) {
                      const participantIds: string[] = [];

                      for (const participant of context.colyseusRoom.state.participants.values()) {
                        if (participant.conferenceUserId)
                          participantIds.push(participant.conferenceUserId);
                      }

                      return context.refs.jitsi?.send({
                        type: "updateParticipantsOnStage",
                        participantIds,
                        itemsOnStage: participantIds.length,
                      });
                    }

                    const participantIds = state.context.stageItems
                      .filter(({ type }) => type === "participant" || type === "shared_screen")
                      .filter((item) => Boolean(item.payload.conferenceUserId))
                      .map((item) => item.payload.conferenceUserId as string);

                    context.refs.jitsi?.send({
                      type: "updateParticipantsOnStage",
                      participantIds,
                      itemsOnStage,
                    });
                  }
                });

                return actorRef;
              },
              colyseusParticipantsRef: (context) => {
                const machineConfig = ColyseusRoomParticipantsMachine.withContext({
                  ...ColyseusRoomParticipantsMachine.context,
                  colyseusRoom: context.colyseusRoom,
                });

                const actorRef = spawn(machineConfig, "colyseus-participants");

                let previousParticipantsValue: string[] = [];

                actorRef.subscribe((state) => {
                  // TODO: rework this into a single function combined with the one of onUpdateStageItems
                  if (state.changed && state.event.type === "onUpdateParticipants") {
                    if (context.colyseusRoom && context.colyseusRoom.state.stage.size > 0) return;
                    if (state.context.participants.length === 0) return;

                    const participantIds: string[] = [];

                    for (const participant of state.context.participants) {
                      if (participant.conferenceUserId)
                        participantIds.push(participant.conferenceUserId);
                    }

                    if (participantIds.length === 0) return;

                    participantIds.sort();
                    previousParticipantsValue.sort();

                    if (participantIds.toString() === previousParticipantsValue.toString()) return;

                    previousParticipantsValue = participantIds;

                    context.refs.jitsi?.send({
                      type: "updateParticipantsOnStage",
                      participantIds,
                      itemsOnStage: participantIds.length,
                    });
                  }
                });

                if (context.jitsiConference) {
                  actorRef.send({
                    type: "setParticipantConferenceUserId",
                    conferenceUserId: context.jitsiConference.myUserId(),
                  });
                }

                return actorRef;
              },
            }),
          ],
          invoke: [
            {
              id: "colyseus-room",
              src: (context) => {
                return ColyseusRoomMachine.withContext({
                  colyseusRoom: context.colyseusRoom,
                });
              },
            },
            {
              id: "colyseus-room-user-role",
              src: (context) => {
                return ColyseusRoomUserRoleMachine.withContext({
                  id: context.roomUser?.role.id ?? "",
                  name: context.roomUser?.role.name ?? "",
                  colyseusRoom: context.colyseusRoom,
                });
              },
            },
            {
              id: "colyseus-room-name",
              src: (context) => {
                return ColyseusRoomNameMachine.withContext({
                  roomName: context.room?.name ?? "",
                  colyseusRoom: context.colyseusRoom,
                });
              },
            },
            {
              id: "room-modals",
              src: RoomModalsMachine,
            },
          ],
          on: {
            leaveRoom: {
              actions: forwardTo("colyseus-room"),
            },
            onRoomLeft: "roomLeft",
            onRoomError: {
              target: "roomError",
              actions: assign((context, event) => {
                context.error = event.error;
                return context;
              }),
            },
            onUpdateRoomName: {
              actions: assign((context, event) => {
                if (!context.room) return context;
                context.room.name = event.name;
                return context;
              }),
            },
            onUpdateRoomUserRole: {
              actions: assign((context, event) => {
                if (!context.roomUser) return context;
                context.roomUser.role.id = event.id;
                context.roomUser.role.name = event.name;
                return context;
              }),
            },
          },
        },
        roomLeft: {
          type: "final",
        },
        roomError: {
          type: "final",
        },
      },
      on: {
        onConferenceJoined: {
          actions: [
            assign({
              jitsiConference: (context, event) => event.conference,
            }),
            (context, event) => {
              context.refs.localTracks?.send({
                type: "setConference",
                conference: event.conference,
              });
              context.colyseusParticipantsRef?.send({
                type: "setParticipantConferenceUserId",
                conferenceUserId: event.conference.myUserId(),
              });
            },
          ],
        },
        onChangeLocalAudioTrackDone: {
          actions: assign({ audioTrack: (context, event) => event.track }),
        },
        onChangeLocalVideoTrackDone: {
          actions: assign({ videoTrack: (context, event) => event.track }),
        },
        onMediaDeviceListChanged: {
          actions: assign({
            devices: (context, event) => event.devices,
          }),
        },
      },
      onDone: [
        {
          cond: (context) => typeof context.error !== "undefined",
          target: "_redirectError",
        },
        {
          target: "terminated",
          actions: (context, event) => {
            Logger.error(context, event);
          },
        },
      ],
    },
    terminated: {
      type: "final",
    },
    _redirectError: {
      always: [
        {
          cond: (context) => {
            const { error } = context;
            return typeof error !== "undefined" && error.code === 403;
          },
          target: "roomAccessDenied",
        },
        {
          cond: (context) => {
            const { error } = context;
            return typeof error !== "undefined" && error.code === 404;
          },
          target: "roomNotFound",
        },
        {
          target: "error",
        },
      ],
    },
    roomNotFound: {},
    roomAccessDenied: {},
    error: {},
  },
});
