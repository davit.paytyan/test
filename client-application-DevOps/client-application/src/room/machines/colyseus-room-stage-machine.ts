import { assign } from "@xstate/immer";
import { RoomSchema } from "colyseus-schema";
import { Room } from "colyseus.js";
import { forwardTo, InvokeCreator, sendParent } from "xstate";
import { createModel } from "xstate/lib/model";
import { ModelEventsFrom } from "xstate/lib/model.types";
import { Logger } from "../../global/logger";
import { NewRoomStageItem, RoomStageActor, RoomStageItem } from "./actors/room-stage-actor";

type MachineContext = {
  colyseusRoom?: Room<RoomSchema>;
  stageItems: RoomStageItem[];
};

const initialContext: MachineContext = { stageItems: [] };

const ColyseusRoomStageMachineModel = createModel(initialContext, {
  events: {
    setColyseusRoom: (room: Room<RoomSchema>) => ({ room }),
    putItemOnStage: (item: NewRoomStageItem) => ({ item }),
    renameItemOnStage: (item: { id: string; title: string; type: string }) => ({ item }),
    removeItemFromStage: (id: string) => ({ id }),
    // stage
    onPutItemOnStageCompleted: (itemId: string) => ({ itemId }),
    onPutItemOnStageFailed: (itemId: string, reason: string) => ({ itemId, reason }),
    onRemoveItemFromStageCompleted: (itemId: string) => ({ itemId }),
    onRemoveItemFromStageFailed: (itemId: string, reason: string) => ({ itemId, reason }),
    onRenameItemOnStageCompleted: (itemId: string, itemType: string, newItemName: string) => ({
      itemId,
      itemType,
      newItemName,
    }),
    onRenameItemOnStageFailed: (itemId: string, reason: string) => ({ itemId, reason }),
    // stage items
    onUpdateStageItems: (items: RoomStageItem[]) => ({ items }),
    onStageItemAdded: (itemId: string) => ({ itemId }),
    onStageItemRemoved: (itemId: string) => ({ itemId }),
    onStageItemChanged: (itemId: string) => ({ itemId }),
  },
});

type Events = ModelEventsFrom<typeof ColyseusRoomStageMachineModel>;

const actorCallback: InvokeCreator<MachineContext, Events> = (context) => {
  let roomSet = false;

  const actor = new RoomStageActor("room-stage");

  function eventListener(event: Events) {
    if (event.type === "setColyseusRoom") {
      if (roomSet) return Logger.warn("colyseus room was already set!");
      actor.send({ type: "onJoinRoomSuccessful", room: event.room });
      roomSet = true;
    }

    if (event.type === "putItemOnStage") actor.send(event);
    if (event.type === "renameItemOnStage") actor.send(event);
    if (event.type === "removeItemFromStage") actor.send(event);
  }

  return (callback, onEvent) => {
    if (context.colyseusRoom) {
      actor.send({ type: "onJoinRoomSuccessful", room: context.colyseusRoom });
    }

    onEvent(eventListener);
    actor.subscribe(callback);

    return () => {
      actor.stop();
    };
  };
};

export const ColyseusRoomStageMachine = ColyseusRoomStageMachineModel.createMachine({
  id: "colyseus-room-stage",
  context: ColyseusRoomStageMachineModel.initialContext,
  invoke: {
    id: "room-stage-actor-callback",
    src: actorCallback,
  },
  on: {
    setColyseusRoom: {
      actions: [
        forwardTo("room-stage-actor-callback"),
        assign((context, event) => (context.colyseusRoom = event.room)),
      ],
    },
    putItemOnStage: {
      actions: forwardTo("room-stage-actor-callback"),
    },
    renameItemOnStage: {
      actions: forwardTo("room-stage-actor-callback"),
    },
    removeItemFromStage: {
      actions: forwardTo("room-stage-actor-callback"),
    },
    onUpdateStageItems: {
      actions: assign((context, event) => {
        context.stageItems = event.items;
      }),
    },
    onRenameItemOnStageCompleted: {
      actions: sendParent((context, event) => event),
    },
  },
});
