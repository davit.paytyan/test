import JitsiConference from "@together/lib-jitsi-meet/JitsiConference";
import { forwardTo, InvokeCreator, sendParent } from "xstate";
import { createModel } from "xstate/lib/model";
import { ModelEventsFrom } from "xstate/lib/model.types";
import {
  ConferenceErrorReason,
  ConferenceFailedReason,
  JitsiConferenceActor,
} from "./actors/jitsi-conference-actor";

type MachineContext = {
  conference: JitsiConference;
};

const initialContext: MachineContext = {
  // you *have* to provide the connection via `Machine.withContext()` method
  conference: undefined as unknown as JitsiConference,
};

export const JitsiConferenceMachineModelCreator = {
  events: {
    joinConference: (password?: string) => ({ password }),
    kickParticipant: (participantId: string, reason: string) => ({ participantId, reason }),
    leaveConference: () => ({}),
    // callbacks
    onConferenceJoined: (conference: JitsiConference) => ({ conference }),
    onConferenceLeft: (conference: JitsiConference) => ({ conference }),
    onConferenceKicked: (conference: JitsiConference) => ({ conference }),
    onConferenceError: (conference: JitsiConference, reason: ConferenceErrorReason) => ({
      conference,
      reason,
    }),
    onConferenceFailed: (conference: JitsiConference, reason: ConferenceFailedReason) => ({
      conference,
      reason,
    }),
  },
};

export const JitsiConferenceMachineModel = createModel(
  initialContext,
  JitsiConferenceMachineModelCreator
);

type Events = ModelEventsFrom<typeof JitsiConferenceMachineModel>;

const actorCallback: InvokeCreator<MachineContext, Events> = (context) => {
  const actor = new JitsiConferenceActor("jitsi-conference", context.conference);

  function eventListener(event: Events) {
    if (event.type === "joinConference") actor.send(event);
    if (event.type === "kickParticipant") actor.send(event);
    if (event.type === "leaveConference") actor.send(event);
  }

  return (callback, onEvent) => {
    onEvent(eventListener);
    actor.subscribe(callback);

    return () => {
      actor.stop();
    };
  };
};

export const JitsiConferenceMachine = JitsiConferenceMachineModel.createMachine({
  id: "jitsi-conference",
  context: JitsiConferenceMachineModel.initialContext,
  invoke: {
    id: "jitsi-conference-actor-callback",
    src: actorCallback,
  },
  on: {
    joinConference: {
      actions: forwardTo("jitsi-conference-actor-callback"),
    },
    leaveConference: {
      actions: forwardTo("jitsi-conference-actor-callback"),
    },
    kickParticipant: {
      actions: forwardTo("jitsi-conference-actor-callback"),
    },
    "*": {
      actions: sendParent((context, event) => event),
    },
  },
});
