import JitsiConference from "@together/lib-jitsi-meet/JitsiConference";
import { InvokeCreator, sendParent } from "xstate";
import { createModel } from "xstate/lib/model";
import { MediaTrack } from "../../library/utils/create-media-track";
import { JitsiConferenceTracksActor } from "./actors/jitsi-conference-tracks-actor";

type MachineContext = {
  conference: JitsiConference;
};

const initialContext: MachineContext = {
  // you *have* to provide the connection via `Machine.withContext()` method
  conference: undefined as unknown as JitsiConference,
};

export const JitsiConferenceTracksMachineModelCreator = {
  events: {
    onTrackAdded: (trackId: string, participantId: string, track: MediaTrack) => ({
      trackId,
      participantId,
      track,
    }),
    onTrackRemoved: (trackId: string, participantId: string, trackType: "audio" | "video") => ({
      trackId,
      participantId,
      trackType,
    }),
    onTrackMuteChanged: (
      trackId: string,
      participantId: string,
      trackType: "audio" | "video",
      trackMuted: boolean
    ) => ({
      trackId,
      participantId,
      trackType,
      trackMuted,
    }),
  },
};

export const JitsiConferenceTracksMachineModel = createModel(
  initialContext,
  JitsiConferenceTracksMachineModelCreator
);

const actorCallback: InvokeCreator<MachineContext, never> = (context) => {
  const actor = new JitsiConferenceTracksActor("jitsi-tracks", context.conference);

  return (callback) => {
    actor.subscribe(callback);
    return () => {
      actor.stop();
    };
  };
};

export const JitsiConferenceTracksMachine = JitsiConferenceTracksMachineModel.createMachine({
  id: "jitsi-conference-tracks",
  context: JitsiConferenceTracksMachineModel.initialContext,
  invoke: {
    id: "jitsi-conference-tracks-actor-callback",
    src: actorCallback,
  },
  on: {
    "*": {
      actions: sendParent((context, event) => event),
    },
  },
});
