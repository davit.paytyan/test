import {
  Form,
  Panel,
  PanelBody,
  PanelDivider,
  PanelHeader,
  PanelWrapper,
  Section,
  SectionItem,
} from "@pxwlab/katana-cb";
import LayoutOnboarding from "components/layout/LayoutOnboarding";
import React, { useCallback, useEffect } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { JoinRoomNicknameForm } from "../components/join-room-nickname-form";
import { JoinRoomVideoThumb } from "../components/join-room-video-thumb";
import { JoinRoomVideoThumbActions } from "../components/join-room-video-thumb-actions";
import { useRoomUserNickname } from "../hooks/room-user/use-room-user-nickname";
import { useActionJoinRoom } from "../hooks/room/use-action-join-room";
import { useRoomName } from "../hooks/room/use-room-name";

export function JoinRoomView() {
  const methods = useForm({
    defaultValues: { displayName: "" },
  });

  const roomName = useRoomName();
  const roomUserNickname = useRoomUserNickname();

  useEffect(() => {
    methods.setValue("displayName", roomUserNickname ?? "");
  }, [roomUserNickname, methods]);

  const joinRoom = useActionJoinRoom();

  const onSubmit = useCallback(() => {
    joinRoom(methods.getValues("displayName"));
  }, [joinRoom, methods]);

  return (
    <LayoutOnboarding>
      <Section center>
        <SectionItem>
          <Panel>
            <PanelWrapper>
              <PanelHeader title={`Joining room: ${roomName}`} />
              <PanelBody>
                <JoinRoomVideoThumb>
                  <JoinRoomVideoThumbActions />
                </JoinRoomVideoThumb>
                <PanelDivider />
                <FormProvider {...methods}>
                  <Form onSubmit={methods.handleSubmit(onSubmit)}>
                    <JoinRoomNicknameForm />
                  </Form>
                </FormProvider>
              </PanelBody>
            </PanelWrapper>
          </Panel>
        </SectionItem>
      </Section>
    </LayoutOnboarding>
  );
}
