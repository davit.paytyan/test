import { Alert, Button, Form, FormFooter } from "@pxwlab/katana-cb";
import React from "react";
import { LayoutPanel } from "components/layout/layout-panel";

export function RoomNotFoundView() {
  return (
    <LayoutPanel>
      <Alert badge="Room not found" message="The room you are requesting does not exist." />
      <Form>
        <FormFooter>
          <Button cta outline label="Visit help center" href="/help" />
        </FormFooter>
      </Form>
    </LayoutPanel>
  );
}
