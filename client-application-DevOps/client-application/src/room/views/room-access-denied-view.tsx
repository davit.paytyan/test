import { Alert, Button, Form, FormFooter } from "@pxwlab/katana-cb";
import React from "react";
import { LayoutPanel } from "components/layout/layout-panel";

export function RoomAccessDeniedVied() {
  return (
    <LayoutPanel>
      <Alert badge="Access Denied!" message="You don't have the permissions to access this room." />
      <Form>
        <FormFooter>
          <Button cta outline label="Visit help center" href="/help" />
        </FormFooter>
      </Form>
    </LayoutPanel>
  );
}
