import {
  Button,
  ButtonGroup,
  CoachingBoxAside,
  CoachingBoxBody,
  HeaderGlobals,
  ModalPortal,
} from "@pxwlab/katana-cb";
import { InviteParticipantsModal } from "components/pages/room/modals/invite-participants-modal";
import React from "react";
import { ContentUploadContextProvider } from "../../content-upload/components/content-upload-context";
import ContentUploadModal from "../../content-upload/components/content-upload-modal";
import { RoomAudience } from "../components/room-audience";
import { RoomAudio } from "../components/room-audio";
import { RoomContentBox } from "../components/room-content-box";
import { RoomContentPanel } from "../components/room-content-panel";
import RoomControlPanel from "../components/room-control-panel";
import { RoomFooter } from "../components/room-footer";
import { RoomHeader } from "../components/room-header";
import { RoomHeaderSettings } from "../components/room-header-settings";
import { RoomStage } from "../components/room-stage";
import { useActionCloseModal } from "../hooks/room-modals/use-action-close-modal";
import { useActionOpenModal } from "../hooks/room-modals/use-action-open-modal";
import { useStateCheckIsModalOpen } from "../hooks/room-modals/use-state-check-is-modal-open";
import { useRoomUserIsOwnerOrModerator } from "../hooks/room-user/use-room-user-is-owner-or-moderator";
import { useRoomId } from "../hooks/room/use-room-id";

export function RoomJoinedView() {
  const roomId = useRoomId();
  const isOwnerOrModerator = useRoomUserIsOwnerOrModerator();

  const isModalOpen = useStateCheckIsModalOpen();
  const openModal = useActionOpenModal();
  const closeModal = useActionCloseModal();

  return (
    <div className="cb-layout cb-layout--box">
      <ContentUploadContextProvider roomId={roomId}>
        <RoomHeader>
          {isOwnerOrModerator && (
            <HeaderGlobals>
              <ButtonGroup panel shadow>
                <Button
                  icon="add-line"
                  label="invite"
                  onClick={() => openModal("invite_participants")}
                  ghost
                />
              </ButtonGroup>
            </HeaderGlobals>
          )}
          <RoomHeaderSettings />
        </RoomHeader>
        <RoomContentBox>
          <CoachingBoxBody>
            <RoomStage />
            <RoomAudience />
          </CoachingBoxBody>
          <CoachingBoxAside>
            <RoomContentPanel />
          </CoachingBoxAside>
          <RoomAudio />
        </RoomContentBox>
        <RoomFooter>
          <RoomControlPanel />
        </RoomFooter>
        {isOwnerOrModerator && (
          <ModalPortal show={isModalOpen("invite_participants")}>
            <InviteParticipantsModal roomId={roomId} onClose={closeModal} />
          </ModalPortal>
        )}
        <ContentUploadModal />
      </ContentUploadContextProvider>
    </div>
  );
}
