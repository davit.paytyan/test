import Portal from "@reach/portal";
import Logger from "loglevel";
import getConfig from "next/config";
import React from "react";
import { useIsJitsiConferenceAudioTrackEnabled } from "../hooks/jitsi/use-is-jitsi-conference-audio-track-enabled";
import { useJitsiConferenceAudioTrack } from "../hooks/jitsi/use-jitsi-conference-audio-track";
import { useDataParticipants } from "../hooks/room-participants/use-data-participants";
import { Participant } from "../machines/actors/room-participants-actor";
import Audio from "./media/audio";

type RoomAudioCardProps = {
  item: Participant;
};

function RoomAudioCard({ item }: RoomAudioCardProps) {
  const audioTrack = useJitsiConferenceAudioTrack(item.userId);
  const isAudioEnabled = useIsJitsiConferenceAudioTrackEnabled(audioTrack);

  if (!isAudioEnabled) return null;
  if (audioTrack?.isLocal) return null;

  const { publicRuntimeConfig } = getConfig();
  const { audioOutputDisabled } = publicRuntimeConfig;

  if (audioOutputDisabled) {
    Logger.warn("#### WARNING: AUDIO OUTPUT IS DISABLED!!! ####");
    return null;
  }

  return <Audio track={audioTrack} autoPlay style={{ visibility: "hidden" }} />;
}

export function RoomAudio() {
  const participant = useDataParticipants();

  return (
    <Portal>
      {participant.map((participant) => {
        return <RoomAudioCard key={participant.userId} item={participant} />;
      })}
    </Portal>
  );
}
