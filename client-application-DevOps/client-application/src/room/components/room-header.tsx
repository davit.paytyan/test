import { Header } from "@pxwlab/katana-cb";
import { directus } from "global/singleton-directus-instance";
import { HEADER_LOGO_PATH } from "library/constants/assets";
import { OPEN_MY_ROOM_URL } from "library/constants/url-mapping";
import Logger from "loglevel";
import React, { useCallback, useEffect, useState } from "react";
import { useActionSetRoomName } from "../hooks/room/use-action-set-room-name";
import { useRoomUserIsOwnerOrModerator } from "../hooks/room-user/use-room-user-is-owner-or-moderator";
import { useRoomId } from "../hooks/room/use-room-id";
import { useRoomName } from "../hooks/room/use-room-name";

export function RoomHeader({ children }: { children?: React.ReactNode }) {
  const roomId = useRoomId();
  const roomName = useRoomName();
  const setRoomStageName = useActionSetRoomName();
  const isOwnerOrModerator = useRoomUserIsOwnerOrModerator();
  // TODO: Remove when onTitleBlur bug is fixed
  const [isEditing, setIsEditing] = useState(false);
  const [titleEditable, setTitleEditable] = useState(true);

  const [previousTitleBuffer, setPreviousTitleBuffer] = useState(roomName ?? "no-name");
  const [titleBuffer, setTitleBuffer] = useState(roomName ?? "no-name");
  const tagline = "together.biz";

  useEffect(() => {
    if (!isEditing) setTitleBuffer(roomName ?? "");
  }, [roomName, isEditing]);

  const blurTitleTextfield = useCallback(() => {
    if (typeof window !== "undefined" && document.activeElement)
      (document.activeElement as HTMLInputElement).blur();
  }, []);

  return (
    <Header
      box
      tagline={tagline}
      logo={HEADER_LOGO_PATH}
      logoLink={OPEN_MY_ROOM_URL}
      title={titleBuffer}
      titleEditable={isOwnerOrModerator && titleEditable}
      onTitleKeyDown={(event) => {
        if (event.key === "Enter") {
          if (!titleBuffer || titleBuffer.trim() === "") setTitleBuffer(previousTitleBuffer);
          blurTitleTextfield();
        }
        if (event.key === "Escape") {
          setTitleBuffer(previousTitleBuffer);
          blurTitleTextfield();
        }
      }}
      onTitleFocus={() => {
        setIsEditing(true);
        setPreviousTitleBuffer(titleBuffer);
      }}
      onTitleBlur={async () => {
        if (!isEditing) return;
        setIsEditing(false);
        try {
          setTitleEditable(false);
          await directus.items("rooms").updateOne(roomId, { name: titleBuffer });
          setTitleEditable(true);
        } catch (error) {
          Logger.error(error);
        }
      }}
      onTitleChange={(value: string) => {
        setRoomStageName(value);
        setTitleBuffer(value);
      }}
    >
      {children}
    </Header>
  );
}
