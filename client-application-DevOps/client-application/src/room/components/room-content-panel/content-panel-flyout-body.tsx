import { ContentPanelBody } from "@pxwlab/katana-cb";
import React from "react";
import { ContentPanelFlyoutFilterMenu } from "./content-panel-flyout-filter-menu";
import { ContentPanelFlyoutList } from "./content-panel-flyout-list";

export function ContentPanelFlyoutBody() {
  return (
    <ContentPanelBody filter={<ContentPanelFlyoutFilterMenu />}>
      <ContentPanelFlyoutList />
    </ContentPanelBody>
  );
}
