import {
  Button,
  ContentPanelHeader,
  ContentPanelHeaderActions,
  ContentPanelHeaderActionsItem,
} from "@pxwlab/katana-cb";
import React from "react";
import { useContentUploadContext } from "../../../content-upload/components/content-upload-context";
import { useContentPanelType } from "../../container/content-panel-context";
import { useActionCloseContentPanel } from "../../hooks/room-content/use-action-close-content-panel";
import { useActionCloseContentPanelActions } from "../../hooks/room-content/use-action-close-content-panel-actions";
import { useActionCreateContentItem } from "../../hooks/room-content/use-action-create-content-item";
import { useActionOpenContentPanelActions } from "../../hooks/room-content/use-action-open-content-panel-actions";
import { useActionOpenUploadModal } from "../../hooks/room-content/use-action-open-upload-modal";
import { useStateAreContentPanelActionsOpen } from "../../hooks/room-content/use-state-are-content-panel-actions-open";

function DirectusContentVariant() {
  const closeContentPanel = useActionCloseContentPanel();
  const openUploadModal = useActionOpenUploadModal();
  const upload = useContentUploadContext();

  const { isUploadInProgress, uploadCount } = upload;

  return (
    <ContentPanelHeader title="Your Documents" showCloseButton onClickClose={closeContentPanel}>
      {!isUploadInProgress && (
        <Button
          utilClassNames="cb-content-panel__header-toggle"
          cta
          outline
          label="Upload images"
          icon="upload-cloud-2-line"
          onClick={() => {
            openUploadModal();
            closeContentPanel();
          }}
        />
      )}
      {isUploadInProgress && (
        <Button
          utilClassNames="cb-content-panel__header-toggle"
          loading
          outline
          cta
          badgeTriangle
          label="Uploading"
          icon="upload-cloud-2-line"
          count={uploadCount.toString()}
          onClick={() => {
            openUploadModal();
            closeContentPanel();
          }}
        />
      )}
    </ContentPanelHeader>
  );
}

function getFormattedCurrentDate() {
  const date = new Date();
  return date.toLocaleDateString("de-DE", {
    year: "2-digit",
    month: "2-digit",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  });
}

function TextpadTemplates() {
  const createContentItem = useActionCreateContentItem();
  const closeActions = useActionCloseContentPanelActions();

  return (
    <>
      <ContentPanelHeaderActions>
        <ContentPanelHeaderActionsItem
          label="empty document"
          tooltip="Create a new empty textboard"
          onClick={() => {
            const formattedDate = getFormattedCurrentDate();
            closeActions();
            createContentItem(`Textboard, ${formattedDate}`);
          }}
        />
        <ContentPanelHeaderActionsItem
          label="Simple Agenda Template"
          tooltip="Create a new textboard using the 'Simple Agenda' template"
          onClick={() => {
            const formattedDate = getFormattedCurrentDate();
            const options = { template: "simple-agenda" };
            closeActions();
            createContentItem(`Agenda Meetingtitle, ${formattedDate}`, options);
          }}
        />
        <ContentPanelHeaderActionsItem
          label="Advanced Agenda Template"
          tooltip="Create a new textboard using the 'Advanced Agenda' template"
          onClick={() => {
            const formattedDate = getFormattedCurrentDate();
            const options = { template: "advanced-agenda" };
            closeActions();
            createContentItem(`Agenda Meetingtitle, ${formattedDate}`, options);
          }}
        />
        <ContentPanelHeaderActionsItem
          label="Decision Template"
          tooltip="Create a new textboard using the 'Decision' template"
          onClick={() => {
            const formattedDate = getFormattedCurrentDate();
            const options = { template: "decision" };
            closeActions();
            createContentItem(`Decision, ${formattedDate}`, options);
          }}
        />
        <ContentPanelHeaderActionsItem
          label="Meeting Minutes Template"
          tooltip="Create a new textboard using the 'Meeting Minutes' template"
          onClick={() => {
            const formattedDate = getFormattedCurrentDate();
            const options = { template: "meeting-minutes" };
            closeActions();
            createContentItem(`Minutes Meetingtitle, ${formattedDate}`, options);
          }}
        />
      </ContentPanelHeaderActions>
      <Button outline cta label="Cancel" onClick={closeActions} />
    </>
  );
}

function TextpadVariant() {
  const closeContentPanel = useActionCloseContentPanel();

  const openActions = useActionOpenContentPanelActions();
  const closeActions = useActionCloseContentPanelActions();
  const areActionsOpen = useStateAreContentPanelActionsOpen();

  return (
    <ContentPanelHeader
      title="Shared Textboards"
      showCloseButton
      showBackButton={areActionsOpen}
      onClickBack={closeActions}
      onClickClose={closeContentPanel}
    >
      {!areActionsOpen && (
        <Button
          utilClassNames="cb-content-panel__header-toggle"
          cta
          outline
          label="Add new textboard"
          icon="file-add-line"
          onClick={openActions}
        />
      )}
      {areActionsOpen && <TextpadTemplates />}
    </ContentPanelHeader>
  );
}

export function ContentPanelFlyoutHeader() {
  const contentPanelType = useContentPanelType();

  if (contentPanelType === "directus_files") return <DirectusContentVariant />;
  if (contentPanelType === "textpad") return <TextpadVariant />;

  return null;
}
