import { ContentPanelFilter } from "@pxwlab/katana-cb";
import React from "react";
import { useActionDeselectAllContentPanelItem } from "../../hooks/room-content/use-action-deselect-all-content-panel-items";
import { useActionSetContentPanelSearchTerm } from "../../hooks/room-content/use-action-set-content-panel-filter";
import { useDataContentPanelSearchTerm } from "../../hooks/room-content/use-data-content-panel-search-term";

export function ContentPanelFlyoutFilterMenu() {
  const setContentPanelSearchTerm = useActionSetContentPanelSearchTerm();
  const contentPanelSearchTerm = useDataContentPanelSearchTerm();
  const deselectAllItems = useActionDeselectAllContentPanelItem();

  return (
    <ContentPanelFilter
      search
      placeholder="Search title"
      searchValue={contentPanelSearchTerm}
      onSearchChange={(event) => {
        setContentPanelSearchTerm(event.target.value);
        deselectAllItems();
      }}
    ></ContentPanelFilter>
  );
}
