import { ContentPanelItem } from "@pxwlab/katana-cb";
import React, { useCallback, useEffect, useState } from "react";
import { Draggable } from "react-beautiful-dnd";
import { useActionDeleteContentPanelItem } from "../../hooks/room-content/use-action-delete-content-panel-item";
import { useActionDeselectContentPanelItem } from "../../hooks/room-content/use-action-deselect-content-panel-item";
import { useActionPutItemOnStage } from "../../hooks/room-stage/use-action-put-item-on-stage";
import { useActionRemoveItemFromStage } from "../../hooks/room-stage/use-action-remove-item-from-stage";
import { useActionRenameContentPanelItem } from "../../hooks/room-content/use-action-rename-content-panel-item";
import { useActionRenameItemOnStage } from "../../hooks/room-stage/use-action-rename-item-on-stage";
import { useActionSelectContentPanelItem } from "../../hooks/room-content/use-action-select-content-panel-item";
import { useDataContentPanelListItem } from "../../hooks/room-content/use-data-content-panel-list-item";
import { useGeneratedImageUrl } from "../../hooks/use-generated-image-url";
import { useStateIsContentPanelItemOnStage } from "../../hooks/room-content/use-state-is-content-panel-item-on-stage";
import { useStateIsContentPanelItemSelected } from "../../hooks/room-content/use-state-is-content-panel-item-selected";
import { useStateIsContentPanelSearchTermSet } from "../../hooks/room-content/use-state-is-content-panel-search-term-set";

type Props = {
  itemId: string;
  index: number;
};

export function ContentPanelListItem({ itemId, index }: Props) {
  const putItemOnStage = useActionPutItemOnStage();
  const removeItemFromStage = useActionRemoveItemFromStage();
  const selectItem = useActionSelectContentPanelItem();
  const deselectItem = useActionDeselectContentPanelItem();
  const deleteItem = useActionDeleteContentPanelItem();
  const renameItem = useActionRenameContentPanelItem();
  const renameItemOnStage = useActionRenameItemOnStage();
  const isSearchTermSet = useStateIsContentPanelSearchTermSet();

  const item = useDataContentPanelListItem(itemId);
  const isSelected = useStateIsContentPanelItemSelected(itemId);
  const isOnStage = useStateIsContentPanelItemOnStage(itemId);
  const thumbnailUrl = useGeneratedImageUrl(item?.file?.id, {
    fit: "cover",
    width: 64,
    height: 64,
  });

  const [previousTitleBuffer, setPreviousTitleBuffer] = useState(item?.title);
  const [titleBuffer, setTitleBuffer] = useState(item?.title);

  useEffect(() => {
    if (item?.title) setTitleBuffer(item?.title);
  }, [item?.title]);

  const blurTitleTextfield = useCallback(() => {
    if (typeof window !== "undefined" && document.activeElement)
      (document.activeElement as HTMLInputElement).blur();
  }, []);

  if (!item) return null;

  return (
    <Draggable key={item.id} index={index} draggableId={item.id} isDragDisabled={isSearchTermSet}>
      {(provided) => {
        return (
          <ContentPanelItem
            index={item.index}
            draggableProps={provided.draggableProps}
            dragHandleProps={provided.dragHandleProps}
            innerRef={provided.innerRef}
            onStage={isOnStage}
            image={thumbnailUrl}
            onStageClick={() => {
              if (isOnStage) {
                removeItemFromStage(itemId);
              } else {
                putItemOnStage({
                  id: itemId,
                  type: item.type,
                  title: item.title,
                  payload: item.file?.id,
                });
              }
            }}
            type={item.type === "textpad" ? "board-text" : "image"}
            title={titleBuffer}
            onTitleKeyDown={(event) => {
              if (event.key === "Enter") {
                if (!titleBuffer || titleBuffer.trim() === "") setTitleBuffer(previousTitleBuffer);
                blurTitleTextfield();
              }
              if (event.key === "Escape") {
                setTitleBuffer(previousTitleBuffer);
                blurTitleTextfield();
              }
            }}
            onTitleFocus={() => {
              setPreviousTitleBuffer(titleBuffer);
            }}
            onTitleBlur={() => {
              renameItem(itemId, titleBuffer ?? item.title);
              if (isOnStage) renameItemOnStage(item.id, titleBuffer ?? item.title, item.type);
            }}
            onTitleChange={(value: string) => {
              setTitleBuffer(value);
            }}
            selected={isSelected}
            onSelect={() => {
              if (isSelected) {
                deselectItem(itemId);
              } else {
                selectItem(itemId);
              }
            }}
            onDelete={() => {
              deleteItem(itemId);
              if (isOnStage) removeItemFromStage(itemId);
            }}
          />
        );
      }}
    </Draggable>
  );
}
