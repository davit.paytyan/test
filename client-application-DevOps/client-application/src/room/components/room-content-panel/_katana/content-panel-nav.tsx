import React, { RefObject } from "react";

type Props = {
  children?: React.ReactNode;
  innerRef?: RefObject<HTMLDivElement>;
};

export function ContentPanelNav({ children, innerRef }: Props) {
  return (
    <div ref={innerRef} className="cb-content-panel__nav">
      {children}
    </div>
  );
}
