import {
  ContentPanelFilter,
  ContentPanelFilterItem,
  ContentPanelFooter,
  FormCheckbox,
} from "@pxwlab/katana-cb";
import React from "react";
import { useActionClearUsersContentFromStage } from "../../hooks/room-content/use-action-clear-users-content-from-stage";
import { useActionDeleteSelectedContentPanelItems } from "../../hooks/room-content/use-action-delete-selected-content-panel-items";
import { useActionDeselectAllContentPanelItem } from "../../hooks/room-content/use-action-deselect-all-content-panel-items";
import { useActionSelectAllContentPanelItems } from "../../hooks/room-content/use-action-select-all-content-panel-items";
import { useDataContentPanelSelectedItems } from "../../hooks/room-content/use-data-content-panel-selected-items";
import { useStateAreAllContentPanelItemsSelected } from "../../hooks/room-content/use-state-are-all-content-panel-items-selected";

export function ContentPanelFlyoutFooter() {
  const selectedItems = useDataContentPanelSelectedItems();
  const selectAllItems = useActionSelectAllContentPanelItems();
  const deselectAllItems = useActionDeselectAllContentPanelItem();
  const deleteSelectedItems = useActionDeleteSelectedContentPanelItems();
  const allItemsSelected = useStateAreAllContentPanelItemsSelected();
  const clearStage = useActionClearUsersContentFromStage();

  return (
    <ContentPanelFooter>
      <ContentPanelFilter bottom clear onClear={() => clearStage()}>
        {selectedItems.size > 0 && (
          <ContentPanelFilterItem
            icon="delete-bin-line"
            onClick={() => {
              // eslint-disable-next-line no-alert
              if (confirm("Delete selected items?")) deleteSelectedItems();
            }}
          />
        )}
        <FormCheckbox
          onChange={() => {
            if (allItemsSelected) {
              deselectAllItems();
            } else {
              selectAllItems();
            }
          }}
          checked={allItemsSelected}
        />
      </ContentPanelFilter>
    </ContentPanelFooter>
  );
}
