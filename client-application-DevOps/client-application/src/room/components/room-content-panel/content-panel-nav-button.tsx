import { Button } from "@pxwlab/katana-cb";
import React from "react";
import { useActionCloseContentPanel } from "../../hooks/room-content/use-action-close-content-panel";
import { useActionOpenContentPanel } from "../../hooks/room-content/use-action-open-content-panel";
import { useStateIsContentPanelOpen } from "../../hooks/room-content/use-state-is-content-panel-open";

export function ContentPanelNavButton({ icon }: { icon: string }) {
  const isContentPanelOpen = useStateIsContentPanelOpen();
  const openContentPanel = useActionOpenContentPanel();
  const closeContentPanel = useActionCloseContentPanel();
  return (
    <Button
      square
      link
      icon={icon}
      active={isContentPanelOpen}
      onClick={() => {
        if (isContentPanelOpen) {
          closeContentPanel();
        } else {
          openContentPanel();
        }
      }}
    />
  );
}
