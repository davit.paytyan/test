import React from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { useActionMoveContentPanelItemToIndex } from "../../hooks/room-content/use-action-move-content-panel-item-to-index";
import { useDataContentPanelListItems } from "../../hooks/room-content/use-data-content-panel-list-items";
import { ContentPanelListItem } from "./content-panel-flyout-list-item";

export function ContentPanelFlyoutList() {
  const contentPanelItems = useDataContentPanelListItems();
  const moveItemToIndex = useActionMoveContentPanelItemToIndex();
  return (
    <DragDropContext
      onDragUpdate={(result) => {
        moveItemToIndex(result.draggableId, result.destination?.index ?? result.source.index);
      }}
      onDragEnd={(result) => {
        moveItemToIndex(result.draggableId, result.destination?.index ?? result.source.index, true);
      }}
    >
      <Droppable droppableId="content-control-panel-contents">
        {(provided) => {
          return (
            <div {...provided.droppableProps} ref={provided.innerRef}>
              {contentPanelItems?.map((itemId, index) => (
                <ContentPanelListItem key={itemId} index={index} itemId={itemId} />
              ))}
              {provided.placeholder}
            </div>
          );
        }}
      </Droppable>
    </DragDropContext>
  );
}
