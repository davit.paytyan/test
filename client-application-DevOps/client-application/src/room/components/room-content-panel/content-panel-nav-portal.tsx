import React, { RefObject } from "react";
import { createPortal } from "react-dom";

type NavPortalProps = { children: React.ReactNode; portalElementRef: RefObject<HTMLDivElement> };

export function ContentPanelNavPortal({ children, portalElementRef }: NavPortalProps) {
  if (!portalElementRef.current) return null;
  return createPortal(children, portalElementRef.current);
}
