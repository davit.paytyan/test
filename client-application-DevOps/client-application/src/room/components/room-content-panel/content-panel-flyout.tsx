import { ContentPanelWrapper } from "@pxwlab/katana-cb";
import React from "react";
import { useStateAreContentPanelActionsOpen } from "../../hooks/room-content/use-state-are-content-panel-actions-open";
import { useStateIsContentPanelListFilled } from "../../hooks/room-content/use-state-is-content-panel-list-filled";
import { useStateIsContentPanelOpen } from "../../hooks/room-content/use-state-is-content-panel-open";
import { ContentPanelFlyoutBody } from "./content-panel-flyout-body";
import { ContentPanelFlyoutFooter } from "./content-panel-flyout-footer";
import { ContentPanelFlyoutHeader } from "./content-panel-flyout-header";

export function ContentPanelFlyout() {
  const isContentPanelOpen = useStateIsContentPanelOpen();
  const isContentPanelListFilled = useStateIsContentPanelListFilled();
  const areActionsOpen = useStateAreContentPanelActionsOpen();

  if (!isContentPanelOpen) return null;

  return (
    <ContentPanelWrapper show={isContentPanelOpen}>
      <ContentPanelFlyoutHeader />
      {!areActionsOpen && isContentPanelListFilled && (
        <>
          <ContentPanelFlyoutBody />
          <ContentPanelFlyoutFooter />
        </>
      )}
    </ContentPanelWrapper>
  );
}
