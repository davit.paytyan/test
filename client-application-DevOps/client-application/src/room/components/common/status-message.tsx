import React, { useEffect, useRef } from "react";
import { Autolinker } from "autolinker";

export default function StatusMessage({ text }: { text: string }) {
  const divElement = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const parsedText = Autolinker.link(text, {
      truncate: { location: "smart", length: 24 },
      sanitizeHtml: true,
    });
    if (divElement.current) divElement.current.innerHTML = parsedText;
  }, [text]);

  return <div ref={divElement} />;
}
