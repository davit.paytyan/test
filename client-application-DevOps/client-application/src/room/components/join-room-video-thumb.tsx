import { VideoThumb } from "@pxwlab/katana-cb";
import React from "react";
import { useActionMuteLocalAudioTrack } from "../hooks/local-tracks/use-action-mute-local-audio-track";
import { useActionMuteLocalVideoTrack } from "../hooks/local-tracks/use-action-mute-local-video-track";
import { useActionUnmuteLocalAudioTrack } from "../hooks/local-tracks/use-action-unmute-local-audio-track";
import { useActionUnmuteLocalVideoTrack } from "../hooks/local-tracks/use-action-unmute-local-video-track";
import { useCurrentLocalVideoTrack } from "../hooks/local-tracks/use-current-local-video-track";
import { useStateIsLocalAudioTrackMuted } from "../hooks/local-tracks/use-state-is-local-audio-track-muted";
import { useStateIsLocalVideoTrackMirrored } from "../hooks/local-tracks/use-state-is-local-video-track-mirrored";
import { useStateIsLocalVideoTrackMuted } from "../hooks/local-tracks/use-state-is-local-video-track-muted";
import { VideoTrack } from "./media/video-track";

export function JoinRoomVideoThumb({ children }: Props) {
  const isLocalAudioTrackMuted = useStateIsLocalAudioTrackMuted();
  const isLocalVideoTrackMuted = useStateIsLocalVideoTrackMuted();

  const muteLocalVideoTrack = useActionMuteLocalVideoTrack();
  const unmuteLocalVideoTrack = useActionUnmuteLocalVideoTrack();
  const muteLocalAudioTrack = useActionMuteLocalAudioTrack();
  const unmuteLocalAudioTrack = useActionUnmuteLocalAudioTrack();

  const isLocalVideoTrackMirrored = useStateIsLocalVideoTrackMirrored();

  const videoTrack = useCurrentLocalVideoTrack();

  return (
    <VideoThumb
      video={<VideoTrack track={videoTrack} autoPlay mirrored={isLocalVideoTrackMirrored} />}
      messageVideo="Your Webcam is disabled. We like video! To be a conductive meeting member, please consider using your webcam."
      labelPermission="Visit help center"
      messagePermission="Seems like the Together.biz App has no permission to access your video or audio devices. Please enable permission in your browser settings."
      audioDisabled={isLocalAudioTrackMuted}
      videoDisabled={isLocalVideoTrackMuted}
      onToggleAudio={() => {
        if (isLocalAudioTrackMuted) {
          unmuteLocalAudioTrack();
        } else {
          muteLocalAudioTrack();
        }
      }}
      onToggleVideo={() => {
        if (isLocalVideoTrackMuted) {
          unmuteLocalVideoTrack();
        } else {
          muteLocalVideoTrack();
        }
      }}
    >
      {children}
    </VideoThumb>
  );
}

type Props = {
  children?: React.ReactNode;
};
