import { Button, ButtonGroup, FormDropdown, FormDropdownItem } from "@pxwlab/katana-cb";
import React from "react";
import { useActionMuteLocalAudioTrack } from "../../hooks/local-tracks/use-action-mute-local-audio-track";
import { useActionUnmuteLocalAudioTrack } from "../../hooks/local-tracks/use-action-unmute-local-audio-track";
import { useCurrentLocalAudioTrack } from "../../hooks/local-tracks/use-current-local-audio-track";
import { useStateIsLocalAudioTrackMuted } from "../../hooks/local-tracks/use-state-is-local-audio-track-muted";
import { useActionChangeAudioInputDevice } from "../../hooks/media-devices/use-action-change-audio-input-device";
import { useActionChangeAudioOutputDevice } from "../../hooks/media-devices/use-action-change-audio-output-device";
import { useAvailableAudioInputDevices } from "../../hooks/media-devices/use-available-audio-input-devices";
import { useAvailableAudioOutputDevices } from "../../hooks/media-devices/use-available-audio-output-devices";
import { useCurrentLocalAudioDeviceInfo } from "../../hooks/media-devices/use-current-local-audio-device-info";

export function LocalAudioControlElements() {
  const localAudioTrack = useCurrentLocalAudioTrack();
  const isLocalAudioTrackSet = Boolean(localAudioTrack);

  const isLocalAudioTrackMuted = useStateIsLocalAudioTrackMuted();

  const muteLocalAudioTrack = useActionMuteLocalAudioTrack();
  const unmuteLocalAudioTrack = useActionUnmuteLocalAudioTrack();

  const isChangeOutputDeviceEnabled = false;

  const audioDeviceInfo = useCurrentLocalAudioDeviceInfo();

  const availableAudioInputDevices = useAvailableAudioInputDevices();
  const availableAudioOutputDevices = useAvailableAudioOutputDevices();

  const changeAudioInputDevice = useActionChangeAudioInputDevice();
  const changeAudioOutputDevice = useActionChangeAudioOutputDevice();

  return (
    <ButtonGroup panel utilClassNames="cb-video-control-panel__switch">
      <Button
        link
        square
        active={isLocalAudioTrackSet}
        icon={isLocalAudioTrackMuted ? "mic-off-line" : "mic-line"}
        onClick={() => {
          if (isLocalAudioTrackMuted) {
            unmuteLocalAudioTrack();
          } else {
            muteLocalAudioTrack();
          }
        }}
      />
      <FormDropdown top>
        <FormDropdownItem label={isChangeOutputDeviceEnabled ? "Microphone" : "Audio"} />
        {availableAudioInputDevices?.map((device) => {
          const isSelected = device.deviceId === audioDeviceInfo?.deviceId;
          return (
            <FormDropdownItem
              key={device.deviceId}
              label={device.label}
              icon={isSelected ? "checkbox-circle-line" : "checkbox-blank-circle-line"}
              onClick={() => changeAudioInputDevice(device.deviceId)}
            />
          );
        })}
        {isChangeOutputDeviceEnabled && <FormDropdownItem divider label="Speaker" />}
        {isChangeOutputDeviceEnabled &&
          availableAudioOutputDevices?.map((device) => {
            const isSelected = device.deviceId === audioDeviceInfo?.deviceId;
            return (
              <FormDropdownItem
                key={device.deviceId}
                label={device.label}
                icon={isSelected ? "checkbox-circle-line" : "checkbox-blank-circle-line"}
                onClick={() => changeAudioOutputDevice(device.deviceId)}
              />
            );
          })}
        <FormDropdownItem divider label="Settings" />
        <FormDropdownItem label="Visit help center" icon="question-line" />
      </FormDropdown>
    </ButtonGroup>
  );
}
