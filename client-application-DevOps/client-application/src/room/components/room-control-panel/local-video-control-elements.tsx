import { Button, ButtonGroup, FormDropdown, FormDropdownItem } from "@pxwlab/katana-cb";
import React from "react";
import { useActionMuteLocalVideoTrack } from "../../hooks/local-tracks/use-action-mute-local-video-track";
import { useActionUnmuteLocalVideoTrack } from "../../hooks/local-tracks/use-action-unmute-local-video-track";
import { useCurrentLocalVideoTrack } from "../../hooks/local-tracks/use-current-local-video-track";
import { useStateIsLocalVideoTrackMuted } from "../../hooks/local-tracks/use-state-is-local-video-track-muted";
import { useActionChangeVideoInputDevice } from "../../hooks/media-devices/use-action-change-video-input-device";
import { useAvailableVideoInputDevices } from "../../hooks/media-devices/use-available-video-input-devices";
import { useCurrentLocalVideoDeviceInfo } from "../../hooks/media-devices/use-current-local-video-device-info";
import { useActionMirrorVideoTrack } from "../../hooks/room-participants/use-action-mirror-video-track";
import { useDataParticipant } from "../../hooks/room-participants/use-data-participant";
import { useRoomUserUserId } from "../../hooks/room-user/use-room-user-user-id";

export function LocalVideoControlElements() {
  const localVideoTrack = useCurrentLocalVideoTrack();
  const isLocalVideoTrackSet = Boolean(localVideoTrack);

  const isLocalVideoTrackMuted = useStateIsLocalVideoTrackMuted();
  const muteLocalVideoTrack = useActionMuteLocalVideoTrack();
  const unmuteLocalVideoTrack = useActionUnmuteLocalVideoTrack();

  const videoDeviceInfo = useCurrentLocalVideoDeviceInfo();
  const availableVideoInputDevices = useAvailableVideoInputDevices();
  const changeVideoInputDevice = useActionChangeVideoInputDevice();

  const userId = useRoomUserUserId();
  const participant = useDataParticipant(userId);
  const mirrorVideoTrack = useActionMirrorVideoTrack();

  const isVideoMirrored = participant?.isVideoMirrored;

  return (
    <ButtonGroup panel utilClassNames="cb-video-control-panel__switch">
      <Button
        link
        square
        active={isLocalVideoTrackSet}
        icon={isLocalVideoTrackMuted ? "vidicon-off-line" : "vidicon-line"}
        onClick={() => {
          if (isLocalVideoTrackMuted) {
            unmuteLocalVideoTrack();
          } else {
            muteLocalVideoTrack();
          }
        }}
      />
      <FormDropdown top>
        <FormDropdownItem label="Camera" />
        {availableVideoInputDevices?.map((device) => {
          const isSelected = device.deviceId === videoDeviceInfo?.deviceId;
          return (
            <FormDropdownItem
              key={device.deviceId}
              label={device.label}
              icon={isSelected ? "checkbox-circle-line" : "checkbox-blank-circle-line"}
              onClick={() => changeVideoInputDevice(device.deviceId)}
            />
          );
        })}
        <FormDropdownItem divider label="Settings" />
        <FormDropdownItem
          label="Mirror my camera"
          icon={isVideoMirrored ? "toggle-fill" : "toggle-line"}
          onClick={() => {
            mirrorVideoTrack(!isVideoMirrored);
          }}
        />
        <FormDropdownItem label="Visit help center" icon="question-line" />
      </FormDropdown>
    </ButtonGroup>
  );
}
