import {
  Button,
  ButtonGroup,
  FormTextarea,
  VideoControlPanelInteractionGroup,
} from "@pxwlab/katana-cb";
import Logger from "loglevel";
import React, { useState } from "react";

export function FreeTextReactionInput({ selected, setQuickReaction, clearQuickReaction }: Props) {
  const active = selected?.type === "free-text" && selected.value !== "";
  const [freeFormText, setFreeFormText] = useState(active ? selected?.value : "");
  const [clipboardDisabled, setClipboardDisabled] = useState(false);

  return (
    <VideoControlPanelInteractionGroup>
      <FormTextarea
        name="quick-answer-text"
        placeholder="Ihre Antwort (120 Zeichen max.)"
        maxlength="120"
        value={freeFormText}
        onChange={(event) => {
          setFreeFormText(event.target.value);
        }}
      />
      <ButtonGroup panel shadow>
        <Button
          icon="paste-line"
          outline
          disabled={clipboardDisabled}
          onClick={() => {
            window.navigator.clipboard
              .readText()
              .then((text) => {
                setFreeFormText(text);
              })
              .catch((error) => {
                Logger.warn("Clipboard:", error.message);
                // eslint-disable-next-line no-alert
                alert("Einfügen wurde durch den Browser blockiert!");
                setClipboardDisabled(true);
              });
          }}
        />
        <Button
          outline
          ghost
          label="Entfernen"
          onClick={() => {
            setFreeFormText("");
            clearQuickReaction();
          }}
        />
        <Button
          outline
          primary
          label="Anzeigen"
          onClick={() => {
            const text = freeFormText?.trim() ?? "";
            if (text !== "") {
              setQuickReaction("free-text", text);
            } else {
              setFreeFormText(text);
              clearQuickReaction();
            }
          }}
        />
      </ButtonGroup>
    </VideoControlPanelInteractionGroup>
  );
}

type Props = {
  selected?: {
    type: string;
    value: string;
  };
  setQuickReaction: (type: string, value: string) => void;
  clearQuickReaction: () => void;
};
