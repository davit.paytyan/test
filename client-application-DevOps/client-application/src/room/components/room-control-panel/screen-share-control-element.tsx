import { Button } from "@pxwlab/katana-cb";
import React from "react";
import { useStateIsStageFull } from "../../hooks/room-stage/use-state-is-stage-full";
import { useScreenSharing } from "../../hooks/use-share-screen-machine";

export function ScreenShareControlElement() {
  const isStageFull = useStateIsStageFull();
  const shareScreenService = useScreenSharing();

  return (
    <Button
      link
      square
      icon="computer-line"
      // FIXME: remove the @ts-ignore rule
      // @ts-ignore PropTypes of the button are wrong
      loading={shareScreenService.isLoading}
      disabled={isStageFull || shareScreenService.isLoading}
      active={shareScreenService.isActive}
      onClick={async () => {
        if (shareScreenService.isLoading) return;

        if (shareScreenService.isActive) {
          await shareScreenService.stopScreenSharing();
          return;
        }

        await shareScreenService.startScreenSharing();
      }}
    />
  );
}
