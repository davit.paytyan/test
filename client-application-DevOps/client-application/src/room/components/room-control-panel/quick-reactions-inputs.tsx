import React from "react";
import { Button, VideoControlPanelInteractionGroup } from "@pxwlab/katana-cb";
import { QUICK_REACTIONS } from "library/constants/quick-reactions";

const mapping = QUICK_REACTIONS;

function QuickReactionButton({
  type,
  value,
  selected,
  setQuickReaction,
  clearQuickReaction,
}: ReactionButtonProps) {
  const active = selected?.type === type && selected.value === value;
  return (
    <Button
      link
      square
      label={mapping.get(`${type}-${value}`)}
      quickAnswer
      quickAnswerActive={active}
      onClick={() => {
        if (active) return clearQuickReaction();
        setQuickReaction(type, value);
      }}
    />
  );
}

function TextReactionButton({
  type,
  value,
  selected,
  setQuickReaction,
  clearQuickReaction,
}: ReactionButtonProps) {
  const active = selected?.type === type && selected.value === value;
  return (
    <Button
      link
      square
      label={mapping.get(`${type}-${value}`)}
      textAnswer
      textAnswerActive={active}
      onClick={() => {
        if (active) return clearQuickReaction();
        setQuickReaction(type, value);
      }}
    />
  );
}

export function QuickReactionsInputs(props: Props) {
  return (
    <>
      <VideoControlPanelInteractionGroup>
        <QuickReactionButton type="mood" value="happy" {...props} />
        <QuickReactionButton type="mood" value="neutral" {...props} />
        <QuickReactionButton type="mood" value="sad" {...props} />
        <QuickReactionButton type="mood" value="wink" {...props} />
        <QuickReactionButton type="bool" value="yes" {...props} />
        <QuickReactionButton type="bool" value="no" {...props} />
      </VideoControlPanelInteractionGroup>
      <VideoControlPanelInteractionGroup>
        <QuickReactionButton type="number" value="1" {...props} />
        <QuickReactionButton type="number" value="2" {...props} />
        <QuickReactionButton type="number" value="3" {...props} />
        <QuickReactionButton type="number" value="4" {...props} />
        <QuickReactionButton type="number" value="5" {...props} />
        <QuickReactionButton type="number" value="6" {...props} />
      </VideoControlPanelInteractionGroup>
      <VideoControlPanelInteractionGroup>
        <TextReactionButton type="text" value="afk" {...props} />
        <TextReactionButton type="text" value="question" {...props} />
        <TextReactionButton type="text" value="notice" {...props} />
      </VideoControlPanelInteractionGroup>
    </>
  );
}

type Props = {
  selected?: {
    type: string;
    value: string;
  };
  setQuickReaction: (type: string, value: string) => void;
  clearQuickReaction: () => void;
};

type ReactionButtonProps = {
  type: string;
  value: string;
  selected?: Props["selected"];
  setQuickReaction: Props["setQuickReaction"];
  clearQuickReaction: Props["clearQuickReaction"];
};
