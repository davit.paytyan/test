import { Button, FormColumn, FormField, FormGroup, FormInput, FormRow } from "@pxwlab/katana-cb";
import React from "react";
import { Controller, useFormContext } from "react-hook-form";

export function JoinRoomNicknameForm() {
  const form = useFormContext();
  const { formState, control } = form;
  const { errors } = formState;

  return (
    <FormField>
      <FormRow>
        <FormColumn>
          <Controller
            control={control}
            name="displayName"
            rules={{ required: "Please specify a display name" }}
            render={({ field }) => {
              return (
                <FormGroup error={errors.displayName?.message}>
                  <FormInput
                    autoComplete="off"
                    placeholder="How would you like to be called?"
                    icon="question-line"
                    tooltip="The name others will see in your meeting."
                    {...field}
                  />
                </FormGroup>
              );
            }}
          />
        </FormColumn>
      </FormRow>
      <FormRow>
        <FormColumn>
          <Button cta label="Join now" type="submit" onClick={() => null} />
        </FormColumn>
      </FormRow>
    </FormField>
  );
}
