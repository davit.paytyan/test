import {
  FormDropdown,
  FormDropdownItem,
  VideoThumbActions,
  VideoThumbActionsItem,
} from "@pxwlab/katana-cb";
import React from "react";
import { useActionMirrorLocalVideoTrack } from "../hooks/local-tracks/use-action-mirror-local-video-track";
import { useStateIsLocalVideoTrackMirrored } from "../hooks/local-tracks/use-state-is-local-video-track-mirrored";
import { useActionChangeAudioInputDevice } from "../hooks/media-devices/use-action-change-audio-input-device";
import { useActionChangeAudioOutputDevice } from "../hooks/media-devices/use-action-change-audio-output-device";
import { useActionChangeVideoInputDevice } from "../hooks/media-devices/use-action-change-video-input-device";
import { useAvailableAudioInputDevices } from "../hooks/media-devices/use-available-audio-input-devices";
import { useAvailableAudioOutputDevices } from "../hooks/media-devices/use-available-audio-output-devices";
import { useAvailableVideoInputDevices } from "../hooks/media-devices/use-available-video-input-devices";
import { useCurrentLocalAudioDeviceInfo } from "../hooks/media-devices/use-current-local-audio-device-info";
import { useCurrentLocalVideoDeviceInfo } from "../hooks/media-devices/use-current-local-video-device-info";

export function JoinRoomVideoThumbActions() {
  const isChangeOutputDeviceEnabled = false;

  const isLocalVideoTrackMirrored = useStateIsLocalVideoTrackMirrored();
  const mirrorLocalVideoTrack = useActionMirrorLocalVideoTrack();

  const videoDeviceInfo = useCurrentLocalVideoDeviceInfo();
  const audioDeviceInfo = useCurrentLocalAudioDeviceInfo();

  const videoInputLabel = videoDeviceInfo?.label ?? "unknown";
  const audioInputLabel = audioDeviceInfo?.label ?? "unknown";
  const audioOutputLabel = "unknown";

  const availableVideoInputDevices = useAvailableVideoInputDevices();
  const availableAudioInputDevices = useAvailableAudioInputDevices();
  const availableAudioOutputDevices = useAvailableAudioOutputDevices();

  const changeVideoInputDevice = useActionChangeVideoInputDevice();
  const changeAudioInputDevice = useActionChangeAudioInputDevice();
  const changeAudioOutputDevice = useActionChangeAudioOutputDevice();

  return (
    <VideoThumbActions toggleLabel="Video / Audio Settings" toggleIcon="settings-3-line">
      <VideoThumbActionsItem label="Camera">
        <FormDropdown label={videoInputLabel}>
          {availableVideoInputDevices?.map((device) => {
            const isSelected = device.deviceId === videoDeviceInfo?.deviceId;
            return (
              <FormDropdownItem
                key={device.deviceId}
                label={device.label}
                icon={isSelected ? "checkbox-circle-line" : "checkbox-blank-circle-line"}
                onClick={() => changeVideoInputDevice(device.deviceId)}
              />
            );
          })}
          <FormDropdownItem
            divider={true}
            label="Mirror my camera"
            icon={isLocalVideoTrackMirrored ? "toggle-fill" : "toggle-line"}
            onClick={() => {
              mirrorLocalVideoTrack(!isLocalVideoTrackMirrored);
            }}
          />
        </FormDropdown>
      </VideoThumbActionsItem>
      <VideoThumbActionsItem label={isChangeOutputDeviceEnabled ? "Micro" : "Audio"}>
        <FormDropdown label={audioInputLabel}>
          {availableAudioInputDevices?.map((device) => {
            const isSelected = device.deviceId === audioDeviceInfo?.deviceId;
            return (
              <FormDropdownItem
                key={device.deviceId}
                label={device.label}
                icon={isSelected ? "checkbox-circle-line" : "checkbox-blank-circle-line"}
                onClick={() => changeAudioInputDevice(device.deviceId)}
              />
            );
          })}
        </FormDropdown>
      </VideoThumbActionsItem>
      {isChangeOutputDeviceEnabled && (
        <VideoThumbActionsItem label="Speaker">
          <FormDropdown label={audioOutputLabel}>
            {availableAudioOutputDevices?.map((device) => {
              const isSelected = device.deviceId === audioDeviceInfo?.deviceId;
              return (
                <FormDropdownItem
                  key={device.deviceId}
                  label={device.label}
                  icon={isSelected ? "checkbox-circle-line" : "checkbox-blank-circle-line"}
                  onClick={() => changeAudioOutputDevice(device.deviceId)}
                />
              );
            })}
          </FormDropdown>
        </VideoThumbActionsItem>
      )}
    </VideoThumbActions>
  );
}
