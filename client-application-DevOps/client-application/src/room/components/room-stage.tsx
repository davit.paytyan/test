import {
  ContentCard,
  ContentCardBody,
  ContentCardBodyContainer,
  ContentCardHeader,
  LoaderBar,
  SharingCard,
  VideoCard,
  VideoStage,
} from "@pxwlab/katana-cb";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import useDirectusSession from "../../hooks/directus/use-directus-session";
import { useActionKickJitsiParticipant } from "../hooks/jitsi/use-action-kick-jitsi-participant";
import { useIsJitsiConferenceAudioTrackEnabled } from "../hooks/jitsi/use-is-jitsi-conference-audio-track-enabled";
import { useIsJitsiConferenceVideoTrackEnabled } from "../hooks/jitsi/use-is-jitsi-conference-video-track-enabled";
import { useJitsiConferenceVideoTrack } from "../hooks/jitsi/use-jitsi-conference-video-track";
import { useActionToggleQuickReaction } from "../hooks/room-participants/use-action-toggle-quick-reaction";
import { useDataParticipant } from "../hooks/room-participants/use-data-participant";
import { useStateIsParticipantQuickReactionOpen } from "../hooks/room-participants/use-state-is-participant-quick-reaction-open";
import { useTransformedQuickReaction } from "../hooks/room-participants/use-transformed-quick-reaction";
import { useActionRemoveItemFromStage } from "../hooks/room-stage/use-action-remove-item-from-stage";
import { useActionRenameItemOnStage } from "../hooks/room-stage/use-action-rename-item-on-stage";
import { useRoomUserIsOwnerOrModerator } from "../hooks/room-user/use-room-user-is-owner-or-moderator";
import { useRoomUserNickname } from "../hooks/room-user/use-room-user-nickname";
import { useRoomUserUserColor } from "../hooks/room-user/use-room-user-user-color";
import { useGeneratedImageUrl } from "../hooks/use-generated-image-url";
import { StageItem, useStageItems } from "../hooks/use-stage-items";
import { RoomStageItem } from "../machines/actors/room-stage-actor";
import StatusMessage from "./common/status-message";
import Video from "./media/video";
import { TextpadCard } from "./room-stage/textpad-card";

type CardProps = {
  item: RoomStageItem;
};

function ParticipantCard({ item }: { item: StageItem }) {
  const isOwnerOrModerator = useRoomUserIsOwnerOrModerator();
  const removeItemFromStage = useActionRemoveItemFromStage();

  const { videoTrack, audioTrack } = item.payload;

  const participant = useDataParticipant(item.id);

  const quickReaction = useTransformedQuickReaction(item.id);
  const isQuickAnswerOpen = useStateIsParticipantQuickReactionOpen(item.id);
  const toggleQuickReaction = useActionToggleQuickReaction(item.id);

  const isVideoEnabled = useIsJitsiConferenceVideoTrackEnabled(videoTrack);
  const isAudioEnabled = useIsJitsiConferenceAudioTrackEnabled(audioTrack);

  if (!participant) return null;

  const { nickname, userColor, isVideoMirrored } = participant;

  return (
    <VideoCard
      userName={nickname}
      userColor={userColor}
      userMuted={!isAudioEnabled}
      userMutedVideo={!isVideoEnabled}
      onStage
      stageKind="stage"
      theaterKind="stage"
      host={isOwnerOrModerator}
      quickAnswerActive={isQuickAnswerOpen}
      quickAnswerType={quickReaction?.type}
      quickAnswerChildren={quickReaction?.value && <StatusMessage text={quickReaction.value} />}
      onQuickAnswerClick={toggleQuickReaction}
      onClick={() => isOwnerOrModerator && removeItemFromStage(item.id)}
      hostMute={!isAudioEnabled}
      // onHostMute={() => console.warn("Mute Participants is not implemented yet!")}
    >
      {videoTrack && isVideoEnabled && (
        <Video track={videoTrack} autoPlay mirrored={isVideoMirrored} />
      )}
    </VideoCard>
  );
}

function ImageCard({ item }: CardProps) {
  const [session] = useDirectusSession();

  const isOwnerOrModerator = useRoomUserIsOwnerOrModerator();
  const removeItemFromStage = useActionRemoveItemFromStage();
  const renameItemOnStage = useActionRenameItemOnStage();

  const imageUrl = useGeneratedImageUrl(item.payload.fileId);
  const coverImageUrl = useGeneratedImageUrl(item.payload.fileId, {
    fit: "cover",
    width: 24,
    height: 24,
  });
  const [isLoading, setIsLoading] = useState(true);
  const canClose = useMemo(
    () => isOwnerOrModerator || item.ownerId === session?.id,
    [isOwnerOrModerator, session, item]
  );

  const [previousTitleBuffer, setPreviousTitleBuffer] = useState(item?.title ?? "no-name");
  const [titleBuffer, setTitleBuffer] = useState(item?.title ?? "no-nome");

  useEffect(() => {
    if (item?.title) setTitleBuffer(item?.title);
  }, [item?.title]);

  const blurTitleTextfield = useCallback(() => {
    if (typeof window !== "undefined" && document.activeElement)
      (document.activeElement as HTMLInputElement).blur();
  }, []);

  return (
    <ContentCard boxed>
      <ContentCardHeader
        image={coverImageUrl}
        coach={canClose}
        onClick={() => removeItemFromStage(item.id)}
        titleEditable={canClose}
        title={titleBuffer}
        onTitleKeyDown={(event) => {
          if (event.key === "Enter") {
            if (!titleBuffer || titleBuffer.trim() === "") setTitleBuffer(previousTitleBuffer);
            blurTitleTextfield();
          }
          if (event.key === "Escape") {
            setTitleBuffer(previousTitleBuffer);
            blurTitleTextfield();
          }
        }}
        onTitleFocus={() => {
          setPreviousTitleBuffer(titleBuffer);
        }}
        onTitleBlur={() => {
          renameItemOnStage(item.id, titleBuffer as string, item.type);
        }}
        onTitleChange={(value: string) => {
          setTitleBuffer(value);
        }}
      />
      <ContentCardBody>
        <ContentCardBodyContainer>
          {isLoading && <LoaderBar />}
          <img
            src={imageUrl}
            onLoad={() => setIsLoading(false)}
            style={{ visibility: isLoading ? "hidden" : "visible" }}
            alt={item.title}
          />
        </ContentCardBodyContainer>
      </ContentCardBody>
    </ContentCard>
  );
}

function _TextpadCard({ item }: CardProps) {
  const roomUserNickname = useRoomUserNickname();
  const roomUserUserColor = useRoomUserUserColor();

  return <TextpadCard item={item} username={roomUserNickname} userColor={roomUserUserColor} />;
}

function SharedScreenCard({ item }) {
  const [sharingCardFullscreen, setSharingCardFullscreen] = useState(false);
  const [session] = useDirectusSession();

  const isOwnerOrModerator = useRoomUserIsOwnerOrModerator();
  const videoTrack = useJitsiConferenceVideoTrack(item.id as string);

  const canClose = useMemo(
    () => isOwnerOrModerator || item.ownerId === session?.id,
    [isOwnerOrModerator, session, item]
  );

  const kickParticipant = useActionKickJitsiParticipant();

  if (canClose) {
    return (
      <SharingCard
        fullscreen={sharingCardFullscreen}
        onFullscreen={() => {
          setSharingCardFullscreen(!sharingCardFullscreen);
        }}
        onClose={() => {
          setSharingCardFullscreen(false);
          kickParticipant(item.id, "screen sharing stopped by mod.");
        }}
      >
        {videoTrack && <Video track={videoTrack} autoPlay />}
      </SharingCard>
    );
  }

  return (
    <SharingCard
      fullscreen={sharingCardFullscreen}
      onFullscreen={() => {
        setSharingCardFullscreen(!sharingCardFullscreen);
      }}
    >
      {videoTrack && <Video track={videoTrack} autoPlay />}
    </SharingCard>
  );
}

export function RoomStage() {
  const stageItems = useStageItems();

  if (stageItems.length === 0) return null;

  return (
    <VideoStage cardCount={stageItems.length as 1 | 2 | 3 | 4}>
      {stageItems.map((item) => {
        if (item.type === "participant") return <ParticipantCard key={item.id} item={item} />;
        if (item.type === "directus_files") return <ImageCard key={item.id} item={item} />;
        if (item.type === "shared_screen") return <SharedScreenCard key={item.id} item={item} />;
        if (item.type === "textpad") return <_TextpadCard key={item.id} item={item} />;
        return null;
      })}
    </VideoStage>
  );
}
