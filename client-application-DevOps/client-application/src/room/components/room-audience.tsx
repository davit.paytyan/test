import { VideoCard, VideoGroup } from "@pxwlab/katana-cb";
import React, { useCallback } from "react";
import { useIsJitsiConferenceAudioTrackEnabled } from "../hooks/jitsi/use-is-jitsi-conference-audio-track-enabled";
import { useIsJitsiConferenceVideoTrackEnabled } from "../hooks/jitsi/use-is-jitsi-conference-video-track-enabled";
import { useActionToggleQuickReaction } from "../hooks/room-participants/use-action-toggle-quick-reaction";
import { useStateIsParticipantQuickReactionOpen } from "../hooks/room-participants/use-state-is-participant-quick-reaction-open";
import { useTransformedQuickReaction } from "../hooks/room-participants/use-transformed-quick-reaction";
import { useActionToggleRoomAudienceItemOnStage } from "../hooks/room-stage/use-action-toggle-room-audience-item-on-stage";
import { useStateIsStageActive } from "../hooks/room-stage/use-state-is-stage-active";
import { useRoomUserIsOwnerOrModerator } from "../hooks/room-user/use-room-user-is-owner-or-moderator";
import { AudienceItem, useAudienceItems } from "../hooks/use-audience-items";
import StatusMessage from "./common/status-message";
import Video from "./media/video";

type RoomAudienceVideoCardProps = {
  audienceItem: AudienceItem;
  stageKind: "audience" | "stage";
};

function RoomAudienceVideoCard({ audienceItem, stageKind }: RoomAudienceVideoCardProps) {
  const { userId, isOnStage, nickname, userColor, isVideoMirrored, videoTrack, audioTrack } =
    audienceItem;

  const isOwnerOrModerator = useRoomUserIsOwnerOrModerator();

  const quickReaction = useTransformedQuickReaction(userId);
  const isQuickAnswerOpen = useStateIsParticipantQuickReactionOpen(userId);
  const toggleQuickReaction = useActionToggleQuickReaction(userId);

  const toggleItemOnStage = useActionToggleRoomAudienceItemOnStage();

  const isVideoEnabled = useIsJitsiConferenceVideoTrackEnabled(videoTrack);
  const isAudioEnabled = useIsJitsiConferenceAudioTrackEnabled(audioTrack);

  const onClick = useCallback(() => {
    toggleItemOnStage(audienceItem);
  }, [toggleItemOnStage, audienceItem]);

  return (
    <VideoCard
      active={isOnStage}
      userName={nickname}
      userColor={userColor}
      userMuted={!isAudioEnabled}
      userMutedVideo={!isVideoEnabled}
      onStage={isOnStage}
      stageKind={stageKind}
      theaterKind="random"
      host={isOwnerOrModerator}
      quickAnswerActive={!isOnStage && isQuickAnswerOpen}
      quickAnswerType={quickReaction?.type}
      quickAnswerChildren={
        !isOnStage && quickReaction?.value && <StatusMessage text={quickReaction.value} />
      }
      onQuickAnswerClick={toggleQuickReaction}
      onClick={onClick}
      hostMute={!isAudioEnabled}
      // onHostMute={() => console.warn("Mute Participants is not implemented yet!")}
    >
      {videoTrack && isVideoEnabled && (
        <Video track={videoTrack} autoPlay mirrored={isVideoMirrored} />
      )}
    </VideoCard>
  );
}

export function RoomAudience() {
  const audienceItems = useAudienceItems();
  const isStageActive = useStateIsStageActive();

  const stageKind = isStageActive ? "audience" : "stage";

  return (
    <VideoGroup stageActive={isStageActive}>
      {audienceItems.map((audienceItem) => {
        return (
          <RoomAudienceVideoCard
            key={audienceItem.userId}
            audienceItem={audienceItem}
            stageKind={stageKind}
          />
        );
      })}
    </VideoGroup>
  );
}
