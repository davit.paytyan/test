import { CoachingBox } from "@pxwlab/katana-cb";
import React from "react";

export function RoomContentBox({ children }: { children?: React.ReactNode }) {
  return (
    <div className="cb-content">
      <CoachingBox>{children}</CoachingBox>
    </div>
  );
}
