import {
  Footer,
  FooterCertificate,
  FooterSettings,
  FormDropdown,
  FormDropdownItem,
} from "@pxwlab/katana-cb";
import { useRouter } from "next/router";
import React from "react";
import { FOOTER_PRIVACY_CERTIFICATE_PATH } from "library/constants/assets";
import { LOGOUT_URL } from "library/constants/url-mapping";

type ComponentProps = React.PropsWithChildren<unknown>;

export function RoomFooter({ children }: ComponentProps) {
  const router = useRouter();
  return (
    <Footer box>
      <FooterSettings>
        <FormDropdown icon="question-line" outline={true} side={true}>
          <FormDropdownItem label="Help Center" icon="information-line" />
          <FormDropdownItem divider={true} label="Legal Inofrmation" icon="external-link-line" />
          <FormDropdownItem label="Privacy Policy" icon="external-link-line" />
          <FormDropdownItem
            divider
            label="Sign Out"
            icon="external-link-line"
            onClick={() => router.push(LOGOUT_URL)}
          />
        </FormDropdown>
      </FooterSettings>
      {children}
      <FooterCertificate image={FOOTER_PRIVACY_CERTIFICATE_PATH} />
    </Footer>
  );
}
