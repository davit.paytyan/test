import React, { useState } from "react";
import Logger from "loglevel";
import { MediaTrack } from "library/utils/create-media-track";

type VideoProps = React.HTMLProps<HTMLVideoElement> & { track?: MediaTrack; mirrored?: boolean };

const Video: React.FC<VideoProps> = ({ track, mirrored, children, ...rest }) => {
  const elementReference = React.useRef<HTMLVideoElement | null>(null);
  const [style, setStyle] = useState({});

  React.useEffect(() => {
    const element = elementReference.current;
    if (element && track) {
      Logger.debug("Attached track to element", elementReference.current, track);
      track._originalTrack.attach(element);
    }
    return () => {
      if (track && element) track._originalTrack.detach(element);
    };
  }, [elementReference, track]);

  React.useEffect(() => {
    if (mirrored) {
      setStyle({ transform: "scaleX(-1)" });
    } else {
      setStyle({});
    }
  }, [mirrored]);

  return (
    // eslint-disable-next-line jsx-a11y/media-has-caption
    <video ref={elementReference} {...rest} style={style}>
      {children}
    </video>
  );
};

export default Video;
