import React from "react";
import Logger from "loglevel";
import { MediaTrack } from "library/utils/create-media-track";

type AudioProps = React.HTMLProps<HTMLAudioElement> & { track?: MediaTrack };

const Audio: React.FC<AudioProps> = ({ track, children, ...rest }) => {
  const elementReference = React.useRef<HTMLAudioElement | null>(null);

  React.useEffect(() => {
    const element = elementReference.current;
    if (element && track) {
      Logger.debug("Attached track to element", elementReference.current, track);
      track._originalTrack.attach(element);
    }
    return () => {
      if (track && element) track._originalTrack.detach(element);
    };
  }, [elementReference, track]);

  return (
    // eslint-disable-next-line jsx-a11y/media-has-caption
    <audio ref={elementReference} {...rest}>
      {children}
    </audio>
  );
};

export default Audio;
