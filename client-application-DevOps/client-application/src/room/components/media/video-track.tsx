import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
import JitsiRemoteTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiRemoteTrack";
import Logger from "loglevel";
import React, { useEffect, useState } from "react";

type VideoProps = React.HTMLProps<HTMLVideoElement> & {
  track?: JitsiLocalTrack | JitsiRemoteTrack;
  mirrored?: boolean;
};

export function VideoTrack({ track, mirrored, children, ...rest }: VideoProps) {
  const elementReference = React.useRef<HTMLVideoElement | null>(null);
  const [style, setStyle] = useState({});

  useEffect(() => {
    const element = elementReference.current;
    if (element && track) {
      Logger.debug("Attached video track to element", elementReference.current);
      track.attach(element);
    }
    return () => {
      if (track && element) track.detach(element);
    };
  }, [elementReference, track]);

  useEffect(() => {
    setStyle({ transform: mirrored ? "scaleX(-1)" : "none" });
  }, [mirrored]);

  if (!track) return null;

  return (
    // eslint-disable-next-line jsx-a11y/media-has-caption
    <video ref={elementReference} {...rest} style={style}>
      {children}
    </video>
  );
}
