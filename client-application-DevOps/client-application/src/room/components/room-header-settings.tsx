import React from "react";
import { Button, HeaderSettings } from "@pxwlab/katana-cb";
import { useContentUploadContext } from "content-upload/components/content-upload-context";
import LocaleSwitchButtons from "components/header/locale-switch-buttons";

export function RoomHeaderSettings() {
  const { isUploadInProgress, uploadCount, toggleUploadModal } = useContentUploadContext();

  return (
    <HeaderSettings>
      {isUploadInProgress && (
        <Button
          icon="upload-cloud-2-line"
          // FIXME: remove the @ts-ignore rule
          // @ts-ignore PropTypes of the button are wrong
          loading
          ghost
          square
          count={uploadCount > 0 ? String(uploadCount) : undefined}
          badgeTriangle={true}
          onClick={() => toggleUploadModal()}
        />
      )}
      <LocaleSwitchButtons />
    </HeaderSettings>
  );
}
