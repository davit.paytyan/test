import { ContentPanel } from "@pxwlab/katana-cb";
import React, { useRef } from "react";
import { useActionCloseAllContentPanels } from "../hooks/room-content/use-action-close-all-content-panels";
import { ContentPanelContextProvider } from "../container/content-panel-context";
import { ContentPanelFlyout } from "./room-content-panel/content-panel-flyout";
import { ContentPanelNavButton } from "./room-content-panel/content-panel-nav-button";
import { ContentPanelNavPortal } from "./room-content-panel/content-panel-nav-portal";
import { ContentPanelWrapperServiceProvider } from "../container/content-panel-wrapper-context";
import { ContentPanelNav } from "./room-content-panel/_katana/content-panel-nav";

function WrappedRoomContentPanel() {
  const navRef = useRef<HTMLDivElement>(null);
  const closeAllContentPanels = useActionCloseAllContentPanels();
  return (
    <ContentPanel onClickOutside={closeAllContentPanels}>
      <ContentPanelNav innerRef={navRef} />
      <ContentPanelContextProvider type="directus_files">
        <ContentPanelNavPortal portalElementRef={navRef}>
          <ContentPanelNavButton icon="image-line" />
        </ContentPanelNavPortal>
        <ContentPanelFlyout />
      </ContentPanelContextProvider>
      <ContentPanelContextProvider type="textpad">
        <ContentPanelNavPortal portalElementRef={navRef}>
          <ContentPanelNavButton icon="file-text-line" />
        </ContentPanelNavPortal>
        <ContentPanelFlyout />
      </ContentPanelContextProvider>
    </ContentPanel>
  );
}

export function RoomContentPanel() {
  return (
    <ContentPanelWrapperServiceProvider>
      <WrappedRoomContentPanel />
    </ContentPanelWrapperServiceProvider>
  );
}
