import {
  ContentCard,
  ContentCardAside,
  ContentCardBody,
  ContentCardHeader,
} from "@pxwlab/katana-cb";
import Collaboration from "@tiptap/extension-collaboration";
import CollaborationCursor from "@tiptap/extension-collaboration-cursor";
import TaskItem from "@tiptap/extension-task-item";
import TaskList from "@tiptap/extension-task-list";
import Typography from "@tiptap/extension-typography";
import { EditorContent, useEditor } from "@tiptap/react";
import StarterKit from "@tiptap/starter-kit";
import getConfig from "next/config";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { WebsocketProvider } from "y-websocket";
import { Doc } from "yjs";
import { useActionRemoveItemFromStage } from "../../hooks/room-stage/use-action-remove-item-from-stage";
import { useActionRenameItemOnStage } from "../../hooks/room-stage/use-action-rename-item-on-stage";
import { useRoomId } from "../../hooks/room/use-room-id";
import { RoomStageItem } from "../../machines/actors/room-stage-actor";

type Props = {
  username: string;
  userColor: string;
  item: RoomStageItem;
};

export function TextpadCard({ username, userColor, item }: Props) {
  const { publicRuntimeConfig } = useMemo(() => getConfig(), []);

  const [yDocument] = useState(new Doc());

  const roomId = useRoomId();

  const yProvider = useMemo(() => {
    if (typeof window === "undefined") return null;
    const provider = new WebsocketProvider(
      publicRuntimeConfig.publicTextpadUrl,
      `${roomId}/${item.id}`,
      yDocument,
      {
        connect: true,
        params: {
          foo: "bar",
        },
      }
    );
    return provider;
  }, [publicRuntimeConfig, yDocument, roomId, item.id]);

  const editor = useEditor(
    {
      extensions: [
        StarterKit.configure({
          history: false,
        }),
        Typography,
        TaskItem.configure({ nested: false }),
        TaskList,
        Collaboration.configure({ document: yDocument }),
        CollaborationCursor.configure({
          provider: yProvider,
          user: {
            name: username,
            color: userColor,
          },
        }),
        // Link,
        // BubbleMenu,
      ],
    },
    [yProvider]
  );

  useEffect(() => {
    return () => {
      if (yDocument && editor && yProvider) {
        yDocument.destroy();
        editor.destroy();
        yProvider.destroy();
      }
    };
  }, [editor, yProvider, yDocument]);

  const removeItemFromStage = useActionRemoveItemFromStage();
  const renameItemOnStage = useActionRenameItemOnStage();

  const [previousTitleBuffer, setPreviousTitleBuffer] = useState(item?.title ?? "no-name");
  const [titleBuffer, setTitleBuffer] = useState(item?.title ?? "no-nome");

  useEffect(() => {
    if (item?.title) setTitleBuffer(item?.title);
  }, [item]);

  const blurTitleTextfield = useCallback(() => {
    if (typeof window !== "undefined" && document.activeElement)
      (document.activeElement as HTMLInputElement).blur();
  }, []);

  return (
    <ContentCard boxed>
      <ContentCardHeader
        type="board-text"
        coach
        onClick={() => removeItemFromStage(item.id)}
        titleEditable
        title={titleBuffer}
        onTitleKeyDown={(event) => {
          if (event.key === "Enter") {
            if (!titleBuffer || titleBuffer.trim() === "") setTitleBuffer(previousTitleBuffer);
            blurTitleTextfield();
          }
          if (event.key === "Escape") {
            setTitleBuffer(previousTitleBuffer);
            blurTitleTextfield();
          }
        }}
        onTitleFocus={() => {
          setPreviousTitleBuffer(titleBuffer);
        }}
        onTitleBlur={() => {
          renameItemOnStage(item.id, titleBuffer as string, item.type);
        }}
        onTitleChange={(value: string) => {
          setTitleBuffer(value);
        }}
      />
      <ContentCardAside
        toolbar={[
          {
            icon: "bold-line",
            onClick: () => editor?.chain().focus().toggleBold().run(),
            buttonProps: {
              active: editor?.isActive("bold"),
            },
          },
          {
            icon: "italic-line",
            onClick: () => editor?.chain().focus().toggleItalic().run(),
            buttonProps: {
              active: editor?.isActive("italic"),
            },
          },
          {
            icon: "strikethrough",
            onClick: () => editor?.chain().focus().toggleStrike().run(),
            divider: true,
            buttonProps: {
              active: editor?.isActive("strike"),
            },
          },
          {
            icon: "h-1",
            onClick: () => editor?.chain().focus().clearNodes().toggleHeading({ level: 1 }).run(),
            buttonProps: {
              active: editor?.isActive("heading", { level: 1 }),
            },
          },
          {
            icon: "h-2",
            onClick: () => editor?.chain().focus().clearNodes().toggleHeading({ level: 2 }).run(),
            buttonProps: {
              active: editor?.isActive("heading", { level: 2 }),
            },
          },
          {
            icon: "h-3",
            onClick: () => editor?.chain().focus().clearNodes().toggleHeading({ level: 3 }).run(),
            buttonProps: {
              active: editor?.isActive("heading", { level: 3 }),
            },
          },
          {
            icon: "paragraph",
            onClick: () => editor?.chain().focus().clearNodes().setParagraph().run(),
            buttonProps: {
              active: editor?.isActive("paragraph"),
            },
          },
          {
            icon: "double-quotes-l",
            onClick: () => editor?.chain().focus().clearNodes().toggleBlockquote().run(),
            divider: true,
            buttonProps: {
              active: editor?.isActive("blockquote"),
            },
          },
          {
            icon: "list-ordered-line",
            onClick: () => editor?.chain().focus().clearNodes().toggleOrderedList().run(),
            buttonProps: {
              active: editor?.isActive("orderedList"),
            },
          },
          {
            icon: "list-unordered-line",
            onClick: () => editor?.chain().focus().clearNodes().toggleBulletList().run(),
            buttonProps: {
              active: editor?.isActive("bulletList"),
            },
          },
          {
            icon: "list-check-2",
            onClick: () => editor?.chain().focus().clearNodes().toggleTaskList().run(),
            divider: true,
            buttonProps: {
              active: editor?.isActive("taskList"),
            },
          },
          // {
          //   icon: editor?.isActive("link") ? "link-unlink" : "link",
          //   onClick: () => (editor?.isActive("link") ? unsetLink() : setLink()),
          //   divider: true,
          // },
          {
            // @ts-ignore missing proptype in katana
            label: "Clear styles",
            icon: "delete-bin-line",
            onClick: () => editor?.chain().focus().unsetAllMarks().clearNodes().run(),
            divider: true,
          },
          {},
        ]}
      />
      <ContentCardBody>
        <EditorContent className="textpad" editor={editor} />
      </ContentCardBody>
    </ContentCard>
  );
}

// const TipTapTipTapMentionList: React.FunctionComponent = ({ items, command }) => {
//   const selectItem = React.useCallback(
//     (index) => items[index] && command({ id: items[index] }),
//     [items, command]
//   );

//   return (
//     <TipTapMentionList>
//       {items.map((item, index) => (
//         <TipTapMentionListItem onClick={() => selectItem(index)} key={index} label={item} />
//       ))}
//     </TipTapMentionList>
//   );
// };

// const setLink = React.useCallback(() => {
//   if (window && editor) {
//     const url = window.prompt("URL");
//     editor.chain().focus().setLink({ href: url }).run();
//   }
// }, [editor]);

// const unsetLink = React.useCallback(() => {
//   if (editor) editor.chain().focus().unsetLink().run();
// }, [editor]);

// Mention.configure({
//   HTMLAttributes: {
//     class: "mention",
//   },
//   suggestion: {
//     items: (query) => {
//       return ["Lea Thompson", "Cyndi Lauper", "Tom Cruise"]
//         .filter((item) => item.toLowerCase().startsWith(query.toLowerCase()))
//         .slice(0, 5);
//     },
//     render: () => {
//       let reactRenderer;
//       let popup;

//       return {
//         onStart: (props) => {
//           reactRenderer = new ReactRenderer(TipTapTipTapMentionList, {
//             props,
//             editor: props.editor,
//           });

//           popup = tippy("body", {
//             getReferenceClientRect: props.clientRect,
//             appendTo: () => document.body,
//             content: reactRenderer.element,
//             showOnCreate: true,
//             interactive: true,
//             trigger: "manual",
//             placement: "bottom-start",
//           });
//         },
//         onUpdate(props) {
//           reactRenderer.updateProps(props);

//           popup[0].setProps({
//             getReferenceClientRect: props.clientRect,
//           });
//         },
//         onKeyDown(props) {
//           return reactRenderer.ref?.onKeyDown(props);
//         },
//         onExit() {
//           popup[0].destroy();
//           reactRenderer.destroy();
//         },
//       };
//     },
//   },
// }),
