import { Button, VideoControlPanel } from "@pxwlab/katana-cb";
import React, { useState } from "react";
import { useActionClearParticipantQuickReaction } from "../hooks/room-participants/use-action-clear-participant-quick-reaction";
import { useActionSetParticipantQuickReaction } from "../hooks/room-participants/use-action-set-participant-quick-reaction";
import { useDataParticipantQuickReaction } from "../hooks/room-participants/use-data-participant-quick-reaction";
import { useRoomUserUserId } from "../hooks/room-user/use-room-user-user-id";
import { FreeTextReactionInput } from "./room-control-panel/free-text-reaction-input";
import { LocalAudioControlElements } from "./room-control-panel/local-audio-control-elements";
import { LocalVideoControlElements } from "./room-control-panel/local-video-control-elements";
import { QuickReactionsInputs } from "./room-control-panel/quick-reactions-inputs";
import { ScreenShareControlElement } from "./room-control-panel/screen-share-control-element";

function InteractionChildren(props: Props) {
  return (
    <>
      <QuickReactionsInputs {...props} />
      <FreeTextReactionInput {...props} />
    </>
  );
}

export default function RoomControlPanel() {
  const userId = useRoomUserUserId();
  const [interactionOpen, setInteractionOpen] = useState(false);

  const currentQuickReaction = useDataParticipantQuickReaction(userId);
  const setParticipantQuickReaction = useActionSetParticipantQuickReaction();
  const clearParticipantQuickReaction = useActionClearParticipantQuickReaction();

  return (
    <VideoControlPanel
      interactionOpen={interactionOpen}
      onClickOutside={() => setInteractionOpen(false)}
      interactionChildren={
        interactionOpen && (
          <InteractionChildren
            selected={currentQuickReaction}
            setQuickReaction={(type, value) => {
              setParticipantQuickReaction({ type, value });
              setInteractionOpen(false);
            }}
            clearQuickReaction={() => clearParticipantQuickReaction()}
          />
        )
      }
    >
      <LocalVideoControlElements />
      <LocalAudioControlElements />
      <Button
        link
        square
        icon="message-line"
        active={interactionOpen}
        onClick={() => setInteractionOpen(!interactionOpen)}
      />
      <ScreenShareControlElement />
    </VideoControlPanel>
  );
}

type Props = {
  selected?: {
    type: string;
    value: string;
  };
  setQuickReaction: (type: string, value: string) => void;
  clearQuickReaction: () => void;
};
