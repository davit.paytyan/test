import { useInterpret } from "@xstate/react";
import getConfig from "next/config";
import React, { createContext, useContext, useMemo } from "react";
import { InterpreterFrom } from "xstate";
import { ConferenceMachine } from "../machines/conference-machine";

export const ConferenceContext = createContext({} as unknown as ConferenceContextValue);

export function useConferenceContext() {
  return useContext(ConferenceContext);
}

export function ConferenceContextProvider({ roomId, children }: Props) {
  // TODO: move this into the nextjs page and use get server side props and use env vars
  const { publicRuntimeConfig } = useMemo(() => getConfig(), []);
  const initialContext = useMemo(
    () => ({
      ...ConferenceMachine.context,
      roomId,
      jitsiConnectionConfig: {
        options: {
          serviceUrl: publicRuntimeConfig.xmppServiceUrl,
          hosts: {
            domain: publicRuntimeConfig.xmppHostDomain,
            muc: publicRuntimeConfig.xmppHostMuc,
          },
        },
      },
      jitsiConferenceConfig: {
        options: {
          channelLastN: 16,
          pvp: {
            enabled: false,
          },
        },
      },
      colyseusEndpoint: publicRuntimeConfig.realtimeUrl,
    }),
    [roomId, publicRuntimeConfig]
  );

  const conferenceService = useInterpret(ConferenceMachine.withContext(initialContext), {
    devTools: false,
  });

  return (
    <ConferenceContext.Provider value={{ conferenceService }}>
      {children}
    </ConferenceContext.Provider>
  );
}

type ConferenceContextValue = {
  conferenceService: InterpreterFrom<typeof ConferenceMachine>;
};

type Props = {
  roomId: string;
  children?: React.ReactNode;
};
