import { useSelector } from "@xstate/react";
import { useConferenceContext } from "./conference-context";

export function ConferenceStateRouter(props: Props) {
  const service = useConferenceContext();
  const state = useSelector(service.conferenceService, (state) => state);

  if (props.renderWhenInitialize && state.matches("initialize")) {
    return props.renderWhenInitialize();
  }

  if (props.renderWhenWaitingRoom && state.matches("initialized.waitingRoom")) {
    return props.renderWhenWaitingRoom();
  }

  if (props.renderWhenJoiningRoom && state.matches("initialized.joiningRoom")) {
    return props.renderWhenJoiningRoom();
  }

  if (props.renderWhenRoomJoined && state.matches("initialized.roomJoined")) {
    return props.renderWhenRoomJoined();
  }

  if (props.renderWhenRoomAccessDenied && state.matches("roomAccessDenied")) {
    return props.renderWhenRoomAccessDenied();
  }

  if (props.renderWhenRoomNotFound && state.matches("roomNotFound")) {
    return props.renderWhenRoomNotFound();
  }

  if (props.renderWhenError && state.matches("error")) {
    return props.renderWhenError();
  }

  return props.children ?? null;
}

// TYPES
//
type Props = {
  renderWhenRoomAccessDenied?: () => JSX.Element;
  renderWhenRoomNotFound?: () => JSX.Element;
  renderWhenError?: () => JSX.Element;
  renderWhenInitialize?: () => JSX.Element;
  renderWhenWaitingRoom?: () => JSX.Element;
  renderWhenJoiningRoom?: () => JSX.Element;
  renderWhenRoomJoined?: () => JSX.Element;
  children?: JSX.Element;
};
