import { useInterpret } from "@xstate/react";
import React, { createContext, useContext, useEffect, useMemo } from "react";
import { InterpreterFrom, Subscription } from "xstate";
import { useContentUploadContext } from "../../content-upload/components/content-upload-context";
import { useRoomStageService } from "../hooks/room-stage/use-room-stage-service";
import { useRoomId } from "../hooks/room/use-room-id";
import { useRoomService } from "../hooks/room/use-room-service";
import { RoomContentMachine } from "../machines/room-content-machine";
import { useContentPanelWrapperService } from "./content-panel-wrapper-context";

type ContentPanelContextValue = {
  type: "directus_files" | "textpad";
  service: InterpreterFrom<typeof RoomContentMachine>;
};

const ContentPanelContext = createContext<ContentPanelContextValue>(
  // shhht, typescript it is okay ...
  {} as unknown as ContentPanelContextValue
);

export function useContentPanelContext() {
  return useContext(ContentPanelContext);
}

export function useContentPanelService() {
  const context = useContentPanelContext();
  return context.service;
}

export function useContentPanelType() {
  return useContentPanelContext().type;
}

type Props = {
  type: ContentPanelContextValue["type"];
  children?: React.ReactNode;
};

export function ContentPanelContextProvider({ children, type }: Props) {
  const roomId = useRoomId();
  const { fileUploadCollectionService } = useContentUploadContext();

  const MachineConfig = useMemo(
    () => RoomContentMachine.withContext({ ...RoomContentMachine.context, roomId, type }),
    [roomId, type]
  );

  const service = useInterpret(MachineConfig);
  const wrapperService = useContentPanelWrapperService();
  const roomService = useRoomService();
  const roomStageService = useRoomStageService();

  useEffect(() => {
    wrapperService.send({ type: "addService", service });

    return () => {
      wrapperService.send({ type: "removeService", service });
    };
  }, [service, wrapperService]);

  //
  // Refresh the image content panel after any upload is completed
  //
  useEffect(() => {
    let subscription: Subscription | undefined;
    if (type === "directus_files") {
      subscription = fileUploadCollectionService?.subscribe((state) => {
        if (state.matches("uploadCompleted") && state.event.type === "fileUploadDone") {
          service.send({ type: "refreshRoomContent" });
        }
      });
    }
    return () => {
      subscription?.unsubscribe();
    };
  }, [type, service, fileUploadCollectionService]);

  //
  // Refresh the textpad content panel if the onRefreshTextboards message is received
  // this syncs any action from other users since the textboards are shared content
  //
  useEffect(() => {
    let subscription: Subscription | undefined;
    if (type === "textpad") {
      subscription = roomService?.subscribe((state) => {
        const { event } = state;
        if (event.type === "onRefreshTextboards") service.send({ type: "refreshRoomContent" });
      });
    }
    return () => {
      subscription?.unsubscribe();
    };
  }, [type, service, roomService]);

  //
  // Send the onRefreshTexboards command to the other clients
  // this syncs any action from other users since the textboards are shared content
  //
  useEffect(() => {
    let subscription: Subscription | undefined;
    if (type === "textpad") {
      subscription = service?.subscribe((state) => {
        // typecast prevents ts error since we don't define those events on the model
        switch (state.event.type as string) {
          case "onCreatedItem":
          case "onDeletedItems":
          case "onRenamedItem":
          case "onSortedItems":
            roomService.send({ type: "refreshTextboards" });
            break;
          default:
            break;
        }
      });
    }
    return () => {
      subscription?.unsubscribe();
    };
  }, [type, service, roomService]);

  //
  // Refresh the content panel when an item was renamed on stage
  //
  useEffect(() => {
    const subscription = roomStageService?.subscribe((state) => {
      const { event } = state;
      if (event.type === "onRenameItemOnStageCompleted" && event.itemType === type) {
        service.send({ type: "refreshRoomContent" });
      }
    });
    return () => {
      subscription?.unsubscribe();
    };
  }, [type, service, roomStageService]);

  const value = useMemo(() => ({ type, service }), [type, service]);

  return <ContentPanelContext.Provider value={value}>{children}</ContentPanelContext.Provider>;
}
