import { useInterpret } from "@xstate/react";
import React, { createContext, useContext } from "react";
import { assign, InterpreterFrom, Subscription } from "xstate";
import { createModel } from "xstate/lib/model";
import { RoomContentMachine } from "../machines/room-content-machine";

type Interpreter = InterpreterFrom<typeof RoomContentMachine>;

type MachineContext = {
  services: Interpreter[];
  subscriptions: WeakMap<Interpreter, Subscription>;
};

const initialContext: MachineContext = {
  services: [],
  subscriptions: new WeakMap(),
};

const ContentPanelWrapperMachineModel = createModel(initialContext, {
  events: {
    closeAllContentPanels: () => ({}),
    addService: (service: Interpreter) => ({ service }),
    removeService: (service: Interpreter) => ({ service }),
  },
});

const ContentPanelWrapperMachine = ContentPanelWrapperMachineModel.createMachine({
  context: ContentPanelWrapperMachineModel.initialContext,
  on: {
    addService: {
      actions: [
        assign({
          services: (context, event) => {
            const references = [...context.services, event.service];
            return references;
          },
        }),
        assign({
          subscriptions: (context) => {
            // recreate all subscriptions
            for (const _service of context.services) {
              // get possible old subscription and unsubscribe it
              const oldSubscription = context.subscriptions.get(_service);
              if (oldSubscription) oldSubscription.unsubscribe();
              // create new subscription
              const unsubscribe = _service.subscribe((state) => {
                if (state.event.type === "openPanel") {
                  for (const service of context.services) {
                    // continue it is the same panel
                    if (service === _service) continue;
                    service.send({ type: "closePanel" });
                  }
                }
              });
              // update the subscription
              context.subscriptions.set(_service, unsubscribe);
            }
            return context.subscriptions;
          },
        }),
      ],
    },
    removeService: {
      actions: assign({
        services: (context, event) => {
          const indexOf = context.services.indexOf(event.service);
          context.services.splice(indexOf, 1);
          return [...context.services];
        },
        subscriptions: (context, event) => {
          const subscription = context.subscriptions.get(event.service);
          if (subscription) {
            subscription.unsubscribe();
            context.subscriptions.delete(event.service);
          }
          return context.subscriptions;
        },
      }),
    },
    closeAllContentPanels: {
      actions: (context) => {
        for (const service of context.services) {
          service.send({ type: "closePanel" });
        }
      },
    },
  },
});

type ContentPanelWrapperContextValue = {
  service: InterpreterFrom<typeof ContentPanelWrapperMachine>;
};

const ContentPanelWrapperContext = createContext<ContentPanelWrapperContextValue>(
  // shhht, typescript it is okay ...
  {} as unknown as ContentPanelWrapperContextValue
);

export function useContentPanelWrapperService() {
  return useContext(ContentPanelWrapperContext).service;
}

type Props = {
  children?: React.ReactNode;
};

export function ContentPanelWrapperServiceProvider({ children }: Props) {
  const service = useInterpret(ContentPanelWrapperMachine);

  return (
    <ContentPanelWrapperContext.Provider value={{ service }}>
      {children}
    </ContentPanelWrapperContext.Provider>
  );
}
