import { Directus } from "@directus/sdk";
import {
  BlockText,
  Image,
  ListIcon,
  ListIconItem,
  Section,
  SectionItem,
  Stage,
} from "@pxwlab/katana-cb";
import { ContentTypeMap, PageIndex } from "client-application";
import { LayoutMarketing } from "components/pages/index/layout-marketing";
import React from "react";

export default function Home({ heroTitle, heroDescription, featuresTitle, features }) {
  return (
    <LayoutMarketing>
      <Stage
        title={heroTitle}
        copy={heroDescription}
        hero={
          <Image float>
            <img src="/images/cb-stage.png" alt="" />
          </Image>
        }
      />

      <Section pattern="1">
        <SectionItem column="7">
          <Image float>
            <img src="/images/cb-section.png" alt="" />
          </Image>
        </SectionItem>

        <SectionItem column="5">
          <BlockText tag="h2" headline={featuresTitle}>
            <ListIcon loud>
              {features.map(({ id, title, description, icon }) => (
                <ListIconItem key={id} title={title} icon={icon} text={description} />
              ))}
            </ListIcon>
          </BlockText>
        </SectionItem>
      </Section>
    </LayoutMarketing>
  );
}

export const getServerSideProps = async () => {
  const { default: environment } = await import("utils/server-env");
  const url = process.env.INTERNAL_API_URL as string;
  const sdk = new Directus<ContentTypeMap>(url);
  await sdk.auth.static(environment.INTERNAL_API_TOKEN as string);

  const { data: pageData } = await sdk
    .items("page_index")
    .readMany({ fields: ["*", "features.*"] });
  const { hero_title, hero_description, features_title, features } =
    (pageData as unknown as PageIndex | undefined) ?? {};

  return {
    props: {
      heroTitle: hero_title,
      heroDescription: hero_description,
      featuresTitle: features_title,
      features: features ?? [],
    },
  };
};
