import {
  Alert,
  FormFooter,
  LoaderCircle,
  Panel,
  PanelBody,
  PanelHeader,
  PanelWrapper,
  Section,
  SectionItem,
} from "@pxwlab/katana-cb";
import LayoutOnboarding from "components/layout/LayoutOnboarding";
import { ResetPasswordPage } from "components/pages/auth/reset-passwort-page";
import useDirectusSDK from "hooks/directus/use-directus-sdk";
import useDirectusSession from "hooks/directus/use-directus-session";
import { GetServerSidePropsContext } from "next";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import Link from "components/common/Link";

type PageProps = {
  token: string | null;
};

export function getServerSideProps(context: GetServerSidePropsContext) {
  const { token = null } = context.query as { token?: string };
  return {
    props: { token },
  };
}

export default function ResetPassword({ token }: PageProps) {
  const sdk = useDirectusSDK();
  const router = useRouter();
  const [session, isValidating] = useDirectusSession();

  useEffect(() => {
    if (session && !isValidating) router.replace("/");
  }, [session, router, isValidating]);

  if (!token) {
    return (
      <LayoutOnboarding>
        <Section center={true}>
          <SectionItem>
            <Panel>
              <PanelWrapper>
                <PanelHeader title="Reset password" />
                <PanelBody>
                  <Alert
                    badge={"Error"}
                    message="The token which was sent to you by e-mail is missing!"
                  />
                  <FormFooter>
                    <Link label="Back to Homepage" href="/" />
                  </FormFooter>
                </PanelBody>
              </PanelWrapper>
            </Panel>
          </SectionItem>
        </Section>
      </LayoutOnboarding>
    );
  }

  if (!session && !isValidating) {
    const onSubmit = async (data) => {
      try {
        await sdk.auth.password.reset(token, data.password);
        router.replace(process.env.NEXT_PUBLIC_LOGIN_URL ?? "/auth/login");
      } catch (error) {
        console.error(error);
        throw error;
      }
    };
    return <ResetPasswordPage onSubmit={onSubmit} />;
  }

  return (
    <LayoutOnboarding>
      <LoaderCircle />
    </LayoutOnboarding>
  );
}
