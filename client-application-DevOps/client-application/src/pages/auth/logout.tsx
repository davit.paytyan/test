import {
  Panel,
  PanelBody,
  PanelHeader,
  PanelWrapper,
  Section,
  SectionItem,
} from "@pxwlab/katana-cb";
import { NextPage } from "next";
import React, { useEffect } from "react";
import LayoutOnboarding from "../../components/layout/LayoutOnboarding";
import useDirectusSDK from "../../hooks/directus/use-directus-sdk";

const SignInPage: NextPage = () => {
  const sdk = useDirectusSDK();

  useEffect(() => {
    sdk.auth.logout();
  }, [sdk]);

  return (
    <LayoutOnboarding footerChildren={null}>
      <Section center={true}>
        <SectionItem>
          <Panel>
            <PanelWrapper>
              <PanelHeader title="Logout" />
              <PanelBody>Logging you out!</PanelBody>
            </PanelWrapper>
          </Panel>
        </SectionItem>
      </Section>
    </LayoutOnboarding>
  );
};

export default SignInPage;
