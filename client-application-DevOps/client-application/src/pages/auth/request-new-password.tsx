import { LoaderCircle } from "@pxwlab/katana-cb";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import LayoutOnboarding from "components/layout/LayoutOnboarding";
import { RequestNewPasswordPage } from "components/pages/auth/request-new-passwort-page";
import useDirectusSDK from "hooks/directus/use-directus-sdk";
import useDirectusSession from "hooks/directus/use-directus-session";

export default function RequestNewPassword() {
  const sdk = useDirectusSDK();
  const router = useRouter();
  const [session, isValidating] = useDirectusSession(false);

  useEffect(() => {
    if (session && !isValidating) router.replace("/");
  }, [session, router, isValidating]);

  if (!session && !isValidating) {
    const onSubmit = async ({ email }) => {
      try {
        await sdk.auth.password.request(email, process.env.NEXT_PUBLIC_PASSWORT_RESET_URL);
      } catch (error) {
        console.log(error);
        throw error;
      }
    };
    return <RequestNewPasswordPage onSubmit={onSubmit} />;
  }

  return (
    <LayoutOnboarding>
      <LoaderCircle />
    </LayoutOnboarding>
  );
}
