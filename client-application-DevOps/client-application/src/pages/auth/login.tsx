import {
  Alert,
  Button,
  ButtonGroup,
  LoaderCircle,
  PanelBody,
  PanelDivider,
  PanelHeader,
} from "@pxwlab/katana-cb";
import { LoginForm } from "components/pages/auth/login";
import useDirectusSDK from "hooks/directus/use-directus-sdk";
import { GetServerSidePropsContext } from "next";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { LayoutPanel } from "../../components/layout/layout-panel";
import LayoutOnboarding from "../../components/layout/LayoutOnboarding";
import useDirectusSession from "../../hooks/directus/use-directus-session";
import { parseCallbackUrl } from "../../utils/url/parse-callback-url";
import Logger from "loglevel";

type PageProps = {
  callbackUrl: string | null;
  encodedCallbackUrl: boolean;
  showRegisterButton: boolean;
};

export function getServerSideProps(context: GetServerSidePropsContext) {
  const {
    callbackUrl = null,
    // eslint-disable-next-line unicorn/prevent-abbreviations
    e = false,
    r = false,
  } = context.query as {
    callbackUrl?: string;
    e?: boolean;
    r?: boolean;
  };
  return {
    props: { callbackUrl, encodedCallbackUrl: e, showRegisterButton: r },
  };
}

export default function LoginPage({
  callbackUrl,
  encodedCallbackUrl,
  showRegisterButton,
}: PageProps) {
  const sdk = useDirectusSDK();
  const router = useRouter();
  const [session, isValidating] = useDirectusSession(false);
  const [serverError, setServerError] = useState("");

  const form = useForm({
    defaultValues: { email: "", password: "", stayLoggedIn: false },
  });

  const { formState } = form;

  useEffect(() => {
    if (session && !formState.isSubmitted && !isValidating) router.replace("/open-my-room");
  }, [session, router, isValidating, formState]);

  const onSubmit = async ({ email, password }) => {
    try {
      await sdk.auth.login({ email, password });
      const targetUrl = parseCallbackUrl(callbackUrl ?? "/open-my-room", encodedCallbackUrl);
      Logger.info(`[Login Success] Redirecting to ${targetUrl}`);
      await router.replace(targetUrl);
    } catch (error) {
      if (error.response && error.response.status === 401) {
        setServerError("Please check your e-mail and passwort and try again.");
      } else {
        setServerError("Couldn't sign you in. Please try again later.");
      }
    }
  };

  if (!session && !isValidating) {
    return (
      <LayoutPanel>
        <PanelHeader title="Login to Together" />
        <PanelBody>
          {serverError && <Alert badge="Login failed" message={serverError} />}
          <FormProvider {...form}>
            <LoginForm onSubmit={form.handleSubmit(onSubmit)} />
          </FormProvider>
        </PanelBody>
        {showRegisterButton && (
          <>
            <PanelDivider />
            <PanelBody>
              <ButtonGroup vertical>
                <Button
                  utilClassNames="cb-block"
                  cta
                  outline
                  label="Create new Account"
                  onClick={() => {
                    router.push({
                      pathname: "/account/create",
                      query: { callbackUrl, e: encodedCallbackUrl },
                    });
                  }}
                />
              </ButtonGroup>
            </PanelBody>
          </>
        )}
      </LayoutPanel>
    );
  }

  return (
    <LayoutOnboarding>
      <LoaderCircle />
    </LayoutOnboarding>
  );
}
