import { TransportError } from "@directus/sdk";
import { GetServerSidePropsContext } from "next";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import getRoomsOfCurrentUser, {
  GetRoomsOfCurrentUserData,
  GetRoomsOfCurrentUserDataItem,
} from "../actionbox-sdk/get-rooms-of-current-user";
import { LayoutPanel } from "../components/layout/layout-panel";
import { FullScreenLoader } from "../components/pages/common/full-screen-loader";
import useDirectusSession from "../hooks/directus/use-directus-session";

export function getServerSideProps(context: GetServerSidePropsContext) {
  const { callbackUrl = null } = context.query as { callbackUrl?: string };
  return {
    props: { callbackUrl },
  };
}

export default function OpenMyRoomPage() {
  const [session, isLoading, sdk] = useDirectusSession(true, "/auth/login");
  const [queryEnabled, setQueryEnabled] = useState(false);
  const [ownRoom, setOwnRoom] = useState<GetRoomsOfCurrentUserDataItem | null>(null);
  const router = useRouter();
  const { data, status, ...query } = useQuery<GetRoomsOfCurrentUserData | null, TransportError>(
    ["rooms"],
    () => getRoomsOfCurrentUser(sdk),
    {
      retry: false,
      enabled: queryEnabled,
      onError: () => setQueryEnabled(false),
      onSuccess: (rooms) => {
        const ownRoom = rooms?.find((room) => room.owner === session?.id);
        if (ownRoom) setOwnRoom(ownRoom);
      },
    }
  );

  useEffect(() => {
    if (Boolean(session) && !isLoading) setQueryEnabled(true);
  }, [session, isLoading]);

  useEffect(() => {
    if (ownRoom) {
      router.replace(`/room/${ownRoom.id}`);
    }
  }, [router, ownRoom]);

  if (query.isError) return <LayoutPanel>{query.error.message}</LayoutPanel>;
  if (query.isSuccess && !ownRoom)
    return <LayoutPanel>{"You don't have your own room yet."}</LayoutPanel>;
  return <FullScreenLoader />;
}
