import { ListIcon, ListIconItem, LoaderCircle, PanelBody, PanelHeader } from "@pxwlab/katana-cb";
import LayoutOnboarding from "components/layout/LayoutOnboarding";
import { CreateAccountPage } from "components/pages/account/create-account-page";
import useDirectusSession from "hooks/directus/use-directus-session";
import { GetServerSidePropsContext } from "next";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { LayoutPanel } from "../../components/layout/layout-panel";

type PageProps = {
  callbackUrl: string | null;
  encodedCallbackUrl: boolean;
};

export function getServerSideProps(context: GetServerSidePropsContext) {
  const {
    callbackUrl = null,
    // eslint-disable-next-line unicorn/prevent-abbreviations
    e = false,
    r = false,
  } = context.query as {
    callbackUrl?: string;
    e?: boolean;
    r?: boolean;
  };
  return {
    props: { callbackUrl, encodedCallbackUrl: e, showRegisterButton: r },
  };
}

function FeatureList() {
  return (
    <>
      <PanelHeader title="Why Together.biz?" />
      <PanelBody>
        <ListIcon>
          <ListIconItem
            title="Feature Title"
            icon="file-text-line"
            text="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes."
          />
          <ListIconItem
            title="Feature Title"
            icon="file-text-line"
            text="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes."
          />
          <ListIconItem
            title="Feature Title"
            icon="file-text-line"
            text="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes."
          />
        </ListIcon>
      </PanelBody>
    </>
  );
}

export default function CreateAccount({ callbackUrl, encodedCallbackUrl }: PageProps) {
  const router = useRouter();
  const [session, isValidating] = useDirectusSession(false);

  useEffect(() => {
    if (session && !isValidating) router.replace("/");
  }, [session, router, isValidating]);

  if (!session && !isValidating) {
    return (
      <LayoutPanel aside={<FeatureList />}>
        <CreateAccountPage
          onClickButtonlogin={() => {
            router.replace({
              pathname: "/auth/login",
              query: { callbackUrl, e: encodedCallbackUrl },
            });
          }}
        />
      </LayoutPanel>
    );
  }

  return (
    <LayoutOnboarding>
      <LoaderCircle />
    </LayoutOnboarding>
  );
}
