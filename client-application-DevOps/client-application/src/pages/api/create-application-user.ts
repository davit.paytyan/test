import { NextApiRequest, NextApiResponse } from "next";
import { Directus } from "@directus/sdk";
import environment from "utils/server-env";
import { withSentry } from "@sentry/nextjs";

const INTERNAL_API_TOKEN = environment.INTERNAL_API_TOKEN as string;
const INTERNAL_API_URL = process.env.INTERNAL_API_URL ?? "";

const handler = async (request: NextApiRequest, response: NextApiResponse) => {
  const { data } = request.body;

  const sdk = new Directus(INTERNAL_API_URL);

  try {
    await sdk.auth.static(INTERNAL_API_TOKEN);
    await sdk.transport.post("/custom/create-application-user", data);
    response.status(200).send(null);
  } catch (error) {
    if (error.errors) {
      console.error(error.errors);
      return response.status(400).send(error.errors[0]);
    }
    console.error(error);
    response.status(500).send("");
  }
};

export default withSentry(handler);
