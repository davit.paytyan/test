import { withSentry } from "@sentry/nextjs";
import { createRoomUser, getRoom } from "actionbox-sdk";
import Logger from "loglevel";
import { NextApiRequest, NextApiResponse } from "next";
import { ApiError } from "next/dist/server/api-utils";
import { ensureDirectusSession } from "utils/api/ensure-user-session";
import environment from "utils/server-env";

const INTERNAL_API_TOKEN = environment.INTERNAL_API_TOKEN as string;

const handler = async (request: NextApiRequest, response: NextApiResponse) => {
  const { method, headers, query } = request;
  const { referer } = headers;
  const { t } = query as { t?: string };

  if (!referer) {
    response.redirect(environment.NEXT_PUBLIC_URL as string);
    return;
  }

  if (String(method).toLowerCase() !== "post" || typeof t === "undefined") {
    response.redirect(referer);
    return;
  }

  const [session, directus] = await ensureDirectusSession(request, response);

  if (!session) {
    response.redirect(referer);
    return;
  }

  let roomId, invitation;

  try {
    invitation = await directus.items("invitations").readOne(t);
    if (!invitation || !invitation.id || !invitation.room)
      throw new ApiError(400, "Invitation does not exist!");
  } catch {
    response.redirect(referer);
    return;
  }

  try {
    const room = await getRoom(directus, invitation.room);
    // Already a room member, redirect to the room
    Logger.info("[Accept Invitation]: Already a room member, redirecting to the room.");
    roomId = room.id;
  } catch {
    // ActionBox SDK throws an error is thrown if the user is not a member of the room
  }

  if (!roomId) {
    await directus.auth.static(INTERNAL_API_TOKEN);
    await createRoomUser(directus, invitation.room, session.id, invitation.id as string);
    Logger.info("[Accept Invitation]: Created a new room user, redirecting to the room.");
    roomId = invitation.room;
  }

  if (!roomId) throw new Error("The room ID is missing.");

  // Invitation process complete, redirect to the room
  response.redirect(`/room/${roomId}`);
};

export default withSentry(handler);
