import { NextApiRequest, NextApiResponse } from "next";
import Cookies from "cookies";

export default async function (request: NextApiRequest, response: NextApiResponse) {
  const cookies = new Cookies(request, response, { secure: true });
  const content = cookies.get("directus_refresh_token");

  response.json({ content });
}
