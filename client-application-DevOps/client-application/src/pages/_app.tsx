import "@pxwlab/katana-cb/dist/katana-components.css";
import SpriteSheet from "@pxwlab/katana-cb/dist/sprite.svg";
import React, { useRef } from "react";
import svg4everybody from "svg4everybody";
import { QueryClient, QueryClientProvider } from "react-query";
import { Hydrate } from "react-query/hydration";
import { AppProps } from "next/app";
import { appWithTranslation } from "next-i18next";
import nextI18NextConfig from "../../next-i18next.config.js";

function App({ Component, pageProps }: AppProps) {
  React.useEffect(() => {
    svg4everybody();
  }, []);

  const queryClientReference = useRef<QueryClient | null>(null);

  if (!queryClientReference.current) {
    queryClientReference.current = new QueryClient();
  }

  return (
    <QueryClientProvider client={queryClientReference.current}>
      <Hydrate state={pageProps.dehydratedState}>
        <Component {...pageProps} />
      </Hydrate>
      <SpriteSheet style={{ position: "absolute", zIndex: -99_999, top: "0px" }} />
    </QueryClientProvider>
  );
}

export default appWithTranslation(App, nextI18NextConfig);
