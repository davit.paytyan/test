import { FullScreenLoader } from "components/pages/common/full-screen-loader";
import useDirectusSession from "hooks/directus/use-directus-session";
import { enableMapSet } from "immer";
import Logger from "loglevel";
import { GetServerSidePropsContext } from "next";
import React from "react";
import { Room } from "room/room";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

enableMapSet();

Logger.setLevel(Logger.levels.INFO);

type PageProps = {
  roomId: string;
};

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const roomId = context.params?.name;
  if (!roomId) return { notFound: true };

  const translations = await serverSideTranslations(context.locale ?? "en", [
    "common",
    "footer",
    "room",
  ]);

  return { props: { roomId, ...translations } };
}

export default function RoomsPage({ roomId }: PageProps) {
  const [session, isLoading] = useDirectusSession(true, "/auth/login");

  if (!session || isLoading) return <FullScreenLoader />;

  return <Room roomId={roomId} />;
}
