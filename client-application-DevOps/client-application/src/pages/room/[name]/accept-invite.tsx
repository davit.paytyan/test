import { Directus, OneItem } from "@directus/sdk";
import { Alert, Button, Form, FormFooter, PanelBody, PanelHeader } from "@pxwlab/katana-cb";
import { getRoom } from "actionbox-sdk";
import { TypedDirectus } from "client-application/actionbox-sdk/models";
import { LayoutPanel } from "components/layout/layout-panel";
import { GetServerSidePropsContext, GetServerSidePropsResult } from "next";
import { stringify } from "querystring";
import React from "react";
import { ensureDirectusSession, Session } from "utils/api/ensure-user-session";
import environment from "utils/server-env";

type PageProps = {
  roomId: string;
  room?: {
    roomName: string;
    roomOwner: string;
  };
  inviteUrl?: string;
  isAlreadyMember?: boolean;
  session?: null | Session;
  loginUrlWithCallback?: string;
  registerUrlWithCallback?: string;
  token?: null | string;
};

export async function getServerSideProps(
  context: GetServerSidePropsContext
): Promise<GetServerSidePropsResult<PageProps>> {
  const roomId = context.params?.name as string;

  if (!roomId) return { notFound: true };

  const { t = null } = context.query as { t?: string };
  if (!t) return { props: { roomId } };

  // get public room data
  // this grants admin access rights to directus, don't use that on the client
  // getServerSideProps is only executed on the server, therefore it is safe to use it here.
  const adminDirectus: TypedDirectus = new Directus(environment.INTERNAL_API_URL as string);
  await adminDirectus.auth.static(String(environment.INTERNAL_API_TOKEN));

  const invitation = await adminDirectus.items("invitations").readOne(t);

  if (!invitation || !invitation.id || !invitation.room || invitation.room !== roomId)
    return { props: { roomId } };

  const roomDetails = (await adminDirectus
    .items("rooms")
    .readOne(roomId, { fields: ["name", "owner.first_name", "owner.last_name"] })) as OneItem<{
    name: string;
    owner: { first_name: string; last_name: string };
  }>;

  if (!roomDetails) return { notFound: true };

  const [session, directus] = await ensureDirectusSession(context.req, context.res);

  if (!session) {
    const inviteUrl = `${environment.NEXT_PUBLIC_URL}/room/${roomId}/accept-invite?t=${t}`;
    const encodedInviteUrl = Buffer.from(inviteUrl, "utf-8").toString("base64");
    const callbackUrlQuery = stringify({ callbackUrl: encodedInviteUrl, e: true });
    const loginUrlWithCallback = `${environment.NEXT_PUBLIC_LOGIN_URL}?${callbackUrlQuery}`;
    const registerUrlWithCallback = `${environment.NEXT_PUBLIC_URL}/account/create?${callbackUrlQuery}`;

    return {
      props: {
        roomId,
        token: t,
        loginUrlWithCallback,
        registerUrlWithCallback,
        room: {
          roomName: roomDetails.name as string,
          roomOwner: `${roomDetails.owner?.first_name} ${roomDetails.owner?.last_name}`,
        },
      },
    };
  }

  let room;

  try {
    room = await getRoom(directus, invitation.room);
  } catch {
    // no-op
  }

  return {
    props: {
      roomId,
      token: t,
      session,
      inviteUrl: `${environment.NEXT_PUBLIC_ACCEPT_INVITATION_URL}?t=${t}`,
      isAlreadyMember: Boolean(room),
      room: {
        roomName: roomDetails.name as string,
        roomOwner: `${roomDetails.owner?.first_name} ${roomDetails.owner?.last_name}`,
      },
    },
  };
}

const AcceptInvitePage = (props: PageProps) => {
  if (!props.token) {
    return (
      <LayoutPanel>
        <PanelBody>
          <Alert
            badge="Invalid invitation"
            message="The invitation link you are using is invalid. Please ask a moderator or the owner of the room for a new or valid invitation link."
          />
          <Form>
            <FormFooter>
              <Button cta outline label="Visit help center" href="/help" />
            </FormFooter>
          </Form>
        </PanelBody>
      </LayoutPanel>
    );
  }

  if (props.session && props.isAlreadyMember) {
    return (
      <LayoutPanel>
        <PanelHeader title={props.room?.roomName} />
        <PanelBody>
          <Alert
            info
            message={`You are already a member of <span style="white-space: nowrap">"${props.room?.roomName}"</span>.`}
          />
          <Form>
            <FormFooter>
              <Button cta label="Go to room" href={`/room/${props.roomId}`} />
            </FormFooter>
          </Form>
        </PanelBody>
      </LayoutPanel>
    );
  }

  if (props.session) {
    return (
      <LayoutPanel>
        <PanelHeader title={props.room?.roomName} />
        <PanelBody>
          <Alert
            info
            message={`${props.room?.roomOwner} has invited you to <span style="white-space: nowrap">"${props.room?.roomName}"</span>.`}
          />
          <Form method="post" action={props.inviteUrl}>
            <FormFooter>
              <Button cta label="Accept invitation" type="submit" href="" />
            </FormFooter>
          </Form>
        </PanelBody>
      </LayoutPanel>
    );
  }

  return (
    <LayoutPanel>
      <PanelHeader title={props.room?.roomName} />
      <PanelBody>
        <Alert
          info
          message={`<strong>Welcome Stranger</strong>. ${props.room?.roomOwner} has invited you to <span style="white-space: nowrap">"${props.room?.roomName}"</span>. The owner has declared access with an account is required. Please login with an existing account or create a free account in five minutes.`}
        />
        <Form>
          <FormFooter>
            <Button cta outline label="Login with account" href={props.loginUrlWithCallback} />
            <Button cta label="Create new account" href={props.registerUrlWithCallback} />
          </FormFooter>
        </Form>
      </PanelBody>
    </LayoutPanel>
  );
};

export default AcceptInvitePage;
