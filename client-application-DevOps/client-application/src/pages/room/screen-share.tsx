import { GetServerSidePropsContext } from "next";
import getConfig from "next/config";
import { useEffect, useRef } from "react";
import { AnyEventObject, interpret } from "xstate";
import useDirectusSession from "hooks/directus/use-directus-session";
import { ScreenShareMachine } from "room/machines/screen-share-machine";

const { publicRuntimeConfig } = getConfig();

function startScreenShare(roomId: string) {
  const initialContext = {
    ...ScreenShareMachine.context,
    roomId,
    jitsiConnectionConfig: {
      options: {
        serviceUrl: publicRuntimeConfig.xmppServiceUrl,
        hosts: {
          domain: publicRuntimeConfig.xmppHostDomain,
          muc: publicRuntimeConfig.xmppHostMuc,
        },
      },
    },
  };

  const machineConfig = ScreenShareMachine.withContext(initialContext);

  const service = interpret(machineConfig, { devTools: false });

  return service;
}

type PageProps = {
  roomId: string;
};

export function getServerSideProps(context: GetServerSidePropsContext) {
  const roomId = context.query?.roomId;
  if (!roomId) return { notFound: true };
  return { props: { roomId } };
}

export default function ScreenSharePage({ roomId }: PageProps) {
  const service = useRef<ReturnType<typeof startScreenShare>>();
  const [session, isLoading] = useDirectusSession(false);

  useEffect(() => {
    if (session && !isLoading && !service.current) {
      service.current = startScreenShare(roomId);

      let conferenceUserId;

      service.current.onEvent((event: AnyEventObject) => {
        if (event.type === "onConferenceJoined") {
          const { conference } = event;
          conferenceUserId = conference.myUserId();
          const customEvent = new CustomEvent("startScreenShare", {
            detail: conferenceUserId,
          });
          window.parent.document.dispatchEvent(customEvent);
        }
      });

      service.current.onDone(() => {
        const customEvent = new CustomEvent("endScreenShare", {
          detail: conferenceUserId,
        });
        window.parent?.document.dispatchEvent(customEvent);
      });

      service.current.start();
    }
  }, [session, isLoading, roomId]);

  return null;
}
