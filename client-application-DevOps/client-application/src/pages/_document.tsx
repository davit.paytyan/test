import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <script src="https://cdn.polyfill.io/v2/polyfill.min.js" />
          <link rel="icon" type="image/png" sizes="16x16" href="/favicon.png"></link>
        </Head>
        <body className="pxw">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
