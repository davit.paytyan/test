import { ErrorPage } from "@pxwlab/katana-cb";
import { useRouter } from "next/router";
import React from "react";
import LayoutOnboarding from "../components/layout/LayoutOnboarding";

const Error = ({ statusCode }) => {
  const router = useRouter();

  console.log({ statusCode });

  const error404Text =
    "<strong>Got Lost?</strong> Sorry, this page could not be found. Please try again later or check your URL.";

  const error500Text =
    "<strong>Whoops!</strong> It may be a good idea to inform Huston about a problem. Please try again later.";

  return (
    <LayoutOnboarding>
      <ErrorPage
        // @ts-ignore wrong prop-types
        code={String(statusCode)}
        copy={statusCode === 404 ? error404Text : error500Text}
        label={"Return Home"}
        image={"/images/cb-robot.svg"}
        onClick={() => {
          router.push("/");
        }}
      />
    </LayoutOnboarding>
  );
};

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode: Number(statusCode) };
};

export default Error;
