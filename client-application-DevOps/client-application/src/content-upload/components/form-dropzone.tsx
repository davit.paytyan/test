/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { ListStatus } from "@pxwlab/katana-cb";
import { useSelector } from "@xstate/react";
import cx from "classnames";
import React, { useCallback, useMemo } from "react";
import { DropzoneOptions, useDropzone } from "react-dropzone";
import { ActorRefFrom } from "xstate";
import { FileUploadCollectionMachine } from "../fsm/file-upload-collection-machine";

type Ref = ActorRefFrom<typeof FileUploadCollectionMachine>;

type Props = React.PropsWithChildren<{
  actorRef: Ref;
  utilClassNames?: string;
  loud?: boolean;
  tabIndex?: number;
  dropzoneOptions?: DropzoneOptions;
}>;

function FormDropzoneMessage({ message }) {
  return (
    <div className="cb-form-dropzone__message">
      <div className="cb-form-dropzone__message-wrapper">{message}</div>
    </div>
  );
}

export function FormDropzone({
  actorRef,
  utilClassNames,
  loud,
  tabIndex,
  dropzoneOptions,
  children,
}: Props) {
  const useDropzoneOptions: DropzoneOptions = useMemo(() => {
    return {
      noClick: true,
      maxFiles: 20,
      ...dropzoneOptions,
      onDrop: (acceptedFiles) => {
        actorRef.send({ type: "addFiles", files: acceptedFiles });
      },
    };
  }, [dropzoneOptions, actorRef]);

  const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, open } =
    useDropzone(useDropzoneOptions);

  const isCollecting = useSelector(actorRef, (state) => {
    return state.matches("collecting");
  });

  const isEmpty = useSelector(actorRef, (state) => {
    return state.matches("collecting.empty");
  });

  const isNotEmpty = !isEmpty;

  const handleDropzoneClick = useCallback(
    (event) => {
      const { classList } = event.target as HTMLElement;
      if (
        classList.contains("cb-form-dropzone") ||
        classList.contains("cb-form-dropzone__message") ||
        classList.contains("cb-form-dropzone__message-wrapper") ||
        classList.contains("cb-form-dropzone__file-wrapper") ||
        classList.contains("cb-list-status__body-content")
      ) {
        open();
      }
    },
    [open]
  );

  const renderDragActiveMessage = useCallback(() => {
    const message = isCollecting && isEmpty ? "Drop the files here ..." : "Add files ...";
    return <FormDropzoneMessage message={message} />;
  }, [isCollecting, isEmpty]);

  const renderDragInactiveMessage = useCallback(() => {
    if (isCollecting && isEmpty) {
      return <FormDropzoneMessage message="Drag 'n' drop files here ..." />;
    }
    return null;
  }, [isCollecting, isEmpty]);

  const classNames = cx("cb-form-dropzone", {
    [`${utilClassNames}`]: utilClassNames,
    "cb-form-dropzone--loud": loud,
    "cb-form-dropzone--active": isDragActive,
    "cb-form-dropzone--accept": isDragAccept,
    "cb-form-dropzone--reject": isDragReject,
    "cb-form-dropzone--filled": isNotEmpty,
  });

  if (isCollecting) {
    return (
      <div className={classNames} {...getRootProps()} onClick={handleDropzoneClick}>
        <input {...getInputProps()} tabIndex={tabIndex} />
        {isDragActive ? renderDragActiveMessage() : renderDragInactiveMessage()}
        <div className="cb-form-dropzone__file">
          <ListStatus>{children}</ListStatus>
        </div>
      </div>
    );
  }

  return (
    <div className={classNames}>
      <div className="cb-form-dropzone__file">
        <ListStatus>{children}</ListStatus>
      </div>
    </div>
  );
}
/* eslint-enable jsx-a11y/no-static-element-interactions */
/* eslint-enable jsx-a11y/click-events-have-key-events */
