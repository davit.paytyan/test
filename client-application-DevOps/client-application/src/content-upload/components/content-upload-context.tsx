import { useInterpret, useSelector } from "@xstate/react";
import useDirectusSDK from "hooks/directus/use-directus-sdk";
import React from "react";
import { ActorRefFrom, InterpreterFrom } from "xstate";
import {
  FileUploadCollectionMachine,
  FileUploadCollectionModel,
} from "../fsm/file-upload-collection-machine";
import { FileUploadRequestMachine } from "../fsm/file-upload-request-machine";
import { useFileCollectionItems } from "../hooks/use-file-collection-actor-data";

const devTools = false;

type ProviderProps = React.PropsWithChildren<{ roomId: string }>;

type ContextValue = {
  fileUploadCollectionService?: InterpreterFrom<typeof FileUploadCollectionMachine>;
  fileCollectionItems?: ActorRefFrom<typeof FileUploadRequestMachine>[];
  isUploadInProgress: boolean;
  showUploadButton: boolean;
  showUploadModal: boolean;
  uploadCount: number;
  startUpload: () => void;
  toggleUploadModal: () => void;
  openUploadModal: () => void;
  closeUploadModal: () => void;
};

const ContentUploadContext = React.createContext<ContextValue>({
  isUploadInProgress: false,
  showUploadButton: false,
  showUploadModal: false,
  uploadCount: 0,
  startUpload: () => null,
  toggleUploadModal: () => null,
  openUploadModal: () => null,
  closeUploadModal: () => null,
});

export function useContentUploadContext() {
  return React.useContext(ContentUploadContext);
}

export function ContentUploadContextProvider({ roomId, children }: ProviderProps) {
  const sdk = useDirectusSDK();

  const fileUploadCollectionService = useInterpret(FileUploadCollectionMachine, { devTools });
  const fileCollectionItems = useFileCollectionItems(fileUploadCollectionService);
  const [showUploadModal, setShowUploadModal] = React.useState(false);

  const isUploadInProgress = useSelector(fileUploadCollectionService, (state) => {
    return state.matches("uploadInProgress");
  });

  const uploadCount = useSelector(fileUploadCollectionService, (state) => {
    return state.context.activeFileUploads;
  });

  const showUploadButton = useSelector(fileUploadCollectionService, (state) => {
    return state.matches("collecting.filled") || state.matches("collecting.full");
  });

  const startUpload = React.useCallback(() => {
    const action = FileUploadCollectionModel.events.startFileUpload(
      sdk.transport,
      "/files",
      roomId
    );
    fileUploadCollectionService.send(action);
  }, [fileUploadCollectionService, sdk, roomId]);

  const toggleUploadModal = React.useCallback(
    () => setShowUploadModal(!showUploadModal),
    [showUploadModal]
  );

  const openUploadModal = React.useCallback(() => setShowUploadModal(true), []);
  const closeUploadModal = React.useCallback(() => setShowUploadModal(false), []);

  const value = React.useMemo(
    () => ({
      fileUploadCollectionService,
      fileCollectionItems,
      showUploadModal,
      isUploadInProgress,
      showUploadButton,
      startUpload,
      toggleUploadModal,
      openUploadModal,
      closeUploadModal,
      uploadCount,
    }),
    [
      fileUploadCollectionService,
      fileCollectionItems,
      showUploadModal,
      isUploadInProgress,
      showUploadButton,
      startUpload,
      toggleUploadModal,
      openUploadModal,
      closeUploadModal,
      uploadCount,
    ]
  );

  return <ContentUploadContext.Provider value={value}>{children}</ContentUploadContext.Provider>;
}
