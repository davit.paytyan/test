import { Button, ListStatusItem } from "@pxwlab/katana-cb";
import { useSelector } from "@xstate/react";
import React, { useMemo } from "react";
import { ActorRefFrom } from "xstate";
import { FileUploadRequestMachine } from "../fsm/file-upload-request-machine";
import { useFormattedFileSize } from "../hooks/use-formatted-file-size";

type Ref = ActorRefFrom<typeof FileUploadRequestMachine>;

type Props = {
  actorRef: Ref;
};

export function FormDropzoneFileItem({ actorRef }: Props) {
  const file = useSelector(actorRef, (state) => {
    return state.context.file;
  });

  const uploadPending = useSelector(actorRef, (state) => {
    return state.matches("pending");
  });

  const uploadInProgress = useSelector(actorRef, (state) => {
    return state.matches("uploading");
  });

  const uploadComplete = useSelector(actorRef, (state) => {
    return state.matches("completed");
  });

  const uploadFailed = useSelector(actorRef, (state) => {
    return state.matches("failed");
  });

  const displaySize = useFormattedFileSize(file ? file.size : 0);

  const icon = useMemo(() => {
    return uploadPending ? "delete-bin-line" : "close-line";
  }, [uploadPending]);

  const status = useMemo(() => {
    if (uploadInProgress) return "uploading";
    if (uploadComplete) return "success";
    if (uploadFailed) return "failed";
    return "";
  }, [uploadInProgress, uploadComplete, uploadFailed]);

  if (!file) return null;

  return (
    <ListStatusItem
      icon="file-text-line"
      title={file.name}
      details={displaySize}
      status={status}
      loading={uploadInProgress}
      error={uploadFailed}
      success={uploadComplete}
    >
      <Button
        ghost
        link
        square
        tiny
        icon={icon}
        onClick={() => actorRef.send({ type: "destroy" })}
      />
    </ListStatusItem>
  );
}
