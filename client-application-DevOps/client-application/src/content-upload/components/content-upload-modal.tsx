import { Button, Modal, ModalBody, ModalFooter, ModalHeader, ModalPortal } from "@pxwlab/katana-cb";
import { FormDropzone } from "content-upload/components/form-dropzone";
import { FormDropzoneFileItem } from "content-upload/components/form-dropzone-file-item";
import React from "react";
import { useContentUploadContext } from "./content-upload-context";

export default function ContentUploadModal() {
  const {
    fileCollectionItems,
    fileUploadCollectionService,
    startUpload,
    showUploadModal,
    showUploadButton,
    closeUploadModal,
  } = useContentUploadContext();

  if (!fileCollectionItems || !fileUploadCollectionService) return null;

  return (
    <ModalPortal show={showUploadModal}>
      <Modal onClose={() => closeUploadModal()}>
        <ModalHeader title="Upload Content" />
        <ModalBody>
          <FormDropzone
            actorRef={fileUploadCollectionService}
            dropzoneOptions={{ accept: "image/*", multiple: true }}
          >
            {fileCollectionItems.map((actorRef) => {
              return <FormDropzoneFileItem key={actorRef.id} actorRef={actorRef} />;
            })}
          </FormDropzone>
          <p>
            <strong>Note:</strong> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor <strong>.JPEG</strong>, <strong>.PPT</strong>,{" "}
            <strong>.PDF</strong> sed diam nonumy eirmod tempor <strong>32 MB</strong>.
          </p>
        </ModalBody>
        <ModalFooter>
          {showUploadButton && (
            <Button
              cta
              label="Upload"
              onClick={() => {
                startUpload();
                closeUploadModal();
              }}
            />
          )}
        </ModalFooter>
      </Modal>
    </ModalPortal>
  );
}
