import { ITransport } from "@directus/sdk";
import { assign } from "@xstate/immer";
import { actions, sendParent } from "xstate";
import { createModel } from "xstate/lib/model";
import { removeFileExtension } from "../utils/remove-file-extension";

const initialContext: {
  file?: File;
  error?: Error;
  bytesTotal: number;
  bytesLoaded: number;
  progress: number;
} = {
  bytesLoaded: 0,
  bytesTotal: 0,
  progress: 0,
};

export const FileUploadRequestModel = createModel(initialContext, {
  events: {
    setFile: (file: File) => {
      return { file };
    },
    startRequest: (transport: ITransport, url: string, room: string) => {
      return { transport, url, room };
    },
    updateProgress: (loaded: number, total: number) => {
      return {
        loaded,
        total,
        progress: total > 0 ? Math.max(1, Math.min(0, loaded / total)) : 0,
      };
    },
    requestCompleted: () => ({}),
    requestFailed: (error: Error) => {
      return { error };
    },
    destroy: () => ({}),
  },
});

export const FileUploadRequestMachine = FileUploadRequestModel.createMachine(
  {
    context: FileUploadRequestModel.initialContext,
    initial: "created",
    states: {
      created: {
        on: {
          setFile: {
            actions: "assignFile",
          },
        },
        always: {
          cond: "ifFileSet",
          target: "pending",
        },
      },
      pending: {
        on: {
          startRequest: "uploading",
        },
      },
      uploading: {
        entry: "onStarted",
        invoke: {
          id: "uploadRequestCallback",
          src: "uploadRequestCallback",
        },
        on: {
          requestCompleted: {
            target: "completed",
          },
          requestFailed: {
            target: "failed",
            actions: "assignError",
          },
        },
      },
      completed: {
        entry: ["log", "onCompleted"],
        data: (context) => ({
          file: context.file,
          success: true,
          error: null,
        }),
      },
      failed: {
        entry: ["log", "onFailed"],
        data: (context) => ({
          file: context.file,
          success: false,
          error: context.error,
        }),
      },
      destroyed: {
        type: "final",
        entry: ["log", "onDestroyed"],
        data: (context) => ({
          file: context.file,
          success: typeof context.error === "undefined",
          error: context.error,
        }),
      },
    },
    on: {
      destroy: {
        target: "destroyed",
      },
    },
  },
  {
    guards: {
      ifFileSet: (context) => {
        return typeof context.file !== "undefined";
      },
    },
    actions: {
      log: actions.log(),
      assignFile: assign((context, event) => {
        if (event.type !== "setFile") return;
        context.file = event.file;
      }),
      assignError: assign((context, event) => {
        if (event.type !== "requestFailed") return;
        context.error = event.error;
      }),
      onStarted: sendParent((context) => ({
        type: "fileUploadStarted",
        file: context.file,
      })),
      onCompleted: sendParent((context) => ({
        type: "fileUploadDone",
        file: context.file,
        success: true,
        error: null,
      })),
      onFailed: sendParent((context) => ({
        type: "fileUploadDone",
        file: context.file,
        success: false,
        error: context.error,
      })),
      onDestroyed: sendParent((context) => {
        return { type: "removeFiles", files: [context.file] };
      }),
    },
    services: {
      uploadRequestCallback: (context, event) => (callback) => {
        const { file } = context;

        if (!file) {
          const error = new Error("File is missing!");
          return callback(FileUploadRequestModel.events.requestFailed(error));
        }

        if (event.type !== "startRequest") {
          const error = new Error("UploadRequestCallback must only be invoked on event 'start'");
          return callback(FileUploadRequestModel.events.requestFailed(error));
        }

        const { transport, url, room } = event;

        const formData = new FormData();

        formData.append("title", removeFileExtension(file.name));
        formData.append("room", room);
        formData.append("contentUploadModule", "true");
        formData.append("file", file);

        const transportOptions = {
          onUploadProgress: (progressEvent) => {
            const { total, loaded } = progressEvent;
            const event = FileUploadRequestModel.events.updateProgress(loaded, total);
            callback(event);
          },
        };

        transport
          .post(url, formData, transportOptions)
          .catch((error) => callback(FileUploadRequestModel.events.requestFailed(error)))
          .then(() => callback(FileUploadRequestModel.events.requestCompleted()));
      },
    },
  }
);
