import { ITransport } from "@directus/sdk";
import { assign } from "@xstate/immer";
import { ActorRefFrom, AnyEventObject, spawn } from "xstate";
import { pure, send, stop } from "xstate/lib/actions";
import { createModel } from "xstate/lib/model";
import { FileUploadRequestMachine, FileUploadRequestModel } from "./file-upload-request-machine";

export const FileUploadCollectionModel = createModel(
  {
    files: [] as File[],
    fileRefs: new WeakMap<File, ActorRefFrom<typeof FileUploadRequestMachine>>(),
    maxFiles: 0,
    activeFileUploads: 0,
  },
  {
    events: {
      lock: () => ({}),
      unlock: () => ({}),
      addFiles: (files: File[]) => {
        return { files };
      },
      removeFiles: (files: File[]) => {
        return { files };
      },
      clearFiles: () => ({}),
      startFileUpload: (transport: ITransport, url: string, room: string) => {
        return { transport, url, room };
      },
      fileUploadStarted: (file: File) => {
        return { file };
      },
      fileUploadDone: (file: File, success: boolean, error: Error | null) => {
        return { file, success, error };
      },
    },
  }
);

export const FileUploadCollectionMachine = FileUploadCollectionModel.createMachine(
  {
    context: FileUploadCollectionModel.initialContext,
    initial: "collecting",
    states: {
      collecting: {
        type: "compound",
        initial: "empty",
        states: {
          empty: {
            on: {
              removeFiles: undefined,
              startFileUpload: undefined,
            },
          },
          filled: {
            always: [
              {
                cond: "isEmpty",
                target: "empty",
              },
              {
                cond: "isFull",
                target: "full",
              },
            ],
          },
          full: {
            on: {
              addFiles: undefined,
            },
          },
        },
        on: {
          addFiles: {
            target: ".filled",
            actions: ["assignFiles", "addFileActors"],
          },
          removeFiles: {
            target: ".filled",
            actions: ["assignFiles", "removeFileActors"],
          },
          clearFiles: {
            actions: "sendRemoveAllFiles",
          },
          startFileUpload: {
            target: "uploadInProgress",
            actions: "startFileUploadRequests",
          },
        },
      },
      uploadInProgress: {
        on: {
          fileUploadStarted: {
            actions: "assignActiveFileUploads",
          },
          fileUploadDone: [
            {
              cond: "ifAllUploadsAreDone",
              target: "uploadCompleted",
              actions: "assignActiveFileUploads",
            },
            {
              actions: "assignActiveFileUploads",
            },
          ],
        },
      },
      uploadCompleted: {
        on: {
          removeFiles: {
            actions: ["assignFiles", "removeFileActors"],
          },
        },
        always: {
          cond: "isEmpty",
          target: "collecting",
        },
      },
    },
  },
  {
    guards: {
      isEmpty: (context) => context.files.length === 0,
      isFull: (context) => context.maxFiles > 0 && context.files.length >= context.maxFiles,
      ifAllUploadsAreDone: (context) => {
        const { files, fileRefs } = context;
        return files.every((file) => {
          const ref = fileRefs.get(file);
          if (!ref) return true;
          const refSnapshot = ref.getSnapshot();
          return refSnapshot?.matches("completed") ?? refSnapshot?.matches("failed");
        });
      },
    },
    actions: {
      assignFiles: FileUploadCollectionModel.assign({
        files: (context, event) => {
          let currentFiles: File[];
          switch (event.type) {
            case "addFiles":
              currentFiles = [...context.files];
              for (const file of event.files) {
                const fileIndex = currentFiles.indexOf(file);
                if (fileIndex !== -1) continue;
                currentFiles.push(file);
              }
              return currentFiles;

            case "removeFiles":
              currentFiles = [...context.files];
              for (const file of event.files) {
                const fileIndex = currentFiles.indexOf(file);
                if (fileIndex === -1) continue;
                currentFiles.splice(fileIndex, 1);
              }
              return currentFiles;

            default:
              return context.files;
          }
        },
      }),
      assignActiveFileUploads: FileUploadCollectionModel.assign({
        activeFileUploads: (context) => {
          let count = 0;
          for (const file of context.files) {
            const ref = context.fileRefs.get(file);
            if (!ref) continue;
            const snapshot = ref.getSnapshot();
            if (snapshot?.matches("uploading")) count++;
          }
          return count;
        },
      }),
      sendRemoveAllFiles: send((context) => {
        return {
          type: "removeFiles",
          files: context.files,
        };
      }),
      addFileActors: assign((context, event) => {
        if (event.type !== "addFiles") return;
        const { files } = event;
        for (const file of files) {
          if (context.fileRefs.has(file)) continue;
          const actorConfig = FileUploadRequestMachine.withContext({
            ...FileUploadRequestMachine.context,
            file,
          });
          context.fileRefs.set(file, spawn(actorConfig));
        }
        return context;
      }),
      removeFileActors: pure((context, event) => {
        if (event.type !== "removeFiles") return;
        const actions = [] as AnyEventObject[];
        const { files } = event;
        for (const file of files) {
          const ref = context.fileRefs.get(file);
          if (!ref) continue;
          context.fileRefs.delete(file);
          actions.push(stop(ref.id));
        }
        return actions;
      }),
      startFileUploadRequests: (context, event) => {
        if (event.type !== "startFileUpload") return;
        const { files, fileRefs } = context;
        for (const file of files) {
          const ref = fileRefs.get(file);
          if (!ref) continue;
          const action = FileUploadRequestModel.events.startRequest(
            event.transport,
            event.url,
            event.room
          );
          ref.send(action);
        }
      },
    },
  }
);
