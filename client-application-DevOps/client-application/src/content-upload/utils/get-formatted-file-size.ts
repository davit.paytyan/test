export const getFormattedFileSize = (size: number) => {
  let suffix = "B";
  let divideFactor = 1;

  if (size > 1000) {
    suffix = "KB";
    divideFactor = 1000;
  }

  if (size > 1000 * 1000) {
    suffix = "MB";
    divideFactor = 1000 * 1000;
  }

  if (size > 1000 * 1000 * 1000) {
    suffix = "GB";
    divideFactor = 1000 * 1000 * 1000;
  }

  return `${(size / divideFactor).toFixed(1)} ${suffix}`;
};
