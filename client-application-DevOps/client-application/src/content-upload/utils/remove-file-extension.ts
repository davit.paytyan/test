export function removeFileExtension(filename: string) {
  return filename.includes(".") ? filename.split(".").slice(0, -1).join(".") : filename;
}
