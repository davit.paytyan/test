import { useCallback, useMemo } from "react";
import { ActorRefFrom } from "xstate";
import { FileUploadCollectionMachine } from "../fsm/file-upload-collection-machine";

export function useFileCollectionActorEvents(
  actorRef: ActorRefFrom<typeof FileUploadCollectionMachine>
) {
  const sendAddFilesEvent = useCallback(
    (files: File[]) => {
      actorRef.send({ type: "addFiles", files });
    },
    [actorRef]
  );

  const sendRemoveFilesEvent = useCallback(
    (files: File[]) => {
      actorRef.send({ type: "removeFiles", files });
    },
    [actorRef]
  );

  const sendClearFilesEvent = useCallback(() => {
    actorRef.send({ type: "clearFiles" });
  }, [actorRef]);

  const events = useMemo(() => {
    return {
      sendAddFilesEvent,
      sendRemoveFilesEvent,
      sendClearFilesEvent,
    };
  }, [sendAddFilesEvent, sendRemoveFilesEvent, sendClearFilesEvent]);

  return events;
}
