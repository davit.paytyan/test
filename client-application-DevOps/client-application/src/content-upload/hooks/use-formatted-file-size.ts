import { useMemo } from "react";
import { getFormattedFileSize } from "../utils/get-formatted-file-size";

export function useFormattedFileSize(size: number) {
  return useMemo(() => {
    return getFormattedFileSize(size);
  }, [size]);
}
