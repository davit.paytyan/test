import { useSelector } from "@xstate/react";
import { useMemo } from "react";
import { ActorRefFrom } from "xstate";
import { FileUploadCollectionMachine } from "../fsm/file-upload-collection-machine";
import { FileUploadRequestMachine } from "../fsm/file-upload-request-machine";

export function useFileCollectionItems(actorRef: ActorRefFrom<typeof FileUploadCollectionMachine>) {
  const actors = useSelector(actorRef, (state) => {
    const { children } = state;
    return children;
  });

  const fileActors = useMemo(() => {
    return Object.values(actors) as ActorRefFrom<typeof FileUploadRequestMachine>[];
  }, [actors]);

  return fileActors;
}
