import { Directus, MemoryStorage, PartialItem, UserItem } from "@directus/sdk";
import { DataModel, TypedDirectus, User } from "client-application/actionbox-sdk/models";
import Cookies from "cookies";
import { IncomingMessage, ServerResponse } from "http";
import { NextApiRequest, NextApiResponse } from "next";
import { NextApiRequestCookies } from "next/dist/server/api-utils";

const COOKIE_NAME = process.env.REFRESH_TOKEN_NAME ?? "directus_refresh_token";
const COOKIE_SECURE = process.env.COOKIE_SECURE === "true";
const COOKIE_DOMAIN = process.env.REFRESH_TOKEN_COOKIE_DOMAIN ?? ".together.localhost";
const INTERNAL_API_URL = process.env.INTERNAL_API_URL ?? "";

export type Session = PartialItem<UserItem<User>> & { id: string };

export async function ensureDirectusSession(
  request:
    | NextApiRequest
    | (IncomingMessage & {
        cookies: NextApiRequestCookies;
      }),
  response: NextApiResponse | ServerResponse
): Promise<[Session | null, TypedDirectus]> {
  const cookies = new Cookies(request, response, { secure: COOKIE_SECURE });
  const refresh_token = cookies.get(COOKIE_NAME);

  const storage = new MemoryStorage();
  storage.auth_token = "none";
  storage.auth_expires = 900;
  storage.auth_refresh_token = refresh_token ?? "";

  const directus = new Directus<DataModel>(INTERNAL_API_URL, { storage });

  if (typeof refresh_token === "undefined") return [null, directus];

  let result;

  try {
    result = await directus.auth.refresh(true);
  } catch {
    return [null, directus];
  }

  cookies.set(COOKIE_NAME, result.refresh_token, { domain: COOKIE_DOMAIN });

  const user = await directus.users.me.read();

  return [user as Session, directus];
}
