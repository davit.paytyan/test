import { parse } from "querystring";

export function parseCallbackUrl(callbackUrl: string, isEncoded = true) {
  if (!isEncoded) return callbackUrl;
  const parsedQuerystring = parse(`callbackUrl=${callbackUrl}`) as { callbackUrl: string };
  return Buffer.from(parsedQuerystring.callbackUrl, "base64").toString("utf-8");
}
