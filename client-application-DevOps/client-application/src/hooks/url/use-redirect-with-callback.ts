import { useRouter } from "next/router";
import { useCallback } from "react";

interface TransitionOptions {
  shallow?: boolean;
  locale?: string | false;
  scroll?: boolean;
}

const useRedirectWithCallback = (url: string) => {
  const router = useRouter();
  const currentUrl = typeof window !== "undefined" ? window.location.href : "";
  return useCallback(
    (replace = false, options?: TransitionOptions) => {
      if (replace) {
        void router.replace(`${url}?callbackUrl=${currentUrl}`, undefined, options);
      }
      void router.push(`${url}?callbackUrl=${currentUrl}`, undefined, options);
    },
    [url, router, currentUrl]
  );
};

export default useRedirectWithCallback;
