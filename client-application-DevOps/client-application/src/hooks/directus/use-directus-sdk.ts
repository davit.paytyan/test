import { directus } from "global/singleton-directus-instance";
import { useRef } from "react";

const useDirectusSDK = () => {
  const sdk = useRef(directus);
  return sdk.current;
};

export default useDirectusSDK;
