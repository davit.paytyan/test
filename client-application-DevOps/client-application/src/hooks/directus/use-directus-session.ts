import { PartialItem, UserItem } from "@directus/sdk";
import { TypedDirectus } from "client-application/actionbox-sdk/models";
import Logger from "loglevel";
import { useCallback, useEffect, useState } from "react";
import useRedirectWithCallback from "../url/use-redirect-with-callback";
import useDirectusSDK from "./use-directus-sdk";

const logger = Logger.getLogger("auth");
logger.setLevel(Logger.levels.DEBUG);

type User = PartialItem<UserItem<{ id: string }>>;
type Returned = [User | null, boolean, TypedDirectus];

const useDirectusSession = (redirect = true, redirectTo = "/auth/login"): Returned => {
  const sdk = useDirectusSDK();
  const redirectWithCallback = useRedirectWithCallback(redirectTo);
  const [isValidating, setIsValidating] = useState(true);
  const [session, setSession] = useState<User | null>(null);

  // TODO: don't call users.me use auth.refresh instead
  const validateSession = useCallback(() => {
    sdk.users.me
      .read()
      .then((result) => {
        Logger.info(result);
        setSession(result);
        setIsValidating(false);
      })
      .catch(() => {
        if (redirect) redirectWithCallback();
        setIsValidating(false);
      });
  }, [sdk, redirect, redirectWithCallback]);

  useEffect(() => validateSession(), [validateSession]);

  return [session, isValidating, sdk];
};

export default useDirectusSession;
