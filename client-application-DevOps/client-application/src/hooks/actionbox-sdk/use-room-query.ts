import getRoom, { GetRoomData } from "actionbox-sdk/getRoom/getRoom";
import { NotFoundError } from "client-application/actionbox-sdk/models";
import useDirectusSDK from "hooks/directus/use-directus-sdk";
import { useQuery, UseQueryOptions, UseQueryResult } from "react-query";

export function useRoomQuery(
  roomId: string,
  options: UseQueryOptions<GetRoomData, NotFoundError> = {}
): [GetRoomData | undefined, Omit<UseQueryResult<GetRoomData, NotFoundError>, "data">] {
  const sdk = useDirectusSDK();
  const { data: room, ...query } = useQuery<GetRoomData, NotFoundError>(
    ["room", roomId],
    () => getRoom(sdk, roomId),
    options
  );
  return [room, query];
}
