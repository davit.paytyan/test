import { createInvitation } from "actionbox-sdk";
import { CreateInvitationData } from "actionbox-sdk/createInvitation/createInvitation";
import useDirectusSDK from "hooks/directus/use-directus-sdk";
import { useMutation, UseMutationOptions, UseMutationResult } from "react-query";

export function useCreateInvitationMutation(
  roomId: string,
  options: UseMutationOptions<CreateInvitationData> = {}
): [CreateInvitationData | undefined, Omit<UseMutationResult<CreateInvitationData>, "data">] {
  const sdk = useDirectusSDK();
  const { data, ...mutation } = useMutation({
    mutationKey: ["room", "invitations", roomId],
    mutationFn: () => createInvitation(sdk, roomId),
    ...options,
  });
  return [data, mutation];
}
