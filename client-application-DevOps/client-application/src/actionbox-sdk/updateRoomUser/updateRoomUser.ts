import { PartialItem } from "@directus/sdk";
import { RoomUser, TypedDirectus } from "client-application/actionbox-sdk/models";

export type Data = Omit<RoomUser, "id" | "date_created" | "date_updated">;

/**
 * Updates a room user
 */
const updateRoomUser = async (
  directus: TypedDirectus,
  roomId: string,
  roomUserData: Data
): Promise<PartialItem<RoomUser> & { id: string }> => {
  const { data } = await directus.transport.patch(`/custom/room-user/${roomId}`, roomUserData);
  return data;
};

export default updateRoomUser;
