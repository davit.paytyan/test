import { Directus, ID } from "@directus/sdk";
import { TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import createInvitation from "../createInvitation/createInvitation";
import createRoom from "../createRoom/createRoom";
import createRoomUser from "../createRoomUser/createRoomUser";
import updateRoomUser from "../updateRoomUser/updateRoomUser";
import { createRandomApplicationUser } from "../__tests__/utils/create-random-application-user";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";

describe("updateRoomUser()", () => {
  let directus: TypedDirectus;
  let adminUser: User;

  const createdRooms: ID[] = [];
  const createdUsers: ID[] = [];

  beforeAll(async () => {
    ({ directus, me: adminUser } = await prepareDirectus());
  });

  afterAll(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
    await directus.users.deleteMany(createdUsers);
  });

  test("Updates a room user", async () => {
    const createdUser = await createRandomApplicationUser(directus);
    createdUsers.push(createdUser.id);

    const room = await createRoom(directus, { name: "test" }, adminUser.id);
    createdRooms.push(room.id);

    const invitation = await createInvitation(directus, room.id);
    await createRoomUser(directus, room.id, createdUser.id, invitation.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: createdUser.email, password: "123456" });

    const newNickname = "arthas1";
    const updatedRoomUser = await updateRoomUser(sdk, room.id, { nickname: newNickname });

    expect(updatedRoomUser).toMatchObject({
      nickname: newNickname,
      date_updated: expect.any(String),
    });
  });

  test("throws an error because the current user is not a room member", async () => {
    const createdUser = await createRandomApplicationUser(directus);
    createdUsers.push(createdUser.id);

    const room = await createRoom(directus, { name: "test" }, adminUser.id);
    createdRooms.push(room.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: createdUser.email, password: "123456" });

    const newNickname = "arthas1";

    await expect(() => updateRoomUser(sdk, room.id, { nickname: newNickname })).rejects.toThrow();
  });

  test("Throws an error if provided incorrect id", async () => {
    await expect(() => updateRoomUser(directus, "smth", {})).rejects.toThrow();
  });
});
