import { Directus } from "@directus/sdk";
import { TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import createRoom from "../createRoom/createRoom";
import getRoom from "../getRoom/getRoom";
import setRoomName from "../setRoomName/setRoomName";
import { createRandomApplicationUser } from "../__tests__/utils/create-random-application-user";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";

describe("setRoomName()", () => {
  let directus: TypedDirectus;
  let adminUser: User;

  const createdRooms: string[] = [];
  const createdUsers: string[] = [];

  beforeEach(async () => {
    ({ directus, me: adminUser } = await prepareDirectus());
  });

  afterEach(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
    await directus.users.deleteMany(createdUsers);
  });

  test("updates the room's name", async () => {
    const adminRoom = await createRoom(directus, { name: "test" }, adminUser.id);
    createdRooms.push(adminRoom.id);

    await setRoomName(directus, adminRoom.id, "toller name");

    const room = await getRoom(directus, adminRoom.id);
    createdRooms.push(room.id);

    expect(room.name).toBe("toller name");
  });

  test("throws an error if the user is not allowed to change the room name", async () => {
    const user = await createRandomApplicationUser(directus);
    const adminRoom = await createRoom(directus, { name: "test" }, adminUser.id);

    createdRooms.push(adminRoom.id);
    createdUsers.push(user.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: user.email, password: "123456" });

    await expect(() => setRoomName(sdk, adminRoom.id, "toller name")).rejects.toThrow("access");
  });

  test("throws an 404 error if room is not found", async () => {
    await expect(() => setRoomName(directus, "blablabla", "foo")).rejects.toThrow("not found");
  });
});
