import { TypedDirectus } from "client-application/actionbox-sdk/models";
import updateRoom from "../updateRoom/updateRoom";

/**
 * Set room's name
 */
const setRoomName = async (directus: TypedDirectus, roomId: string, name: string) => {
  return updateRoom(directus, roomId, { name });
};

export default setRoomName;
