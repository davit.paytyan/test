import { PartialItem } from "@directus/sdk";
import { Room, TypedDirectus } from "client-application/actionbox-sdk/models";
import { ServerError } from "library/server-error";

export type GetRoomData = PartialItem<Room> & { id: string };

/**
 * Returns rooms by id
 */
const getRoom = async (directus: TypedDirectus, roomId: string): Promise<GetRoomData> => {
  try {
    const { data } = await directus.transport.get(`/custom/rooms/${roomId}`);
    return data;
  } catch (error) {
    const { response } = error;
    if (response?.status === 403) throw new ServerError(403, "Access Denied!");
    if (response?.status === 404) throw new ServerError(404, "Room not Found!");
    throw error;
  }
};

export default getRoom;
