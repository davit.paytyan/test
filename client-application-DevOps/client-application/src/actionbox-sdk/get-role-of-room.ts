import { Room } from "client-application/actionbox-sdk/models";

export function getRoleOfRoom(roleName: string, room: Room) {
  return room.roles
    ? room.roles.find((role) => role.name.toLowerCase() === roleName.toLowerCase())
    : null;
}
