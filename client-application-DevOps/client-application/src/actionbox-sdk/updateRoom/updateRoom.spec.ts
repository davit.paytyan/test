import { Directus, TransportError } from "@directus/sdk";
import { Room, TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import createInvitation from "../createInvitation/createInvitation";
import createRoom from "../createRoom/createRoom";
import createRoomUser from "../createRoomUser/createRoomUser";
import { getRoleOfRoom } from "../get-role-of-room";
import { createRandomApplicationUser } from "../__tests__/utils/create-random-application-user";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";
import updateRoom from "./updateRoom";

describe("updateRoom()", () => {
  let directus: TypedDirectus;
  let admin: User;

  const createdRooms: string[] = [];
  const createdUsers: string[] = [];

  beforeEach(async () => {
    ({ directus, me: admin } = await prepareDirectus());
  });

  afterEach(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
    await directus.users.deleteMany(createdUsers);
  });

  test("Updates a room if the current logged in user is the owner", async () => {
    const mockData = { name: "Civilization V FFA" };

    const user = await createRandomApplicationUser(directus);
    createdUsers.push(user.id);

    const room = await createRoom(directus, { name: mockData.name }, user.id);
    createdRooms.push(room.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: user.email, password: "123456" });

    const updateData = { name: "Civilization VI FFA" };
    const updatedRoom = await updateRoom(sdk, room.id, updateData);

    expect(updatedRoom.name).toMatch(updateData.name);
  });

  test("Updates a room if the current logged in user is a moderator", async () => {
    const mockData = { name: "Civilization V FFA" };

    const user = await createRandomApplicationUser(directus);
    createdUsers.push(user.id);
    const room = await createRoom(directus, { name: mockData.name }, admin.id);
    createdRooms.push(room.id);

    const role = getRoleOfRoom("moderator", room as Room);
    const invitation = await createInvitation(directus, room.id);

    await createRoomUser(directus, room.id, user.id, invitation.id, role?.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: user.email, password: "123456" });

    const updateData = { name: "Civilization VI FFA" };
    const updatedRoom = await updateRoom(sdk, room.id, updateData);

    expect(updatedRoom.name).toMatch(updateData.name);
  });

  test("Throws an error if a user is not an owner or a moderator", async () => {
    const mockData = { name: "Civilization V FFA" };

    const user = await createRandomApplicationUser(directus);
    createdUsers.push(user.id);
    const room = await createRoom(directus, { name: mockData.name }, admin.id);
    createdRooms.push(room.id);

    const invitation = await createInvitation(directus, room.id);

    await createRoomUser(directus, room.id, user.id, invitation.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: user.email, password: "123456" });

    const updateData = { name: "Civilization VI FFA" };

    await expect(() => updateRoom(sdk, room.id, updateData)).rejects.toThrow(
      new TransportError(null, {
        status: 403,
        errors: [
          {
            message: "You don't have permission to access this.",
            extensions: { code: "FORBIDDEN" },
          },
        ],
      })
    );
  });

  test("throws an 404 error if room is not found", async () => {
    await expect(() => updateRoom(directus, "blablabla", { name: "foo" })).rejects.toThrow(
      new TransportError(null, {
        status: 404,
        errors: [{ message: "Room not found.", extensions: { code: "NOT FOUND" } }],
      })
    );
  });
});
