import { Room, TypedDirectus } from "client-application/actionbox-sdk/models";

export type RoomData = Omit<Room, "id" | "date_created" | "date_updated">;

/**
 * Updates a room
 */
const updateRoom = async (directus: TypedDirectus, roomId: string, roomData: RoomData) => {
  const { data } = await directus.transport.patch(`/custom/rooms/${roomId}`, roomData);
  return data;
};

export default updateRoom;
