import { Directus } from "@directus/sdk";

import { User, DataModel } from "client-application/actionbox-sdk/models";

export const fields = ["id", "first_name", "last_name", "email", "title", "avatar", "language"];

/**
 * Returns the current logged in user
 */
const getUser = async (directus: Directus<DataModel>): Promise<User> => {
  try {
    const user = await directus.users.me.read({ fields });

    return user as User;
  } catch {
    throw new Error("Cannot get a user");
  }
};

export default getUser;
