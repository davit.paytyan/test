import { Directus } from "@directus/sdk";
import { TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";
import getUser, { fields } from "./getUser";

describe("getUser()", () => {
  let directus: TypedDirectus;
  let adminUser: User;

  beforeEach(async () => {
    ({ directus, me: adminUser } = await prepareDirectus());
  });

  test("Returns a user", async () => {
    const foundUser = await getUser(directus);
    expect(adminUser).toMatchObject(foundUser);
  });

  test("Returns only particular fields", async () => {
    const foundUser = await getUser(directus);
    expect(Object.keys(foundUser)).toEqual(fields);
  });

  test("Throws an error if user not logged in", async () => {
    const sdk = new Directus(directus.transport.url);
    await expect(() => getUser(sdk)).rejects.toThrow();
  });
});
