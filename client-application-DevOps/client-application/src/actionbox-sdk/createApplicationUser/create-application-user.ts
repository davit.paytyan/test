import { TypedDirectus, User } from "client-application/actionbox-sdk/models";

type ResponseRaw = { data: { id: string } };
type ResponseData = { id: string };
type CreateUserData = Pick<User, "email" | "first_name" | "last_name" | "password" | "tags">;

/**
 * Creates a new application user
 */
const createApplicationUser = async (directus: TypedDirectus, userData: CreateUserData) => {
  // TODO: add proper error handling
  const { data } = await directus.transport.post<ResponseData, CreateUserData, ResponseRaw>(
    "/custom/create-application-user",
    userData
  );

  if (!data || !data.id) throw new Error("Could not create user!");

  return data.id;
};

export default createApplicationUser;
