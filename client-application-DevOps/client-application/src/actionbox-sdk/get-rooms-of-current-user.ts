import { PartialItem } from "@directus/sdk";
import { Room, TypedDirectus } from "client-application/actionbox-sdk/models";

export type GetRoomsOfCurrentUserDataItem = PartialItem<Room> & { id: string };
export type GetRoomsOfCurrentUserData = GetRoomsOfCurrentUserDataItem[];

/**
 * @returns the RoomUser of a room for the current logged in user
 * @returns null if user is not authorized or a member of the room
 * @throws {BaseException} 404 if room is not found
 *
 */
const getRoomsOfCurrentUser = async (
  directus: TypedDirectus
): Promise<null | GetRoomsOfCurrentUserData> => {
  try {
    const { data } = await directus.transport.get(`/custom/rooms`);
    return data;
  } catch (error) {
    const { response } = error;
    if (response.status === 403) return null;
    throw error;
  }
};

export default getRoomsOfCurrentUser;
