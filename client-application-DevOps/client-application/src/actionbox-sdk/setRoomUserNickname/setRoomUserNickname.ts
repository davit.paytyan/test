import { TypedDirectus } from "client-application/actionbox-sdk/models";
import updateRoomUser from "../updateRoomUser/updateRoomUser";

/**
 * Set new room user nickname
 */
const setRoomUserNickname = async (
  directus: TypedDirectus,
  roomUserId: string,
  nickname: string
) => {
  return updateRoomUser(directus, roomUserId, { nickname });
};

export default setRoomUserNickname;
