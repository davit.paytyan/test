import { Directus } from "@directus/sdk";
import { TypedDirectus } from "client-application/actionbox-sdk/models";
import "jest-extended";
import createRoom from "../createRoom/createRoom";
import getRoomUser from "../getRoomUser/getRoomUser";
import setRoomUserNickname from "../setRoomUserNickname/setRoomUserNickname";
import { createRandomApplicationUser } from "../__tests__/utils/create-random-application-user";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";

describe("setRoomUserNickname()", () => {
  let directus: TypedDirectus;

  const createdRooms: string[] = [];
  const createdUsers: string[] = [];

  beforeEach(async () => {
    ({ directus } = await prepareDirectus());
  });

  afterEach(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
    await directus.users.deleteMany(createdUsers);
  });

  test("Updates a room user nickname", async () => {
    const user = await createRandomApplicationUser(directus);
    createdUsers.push(user.id);

    const room = await createRoom(directus, { name: "test" }, user.id);
    createdRooms.push(room.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: user.email, password: "123456" });

    const newNickname = "Leeroy Jenkins";
    await setRoomUserNickname(sdk, room.id, newNickname);
    const roomUser = await getRoomUser(sdk, room.id);

    expect(roomUser?.nickname).toMatch(newNickname);
  });
});
