import { PartialItem } from "@directus/sdk";
import { RoomUser, TypedDirectus } from "client-application/actionbox-sdk/models";
import { ServerError } from "library/server-error";

/**
 * @returns the RoomUser of a room for the current logged in user
 * @returns null if user is not authorized or a member of the room
 * @throws {BaseException} 404 if room is not found
 *
 */
const getRoomUser = async (
  directus: TypedDirectus,
  roomId: string
): Promise<null | (PartialItem<RoomUser> & { id: string })> => {
  try {
    const { data } = await directus.transport.get(`/custom/room-user/${roomId}`);
    return data;
  } catch (error) {
    const { response } = error;
    if (response?.status === 403) throw new ServerError(403, "Access Denied!");
    if (response?.status === 404) throw new ServerError(404, "Room not Found!");
    throw error;
  }
};

export default getRoomUser;
