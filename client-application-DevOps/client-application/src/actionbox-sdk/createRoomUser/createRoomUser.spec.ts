import { PartialItem } from "@directus/sdk";
import { Role, TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import createInvitation from "../createInvitation/createInvitation";
import createRoom from "../createRoom/createRoom";
import { createRandomApplicationUser } from "../__tests__/utils/create-random-application-user";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";
import createRoomUser from "./createRoomUser";

describe("createRoomUser()", () => {
  let directus: TypedDirectus;
  let admin: User;

  const createdRooms: string[] = [];
  const createdUsers: string[] = [];

  beforeEach(async () => {
    ({ directus, me: admin } = await prepareDirectus());
  });

  afterEach(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
    await directus.users.deleteMany(createdUsers);
  });

  test("Adds a user to a room", async () => {
    const room = await createRoom(directus, { name: "test" }, admin.id);
    createdRooms.push(room.id);
    const user = await createRandomApplicationUser(directus);
    createdUsers.push(user.id);

    const { data } = await directus.items("roles").readMany({
      filter: {
        room: { _eq: room.id },
        name: { _eq: "Participant" },
      },
    });

    const [participantRole] = data as PartialItem<Role>[];
    const invitation = await createInvitation(directus, room.id);

    const roomUser = await createRoomUser(directus, room.id, user.id, invitation.id);

    expect(roomUser).toMatchObject({
      user: user.id,
      role: participantRole.id,
      room: room.id,
    });
  });

  test("Throws an error if the user does not exist", async () => {
    const randomString = await directus.utils.random.string();

    const room = await createRoom(directus, { name: "test" }, admin.id);
    createdRooms.push(room.id);
    const invitation = await createInvitation(directus, room.id);

    await expect(createRoomUser(directus, room.id, randomString, invitation.id)).rejects.toThrow(
      "not found"
    );
  });

  test("Throws an error if the room does not exist", async () => {
    const randomString = await directus.utils.random.string();
    const room = await createRoom(directus, { name: "test" }, admin.id);
    createdRooms.push(room.id);
    const invitation = await createInvitation(directus, room.id);

    await expect(
      createRoomUser(directus, randomString, randomString, invitation.id)
    ).rejects.toThrow("not found");
  });

  test("Throws an error if there is no invitation", async () => {
    const room = await createRoom(directus, { name: "test" }, admin.id);
    createdRooms.push(room.id);
    const createdUser1 = await createRandomApplicationUser(directus);
    const createdUser2 = await createRandomApplicationUser(directus);

    createdUsers.push(createdUser1.id, createdUser2.id);

    const invitation = await createInvitation(directus, room.id, createdUser1.email);

    await expect(
      createRoomUser(directus, room.id, createdUser1.id, invitation.id)
    ).resolves.not.toThrow();

    await expect(createRoomUser(directus, room.id, createdUser2.id, invitation.id)).rejects.toThrow(
      "permission to"
    );
  });

  test("Throws an error if user is already a member of the room", async () => {
    const room = await createRoom(directus, { name: "test" }, admin.id);
    createdRooms.push(room.id);

    const createdUser = await createRandomApplicationUser(directus);
    createdUsers.push(createdUser.id);

    const invitation = await createInvitation(directus, room.id, createdUser.email);

    await expect(
      createRoomUser(directus, room.id, createdUser.id, invitation.id)
    ).resolves.not.toThrow();

    await expect(createRoomUser(directus, room.id, createdUser.id, invitation.id)).rejects.toThrow(
      "Already a member!"
    );
  });
});
