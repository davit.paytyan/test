import { RoomUser, TypedDirectus } from "client-application/actionbox-sdk/models";

/**
 * Creates a new room user
 *
 * The user has to have an invitation to this particular room
 */
const createRoomUser = async (
  directus: TypedDirectus,
  roomId: string,
  userId: string,
  invitationId: string,
  roleId?: string
) => {
  const { data } = await directus.transport.post<RoomUser>("/custom/room-user", {
    roomId,
    userId,
    invitationId,
    roleId,
  });

  return data;
};

export default createRoomUser;
