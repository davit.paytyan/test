import { Directus, ID } from "@directus/sdk";
import { TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import createInvitation from "../createInvitation/createInvitation";
import createRoom from "../createRoom/createRoom";
import createRoomUser from "../createRoomUser/createRoomUser";
import { createRandomApplicationUser } from "../__tests__/utils/create-random-application-user";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";
import getRoomsOfCurrentUser from "../get-rooms-of-current-user";

describe("getRoomsOfCurrentUser()", () => {
  let directus: TypedDirectus;
  let adminUser: User;

  const createdRooms: ID[] = [];
  const createdUsers: ID[] = [];

  beforeAll(async () => {
    ({ directus, me: adminUser } = await prepareDirectus());
  });

  afterAll(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
    await directus.users.deleteMany(createdUsers);
  });

  test("returns the 'RoomUser' of a room for the current logged in use", async () => {
    const createdUser1 = await createRandomApplicationUser(directus);
    createdUsers.push(createdUser1.id);

    const createdUser2 = await createRandomApplicationUser(directus);
    createdUsers.push(createdUser2.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: createdUser1.email, password: "123456" });

    const room1 = await createRoom(directus, { name: "room 1" }, adminUser.id);
    createdRooms.push(room1.id);

    const room2 = await createRoom(directus, { name: "room 2" }, createdUser1.id);
    createdRooms.push(room2.id);

    const room3 = await createRoom(directus, { name: "room 3" }, createdUser2.id);
    createdRooms.push(room3.id);

    const invitation = await createInvitation(directus, room1.id);
    await createRoomUser(directus, room1.id, createdUser1.id, invitation.id);

    const rooms = await getRoomsOfCurrentUser(sdk);

    expect(rooms).toBeArrayOfSize(2);
    expect(rooms?.map((room) => room.id)).toContainValues([room1.id, room2.id]);
    expect(rooms?.filter((room) => room.owner === createdUser1.id)).toBeDefined();
  });

  test("returns `null` because the current logged in user is not a member of any room", async () => {
    const createdUser1 = await createRandomApplicationUser(directus);
    createdUsers.push(createdUser1.id);

    const createdUser2 = await createRandomApplicationUser(directus);
    createdUsers.push(createdUser2.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: createdUser1.email, password: "123456" });

    const room1 = await createRoom(directus, { name: "room 1" }, adminUser.id);
    createdRooms.push(room1.id);

    const room3 = await createRoom(directus, { name: "room 3" }, createdUser2.id);
    createdRooms.push(room3.id);

    const rooms = await getRoomsOfCurrentUser(sdk);

    expect(rooms).toBeArrayOfSize(0);
  });
});
