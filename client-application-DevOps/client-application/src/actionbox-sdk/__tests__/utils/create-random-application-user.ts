import { PartialItem } from "@directus/sdk";
import { TypedDirectus, User } from "client-application/actionbox-sdk/models";
import createApplicationUser from "../../createApplicationUser/create-application-user";

export async function createRandomApplicationUser(
  directus: TypedDirectus
): Promise<PartialItem<User> & { id: string; email: string }> {
  const randomString = await directus.utils.random.string();
  const credentials = {
    first_name: "Max",
    last_name: "Mustermann",
    email: `${randomString}@gmail.com`,
    password: "123456",
  };

  const createdUserId = await createApplicationUser(directus, credentials);
  const user = await directus.items("directus_users").readOne(createdUserId);
  if (!user || !user.id) throw new Error("Could not find user!");
  return user as PartialItem<User> & { id: string; email: string };
}
