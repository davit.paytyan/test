import { Directus, MemoryStorage } from "@directus/sdk";
import { DataModel, User } from "client-application/actionbox-sdk/models";
import { randomBytes } from "crypto";

const host = process.env.DIRECTUS_HOST ?? "localhost";
const port = process.env.DIRECTUS_PORT ?? 8055;
const email = process.env.DIRECTUS_EMAIL ?? "";
const password = process.env.DIRECTUS_PASSWORD ?? "";

export async function prepareDirectus() {
  const prefix = randomBytes(8).toString("hex");
  const storage = new MemoryStorage(prefix);
  const url = `http://${host}:${port}`;
  const directus = new Directus<DataModel>(url, { storage });
  await directus.auth.login({ email, password });
  // Since we are in a test environment we don't need to check if something is
  // defined and catch eventual errors
  const me = (await directus.users.me.read()) as User;
  return { directus, me };
}
