import { Directus, PartialItem } from "@directus/sdk";
import { DataModel, Room } from "client-application/actionbox-sdk/models";

type CreateRoomData = Omit<Room, "id" | "date_created" | "date_updated">;

/**
 * Creates a new Room
 *
 * The current logged in user will be set as the owner of the newly created room.
 * You can choose a different owner by providing an existing user ID as the `ownerId` param
 */
const createRoom = async (
  directus: Directus<DataModel>,
  roomData: CreateRoomData,
  ownerId?: string
): Promise<PartialItem<Room> & { id: string }> => {
  // Existence check of the owner is handled on the backend side
  const room = await directus
    .items("rooms")
    .createOne(
      { ...roomData, owner: ownerId },
      { fields: ["id", "name", "owner", "description", "state", "roles.id", "roles.name"] }
    );

  // TODO: add proper error handling
  if (!room) throw new Error(`Can't create room!`);

  return room as PartialItem<Room> & { id: string };
};

export default createRoom;
