import { TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import { createRandomApplicationUser } from "../__tests__/utils/create-random-application-user";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";
import createRoom from "./createRoom";

const sortByKey = (key) => (a, b) => {
  if (a[key] < b[key]) return -1;
  if (a[key] > b[key]) return 1;
  return 0;
};

describe("createRoom()", () => {
  let directus: TypedDirectus;
  let user: User;

  const createdRooms: string[] = [];
  const createdUsers: string[] = [];

  beforeEach(async () => {
    ({ directus, me: user } = await prepareDirectus());
  });

  afterEach(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
    await directus.users.deleteMany(createdUsers);
  });

  test("create a room including relations for the current logged in user", async () => {
    const roomData = {
      name: "UI/UX design",
      description: "Learn neomorphism",
    };

    const room = await createRoom(directus, roomData);

    createdRooms.push(room.id);

    let roles = await directus.items("roles").readMany({
      filter: {
        room: { _eq: room.id },
      },
    });

    let roomUsers = await directus.items("room_users").readMany({
      filter: {
        room: { _eq: room.id },
      },
    });

    // let rolesPermissions = await directus.items("roles_permissions").readMany({
    //   filter: {
    //     room: {
    //       _eq: room.id,
    //     },
    //   },
    // });

    const [roomUser] = roomUsers.data ?? [];

    // expect(rolesPermissions.data?.length).not.toBe(0);

    expect(room).toMatchObject({ ...roomData, owner: user.id });

    expect(roles.data?.sort(sortByKey("name"))).toMatchObject(
      [
        {
          name: "Owner",
          room: room.id,
          permissions: expect.any(Array),
        },
        {
          name: "Moderator",
          room: room.id,
          permissions: expect.any(Array),
        },
        {
          name: "Participant",
          room: room.id,
          permissions: expect.any(Array),
        },
      ].sort(sortByKey("name"))
    );

    const ownerRole = roles.data?.find((role) => role.name === "Owner");

    expect(roomUser).toMatchObject({
      user: user.id,
      role: ownerRole?.id,
      room: room.id,
    });

    await directus.items("rooms").deleteOne(room.id);

    roles = await directus.items("roles").readMany({
      filter: {
        room: { _eq: room.id },
      },
    });

    roomUsers = await directus.items("room_users").readMany({
      filter: {
        room: { _eq: room.id },
      },
    });

    // rolesPermissions = await directus.items("roles_permissions").readMany({
    //   filter: {
    //     room: {
    //       _eq: room.id,
    //     },
    //   },
    // });

    // expect(rolesPermissions.data?.length).toBe(0);
    expect(roles.data?.length).toBe(0);
    expect(roomUsers.data?.length).toBe(0);
  });

  test("create a room with custom ownerId", async () => {
    const roomData = {
      name: "UI/UX design",
      description: "Learn neomorphism",
    };

    const someUser = await createRandomApplicationUser(directus);
    createdUsers.push(someUser.id);
    const room = await createRoom(directus, roomData, someUser.id);
    createdRooms.push(room.id);
    expect(room).toMatchObject({ ...roomData, owner: someUser.id });
  });
});
