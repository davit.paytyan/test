import { TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import createRoom from "../createRoom/createRoom";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";
import createInvitation from "./createInvitation";

let directus: TypedDirectus;
let user: User;

// Don't use external emails since it might be possible that our system will
// send actuall emails in the future
const emailToInvite = "atlas@together.de";

describe("createInvitation()", () => {
  beforeEach(async () => {
    ({ directus, me: user } = await prepareDirectus());
  });

  const createdRooms: string[] = [];
  const createdInvitations: string[] = [];

  afterEach(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
  });

  test("Creates an invitation", async () => {
    const room = await createRoom(directus, { name: "test" }, user.id);
    createdRooms.push(room.id);
    const invitation = await createInvitation(directus, room.id, emailToInvite);
    createdInvitations.push(invitation.id);
    expect(invitation).toMatchObject({ email: emailToInvite, room: room.id });
  });
});
