import { PartialItem } from "@directus/sdk";
import { Invitation, TypedDirectus } from "client-application/actionbox-sdk/models";

export type CreateInvitationData = PartialItem<Invitation> & { id: string };

/**
 * creates a new invitation for both registered and unregistered users
 */
const createInvitation = async (
  directus: TypedDirectus,
  roomId: string,
  email?: string
): Promise<CreateInvitationData> => {
  const invitation = await directus.items("invitations").createOne({ room: roomId, email });

  if (!invitation) throw new Error("Can't create invitation");

  return invitation as CreateInvitationData;
};

export default createInvitation;
