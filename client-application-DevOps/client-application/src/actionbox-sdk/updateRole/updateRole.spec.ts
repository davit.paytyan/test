import { TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import createRoom from "../createRoom/createRoom";
import updateRole from "../updateRole/updateRole";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";

const sortByKey = (key) => (a, b) => {
  if (a[key] < b[key]) return -1;
  if (a[key] > b[key]) return 1;
  return 0;
};

describe("updateRole()", () => {
  let directus: TypedDirectus;
  let user: User;

  const createdRooms: string[] = [];
  const createdPermissions: string[] = [];

  beforeAll(async () => {
    ({ directus, me: user } = await prepareDirectus());
  });

  afterAll(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
    await directus.items("permissions").deleteMany(createdPermissions);
  });

  test("Updates a role", async () => {
    const room = await createRoom(directus, { name: "test", owner: user.id });

    createdRooms.push(room.id);

    const roles = await directus.items("roles").readMany({
      filter: {
        room: { _eq: room.id },
        name: { _eq: "Participant" },
      },
      fields: ["*", "permissions.permissions_id"],
    });

    const participantRoleId = roles.data![0].id as string;

    const { data: newPermissions } = await directus.items("permissions").createMany([
      { title: "some other", key: "some.other.permission" },
      { title: "some other 2", key: "some.other.permission2" },
      { title: "some other 3", key: "some.other.permission3" },
    ]);

    const permissions = newPermissions!.map((permission) => permission.id as string).sort();

    createdPermissions.push(...permissions);

    const updatedRole = await updateRole(directus, participantRoleId, {
      permissions,
    });

    updatedRole.permissions = updatedRole.permissions?.sort(sortByKey("permissions_id"));

    expect(updatedRole).toMatchObject({
      permissions: permissions
        .map((permission) => ({
          permissions_id: permission,
          roles_id: updatedRole.id,
          room: room.id,
        }))
        .sort(sortByKey("permissions_id")),
      date_updated: expect.any(String),
    });
  });

  test("Throws an error if provided incorrect id", async () => {
    await expect(() => updateRole(directus, "smth", {})).rejects.toThrow();
  });
});
