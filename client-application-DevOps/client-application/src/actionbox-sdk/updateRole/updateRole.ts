import { Role, TypedDirectus } from "client-application/actionbox-sdk/models";

/**
 * Updates a room's role
 */
const updateRole = async (directus: TypedDirectus, roleId: string, data: Partial<Role>) => {
  const payload: Partial<Role> = { ...data };
  const permissions = data.permissions?.map((id) => ({ permissions_id: id, roles_id: roleId }));

  if (permissions) payload.permissions = permissions;

  const updatedRole = await directus
    .items("roles")
    .updateOne(roleId, payload, { fields: ["*", "permissions.*"] });

  if (!updatedRole) throw new Error(`Can't find role with id ${roleId}!`);

  return updatedRole;
};

export default updateRole;
