import { Directus } from "@directus/sdk";
import { TypedDirectus, User } from "client-application/actionbox-sdk/models";
import "jest-extended";
import createInvitation from "../createInvitation/createInvitation";
import createRoom from "../createRoom/createRoom";
import createRoomUser from "../createRoomUser/createRoomUser";
import { createRandomApplicationUser } from "../__tests__/utils/create-random-application-user";
import { prepareDirectus } from "../__tests__/utils/prepare-directus";
import getRoomUserPermissions from "./getRoomUserPermissions";

describe("getRoomUserPermissions()", () => {
  let directus: TypedDirectus;
  let adminUser: User;

  const createdRooms: string[] = [];
  const createdUsers: string[] = [];

  beforeEach(async () => {
    ({ directus, me: adminUser } = await prepareDirectus());
  });

  afterAll(async () => {
    await directus.items("rooms").deleteMany(createdRooms);
    await directus.users.deleteMany(createdUsers);
  });

  test("returns the correct user permissions and role for each tested user", async () => {
    const createdUser = await createRandomApplicationUser(directus);
    createdUsers.push(createdUser.id);

    const adminRoom = await createRoom(directus, { name: "test" }, adminUser.id);
    createdRooms.push(adminRoom.id);

    const invitation = await createInvitation(directus, adminRoom.id);
    await createRoomUser(directus, adminRoom.id, createdUser.id, invitation.id);

    const ownerPermissions = await getRoomUserPermissions(directus, adminRoom.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: createdUser.email, password: "123456" });

    const particantPermissions = await getRoomUserPermissions(sdk, adminRoom.id);

    expect(ownerPermissions?.role.name).toBe("Owner");
    expect(particantPermissions?.role.name).toBe("Participant");
  });

  test("returns null if the user is not a member of the room", async () => {
    const createdUser = await createRandomApplicationUser(directus);
    createdUsers.push(createdUser.id);

    const adminRoom = await createRoom(directus, { name: "test" }, adminUser.id);
    createdRooms.push(adminRoom.id);

    const ownerPermissions = await getRoomUserPermissions(directus, adminRoom.id);

    const sdk = new Directus(directus.transport.url);
    await sdk.auth.login({ email: createdUser.email, password: "123456" });

    const particantPermissions = await getRoomUserPermissions(sdk, adminRoom.id);

    expect(ownerPermissions?.role.name).toBe("Owner");
    expect(particantPermissions).toBeNull();
  });

  test("throws an 404 error if room is not found", async () => {
    await expect(() => getRoomUserPermissions(directus, "blablabla")).rejects.toThrow("not found");
  });
});
