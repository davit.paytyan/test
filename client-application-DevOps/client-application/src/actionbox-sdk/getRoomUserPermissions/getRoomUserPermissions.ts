import { Permission, Role, TypedDirectus } from "client-application/actionbox-sdk/models";

interface UserPermissions {
  role: Role;
  permissions: Permission[];
}

const getRoomUserPermissions = async (directus: TypedDirectus, roomId: string) => {
  try {
    const { data } = await directus.transport.get<UserPermissions>(
      `/custom/room-user/${roomId}/permissions`
    );
    return data;
  } catch (error) {
    const { response } = error;
    if (response.status === 403) return null;
    throw error;
  }
};

export default getRoomUserPermissions;
