import { createNullActor } from "xstate/lib/Actor";

export const NullActor = createNullActor("null-actor");
