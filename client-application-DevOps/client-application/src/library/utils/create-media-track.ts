import JitsiLocalTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiLocalTrack";
import JitsiRemoteTrack from "@together/lib-jitsi-meet/modules/RTC/JitsiRemoteTrack";

export type JitsiTrack = JitsiLocalTrack | JitsiRemoteTrack;

export type MediaTrack = {
  id: string;
  deviceId?: string;
  participantId: string;
  type: "audio" | "video";
  stream: MediaStream;
  isLocal: boolean;
  isMuted: boolean;
  _originalTrack: JitsiTrack;
};

export function createMediaTrack(track: JitsiTrack): MediaTrack {
  const result: MediaTrack = {
    id: track.getId() ?? "fake",
    participantId: track.getParticipantId(),
    type: track.getType() as "audio" | "video",
    stream: track.getOriginalStream(),
    isLocal: track.isLocal(),
    isMuted: track.isMuted(),
    _originalTrack: track,
  };

  if (track.isLocal()) result.deviceId = (track as JitsiLocalTrack).getDeviceId();

  return result;
}
