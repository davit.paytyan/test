export function assert404Response(response: unknown) {
  return response instanceof Response && response.status === 404;
}

export function assert403Response(response: unknown) {
  return response instanceof Response && response.status === 403;
}
