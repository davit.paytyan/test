export const QUICK_REACTIONS = new Map<string, string>();

QUICK_REACTIONS.set("mood-happy", "🙂");
QUICK_REACTIONS.set("mood-neutral", "😐");
QUICK_REACTIONS.set("mood-sad", "🙁");
QUICK_REACTIONS.set("mood-wink", "😉");

QUICK_REACTIONS.set("bool-yes", "👍");
QUICK_REACTIONS.set("bool-no", "👎");

QUICK_REACTIONS.set("number-1", "1️⃣");
QUICK_REACTIONS.set("number-2", "2️⃣");
QUICK_REACTIONS.set("number-3", "3️⃣");
QUICK_REACTIONS.set("number-4", "4️⃣");
QUICK_REACTIONS.set("number-5", "5️⃣");
QUICK_REACTIONS.set("number-6", "6️⃣");

QUICK_REACTIONS.set("text-afk", "Ich bin mal kurz weg");
QUICK_REACTIONS.set("text-question", "Ich habe eine Frage");
QUICK_REACTIONS.set("text-notice", "Ich habe einen Hinweis");
