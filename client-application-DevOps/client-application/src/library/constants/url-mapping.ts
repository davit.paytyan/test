export const LOGIN_URL = "/auth/login";
export const LOGOUT_URL = "/auth/logout";
export const OPEN_MY_ROOM_URL = "/open-my-room";
