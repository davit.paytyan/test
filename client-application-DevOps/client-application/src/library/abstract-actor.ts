import { AnyEventObject, EventObject, Observer, Subscription } from "xstate";
import Logger from "loglevel";

export abstract class AbstractActor<T, E extends EventObject = AnyEventObject> {
  public id: string;

  private _subscribers: Observer<T>[] = [];

  protected _stopped = false;

  constructor(id: string) {
    this.id = id;
    this.send = this.send.bind(this);
  }

  public toJSON() {
    return { id: this.id };
  }

  public subscribe(
    nextOrObserver: Observer<T> | ((value: T) => void),
    error?: (error: unknown) => void,
    complete?: () => void
  ): Subscription {
    if (this._stopped) Logger.warn(`Subscribed to a stopped actor ${this.id}!`);
    if (typeof nextOrObserver === "function") {
      return this.subscribe({
        next: nextOrObserver,
        error(_error) {
          if (error) error(_error);
        },
        complete() {
          if (complete) complete();
        },
      });
    }

    this._subscribers.push(nextOrObserver);

    return {
      unsubscribe: () => {
        this._subscribers = this._subscribers.filter((subscriber) => {
          return subscriber !== nextOrObserver;
        });
      },
    };
  }

  public send(event: E) {
    if (this._stopped) {
      Logger.warn(`Sent event ${event.type} to stopped actor ${this.id}!`);
      return;
    }
    this._send(event);
  }

  protected _send(event: E) {
    Logger.debug(`Override me to to something upon receiving ${event.type}!`);
  }

  public stop() {
    Logger.info(`Stopped actor ${this.id}.`);
    this._sendCompleteToSubscribers();
    this._subscribers = [];
    this._stopped = true;
  }

  protected isStopped() {
    return this._stopped;
  }

  protected _notifySubscribers(event: T) {
    if (this._stopped || this._subscribers.length === 0) return;
    for (const sub of this._subscribers) sub.next(event);
  }

  protected _sendErrorToSubscribers(error: Error) {
    if (this._stopped || this._subscribers.length === 0) return;
    for (const sub of this._subscribers) sub.error(error);
  }

  protected _sendCompleteToSubscribers() {
    if (this._stopped || this._subscribers.length === 0) return;
    for (const sub of this._subscribers) sub.complete();
  }
}
