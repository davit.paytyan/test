import { Button, ButtonGroup } from "@pxwlab/katana-cb";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { useTranslation } from "react-i18next";

const LocaleSwitchButtons = () => {
  const router = useRouter();

  const { t } = useTranslation("common");

  return (
    <ButtonGroup panel shadow>
      <Link href={{ pathname: router.pathname, query: router.query }} locale="de">
        <Button
          label={t("locale.de")}
          ghost
          square
          active={router.locale === "de"}
          disabled={router.locale === "de"}
        />
      </Link>
      <Link href={{ pathname: router.pathname, query: router.query }} locale="en">
        <Button
          label={t("locale.en")}
          ghost
          square
          active={router.locale === "en"}
          disabled={router.locale === "en"}
        />
      </Link>
    </ButtonGroup>
  );
};

export default LocaleSwitchButtons;
