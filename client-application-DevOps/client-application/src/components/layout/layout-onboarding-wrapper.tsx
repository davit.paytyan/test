import React from "react";

type ComponentProps = React.PropsWithChildren<never>;

export function LayoutOnboardingWrapper({ children }: ComponentProps) {
  return <div className="cb-layout cb-layout--onboarding">{children}</div>;
}
