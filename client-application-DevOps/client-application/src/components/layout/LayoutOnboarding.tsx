import {
  Footer,
  FooterCertificate,
  FooterSettings,
  FormDropdown,
  FormDropdownItem,
  Header,
  HeaderSettings,
} from "@pxwlab/katana-cb";
import { useRouter } from "next/router";
import React from "react";
import { useTranslation } from "react-i18next";
import LocaleSwitchButtons from "../header/locale-switch-buttons";

type Props = React.PropsWithChildren<{ footerChildren?: JSX.Element | null }>;

const LayoutOnboarding = ({ children, footerChildren = null }: Props) => {
  const router = useRouter();

  const { t } = useTranslation();

  return (
    <div className="cb-layout cb-layout--onboarding">
      <Header logo={"/images/to-logo.svg"} logoLink="/">
        <HeaderSettings>
          <LocaleSwitchButtons />
        </HeaderSettings>
      </Header>
      <div className="cb-content">{children}</div>
      <Footer>
        <FooterSettings>
          <FormDropdown icon="question-line" outline={true} side={true}>
            <FormDropdownItem
              label={t("footer:footer_settings.help.label")}
              icon="information-line"
            />
            <FormDropdownItem
              divider
              label={t("footer:footer_settings.legal.label")}
              icon="external-link-line"
            />
            <FormDropdownItem
              label={t("footer:footer_settings.privacy.label")}
              icon="external-link-line"
            />
            <FormDropdownItem
              divider
              label={t("footer:footer_settings.sign_out.label")}
              icon="external-link-line"
              onClick={() => {
                router.push("/auth/logout");
              }}
            />
          </FormDropdown>
        </FooterSettings>
        {footerChildren}
        <FooterCertificate image={"/images/cb-privacy.svg"} />
      </Footer>
    </div>
  );
};

export default LayoutOnboarding;
