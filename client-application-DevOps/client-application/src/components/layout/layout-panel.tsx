import { Panel, PanelWrapper, Section, SectionItem } from "@pxwlab/katana-cb";
import React from "react";
import LayoutOnboarding from "./LayoutOnboarding";

type Props = React.PropsWithChildren<{
  aside?: JSX.Element | null;
  footerChildren?: JSX.Element | null;
}>;

export const LayoutPanel = ({ children, aside = null, footerChildren = null }: Props) => {
  return (
    <LayoutOnboarding footerChildren={footerChildren}>
      <Section center>
        <SectionItem>
          <Panel booklet={Boolean(aside)}>
            <PanelWrapper>{children}</PanelWrapper>
            {Boolean(aside) && <PanelWrapper>{aside}</PanelWrapper>}
          </Panel>
        </SectionItem>
      </Section>
    </LayoutOnboarding>
  );
};
