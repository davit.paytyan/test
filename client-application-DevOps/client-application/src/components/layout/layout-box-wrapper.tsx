import React from "react";

type ComponentProps = React.PropsWithChildren<never>;

export function LayoutBoxWrapper({ children }: ComponentProps) {
  return <div className="cb-layout cb-layout--box">{children}</div>;
}
