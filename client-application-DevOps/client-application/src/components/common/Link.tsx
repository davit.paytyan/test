import React, { FC } from "react";
import NextLink, { LinkProps as NextLinkProps } from "next/link";

type LinkProps = NextLinkProps & { label: string };

const Link: FC<LinkProps> = ({ label, href, ...rest }) => {
  return (
    <NextLink href={href} {...rest}>
      <a className="cb-link">{label}</a>
    </NextLink>
  );
};

export default Link;
