import {
  FormCheckbox,
  FormCheckboxProps,
  FormGroup,
  FormInput,
  FormInputProps,
} from "@pxwlab/katana-cb";
import React, { useState } from "react";
import { Controller, ControllerProps, useFormContext } from "react-hook-form";

type TextInputProps = FormInputProps & {
  name: string;
  rules?: ControllerProps["rules"];
};

export function TextInput({ name, rules, ...inputProps }: TextInputProps) {
  const { control, formState } = useFormContext();
  const { errors } = formState;

  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field }) => {
        const error = errors[name]?.message;
        return (
          <FormGroup error={error}>
            <FormInput
              name={name}
              defaultValue={field.value}
              onChange={field.onChange}
              {...inputProps}
            />
          </FormGroup>
        );
      }}
    />
  );
}

export function PasswordInput(props: Omit<TextInputProps, "type">) {
  const [passwordVisible, setPasswordVisible] = useState(false);
  return (
    <TextInput
      {...props}
      icon="eye-line"
      type={passwordVisible ? "text" : "password"}
      onClick={(event) => {
        setPasswordVisible(!passwordVisible);
        event.preventDefault();
      }}
    />
  );
}

type CheckboxInputProps = FormCheckboxProps & {
  name: string;
  rules?: ControllerProps["rules"];
};

export function CheckboxInput({ name, rules, ...inputProps }: CheckboxInputProps) {
  const { control, formState } = useFormContext();
  const { errors } = formState;

  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field }) => {
        const error = errors[name]?.message;
        return (
          <FormGroup error={error}>
            <FormCheckbox
              name={name}
              checked={field.value}
              onChange={(event) => field.onChange(event.target.checked)}
              {...inputProps}
            />
          </FormGroup>
        );
      }}
    />
  );
}
