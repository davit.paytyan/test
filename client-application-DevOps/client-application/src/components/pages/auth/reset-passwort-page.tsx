import {
  Button,
  Form,
  FormColumn,
  FormField,
  FormFooter,
  FormGroup,
  FormInput,
  FormRow,
  Panel,
  PanelBody,
  PanelHeader,
  PanelWrapper,
  Section,
  SectionItem,
} from "@pxwlab/katana-cb";
import Link from "components/common/Link";
import LayoutOnboarding from "components/layout/LayoutOnboarding";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";

export function ResetPasswordPage({ onSubmit }) {
  const [passwordVisible, setPasswordVisible] = useState(false);

  const { handleSubmit, control, setError, formState, getValues } = useForm({
    defaultValues: { password: "", password_check: "" },
    shouldFocusError: false,
  });

  const { errors } = formState;

  return (
    <LayoutOnboarding>
      <Section center>
        <SectionItem>
          <Panel>
            <PanelWrapper>
              <PanelHeader title="Reset password" />
              <PanelBody>
                <Form onSubmit={handleSubmit(onSubmit)}>
                  <FormField>
                    <FormRow>
                      <FormColumn>
                        <Controller
                          name="password"
                          control={control}
                          rules={{ required: "This field is required!" }}
                          render={({ field }) => {
                            const error = errors.password?.message;
                            return (
                              <FormGroup error={error}>
                                <FormInput
                                  placeholder="New password"
                                  icon="eye-line"
                                  autoComplete="new-password"
                                  type={passwordVisible ? "text" : "password"}
                                  defaultValue={field.value}
                                  onChange={field.onChange}
                                  onClick={(event) => {
                                    setPasswordVisible(!passwordVisible);
                                    event.preventDefault();
                                  }}
                                />
                              </FormGroup>
                            );
                          }}
                        />
                      </FormColumn>
                    </FormRow>
                    <FormRow>
                      <FormColumn>
                        <Controller
                          name="password_check"
                          control={control}
                          rules={{ required: "This field is required!" }}
                          render={({ field }) => {
                            const error = errors.password_check?.message;
                            return (
                              <FormGroup error={error}>
                                <FormInput
                                  placeholder="Repeat password"
                                  autoComplete="new-password"
                                  type={passwordVisible ? "text" : "password"}
                                  defaultValue={field.value}
                                  onChange={field.onChange}
                                />
                              </FormGroup>
                            );
                          }}
                        />
                      </FormColumn>
                    </FormRow>
                  </FormField>
                  <FormFooter>
                    <Button
                      cta
                      type="submit"
                      label="Save & Login"
                      onClick={(event) => {
                        const values = getValues();
                        if (values.password !== values.password_check) {
                          setError("password_check", {
                            type: "validate",
                            message: "Given password does not match!",
                          });
                          event.preventDefault();
                        }
                      }}
                    />
                    <Link label="Return to login" href="/account/login" />
                  </FormFooter>
                </Form>
              </PanelBody>
            </PanelWrapper>
          </Panel>
        </SectionItem>
      </Section>
    </LayoutOnboarding>
  );
}
