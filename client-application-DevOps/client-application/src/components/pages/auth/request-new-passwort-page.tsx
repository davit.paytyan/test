import {
  Alert,
  Button,
  Form,
  FormColumn,
  FormField,
  FormFooter,
  FormGroup,
  FormInput,
  FormRow,
  Panel,
  PanelBody,
  PanelHeader,
  PanelWrapper,
  Section,
  SectionItem,
} from "@pxwlab/katana-cb";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import Link from "components/common/Link";
import LayoutOnboarding from "components/layout/LayoutOnboarding";

export function RequestNewPasswordPage({ onSubmit }) {
  const { handleSubmit, control, formState } = useForm({
    defaultValues: { email: "" },
  });

  const { isSubmitSuccessful, errors } = formState;

  return (
    <LayoutOnboarding footerChildren={null}>
      <Section center={true}>
        <SectionItem>
          <Panel>
            <PanelWrapper>
              <PanelHeader title="Reset password" />
              <PanelBody>
                <Form onSubmit={handleSubmit(onSubmit)}>
                  {isSubmitSuccessful && (
                    <Alert
                      badge={"E-Mail send"}
                      info={true}
                      message="<strong>Sucess!</strong> An E-Mail with instruction to reset your password has been send. Please check, lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
                    />
                  )}
                  {!isSubmitSuccessful && (
                    <FormField>
                      <FormRow>
                        <FormColumn>
                          <Controller
                            name="email"
                            control={control}
                            rules={{ required: "This field is required!" }}
                            render={({ field }) => {
                              const error = errors.email?.message;
                              return (
                                <FormGroup error={error}>
                                  <FormInput
                                    placeholder="E-Mail address"
                                    type="email"
                                    defaultValue={field.value}
                                    onChange={field.onChange}
                                  />
                                </FormGroup>
                              );
                            }}
                          />
                        </FormColumn>
                      </FormRow>
                    </FormField>
                  )}
                  <FormFooter>
                    {!isSubmitSuccessful && (
                      <Button cta type="submit" label="Request password" onClick={() => null} />
                    )}
                    <Link label="Return to login" href="/auth/login" />
                  </FormFooter>
                </Form>
              </PanelBody>
            </PanelWrapper>
          </Panel>
        </SectionItem>
      </Section>
    </LayoutOnboarding>
  );
}
