import { Button, Form, FormColumn, FormField, FormFooter, FormRow } from "@pxwlab/katana-cb";
import { PasswordInput, TextInput } from "components/common/form/form-inputs";
import Link from "components/common/Link";
import React from "react";
import isEmail from "validator/lib/isEmail";

type LoginFormProps = {
  onSubmit: (...arguments_: unknown[]) => Promise<unknown>;
};

export const LoginForm: React.FC<LoginFormProps> = ({ onSubmit }) => {
  return (
    <Form onSubmit={onSubmit}>
      <FormField>
        <FormRow>
          <FormColumn>
            <TextInput
              name="email"
              placeholder="E-mail Address"
              rules={{
                required: "This field is required",
                validate: (value) => {
                  return isEmail(value) ? true : "Please provide a valid e-mail address";
                },
              }}
            />
          </FormColumn>
        </FormRow>
        <FormRow>
          <FormColumn>
            <PasswordInput
              name="password"
              placeholder="Password"
              autoComplete="current-password"
              rules={{ required: "This field is required" }}
            />
          </FormColumn>
        </FormRow>
      </FormField>
      <FormFooter>
        <Button cta type="submit" label="Login now" onClick={() => null} />
        <Link href="/auth/request-new-password" label="Forgot your password?" />
      </FormFooter>
    </Form>
  );
};
