import { TransportErrorDescription } from "@directus/sdk";
import {
  Alert,
  Button,
  Form,
  FormColumn,
  FormField,
  FormFooter,
  FormRow,
  PanelBody,
  PanelHeader,
} from "@pxwlab/katana-cb";
import axios from "axios";
import { CheckboxInput, PasswordInput, TextInput } from "components/common/form/form-inputs";
import React, { useCallback, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";

function isTransportError(
  error: Error | TransportErrorDescription
): error is TransportErrorDescription {
  return (
    (error as TransportErrorDescription).extensions !== undefined ||
    (error as TransportErrorDescription).message !== undefined
  );
}

type Props = {
  onClickButtonlogin: () => void;
};

export function CreateAccountPage({ onClickButtonlogin }: Props) {
  const [errorMessage, setErrorMessage] = useState<string | null>();
  const [accountCreated, setAccountCreated] = useState<boolean>(false);
  const form = useForm({
    defaultValues: {
      first_name: "",
      last_name: "",
      email: "",
      password: "",
      acceptTerms: false,
      newsletter: false,
    },
  });

  const onSubmit = useCallback(
    async (data) => {
      setErrorMessage(null);
      form.clearErrors();
      try {
        await axios.post("/api/create-application-user", { data });
        setAccountCreated(true);
      } catch (error) {
        if (axios.isAxiosError(error)) {
          const errorData = error.response?.data;
          if (isTransportError(errorData) && errorData.extensions?.code === "FAILED_VALIDATION") {
            form.setError(errorData.extensions.field, {
              type: "validate",
              message: errorData.message,
            });
          } else if (isTransportError(errorData)) {
            setErrorMessage(errorData.message);
          } else {
            setErrorMessage(error.message);
          }
          return;
        }

        // TODO: add Sentry logging
        throw error;
      }
    },
    [form]
  );

  return (
    <>
      <PanelHeader title="New account" />
      <PanelBody>
        {accountCreated && <Alert info message="Successfuly created an account!" />}
        {errorMessage && <Alert badge="Error" message={errorMessage} />}
        {!accountCreated && (
          <FormProvider {...form}>
            <Form onSubmit={form.handleSubmit(onSubmit)}>
              <FormField>
                <FormRow>
                  <FormColumn>
                    <TextInput
                      placeholder="First name"
                      name="first_name"
                      rules={{
                        required: "Please enter your first name!",
                      }}
                    />
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn>
                    <TextInput
                      name="last_name"
                      placeholder="Last name"
                      rules={{
                        required: "Please enter your last name!",
                      }}
                    />
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn>
                    <TextInput
                      name="email"
                      placeholder="E-Mail address"
                      rules={{ required: "This field is required" }}
                    />
                  </FormColumn>
                </FormRow>
                <FormRow>
                  <FormColumn>
                    <PasswordInput
                      name="password"
                      placeholder="Password"
                      autoComplete="current-password"
                      rules={{ required: "This field is required" }}
                    />
                  </FormColumn>
                </FormRow>
              </FormField>
              <FormField>
                <FormRow>
                  <FormColumn>
                    <CheckboxInput
                      name="acceptTerms"
                      rules={{
                        required:
                          "To proceed, please read accept our terms of service and data privacy policy.",
                      }}
                      description={
                        <>
                          I agree to the Together.biz{" "}
                          <a href="/about/terms-of-service">Terms of Service</a> and the{" "}
                          <a href="/about/data-privacy-polidy">Data Privacy Policy.</a>
                        </>
                      }
                    />
                    <CheckboxInput
                      name="newsletter"
                      description={
                        <>
                          I agree to get product updates or newsletter content regarding the
                          Together.biz product{" "}
                          <a href="/about/data-privacy-policy">Data Privacy Policy</a>.
                        </>
                      }
                    />
                  </FormColumn>
                </FormRow>
              </FormField>
              <FormFooter>
                <Button cta label="Create account" type="submit" onClick={() => null} />
              </FormFooter>
            </Form>
          </FormProvider>
        )}
        {accountCreated && (
          <Form>
            <FormFooter>
              <Button cta label="Continue to Login" type="button" onClick={onClickButtonlogin} />
            </FormFooter>
          </Form>
        )}
      </PanelBody>
    </>
  );
}
