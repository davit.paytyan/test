import React from "react";
import {
  Button,
  Footer,
  FooterInteraction,
  FooterInteractionItem,
  Header,
  HeaderNavigation,
  HeaderNavigationItem,
  HeaderSettings,
} from "@pxwlab/katana-cb";

export const LayoutMarketing = ({ children }) => {
  return (
    <div className="cb-layout cb-layout--marketing">
      <Header
        logo="/images/to-logo.svg"
        logoLink="/"
        logoClaim={"Talk together, work together - be in flow"}
        // @ts-ignore marketing property should be defined!
        marketing
      >
        <HeaderNavigation>
          <HeaderNavigationItem link={"/"} label={"Product"} disabled={true} />
          <HeaderNavigationItem link={"/"} label={"Stories"} disabled={true} />
          <HeaderNavigationItem link={"/"} label={"Company"} disabled={true} />
        </HeaderNavigation>
        <HeaderSettings>
          <Button
            utilClassNames={"cb-header__settings-login"}
            label={"Login now"}
            icon={"account-circle-line"}
            cta={true}
            href="/auth/login"
            onClick={() => null}
          />
        </HeaderSettings>
      </Header>
      <div className="cb-content">{children}</div>
      <Footer logo={"/images/to-logo.svg"} logoLink={"/"}>
        <FooterInteraction
          copy={"© Copyright 2021 Fructus GmbH. All rights reserved."}
          about={
            "TOGETHER is a product by Fructus, a digital innovation company that sows digital plants and always waters them abundantly so that they grow."
          }
        >
          <FooterInteractionItem label={"Legal Notice"} link={"/article"} />
          <FooterInteractionItem label={"Terms of Use"} link={"/article"} />
        </FooterInteraction>
      </Footer>
    </div>
  );
};
