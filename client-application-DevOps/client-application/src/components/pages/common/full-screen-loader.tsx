import { LoaderCircle } from "@pxwlab/katana-cb";
import React from "react";
import LayoutOnboarding from "../../layout/LayoutOnboarding";

export function FullScreenLoader() {
  return (
    <LayoutOnboarding>
      <LoaderCircle />
    </LayoutOnboarding>
  );
}
