import {
  FormColumn,
  FormField,
  FormGroup,
  FormInput,
  FormRow,
  Modal,
  ModalBody,
  ModalHeader,
} from "@pxwlab/katana-cb";
import { useCreateInvitationMutation } from "hooks/actionbox-sdk/use-create-invitation-mutation";
import { useRoomQuery } from "hooks/actionbox-sdk/use-room-query";
import Logger from "loglevel";
import React, { useMemo, useState } from "react";

const NEXT_PUBLIC_URL = process.env.NEXT_PUBLIC_URL ?? "";

export function InviteParticipantsModal({ onClose, roomId }) {
  const [invitationCode, setInvitationCode] = useState("");
  const [isCopied, setIsCopied] = useState(false);
  const [, createInvitationMutation] = useCreateInvitationMutation(roomId, {
    onSuccess: (data) => {
      setInvitationCode(data.id);
    },
  });

  const [, query] = useRoomQuery(roomId, {
    onSuccess: (data) => {
      if (!data.invitations || data.invitations?.length === 0) {
        createInvitationMutation.mutateAsync(null);
      } else {
        setInvitationCode(data.invitations[0]);
      }
    },
  });

  const invitationLink = useMemo(() => {
    return `${NEXT_PUBLIC_URL}/room/${roomId}/accept-invite?t=${invitationCode}`;
  }, [invitationCode, roomId]);

  return (
    <Modal onClose={onClose}>
      <ModalHeader title="Invite" />
      <ModalBody>
        <FormField>
          <FormRow>
            <FormColumn>
              <FormGroup
                label="Share this room via a link"
                error={isCopied ? "Link copied to clipbard!" : undefined}
              >
                {query.isLoading && <FormInput value="Loading ..." disabled />}
                {query.isFetched && (
                  <FormInput
                    value={invitationLink}
                    icon={isCopied ? "check-line" : "copy-line"}
                    onChange={() => null}
                    onClick={() => {
                      window.navigator.clipboard
                        .writeText(invitationLink)
                        .then(() => {
                          setIsCopied(true);
                          setTimeout(() => setIsCopied(false), 1000);
                        })
                        .catch((error) => {
                          Logger.warn("Clipboard:", error.message);
                          // eslint-disable-next-line no-alert
                          alert("Kopieren wurde blockiert durch den Benutzer!");
                        });
                    }}
                  />
                )}
              </FormGroup>
            </FormColumn>
          </FormRow>
        </FormField>
      </ModalBody>
    </Modal>
  );
}
