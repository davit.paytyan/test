# Redux & State

- We use the **Redux Toolkit** library (https://redux-toolkit.js.org/)
- Each page has its own Store, they should not be connected to each other. 
- Think of them as namespaces, each with its own set of reducers and actions.
- reducers and their actions can be shared across the namespaces, if applicable 
## Using Redux on pages

If you pass a `store` property to any `NextPage`, it is wrapped with the
`Provider` component of `redux`, granting access to the functionallity of it.

