const path = require("path");
require("@testing-library/jest-dom");
require("@testing-library/jest-dom/extend-expect");
require("dotenv").config({ path: path.resolve(__dirname, "test.env") });
