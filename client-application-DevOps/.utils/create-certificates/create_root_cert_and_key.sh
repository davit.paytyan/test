#!/bin/sh
SUBJECT="/C=DE/ST=None/L=HH/O=together.localhost/CN=together.localhost"
openssl genrsa -out rootCA.key 2048
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -out rootCA.pem -subj "$SUBJECT"
