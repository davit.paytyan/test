#!/bin/sh

FILE_PATH=/etc/certs/out/rootCA.pem

if [ ! -f "$FILE_PATH" ]; then
  mkdir -p /etc/certs/out/together.localhost
  mkdir -p /etc/certs/out/api.together.localhost
  mkdir -p /etc/certs/out/mailhog.together.localhost

  ./create_root_cert_and_key.sh
  ./create_certificate_for_domain.sh together.localhost together.localhost
  ./create_certificate_for_domain.sh api.together.localhost api.together.localhost
  ./create_certificate_for_domain.sh mailhog.together.localhost mailhog.together.localhost

  cp ./rootCA.pem ./out/rootCA.pem
fi
