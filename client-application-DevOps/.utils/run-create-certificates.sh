#! /bin/sh
if [ -z "$1" ]
  then
    echo "Please specify a target directory!"
  else
    docker build -t together-create-certificates $(dirname $0)/create-certificates
    docker run -a stdin -a stdout --rm -v $(pwd)/$1:/etc/certs/out together-create-certificates
fi