Together.biz Application
========================

# Contributing
## Writing good commit messages 📝

--> https://365git.tumblr.com/post/3308646748/writing-git-commit-messages

The form of your commit message is before you commit your code to the repository.    
Your message has to follow the form of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/), this will enable us to automatically create changelogs from our commit messages.

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

### Referencing a JIRA Ticket

You can reference a JIRA ticket by using one of the folloing keywords enclosed by the Ticket ID (e.g. TDE-123)
- Fixes
- Resolves
- Closes

### Example

```
feat(actionbox-sdk): add createRoom

(... further description if needed ...)

Closes TDE-394
```

## Local Setup

### Create Self Signed Certificates

The app requires a secure browser environment to run properly, which the local nginx server is setup for.
The self-signed certificates can be created by the docker image, located in `<project>/.utils/create-certificates`.

To build and run the docker image on a mac, you can use the following command within the `<project>` directory:

`$ .utils/run-create-certificates.sh ./nginx/certs`

**Tested on a MacOS machine only. Other systems might need manual steps, to build and run the docker image**

To allow the self signed certificates to be FULLY trusted in Chrome and Safari, you need to import a 
new certificate authority into your Mac. To do so follow these instructions:

You can do this one of 2 ways, at the command line, using this command which will prompt you for your password:

`$ sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain rootCA.pem`

or by using the Keychain Access app:

1. Open Keychain Access
2. choose "System" in the "Keychains" list
3. Choose "Certificates" in the "Category" list
4. Choose "File | Import Items..."
5. Browse to the file created above, "rootCA.pem", select it, and click "Open"
6. Select your newly imported certificate in the "Certificates" list.
7. Click the "i" button, or right click on your certificate, and choose "Get Info"
8. Expand the "Trust" option
9. Change "When using this certificate" to "Always Trust"
10. Close the dialog, and you'll be prompted for your password.
11. Close and reopen any tabs that are using your target domain, and it'll be loaded securely!

[Source](https://stackoverflow.com/questions/7580508/getting-chrome-to-accept-self-signed-localhost-certificate/43666288#43666288)

## Running the development environment

After you've setup the certificates you can start the whole app with the command:

```
$ ./start-dev.sh
```

If you'd like to start a single service, you can do that by adding the service name(s) to the command.

```
$ ./start-dev.sh mariadb client-app
```

*Note:* The services will be stopped, recreated and then started.

To run the services in a detachted mode, you can prepend the `docker compose up` flag `-d` 

```
$ ./start-dev.sh -d mariadb
```
