#! /bin/sh

if [[ ! -f .env ]]; then
  echo 'The .env file is missing.'
  exit
fi

docker-compose -f docker-compose.yml down --remove-orphans --rmi local
docker-compose -f docker-compose.yml build --force-rm --no-cache --progress plain
docker-compose -f docker-compose.yml up --remove-orphans "$@"
