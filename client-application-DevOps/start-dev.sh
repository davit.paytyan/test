#! /bin/sh
if [[ ! -f nginx/certs/rootCA.pem ]]; then
  echo '\n\t🚨 Warning\n'
  echo '\tThe certificates for the nginx server are missing.'
  echo '\tPlease refer to the README how to create them.\n'
  exit
fi

docker compose -f docker-compose.yml -f docker-compose.development.yml --env-file development.env rm -s -f -v "$@"
docker compose -f docker-compose.yml -f docker-compose.development.yml --env-file development.env up --force-recreate --remove-orphans "$@"
