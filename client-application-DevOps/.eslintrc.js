const path = require("path");

module.exports = {
  env: {
    browser: true,
    es2020: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:eslint-comments/recommended",
    "plugin:import/errors",
    "plugin:import/typescript",
    "plugin:import/warnings",
    "plugin:node/recommended",
    "plugin:unicorn/recommended",
    "prettier",
  ],
  plugins: [
    "@typescript-eslint",
    "eslint-comments",
    "import",
    "node",
    "prettier",
    "unicorn",
  ],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
    project: path.resolve(__dirname, "tsconfig.json"),
  },
  rules: {
    // Override default configurations and (de)activate rules that we (do not)
    // opt-in/out to using.
    "@typescript-eslint/ban-ts-comment": [
      "error",
      {
        minimumDescriptionLength: 15,
        "ts-check": "allow-with-description",
        "ts-expect-error": "allow-with-description",
        "ts-ignore": "allow-with-description",
        "ts-nocheck": "allow-with-description",
      },
    ],
    // Always infer the return type.
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "@typescript-eslint/no-unused-expressions": "error",
    "@typescript-eslint/no-unused-vars": [
      "error",
      {
        ignoreRestSiblings: true,
      },
    ],
    "@typescript-eslint/no-var-requires": "off",
    "@typescript-eslint/no-explicit-any": ["error", { fixToUnknown: true }],
    "@typescript-eslint/no-base-to-string": "error",
    "@typescript-eslint/no-extra-non-null-assertion": "error",
    "@typescript-eslint/no-unnecessary-condition": "off",
    "@typescript-eslint/no-unnecessary-qualifier": "error",
    "@typescript-eslint/no-unnecessary-type-arguments": "error",
    "@typescript-eslint/prefer-for-of": "error",
    "@typescript-eslint/prefer-includes": "error",
    "@typescript-eslint/prefer-nullish-coalescing": "error",
    "@typescript-eslint/prefer-optional-chain": "error",
    "@typescript-eslint/prefer-string-starts-ends-with": "error",
    "@typescript-eslint/switch-exhaustiveness-check": "error",
    "@typescript-eslint/unified-signatures": "error",

    // Deactivate some rules
    "@typescript-eslint/no-unsafe-assignment": "off",
    "@typescript-eslint/no-unsafe-call": "off",
    "@typescript-eslint/no-unsafe-member-access": "off",
    "@typescript-eslint/no-unsafe-return": "off",
    "array-callback-return": "error",
    "capitalized-comments": "off",
    "dot-notation": "error",
    eqeqeq: ["error", "smart"],
    "eslint-comments/no-unused-disable": "error",
    "func-name-matching": "error",
    "func-style": [
      "error",
      "declaration",
      {
        allowArrowFunctions: true,
      },
    ],
    "import/namespace": [
      "error",
      {
        allowComputed: true,
      },
    ],
    "no-alert": "error",
    "no-await-in-loop": "error",
    // TODO: Activate this rule when we switched to the logger. We use loglevel.
    // "no-console": "error",
    "no-constructor-return": "error",
    "no-duplicate-imports": "error",
    "no-else-return": [
      "error",
      {
        allowElseIf: false,
      },
    ],
    "no-eval": "error",
    "no-extend-native": "error",
    "no-extra-bind": "error",
    "no-implicit-coercion": "error",
    "no-lonely-if": "error",
    "no-mixed-requires": "error",
    "no-multi-spaces": [
      "error",
      {
        ignoreEOLComments: true,
      },
    ],
    "no-new-func": "error",
    "no-new-require": "error",
    "no-new-wrappers": "error",
    "no-path-concat": "error",
    "no-proto": "error",
    "no-return-assign": "error",
    "no-return-await": "error",
    "no-self-compare": "error",
    "no-sequences": "error",
    "no-template-curly-in-string": "error",
    "no-throw-literal": "error",
    "no-trailing-spaces": "error",
    "no-undef": [
      "error",
      {
        typeof: true,
      },
    ],
    "no-undef-init": "error",
    "no-unmodified-loop-condition": "error",
    "no-use-before-define": "off",
    "@typescript-eslint/no-use-before-define": [
      "error",
      {
        classes: true,
        functions: false,
        variables: false,
      },
    ],
    "no-useless-call": "error",
    "no-useless-computed-key": "error",
    "no-useless-concat": "error",
    "no-useless-constructor": "error",
    "no-useless-rename": "error",
    "no-useless-return": "error",
    "node/no-extraneous-import": [
      "error",
      {
        allowModules: ["react"],
      },
    ],
    "node/no-missing-import": ["off"],
    "node/no-process-exit": "error",
    // The code is transpiled and most features are pretty stable when they are
    // added to Node.js. Thus, these rules are not needed.
    "node/no-unsupported-features/es-builtins": "off",
    "node/no-unsupported-features/es-syntax": "off",
    // TODO: Activate this as soon as we have environment variables encapsulated
    // as configuration module.
    // "node/no-process-env": "error",
    "object-shorthand": ["error", "always"],
    "one-var": [
      "error",
      {
        initialized: "never",
      },
    ],
    "prefer-const": [
      "error",
      {
        ignoreReadBeforeAssign: true,
      },
    ],
    "prefer-destructuring": "error",
    "prefer-numeric-literals": "error",
    "prefer-regex-literals": "error",
    "prefer-template": "error",
    "prettier/prettier": "error",
    "require-atomic-updates": "error",
    "spaced-comment": [
      "error",
      "always",
      {
        block: {
          balanced: true,
        },
        exceptions: ["-", "/"],
        markers: ["/"],
      },
    ],
    "symbol-description": "error",
    "unicorn/prefer-node-protocol": "off",
    "unicode-bom": "error",
    "unicorn/filename-case": "off",
    "unicorn/no-unsafe-regex": "error",
    "unicorn/no-unused-properties": "error",
    "unicorn/prevent-abbreviations": [
      "error",
      {
        checkFilenames: false,
        replacements: {
          props: false,
          ref: false,
          dev: false,
        },
      },
    ],
    yoda: "error",
  },
  settings: {
    "import/resolver": {
      node: {
        extensions: [".js", ".ts", ".d.ts"],
      },
      typescript: {},
    },
    "jest": {
      "version": 26
    }
  },
  overrides: [
    {
      files: ["./client-application/src/**/*"],
      parserOptions: {
        project: path.resolve(__dirname, "client-application", "tsconfig.json"),
        ecmaFeatures: {
          jsx: true,
        },
      },
      globals: {
        JSX: true,
      },
      plugins: ["jsx-a11y", "react-hooks", "react"],
      extends: [
        "plugin:jsx-a11y/recommended",
        "plugin:react-hooks/recommended",
        "plugin:react/recommended",
      ],
      rules: {
        "jsx-a11y/anchor-is-valid": [
          "error",
          {
            aspects: ["invalidHref", "preferButton"],
            components: ["Link"],
            specialLink: ["href"],
          },
        ],
        "react/jsx-filename-extension": [
          "error",
          {
            extensions: [".tsx", ".jsx"],
          },
        ],
        "react/prop-types": [
          "error",
          {
            skipUndeclared: true,
          },
        ],
      },
      settings: {
        "import/resolver": {
          node: {
            extensions: [".js", ".jsx", ".ts", ".tsx", ".d.ts"],
          },
          typescript: {},
        },
        react: {
          version: "detect",
        },
      },
    },
    {
      files: ["./colyseus-server/src/**/*"],
      parserOptions: {
        project: path.resolve(__dirname, "colyseus-server", "tsconfig.json"),
      },
    },
    {
      files: ["*.ts?(x)"],
      rules: {
        "@typescript-eslint/no-var-requires": "error",
        "import/no-unresolved": "off",
      },
    },
    {
      files: ["*.@(j|t)s?(x)"],
      rules: {
        "unicorn/no-null": "off",
      },
    },
    {
      files: ["*.@(spec|test).ts"],
      plugins: ["jest", "jest-dom", "jest-formatting"],
      rules: {
        "node/no-unpublished-import": "off",
        "node/no-unpublished-require": "off",
        "node/no-missing-require": "off",
        "@typescript-eslint/no-non-null-assertion": "off"
      },
      extends: [
        "plugin:jest-dom/recommended",
        "plugin:jest/recommended",
        "plugin:jest/style",
      ],
    },
    {
      files: ["*.@(spec|test).@(j|t)sx"],
      plugins: ["testing-library"],
      extends: ["plugin:testing-library/react"],
    },
    {
      files: ["jest.setup.js", "email-templates/gulpfile.js", "client-application/next.config.js",
        "client-application/@types/coluyeus-schema/index.d.ts"],
      rules: {
        "node/no-unpublished-import": 0,
        "node/no-unpublished-require": 0,
        "node/no-missing-require": 0,
      },
    },
    {
      files: ["next.config.js", "next-i18next.config.js", "client-application/env.js", "client-application/sentry*", "jest.setup.js", "jest.config.js"],
      rules: {
        "unicorn/prefer-module": "off",
      },
    },
  ],
};
