import fs from "fs";

type ENV_VALUE = string | null | boolean | number | undefined;

function processValues(input: Record<string, string | undefined>, allowedKeys?: string[]) {
  const environment: Record<string, ENV_VALUE> = { ...input };

  for (let [key, value] of Object.entries(environment)) {
    // If key ends with '_FILE', try to get the value from the file defined in this variable
    // and store it in the variable with the same name but without '_FILE' at the end
    let newKey;
    if (key.length > 5 && key.endsWith("_FILE")) {
      try {
        newKey = key.slice(0, -5);
        // abort if key is not included in an optional given map
        if (allowedKeys && !allowedKeys.includes(newKey)) continue;
        value = fs.readFileSync(String(value), { encoding: "utf8" });
        if (newKey in environment) {
          throw new Error(
            `Duplicate environment variable encountered: you can't use "${key}" and "${newKey}" simultaneously.`
          );
        }
        key = newKey;
      } catch {
        throw new Error(
          `Failed to read value from file "${value}", defined in environment variable "${key}".`
        );
      }
    }

    // abort if key is not included in an optional given map
    if (allowedKeys && !allowedKeys.includes(key)) continue;

    // Try to convert remaining values:
    // - boolean values to boolean
    // - 'null' to null
    // - number values (> 0 <= Number.MAX_SAFE_INTEGER) to number
    if (value === "true") {
      environment[key] = true;
      continue;
    }

    if (value === "false") {
      environment[key] = false;
      continue;
    }

    if (value === "null") {
      environment[key] = null;
      continue;
    }

    if (
      String(value).startsWith("0") === false &&
      Number.isNaN(value) === false &&
      String(value).length > 0 &&
      Number(value) <= Number.MAX_SAFE_INTEGER
    ) {
      environment[key] = Number(value);
      continue;
    }

    if (newKey) {
      environment[key] = value;
    }
  }

  return environment;
}

const environment = processValues({ ...process.env });

export default environment;
