import { Directus, ID } from "@directus/sdk";
import { onCreateDocumentPayload, onDisconnectPayload, Server } from "@hocuspocus/server";
import { TiptapTransformer } from "@hocuspocus/transformer";
import { TaskItem } from "@tiptap/extension-task-item";
import { TaskList } from "@tiptap/extension-task-list";
import { Typography } from "@tiptap/extension-typography";
import StarterKit from "@tiptap/starter-kit";
import fs from "fs";
import { fromUint8Array, toUint8Array } from "js-base64";
import path from "path";
import { applyUpdate, encodeStateAsUpdate } from "yjs";
import environment from "./env.js";

type RoomContent = {
  id: ID;
  room: string;
  type: string;
  title?: string;
  payload?: string;
};

type DirectusCollections = {
  room_content: RoomContent;
};

async function loadTemplate(templateName: string) {
  const templatePath = path.resolve("templates", `${templateName}.json`);
  const template = await fs.promises.readFile(templatePath, "utf-8");
  return template;
}

function transformTemplate(template: string) {
  const parsedTemplate = JSON.parse(template);
  const transformedTemplate = TiptapTransformer.toYdoc(parsedTemplate, "default", [
    StarterKit,
    Typography,
    TaskItem,
    TaskList,
  ]);
  const update = encodeStateAsUpdate(transformedTemplate);
  return fromUint8Array(update);
}

async function loadAndPrepareDocumentTemplates() {
  const templates = await Promise.all([
    loadTemplate("simple-agenda"),
    loadTemplate("advanced-agenda"),
    loadTemplate("decision"),
    loadTemplate("meeting-minutes"),
  ]);

  const map = new Map<string, string>();
  map.set("simple-agenda", transformTemplate(templates[0]));
  map.set("advanced-agenda", transformTemplate(templates[1]));
  map.set("decision", transformTemplate(templates[2]));
  map.set("meeting-minutes", transformTemplate(templates[3]));

  return map;
}

async function main() {
  const apiUrl = environment.INTERNAL_API_URL as string;
  const apiToken = environment.INTERNAL_API_TOKEN as string;
  const directus: Directus<DirectusCollections> = new Directus(apiUrl);

  const documentTemplates = await loadAndPrepareDocumentTemplates();

  const authorized = await directus.auth.static(apiToken);
  if (!authorized) throw new Error("Not authorized!");

  async function getRoomContent(itemId: string): Promise<RoomContent | null> {
    const data = await directus.items("room_content").readOne(itemId);
    return (data as RoomContent) ?? null;
  }

  // async function createRoomContent(roomId: string) {
  //   await directus
  //     .items("room_content")
  //     .createOne({ room: roomId, title: "New Textpad", type: "textpad" });
  // }

  async function updateRoomContent(id: ID, payload: string) {
    await directus.items("room_content").updateOne(id, { payload });
  }

  const server = Server.configure({
    port: 1234,

    async onRequest(data) {
      return new Promise((resolve, reject) => {
        const { request, response } = data;

        if (request.url) {
          let template;
          switch (request.url) {
            case "/textpad/template/simple-agenda":
              template = documentTemplates.get("simple-agenda");
              break;
            case "/textpad/template/advanced-agenda":
              template = documentTemplates.get("advanced-agenda");
              break;
            case "/textpad/template/meeting-minutes":
              template = documentTemplates.get("meeting-minutes");
              break;
            case "/textpad/template/decision":
              template = documentTemplates.get("decision");
              break;
          }

          if (template) {
            response.writeHead(200, { "Content-Type": "text/plain" });
            response.end(template);
            return reject();
          }
        }

        resolve(null);
      });
    },

    async onCreateDocument(data: onCreateDocumentPayload): Promise<unknown> {
      // The tiptap collaboration extension uses shared types of a single y-doc
      // to store different fields in the same document.
      // The default field in tiptap is simply called "default"
      const fieldName = "default";

      // Check if the given field already exists in the given y-doc.
      // Important: Only import a document if it doesn't exist in the primary data storage!
      if (!data.document.isEmpty(fieldName)) return;

      const [type, roomId, itemId] = data.documentName.split("/");

      if (!itemId) console.error(`Missing item id! ${roomId}`);

      data.document.meta = { type, roomId, itemId };

      const content = await getRoomContent(itemId);

      if (content?.payload && content?.payload.trim().length !== 0) {
        applyUpdate(data.document, toUint8Array(content.payload));
      }
    },

    async onDisconnect(data: onDisconnectPayload) {
      if (data.clientsCount === 0) {
        const state = encodeStateAsUpdate(data.document);
        const encodedSate = fromUint8Array(state);

        if (data.document.meta.itemId) {
          const { itemId } = data.document.meta;
          updateRoomContent(itemId, encodedSate);
        }
      }
    },
  });

  server.listen();
}

async function init() {
  try {
    await main();
    console.log("Started Textpad on port 1234");
  } catch (error) {
    console.error("ERROR:", error.message);
    console.warn("Could not start server due to an error. Retry in 5000ms!");
    setTimeout(() => {
      init();
    }, 5000);
  }
}

init();
