const { v4: uuid } = require("uuid");
const getUserColor = require("@pxwlab/cb-user-colors");

const addRoomUserColorAndIndex = async ({ services }, event) => {

  const { ItemsService } = services;
  const { database: knex, accountability, schema, payload } = event;

  const opts = { knex, accountability, schema };
  const roomUsersService = new ItemsService("room_users", opts);

  const items = await roomUsersService.readByQuery({ filter: { room: { _eq: payload.room } } });
  const user_index = Array.isArray(items) ? items.length : 0;
  const user_colors = await getUserColor({ userIndex: user_index < 24 ? user_index : user_index - 24 });

  return { ...payload, user_colors, user_index };
}

const createRoomForUser = async ({ services }, event) => {
  const { ItemsService } = services;
  const { database: knex, accountability, item, payload, schema } = event;

  const opts = { knex, accountability, schema };

  const roomsService = new ItemsService("rooms", {
    ...opts, accountability: { ...opts.accountability, user: item }
  });

  await roomsService.createOne({
    name: `${payload.first_name} ${payload.last_name}'s Raum`,
    owner: event.item
  });

}

module.exports = (context) => ({
  "users.create": async (event) => {
    if (event.accountability?.admin && event.payload.tags && event.payload.tags.includes("beta")) {
      createRoomForUser(context, event);
    }
  },

  "items.create.before": async (data, event) => {
    switch (event.collection) {
      case "room_users":
        const newData = await addRoomUserColorAndIndex(context, event);
        return newData;

      case "rooms":
        if (!data.owner) data.owner = event.accountability.user;
        data.roles = [
          { id: uuid(), name: "Owner" },
          { name: "Moderator" },
          { name: "Participant" }
        ];
        data.room_users = [
          { role: data.roles[0].id, user: data.owner }
        ]
        return data;
    }
  },
});
