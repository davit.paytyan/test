async function upgradeAccountability(user, accountability, database) {
  const userData = await database
    .select('role', 'directus_roles.admin_access', 'directus_roles.app_access')
    .from('directus_users')
    .leftJoin('directus_roles', 'directus_users.role', 'directus_roles.id')
    .where({
      'directus_users.id': user,
      status: 'active',
    })
    .first();

  if (!userData) return accountability;

  return {
    ...accountability,
    user,
    role: userData.role,
    admin: userData.admin_access === true || userData.admin_access == 1,
    app: userData.app_access === true || userData.app_access == 1
  }
}

async function upgradeSchema(getSchema, accountability, database) {
  return await getSchema({ accountability, database });
}

async function ensureUserUploadDirectory(services, accountability, schema, database) {
  const { FoldersService } = services;
  const foldersService = new FoldersService({ schema, knex: database, accountability });
  
  let folder;
  const folderPayload = { owner: accountability.user, name: accountability.user };
  
  try {
    [folder] = await foldersService.readByQuery({
      filter: folderPayload,
      fields: ["id"]
    });
  } catch {
    // nothing to catch ...
  }

  // if the users directory exist, exit
  if (folder) return;

  try {
    await foldersService.createOne(folderPayload);
    console.info("Created users upload directory.");
  } catch (error) {
    console.error(error);
  }
}

module.exports = ({ services, getSchema }) => ({
  "auth.login": async (payload, { user, status, database, schema, accountability }) => {

    if (status !== "success") return;

    //
    // Workaround for missing accountability data on auth.login hook
    //
    accountability = await upgradeAccountability(user, accountability, database);
    schema = await upgradeSchema(getSchema, accountability, database);
    
    await ensureUserUploadDirectory(services, accountability, schema, database);
  },
  "files.upload": async ({ item, payload, schema, database, accountability }) => {
    //
    // NOTE: 
    // The content-upload module of the client application uses this hook and
    // appends the contentUmploadModule property to the uploaded payload. 
    // Therefore, we can check if the content-upload module triggered the upload
    // event and bail out if not.
    //

    if (!payload.contentUploadModule || !payload.contentUploadModule === "true") return;

    //
    // -- CONTENT UPLOAD MODULE SPECIFIC HOOK --
    //
    const { ItemsService, FoldersService, FilesService } = services;
    const serviceOptions = { schema, knex: database, accountability };
    const roomContentsService = new ItemsService("room_content", serviceOptions);

    // put file into directory of the user

    const foldersService = new FoldersService(serviceOptions);
    const filesService = new FilesService(serviceOptions);

    let folder;

    try {
      [folder] = await foldersService.readByQuery({
        filter: {
          owner: accountability.user,
          name: accountability.user,
        }
      });
      if (folder) folder = folder.id;
    } catch (error) {
      console.error(error);
    }

    // TODO: add error number and put error into error list
    // what should happen if this error gets thrown?
    if (!folder) throw new Error("Upload directory of the User is missing!");

    // move file into users folder
    try {
      await filesService.updateOne(item, { folder });
    } catch (error) {
      console.error(error);
    }

    //
    // NOTE:
    // The following should only be executed if the payload has a room property.
    //

    if (!payload.room) return;

    // check if room exists && current user is a member of it
    const roomUsersService = new ItemsService("room_users", serviceOptions);
    const roomUser = await roomUsersService.readByQuery({
      filter: {
        room: payload.room,
        user: accountability.user,
      }
    });

    // TODO: we should clarify if the file should be deleted at this point
    if (!roomUser) {
      console.warn("The user is not a member of the targeted room! Abort room_content creation");
      return;
    }

    // TODO: implement the permission check here as soon as file uploads can
    // be constrained by the room owner

    // if (roomUser.role ....

    // create room_content for this file and the designated room
    try {
      const id = await roomContentsService.createOne({
        room: payload.room,
        type: "directus_files",
        title: payload.title,
        file: item
      });
      // TODO: trigger a webhook to notify the client application about new files
      console.log({ id });
    } catch (error) {
      // TODO: what should we do if this doesn't work?
      console.error(error);
    }
  },
});