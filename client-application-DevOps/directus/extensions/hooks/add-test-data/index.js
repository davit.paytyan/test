const createRoomAndTestUsers = async (ItemsService, serviceOptions, userService, roomUsersService, roomId, roomName, users) => {
  const owner = await userService.upsertOne({
    ...users[0],
    password: "asdf1234",
    role: "5c3407a4-38f2-4f75-a872-1cc7d3417840",
  });

  const moderator = await userService.upsertOne({
    ...users[1],
    password: "asdf1234",
    role: "5c3407a4-38f2-4f75-a872-1cc7d3417840",
  });

  const participant = await userService.upsertOne({
    ...users[2],
    password: "asdf1234",
    role: "5c3407a4-38f2-4f75-a872-1cc7d3417840",
  });

  const roomsService = new ItemsService("rooms", {
    ...serviceOptions,
    accountability: {
      admin: true,
      user: owner,
      ip: "localhost",
      userAgent: "bootstrap",
    },
  });
  
  const room = await roomsService.upsertOne({
    id: roomId,
    name: roomName,
    owner: users[0].id,
  });

  const createdRoom = await roomsService.readOne(room, {
    fields: ["*", "roles.id", "roles.name"],
  });

  const modRole = createdRoom.roles.find(
    (role) => role.name.toLowerCase() === "moderator"
  );
  
  const partRole = createdRoom.roles.find(
    (role) => role.name.toLowerCase() === "participant"
  );

  await roomUsersService.upsertOne({
    id: moderator,
    user: moderator,
    room,
    role: modRole.id,
  });

  await roomUsersService.upsertOne({
    id: participant,
    user: participant,
    room,
    role: partRole.id,
  });
}


const createTestUsers = async ({ services, getSchema, database }, event) => {
  const { ItemsService } = services;
  
  const schema = await getSchema();
  const serviceOptions = { schema, knex: database };
  
  const userService = new ItemsService("directus_users", serviceOptions);
  const roomUsersService = new ItemsService("room_users", serviceOptions);
  

  await createRoomAndTestUsers(ItemsService, serviceOptions, userService, roomUsersService, "test-room", "Test Raum", [
    {
      id: "test-owner",
      first_name: "Max",
      last_name: "Mustermann",
      email: "test-owner@together.biz",
    }, {
      id: "test-moderator",
      first_name: "Hildegard",
      last_name: "Mustermann",
      email: "test-moderator@together.biz",
    }, {
      id: "test-participant",
      first_name: "Ernst",
      last_name: "Mustermann",
      email: "test-participant@together.biz",
    }
  ]);
  await createRoomAndTestUsers(ItemsService, serviceOptions, userService, roomUsersService, "springfield", "Springfield", [
    {
      id: "homer.simpson",
      first_name: "Homer",
      last_name: "Simpson",
      email: "homer.simpson@together.biz",
    }, {
      id: "marge.simpson",
      first_name: "Marge",
      last_name: "Simpson",
      email: "marge.simpson@together.biz",
    }, {
      id: "bart.simpson",
      first_name: "Bart",
      last_name: "Simpson",
      email: "bart.simpson@together.biz",
    }
  ]);
  await createRoomAndTestUsers(ItemsService, serviceOptions, userService, roomUsersService, "entenhausen", "Entenhausen", [
    {
      id: "donald.duck",
      first_name: "Donald",
      last_name: "Duck",
      email: "donald.duck@together.biz",
    }, {
      id: "dagobert.duck",
      first_name: "Dagobert",
      last_name: "Duck",
      email: "dagobert.duck@together.biz",
    }, {
      id: "daisy.duck",
      first_name: "Daisy",
      last_name: "Duck",
      email: "daisy.duck@together.biz",
    }
  ]);
  await createRoomAndTestUsers(ItemsService, serviceOptions, userService, roomUsersService, "gotham", "Gotham City", [
    {
      id: "batman",
      first_name: "Bruce",
      last_name: "Wayne",
      email: "batman@together.biz",
    }, {
      id: "catwoman",
      first_name: "Selina",
      last_name: "Kyle",
      email: "catwoman@together.biz",
    }, {
      id: "robin",
      first_name: "Dick",
      last_name: "Grayson",
      email: "robin@together.biz",
    }
  ]);
};

module.exports = (context) => {
  return {
    "server.start": async (event) => {
      await createTestUsers(context, event);
    },
  };
};
