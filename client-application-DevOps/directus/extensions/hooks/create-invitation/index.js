const checkAccess = async ({ services, exceptions }, event) => {
  const { ItemsService } = services;
  const { BaseException, ForbiddenException } = exceptions;
  const { database: knex, accountability, item, schema } = event;

  const { user } = event.accountability;
  const roomsService = new ItemsService("rooms", {
    knex,
    accountability,
    schema,
  });

  let room;

  try {
    room = await roomsService.readOne(item);
  } catch (error) {
    throw new BaseException("Room not found.", 404, "NOT FOUND");
  }

  
  if (room.owner !== user) throw new ForbiddenException();
  
  return;
};

module.exports = (context) => ({
  "items.create.before": async (data, event) => {
    switch (event.collection) {
      case "invitations":
        await checkAccess(context, { ...event, item: data.room });
        return data;
    }
  },
});
