const path = require("path");
const fs = require("fs");

const markedFiles = {};

async function markFilesAsToBeDeleted({ services }, event) {
  const { ItemsService } = services;
  const { database: knex, accountability, item, schema } = event;
  const opts = { knex, accountability, schema };

  const roomsContentService = new ItemsService("room_content", opts);

  const items = await roomsContentService.readMany(item, { fields: ["id", "file.id"] });

  for (const item of items) {
    const { id, file } = item;
    if (!file) continue;
    if (markedFiles[id]) {
      markedFiles[id].push(file.id);
    } else {
      markedFiles[id] = [file.id]
    }
  }
}

async function deleteMarkedFiles({ services }, event) {
  const { FilesService } = services;
  const { database: knex, accountability, item, schema } = event;
  const opts = { knex, accountability, schema };

  const service = new FilesService(opts);

  try {
    const filesToDelete = markedFiles[item[0]];
    if (filesToDelete) await service.deleteMany(filesToDelete);
  } catch (error) {
    console.log(error);
  }
}

async function loadTemplate(templateType) {
  const templatePath = path.resolve(__dirname, "templates", `${templateType}.txt`);
  const file = await fs.promises.readFile(templatePath, { encoding: "utf-8" });
  return file;
}

async function handleBeforeCreateRoomContent(context, item, event) {
  if (item.type !== "textpad" || typeof item.payload === "undefined") return Promise.resolve(item);

  const options = JSON.parse(item.payload);

  if (typeof options.template === "undefined") return Promise.resolve(item);

  const template = await loadTemplate(options.template);
  const payload = template;
  return { ...item, payload };
}

module.exports = (context) => ({
  "items.create.before": async (item, event) => {
    switch (event.collection) {
      case "room_content":
        const newData = await handleBeforeCreateRoomContent(context, item, event);
        return newData;
    }
  },
  "items.delete.before": async (event) => {
    switch (event.collection) {
      case "room_content":
        markFilesAsToBeDeleted(context, event);
        break;
    }
  },
  "items.delete": async (event) => {
    switch (event.collection) {
      case "room_content":
        deleteMarkedFiles(context, event);
        break;
    }
  },
});
