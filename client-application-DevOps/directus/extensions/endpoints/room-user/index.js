module.exports = (router, context) => {
  const { services, exceptions, database } = context;
  const { ItemsService } = services;
  const { BaseException, ForbiddenException } = exceptions;

  router.get("/:roomId/permissions", async (request, response, next) => {
    try {
      const { roomId } = request.params;
      const { user: userId } = request.accountability;

      const serviceOpts = {
        accountability: request.accountability,
        schema: request.schema,
        knex: database,
      };

      const roomsService = new ItemsService("rooms", serviceOpts);

      try {
        room = await roomsService.readOne(roomId);
      } catch {
        throw new BaseException(`Room not found!`, 404, "NOT FOUND");
      }

      const roomUserService = new ItemsService("room_users", serviceOpts);

      const [roomUser] = await roomUserService.readByQuery({
        fields: ["role.name"],
        filter: {
          room: { _eq: roomId },
          user: { _eq: userId },
        },
      });

      if (!roomUser) throw new ForbiddenException();

      response.json({ data: { role: roomUser.role, permissions: {} } });
    } catch (error) {
      return next(error);
    }
  });

  router.get("/:roomId", async (request, response, next) => {
    try {
      const { roomId } = request.params;
      const { user: userId } = request.accountability;

      const serviceOpts = {
        accountability: request.accountability,
        schema: request.schema,
        knex: database,
      };

      const roomsService = new ItemsService("rooms", serviceOpts);

      try {
        room = await roomsService.readOne(roomId);
      } catch {
        throw new BaseException(`Room not found!`, 404, "NOT FOUND");
      }

      const roomUserService = new ItemsService("room_users", serviceOpts);

      const [roomUser] = await roomUserService.readByQuery({
        fields: ["*", "role.id", "role.name"],
        filter: {
          room: { _eq: roomId },
          user: { _eq: userId },
        },
      });

      if (!roomUser) throw new ForbiddenException();

      response.json({ data: roomUser });
    } catch (error) {
      return next(error);
    }
  });

  router.patch("/:roomId", async (request, response, next) => {
    try {
      const { roomId } = request.params;
      const { user: userId } = request.accountability;

      const serviceOpts = {
        accountability: request.accountability,
        schema: request.schema,
        knex: database,
      };

      const roomsService = new ItemsService("rooms", serviceOpts);

      try {
        room = await roomsService.readOne(roomId);
      } catch {
        throw new BaseException(`Room not found!`, 404, "NOT FOUND");
      }

      const roomUserService = new ItemsService("room_users", serviceOpts);

      const [roomUser] = await roomUserService.readByQuery({
        filter: {
          room: { _eq: roomId },
          user: { _eq: userId },
        },
      });

      if (!roomUser) throw new ForbiddenException();

      const updatedPrimaryKey = await roomUserService.updateOne(
        roomUser.id,
        request.body
      );

      const updatedRoomUser = await roomUserService.readOne(
        updatedPrimaryKey,
        request.sanitizedQuery
      );

      response.json({ data: updatedRoomUser });
    } catch (error) {
      return next(error);
    }
  });

  router.post("/", async (request, response, next) => {
    try {
      /**
       * @typedef Payload
       * @property {string} roomId
       * @property {string} userId
       * @property {string} invitationId
       */
      /**
       * @type {Payload}
       */
      const { userId, roomId, invitationId, roleId } = request.body;
      const serviceOpts = {
        accountability: request.accountability,
        schema: request.schema,
        knex: database,
      };

      const usersService = new ItemsService("directus_users", serviceOpts);
      const roomsService = new ItemsService("rooms", serviceOpts);
      const roomRolesService = new ItemsService("roles", serviceOpts);
      const invitationsService = new ItemsService("invitations", serviceOpts);

      let user, invitation;

      try {
        user = await usersService.readOne(userId);
      } catch {
        throw new BaseException(`User not found!`, 404, "NOT FOUND");
      }

      try {
        room = await roomsService.readOne(roomId);
      } catch {
        throw new BaseException(`Room not found!`, 404, "NOT FOUND");
      }

      try {
        invitation = await invitationsService.readOne(invitationId);
      } catch {
        throw new ForbiddenException();
      }

      if (invitation.email && invitation.email !== user.email)
        throw new ForbiddenException();

      const [participantRole] = await roomRolesService.readByQuery(
        roleId
          ? { filter: { id: { _eq: roleId } } }
          : {
              filter: {
                name: {
                  _eq: "Participant",
                },
                room: {
                  _eq: roomId,
                },
              },
            }
      );

      const roomUserService = new ItemsService("room_users", {
        accountability: request.accountability,
        schema: request.schema,
      });

      const [existingRoomUser] = await roomUserService.readByQuery({
        filter: {
          user: {
            _eq: userId,
          },
          room: {
            _eq: roomId,
          },
        },
      });

      if (existingRoomUser)
        throw new BaseException(`Already a member!`, 404, "ALREADY A MEMBER");

      const id = await roomUserService.createOne({
        user: userId,
        room: roomId,
        role: participantRole.id,
      });

      const roomUser = await roomUserService.readOne(id, {
        fields: ["room", "user", "role", "nickname", "user_colors", "user_index"],
      });

      response.json({ data: roomUser });
    } catch (error) {
      return next(error);
    }
  });
};
