module.exports = (router, context) => {
  const { services, database } = context;
  const { ItemsService } = services;

  router.get("/:room/:type?", async (request, response, next) => {
    try {
      const { room, type } = request.params;

      const serviceOpts = {
        accountability: request.accountability,
        schema: request.schema,
        knex: database,
      };

      //
      // access to the room_contents items is handled via directus permissions
      //
      const roomContentsService = new ItemsService("room_content", serviceOpts);
      const roomContents = await roomContentsService.readByQuery({
        fields: ["id", "title", "sortOrder", "type", "file.id", "file.width", "file.height", "file.type", "file.filesize"],
        filter: {
          _and: [
            { room: { _eq: room } },
            { type: { _eq: type ?? "directus_files" } }
          ]
        },
        sort: [{column: "sortOrder", order: "asc"}],
        limit: -1
      });

      response.json({ data: roomContents });
    } catch (error) {
      return next(error);
    }
  });

  router.patch("/:room/sort", async (request, response, next) => {
    try {
      const serviceOpts = {
        accountability: request.accountability,
        schema: request.schema,
        knex: database,
      };

      const ids = request.body;
      
      const roomContentsService = new ItemsService("room_content", serviceOpts);

      let index = 1;
      for (const id of ids) {
        await roomContentsService.updateOne(id, { sortOrder: index });
        index++;
      }

      response.status(204).send();
    } catch (error) {
      return next(error);
    }
  });
};
