const Joi = require("joi");

// TODO: make this an env var
const APP_ROLE_ID = "5c3407a4-38f2-4f75-a872-1cc7d3417840";

module.exports = async function registerEndpoint(
  router,
  { services, exceptions, database }
) {
  const { UsersService, RolesService } = services;
  const { FailedValidationException, InvalidPayloadException } = exceptions;

  const appUserSchema = Joi.object().keys({
    email: Joi.string().email().required(),
    first_name: Joi.string().required(),
    last_name: Joi.string().required(),
    password: Joi.string().required(),
    tags: Joi.array(),
    acceptTerms: Joi.boolean().truthy(),
    newsletter: Joi.boolean(),
    role: Joi.string().allow(APP_ROLE_ID).required(),
  });

  router.post("/", async (req, res, next) => {
    const usersService = new UsersService({
      schema: req.schema,
      accountability: req.accountability,
      knex: database,
    });

    const rolesService = new RolesService({
      schema: req.schema,
      accountability: req.accountability,
      knex: database,
    });

    try {
      const role = await rolesService.readOne(APP_ROLE_ID);

      if (!role) throw new Error("Required role 'Application User' not found!");

      const data = req.body;
      data.role = role.id;

      const { error } = appUserSchema.validate(data);

      if (error) {
        const [detail] = error.details;
        if (detail.type === "object.unknown") {
          throw new InvalidPayloadException(detail.message);
        }
        throw new FailedValidationException(detail);
      }

      const id = await usersService.createOne(data);
      res.json({ data: { id } });
    } catch (error) {
      next(error);
    }
  });
};
