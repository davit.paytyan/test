module.exports = (router, { services, exceptions }) => {
  const { ItemsService } = services;
  const { BaseException, ForbiddenException } = exceptions;

  // TODO: this could be realized via permission filters in directus directly
  router.get("/", async (request, response, next) => {
    try {
      const { user: userId } = request.accountability;

      const serviceOpts = {
        accountability: request.accountability,
        schema: request.schema,
      };

      const roomService = new ItemsService("rooms", serviceOpts);

      const rooms = await roomService.readByQuery({
        filter: {
          _or: [
            {
              room_users: {
                user: {
                  _eq: userId,
                },
              },
            },
            {
              owner: {
                _eq: userId,
              },
            },
          ],
        },
      });

      response.json({ data: rooms });
    } catch (error) {
      next(error);
    }
  });

  router.get("/:id", async (request, response, next) => {
    try {
      const { id } = request.params;
      const { user } = request.accountability;

      const serviceOpts = {
        accountability: request.accountability,
        schema: request.schema,
      };

      const roomService = new ItemsService("rooms", serviceOpts);

      let room;

      try {
        room = await roomService.readOne(id, {
          fields: [
            "id",
            "name",
            "owner",
            "description",
            "state",
            "invitations",
            "roles",
            "room_users.user",
          ],
        });
      } catch (error) {
        throw new BaseException("Room not found.", 404, "NOT FOUND");
      }

      room.room_users = room.room_users.map(({ user }) => user);

      const isOwner = room.owner === user;
      const isRoomUser = room.room_users.includes(user);

      if (!isOwner && !isRoomUser) throw new ForbiddenException();

      response.json({ data: room });
    } catch (error) {
      next(error);
    }
  });

  router.patch("/:roomId", async (request, response, next) => {
    try {
      const { roomId } = request.params;
      const { user: userId } = request.accountability;

      const serviceOpts = {
        accountability: request.accountability,
        schema: request.schema,
      };

      const roomService = new ItemsService("rooms", serviceOpts);
      const roomUserService = new ItemsService("room_users", serviceOpts);

      let room;

      try {
        room = await roomService.readOne(roomId);
      } catch (error) {
        throw new BaseException("Room not found.", 404, "NOT FOUND");
      }

      let allow = room.owner === userId;

      if (!allow) {
        const [roomUser] = await roomUserService.readByQuery({
          filter: {
            room: { _eq: roomId },
            user: { _eq: userId },
          },
          fields: ["*", "role.*"],
        });

        allow = roomUser && roomUser.role.name === "Moderator";
      }

      if (!allow) throw new ForbiddenException();

      const updatedPrimaryKey = await roomService.updateOne(
        roomId,
        request.body
      );

      room = await roomService.readOne(
        updatedPrimaryKey,
        request.sanitizedQuery
      );

      response.json({ data: room });
    } catch (error) {
      return next(error);
    }
  });
};
