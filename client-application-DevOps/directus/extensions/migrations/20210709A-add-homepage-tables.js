const {
  PREFIX,
  addDefaultColumns,
  addReferenceTo,
  createInsertReference,
  createInsertField,
  createInsertCollection,
  insertDefaultFields,
  insertManyToOneField,
  insertOneToManyField,
  createRemoveReferencesToTable,
  createRemoveFieldsOfTable,
  createRemoveCollectionOfTable,
} = require("./utils");

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    const insertReference = createInsertReference(knex);
    const insertField = createInsertField(knex);
    const insertCollection = createInsertCollection(knex);

    //
    // HOME PAGE TABLE
    //
    await knex.schema.createTable(`${PREFIX}page_index`, (table) => {
      addDefaultColumns(knex, table);
      table.string("hero_title").notNullable();
      table.string("hero_description").nullable();
      table.string("features_title").nullable();
    });

    await insertCollection({
      collection: `${PREFIX}page_index`,
      note: "Homepage",
      accountability: null,
      singleton: true,
    });

    await insertDefaultFields(insertField, `${PREFIX}page_index`);

    await insertField({
      collection: `${PREFIX}page_index`,
      field: "hero_title",
      interface: "input",
    });

    await insertField({
      collection: `${PREFIX}page_index`,
      field: "hero_description",
      interface: "input",
    });

    await insertField({
      collection: `${PREFIX}page_index`,
      field: "features_title",
      interface: "input",
    });

    await insertOneToManyField(insertField, {
      collection: `${PREFIX}page_index`,
      field: "features",
    });

    //
    // HOME PAGE FEATURES TABLE
    //
    await knex.schema.createTable(`${PREFIX}page_index_features`, (table) => {
      addDefaultColumns(knex, table);
      addReferenceTo(table, `${PREFIX}page_index`, "page");
      table.string("title").nullable();
      table.string("description").nullable();
      table.string("icon").nullable();
    });

    await insertCollection({
      collection: `${PREFIX}page_index_features`,
      note: "Feature blocks on the homepage",
      accountability: null,
      display_template: "{{title}}"
    });

    await insertDefaultFields(insertField, `${PREFIX}page_index_features`);

    await insertField({
      collection: `${PREFIX}page_index_features`,
      field: "title",
      interface: "input",
    });

    await insertField({
      collection: `${PREFIX}page_index_features`,
      field: "description",
      interface: "input",
    });

    await insertField({
      collection: `${PREFIX}page_index_features`,
      field: "icon",
      interface: "input",
    });

    await insertManyToOneField(insertField, {
      collection: `${PREFIX}page_index_features`,
      field: "page",
    });

    await insertReference({
      many_collection: `${PREFIX}page_index_features`,
      many_field: "page",
      one_collection: `${PREFIX}page_index`,
      one_field: "features"
    });
  },

  async down(knex) {
    const removeReferencesTo = createRemoveReferencesToTable(knex);
    const removeFieldsOf = createRemoveFieldsOfTable(knex);
    const removeCollectionOf = createRemoveCollectionOfTable(knex);

    await knex.schema.dropTableIfExists(`${PREFIX}page_index_features`);
    await removeReferencesTo(`${PREFIX}page_index_features`);
    await removeFieldsOf(`${PREFIX}page_index_features`);
    await removeCollectionOf(`${PREFIX}page_index_features`);

    await knex.schema.dropTableIfExists(`${PREFIX}page_index`);
    await removeReferencesTo(`${PREFIX}page_index`);
    await removeFieldsOf(`${PREFIX}page_index`);
    await removeCollectionOf(`${PREFIX}page_index`);
  }
}