module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    await knex(`directus_roles`).insert({
      id: "0318da8f-f15a-424a-9d79-114bf4bf34ec",
      name: "Secure API",
      icon: "android",
      description: "System Role; DO NOT DELETE!",
      enforce_tfa: false,
      admin_access: true,
      app_access: false
    });

    await knex(`directus_roles`).insert({
      id: "5c3407a4-38f2-4f75-a872-1cc7d3417840",
      name: "Application",
      icon: "account_box",
      description: "System Role; Used for by application users; DO NOT DELETE!",
      ip_access: null,
      enforce_tfa: false,
      module_list: null,
      collection_list: null,
      admin_access: false,
      app_access: true,
    });

    for (const permission of initialApplicationPermissions) {
      await knex("directus_permissions").insert({
        ...permission,
        fields: permission.fields && Array.isArray(permission.fields) ? permission.fields.join(",") : permission.fields,
        permissions: JSON.stringify(permission.permissions),
        validation: JSON.stringify(permission.validation),
        presets: JSON.stringify(permission.presets),
      });
    }

  },

  async down(knex) {
    await knex(`directus_roles`).where({ "id": "0318da8f-f15a-424a-9d79-114bf4bf34ec" }).del();
    await knex(`directus_roles`).where({ "id": "5c3407a4-38f2-4f75-a872-1cc7d3417840" }).del();
    await knex(`directus_permissions`).where({ "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840" }).del();
  }
}

const initialApplicationPermissions = [
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "rooms",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "room_users",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "roles",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "roles_permissions",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": "*",
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "permissions",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "permissions_groups",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "invitations",
    "action": "create",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "invitations",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "rooms",
    "action": "update",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "room_users",
    "action": "update",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_presets",
    "action": "update",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": null,
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_activity",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": null,
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_activity",
    "action": "create",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": null,
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_permissions",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": null,
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_revisions",
    "action": "read",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_revisions",
    "action": "create",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_revisions",
    "action": "delete",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_revisions",
    "action": "update",
    "permissions": null,
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_folders",
    "action": "create",
    "permissions": null,
    "validation": {
      "owner": {
        "_eq": "$CURRENT_USER"
      }
    },
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_folders",
    "action": "read",
    "permissions": {
      "owner": "$CURRENT_USER"
    },
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_files",
    "action": "create",
    "permissions": null,
    "validation": {
      "uploaded_by": {
        "_eq": "$CURRENT_USER"
      }
    },
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_files",
    "action": "read",
    "permissions": {
      "uploaded_by": {
        "_eq": "$CURRENT_USER"
      }
    },
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_files",
    "action": "update",
    "permissions": {
      "uploaded_by": {
        "_eq": "$CURRENT_USER"
      }
    },
    "validation": {
      "uploaded_by": {
        "_eq": "$CURRENT_USER"
      }
    },
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "directus_files",
    "action": "delete",
    "permissions": {
      "uploaded_by": {
        "_eq": "$CURRENT_USER"
      }
    },
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "room_content",
    "action": "create",
    "permissions": null,
    "validation": {
      "type": {
        "_eq": "directus_files"
      }
    },
    "presets": null,
    "fields": [
      "room",
      "title",
      "type",
      "file",
      "payload"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "room_content",
    "action": "read",
    "permissions": {
      "type": {
        "_eq": "directus_files"
      },
      "file": {
        "uploaded_by": {
          "_eq": "$CURRENT_USER"
        }
      },
      "room": {
        "room_users": {
          "user": {
            "_eq": "$CURRENT_USER"
          }
        }
      }
    },
    "validation": null,
    "presets": null,
    "fields": [
      "*"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "room_content",
    "action": "update",
    "permissions": {
      "type": {
        "_eq": "directus_files"
      },
      "file": {
        "uploaded_by": {
          "_eq": "$CURRENT_USER"
        }
      },
      "room": {
        "room_users": {
          "user": {
            "_eq": "$CURRENT_USER"
          }
        }
      }
    },
    "validation": null,
    "presets": null,
    "fields": [
      "title",
      "file",
      "payload"
    ],
    "limit": null
  },
  {
    "role": "5c3407a4-38f2-4f75-a872-1cc7d3417840",
    "collection": "room_content",
    "action": "delete",
    "permissions": {
      "type": {
        "_eq": "directus_files"
      },
      "file": {
        "uploaded_by": {
          "_eq": "$CURRENT_USER"
        }
      },
      "room": {
        "room_users": {
          "user": {
            "_eq": "$CURRENT_USER"
          }
        }
      }
    },
    "validation": null,
    "presets": null,
    "fields": null,
    "limit": null
  }
];
