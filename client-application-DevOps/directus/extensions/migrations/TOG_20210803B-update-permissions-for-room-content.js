module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {
    await knex("directus_permissions")
      .where({ role: "5c3407a4-38f2-4f75-a872-1cc7d3417840", collection: "room_content", action: "update" })
      .update({ fields: "title,file,payload,sortOrder" });
  },


  async down(knex) {
    await knex("directus_permissions")
      .where({ role: "5c3407a4-38f2-4f75-a872-1cc7d3417840", collection: "room_content", action: "update" })
      .update({ fields: "title,file,payload" });
  }
}

