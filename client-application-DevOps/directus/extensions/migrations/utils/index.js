const _ = require("lodash");

function addDefaultColumns(knex, table) {
  table.uuid("id").primary();
  table.timestamp("date_created").nullable().defaultTo(knex.fn.now());
  table.timestamp("date_updated").nullable();
}

function addReferenceToDirectusUser(table, columnName) {
  table
    .uuid(columnName)
    .notNullable()
    .references("directus_users.id")
    .onDelete("CASCADE");
}

function addReferenceTo(table, tableName, columnName, key = "id", onDelete = "CASCADE") {
  table
    .uuid(columnName)
    .notNullable()
    .references(`${tableName}.${key}`)
    .onDelete(onDelete);
}

function addOptionalReferenceTo(table, tableName, columnName, key = "id", onDelete = "CASCADE") {
  table
    .uuid(columnName)
    .nullable()
    .references(`${tableName}.${key}`)
    .onDelete(onDelete);
}

const defaultRelationObject = {
  many_collection: null,
  many_field: null,
  one_collection: null,
  one_field: null,
  one_allowed_collections: null,
  one_collection_field: null,
  one_deselect_action: "nullify",
  junction_field: null,
  sort_field: null
}

const defaultFieldObject = {
  collection: null,
  field: null,
  special: null,
  interface: null,
  options: null,
  display: null,
  display_options: null,
  readonly: false,
  hidden: false,
  sort: null,
  width: "full",
  group: null,
  translations: null,
  note: null,
}

const defaultCollectionObject = {
  collection: null,
  icon: null,
  note: null,
  display_template: null,
  hidden: false,
  singleton: false,
  translations: null,
  archive_field: null,
  archive_app_filter: true,
  archive_value: null,
  unarchive_value: null,
  sort_field: null,
  accountability: "all",
  color: null,
  item_duplication_fields: null,
}

const createReferenceObject = (options) => {
  return _.merge({}, defaultRelationObject, options);
}

const createFieldObject = (options) => {
  return _.merge({}, defaultFieldObject, options);
}

const createCollectionObject = (options) => {
  return _.merge({}, defaultCollectionObject, options);
}

const createInsertReference = (knex) => async (payload) => {
  const data = createReferenceObject(payload);
  return await knex("directus_relations").insert(data);
}

const createInsertField = (knex) => {
  /**
   * 
   * @param {Object} payload
   * @param {string} payload.collection
   * @param {string} payload.field
   * @param {string} payload.special
   * @param {string} payload.interface
   * @param {string} payload.options
   * @param {string} payload.display
   * @param {string} payload.display_options
   * @param {boolean} payload.readonly
   * @param {boolean} payload.hidden
   * @param {number} payload.sort
   * @param {string} payload.width
   * @param {string} payload.group
   * @param {string} payload.translations
   * @param {string} payload.note
   * @returns 
   */
  return async function insertField(payload) {
    const data = createFieldObject(payload);
    return await knex("directus_fields").insert(data);
  }
}

const createInsertCollection = (knex) => {
  /**
   * 
   * @param {Object} payload
   * @param {string} payload.collection
   * @param {string} payload.icon
   * @param {string} payload.note
   * @param {string} payload.display_template
   * @param {boolean} payload.hidden
   * @param {boolean} payload.singleton
   * @param {string} payload.archive_field
   * @param {string} payload.archive_app_filter
   * @param {string} payload.archive_value
   * @param {string} payload.sort_field
   * @param {string} payload.accountability
   * @param {string} payload.sort
   * @param {string} payload.item_duplication_fields
   * @returns 
   */
  return async function insertCollection(payload) {
    const data = createCollectionObject(payload);
    return await knex("directus_collections").insert(data);
  }
}

async function insertDefaultFields(insertField, collection) {
  await insertField({
    collection,
    field: "id",
    special: "uuid",
    readonly: true,
  });
  await insertField({
    collection,
    field: "date_created",
    special: "date-created",
    interface: "datetime",
    options: JSON.stringify({ includeSeconds: true }),
    display: "datetime",
    display_options: JSON.stringify({ relative: true }),
    readonly: true,
  });
  await insertField({
    collection,
    field: "date_updated",
    special: "date-updated",
    interface: "datetime",
    options: JSON.stringify({ includeSeconds: true }),
    display: "datetime",
    display_options: JSON.stringify({ relative: true }),
    readonly: true,
  });
}

/**
 * @param {Object} payload
 * @param {string} payload.collection
 * @param {string} payload.field
 * @param {string} payload.special
 * @param {string} payload.options
 * @param {string} payload.display_options
 * @param {boolean} payload.readonly
 * @param {boolean} payload.hidden
 * @param {number} payload.sort
 * @param {string} payload.width
 * @param {string} payload.group
 * @param {string} payload.translations
 * @param {string} payload.note
 * @returns 
 */
async function insertManyToOneField(insertField, payload) {
  const data = _.merge({}, {
    interface: "select-dropdown-m2o",
    display: "related-values",
  }, payload);
  return insertField(data);
}

/**
 * @param {Object} payload
 * @param {string} payload.collection
 * @param {string} payload.field
 * @param {string} payload.options
 * @param {string} payload.display_options
 * @param {boolean} payload.readonly
 * @param {boolean} payload.hidden
 * @param {number} payload.sort
 * @param {string} payload.width
 * @param {string} payload.group
 * @param {string} payload.translations
 * @param {string} payload.note
 * @returns 
 */
 async function insertOneToManyField(insertField, payload) {
   const data = _.merge({}, {
    special: "o2m",
    interface: "list-o2m",
    display: "related-values",
  }, payload);
  return insertField(data);
}

const createRemoveReferencesToTable = (knex) => async (tableName) => {
  await knex("directus_relations").where({ many_collection: tableName }).del();
  await knex("directus_relations").where({ one_collection: tableName }).del();
}

const createRemoveFieldsOfTable = (knex) => async (tableName) => {
  await knex("directus_fields").where({ collection: tableName }).del();
}

const createRemoveCollectionOfTable = (knex) => async (tableName) => {
  await knex("directus_collections").where({ collection: tableName }).del();
}

module.exports = {
  PREFIX: "",
  addDefaultColumns,
  addReferenceToDirectusUser,
  addReferenceTo,
  addOptionalReferenceTo,
  createInsertReference,
  createInsertField,
  createInsertCollection,
  insertDefaultFields,
  insertManyToOneField,
  insertOneToManyField,
  createRemoveReferencesToTable,
  createRemoveFieldsOfTable,
  createRemoveCollectionOfTable,
}