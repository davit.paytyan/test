const {
  PREFIX,
  addDefaultColumns,
  addOptionalReferenceTo,
  createInsertReference,
  createInsertField,
  createInsertCollection,
  insertDefaultFields,
  insertManyToOneField,
  createRemoveReferencesToTable,
  createRemoveFieldsOfTable,
  createRemoveCollectionOfTable,
} = require("./utils");

function addReferenceToRoom(table, columnName) {
  table
    .uuid(columnName)
    .notNullable()
    .references(`${PREFIX}rooms.id`)
    .onDelete("CASCADE");
}

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    const insertReference = createInsertReference(knex);
    const insertField = createInsertField(knex);
    const insertCollection = createInsertCollection(knex);

    //
    // ROOM CONTENT
    //
    await knex.schema.createTable(`${PREFIX}room_content`, (table) => {
      addDefaultColumns(knex, table);
      addReferenceToRoom(table, "room");
      addOptionalReferenceTo(table, "directus_files", "file");
      table.string("title").notNullable();
      table.string("type").notNullable();
      table.text("payload").nullable().defaultTo(null);
    });

    await insertCollection({
      collection: `${PREFIX}room_content`,
      icon: "insert_drive_file",
      note: "The content our users share with others.",
      accountability: null,
      display_template: "{{title}} a {{type}} shared in {{room.name}}"
    });

    await insertDefaultFields(insertField, `${PREFIX}room_content`);

    await insertManyToOneField(insertField, {
      collection: `${PREFIX}room_content`,
      field: "room",
    });

    await insertManyToOneField(insertField, {
      collection: `${PREFIX}room_content`,
      field: "file",
      display: "file",
    });

    await insertField({
      collection: `${PREFIX}room_content`,
      field: "title",
      interface: "input",
    });

    await insertField({
      collection: `${PREFIX}room_content`,
      field: "type",
      readonly: true,
    });

    await insertField({
      collection: `${PREFIX}room_content`,
      field: "payload",
      readonly: true,
    });

    await insertReference({
      many_collection: `${PREFIX}room_content`,
      many_field: "room",
      one_collection: `${PREFIX}rooms`,
    });

    await insertReference({
      many_collection: `${PREFIX}room_content`,
      many_field: "file",
      one_collection: "directus_files",
    });
  },
  async down(knex) {
    const removeReferencesTo = createRemoveReferencesToTable(knex);
    const removeFieldsOf = createRemoveFieldsOfTable(knex);
    const removeCollectionOf = createRemoveCollectionOfTable(knex);

    await knex.schema.dropTableIfExists(`${PREFIX}room_content`);
    await removeReferencesTo(`${PREFIX}room_content`);
    await removeFieldsOf(`${PREFIX}room_content`);
    await removeCollectionOf(`${PREFIX}room_content`);
  }
}