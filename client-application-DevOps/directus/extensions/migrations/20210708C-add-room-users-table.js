const {
  PREFIX,
  addDefaultColumns,
  addReferenceToDirectusUser,
  addReferenceTo,
  createInsertReference,
  createInsertField,
  createInsertCollection,
  insertDefaultFields,
  insertManyToOneField,
  createRemoveReferencesToTable,
  createRemoveFieldsOfTable,
  createRemoveCollectionOfTable,
} = require("./utils");

function addReferenceToRoom(table, columnName) {
  table
    .uuid(columnName)
    .notNullable()
    .references(`${PREFIX}rooms.id`)
    .onDelete("CASCADE");
}

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    const insertReference = createInsertReference(knex);
    const insertField = createInsertField(knex);
    const insertCollection = createInsertCollection(knex);

    //
    // ROOM USERS TABLE
    //
    await knex.schema.createTable(`${PREFIX}room_users`, (table) => {
      addDefaultColumns(knex, table);
      addReferenceToDirectusUser(table, "user");
      addReferenceToRoom(table, "room");
      addReferenceTo(table, `${PREFIX}roles`, "role", "id", "RESTRICT");
      table.string("nickname").nullable().defaultTo(null);
      table.json("user_colors").nullable().defaultTo(null);
      table.integer("user_index").notNullable().defaultTo(0);
    });

    await insertCollection({
      collection: `${PREFIX}room_users`,
      icon: "vpn_key",
      note: "A user needs a room user to get access to a room. Think of it like a key with a name tag on it.",
      accountability: null,
      display_template: "{{nickname}} member of {{room.name}} as {{role.name}}"
    });

    await insertDefaultFields(insertField, `${PREFIX}room_users`);

    await insertManyToOneField(insertField, {
      collection: `${PREFIX}room_users`,
      field: "user",
      display: "user",
      display_options: JSON.stringify({ circle: true }),
    });

    await insertManyToOneField(insertField, {
      collection: `${PREFIX}room_users`,
      field: "room",
    });

    await insertManyToOneField(insertField, {
      collection: `${PREFIX}room_users`,
      field: "role",
    });

    await insertField({
      collection: `${PREFIX}room_users`,
      field: "nickname",
      interface: "input",
    });

    await insertField({
      collection: `${PREFIX}room_users`,
      field: "user_colors",
      special: "json",
      interface: "formatted-json-value",
    });

    await insertField({
      collection: `${PREFIX}room_users`,
      field: "user_index",
      display: "raw",
    });

    await insertReference({
      many_collection: `${PREFIX}room_users`,
      many_field: "user",
      one_collection: "directus_users"
    });

    await insertReference({
      many_collection: `${PREFIX}room_users`,
      many_field: "room",
      one_collection: `${PREFIX}rooms`,
      one_field: "room_users"
    });

    await insertReference({
      many_collection: `${PREFIX}room_users`,
      many_field: "roles",
      one_collection: `${PREFIX}roles`,
      one_field: "room_users"
    });

  },
  async down(knex) {
    const removeReferencesTo = createRemoveReferencesToTable(knex);
    const removeFieldsOf = createRemoveFieldsOfTable(knex);
    const removeCollectionOf = createRemoveCollectionOfTable(knex);

    await knex.schema.dropTableIfExists(`${PREFIX}room_users`);
    await removeReferencesTo(`${PREFIX}room_users`);
    await removeFieldsOf(`${PREFIX}room_users`);
    await removeCollectionOf(`${PREFIX}room_users`);
  }
}