const {
  PREFIX,
  addDefaultColumns,
  createInsertReference,
  createInsertField,
  createInsertCollection,
  insertDefaultFields,
  insertManyToOneField,
  createRemoveReferencesToTable,
  createRemoveFieldsOfTable,
  createRemoveCollectionOfTable,
} = require("./utils");

function addReferenceToRoom(table, columnName) {
  table
    .uuid(columnName)
    .notNullable()
    .references(`${PREFIX}rooms.id`)
    .onDelete("CASCADE");
}

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    const insertReference = createInsertReference(knex);
    const insertField = createInsertField(knex);
    const insertCollection = createInsertCollection(knex);

    //
    // ROLES TABLE
    //
    await knex.schema.createTable(`${PREFIX}roles`, (table) => {
      addDefaultColumns(knex, table);
      addReferenceToRoom(table, "room");
      table.string("name").notNullable();
    });

    await insertCollection({
      collection: `${PREFIX}roles`,
      icon: "verified_user",
      note: "The roles that our users can have while being in a room.",
      accountability: null,
      display_template: "{{name}} of {{room.name}}"
    });

    await insertDefaultFields(insertField, `${PREFIX}roles`);

    await insertManyToOneField(insertField, {
      collection: `${PREFIX}roles`,
      field: "room",
    });

    await insertField({
      collection: `${PREFIX}roles`,
      field: "name",
      interface: "input",
      readonly: true,
    });

    await insertReference({
      many_collection: `${PREFIX}roles`,
      many_field: "room",
      one_collection: `${PREFIX}rooms`,
      one_field: "roles"
    });


  },
  async down(knex) {
    const removeReferencesTo = createRemoveReferencesToTable(knex);
    const removeFieldsOf = createRemoveFieldsOfTable(knex);
    const removeCollectionOf = createRemoveCollectionOfTable(knex);

    await knex.schema.dropTableIfExists(`${PREFIX}roles`);
    await removeReferencesTo(`${PREFIX}roles`);
    await removeFieldsOf(`${PREFIX}roles`);
    await removeCollectionOf(`${PREFIX}roles`);
  }
}