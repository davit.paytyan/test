const {
  createInsertField,
  insertManyToOneField,
} = require("./utils");

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    const insertField = createInsertField(knex);

    //
    // ALTER DIRECTUS_FOLDERS TABLE
    //

    await knex.schema.table("directus_folders", (table) => {
      table
        .uuid("owner")
        .nullable()
        .references("directus_users.id")
        .onDelete("CASCADE");
    });

    await insertManyToOneField(insertField, {
      collection: "directus_folders",
      field: "owner",
      display: "user",
      display_options: JSON.stringify({ circle: true }),
    });

  },
  async down(knex) {

    await knex.schema.table("directus_folders", (table) => {
      table.dropForeign("owner");
      table.dropColumn("owner");
    });

    await knex("directus_fields").where({
      collection: "directus_folders",
      field: "owner"
    }).del();
    
    await knex("directus_relations").where({
      many_collection: "directus_folders",
      many_field: "owner",
      one_collection: "directus_users"
    }).del();
  }
}