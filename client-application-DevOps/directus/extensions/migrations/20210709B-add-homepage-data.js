const { PREFIX } = require("./utils");
const { v4: uuid } = require("uuid");

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {
    
    const id = uuid();

    await knex(`${PREFIX}page_index`).insert({
      id,
      hero_title: "Together helps you to be active, be in flow and be satisfied",
      hero_description: "To get more to the point, sharpen topics and get better & faster decisions.",
      features_title: "Coming soon – Better Meetings with Together",
    });

    await knex(`${PREFIX}page_index_features`).insert({
      id: uuid(),
      page: id,
      title: "BEST DEAL",
      description: "The best of four apps for the price of one, without hazzle in your browser.",
      icon: "gift-line",
    });

    await knex(`${PREFIX}page_index_features`).insert({
      id: uuid(),
      page: id,
      title: "TALK TOGETHER, WORK TOGETHER – BE IN FLOW",
      description: "Together is the first solution that combines videoconferencing with collaboration in an easy and frictionless way for professional people.",
      icon: "apps-line",
    });

    await knex(`${PREFIX}page_index_features`).insert({
      id: uuid(),
      page: id,
      title: "TOUGH EU DATA PROTECTION LEVEL",
      description: "Together is strictly DSGVO compliant, does not set any tracking or marketing cookies, and never shares any data.",
      icon: "shield-star-line",
    });

  },

  async down(knex) {
    await knex(`${PREFIX}page_index_features`).whereNotNull("id").del();
    await knex(`${PREFIX}page_index`).whereNotNull("id").del();
  }
}