const env = require("directus/dist/env").default;

const API_USER_EMAIL = "api-user@together.biz"

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    if (!env.INTERNAL_API_TOKEN) {
      throw new Error("INTERNAL_API_TOKEN must be set as environment variable!");
    }

    await knex(`directus_users`).insert({
      id: "api-user",
      email: API_USER_EMAIL,
      token: env.INTERNAL_API_TOKEN,
      role: "0318da8f-f15a-424a-9d79-114bf4bf34ec",
    });
  },

  async down(knex) {
    await knex("directus_users").where({ email: API_USER_EMAIL }).del();
  }
}