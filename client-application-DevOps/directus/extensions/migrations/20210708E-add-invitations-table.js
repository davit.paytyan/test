const {
  PREFIX,
  addDefaultColumns,
  createInsertReference,
  createInsertField,
  createInsertCollection,
  insertDefaultFields,
  insertManyToOneField,
  createRemoveReferencesToTable,
  createRemoveFieldsOfTable,
  createRemoveCollectionOfTable,
} = require("./utils");

function addReferenceToRoom(table, columnName) {
  table
    .uuid(columnName)
    .notNullable()
    .references(`${PREFIX}rooms.id`)
    .onDelete("CASCADE");
}

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    const insertReference = createInsertReference(knex);
    const insertField = createInsertField(knex);
    const insertCollection = createInsertCollection(knex);

    //
    // INVITATIONS TABLE
    //
    await knex.schema.createTable(`${PREFIX}invitations`, (table) => {
      addDefaultColumns(knex, table);
      addReferenceToRoom(table, "room");
      table.string("email").nullable().defaultTo(null);
    });

    await insertCollection({
      collection: `${PREFIX}invitations`,
      icon: "how_to_reg",
      note: "The tickets to greatness.",
      accountability: null,
      display_template: "invitation to {{room.name}} {{email}}"
    });

    await insertDefaultFields(insertField, `${PREFIX}invitations`);

    await insertManyToOneField(insertField, {
      collection: `${PREFIX}invitations`,
      field: "room",
    });

    await insertField({
      collection: `${PREFIX}invitations`,
      field: "email",
      readonly: true,
    });

    await insertReference({
      many_collection: `${PREFIX}invitations`,
      many_field: "room",
      one_collection: `${PREFIX}rooms`,
      one_field: "invitations",
    });

  },
  async down(knex) {
    const removeReferencesTo = createRemoveReferencesToTable(knex);
    const removeFieldsOf = createRemoveFieldsOfTable(knex);
    const removeCollectionOf = createRemoveCollectionOfTable(knex);

    await knex.schema.dropTableIfExists(`${PREFIX}invitations`);
    await removeReferencesTo(`${PREFIX}invitations`);
    await removeFieldsOf(`${PREFIX}invitations`);
    await removeCollectionOf(`${PREFIX}invitations`);
  }
}