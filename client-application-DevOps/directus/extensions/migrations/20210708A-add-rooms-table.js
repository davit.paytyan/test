const {
  PREFIX,
  addDefaultColumns,
  addReferenceToDirectusUser,
  createInsertReference,
  createInsertField,
  createInsertCollection,
  insertDefaultFields,
  insertManyToOneField,
  insertOneToManyField,
  createRemoveReferencesToTable,
  createRemoveFieldsOfTable,
  createRemoveCollectionOfTable,
} = require("./utils");

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    const insertReference = createInsertReference(knex);
    const insertField = createInsertField(knex);
    const insertCollection = createInsertCollection(knex);

    //
    // ROOMS TABLE
    //
    await knex.schema.createTable(`${PREFIX}rooms`, (table) => {
      addDefaultColumns(knex, table);
      addReferenceToDirectusUser(table, "owner");
      table.string("name").notNullable();
      table.string("state").notNullable().defaultTo("created");
      table.string("description").nullable().defaultTo(null);
      table.string("password").nullable().defaultTo(null);
    });

    await insertCollection({
      collection: `${PREFIX}rooms`,
      icon: "meeting_room",
      note: "Where our users meet and achive great things!",
      accountability: null,
      display_template: "{{name}} - owned by: {{owner.first_name}} {{owner.last_name}}"
    });

    await insertDefaultFields(insertField, `${PREFIX}rooms`);

    await insertField({
      collection: `${PREFIX}rooms`,
      field: "name",
      interface: "input",
    });

    await insertField({
      collection: `${PREFIX}rooms`,
      field: "state",
      readonly: true,
    });

    await insertField({
      collection: `${PREFIX}rooms`,
      field: "description",
      interface: "input",
    });

    await insertField({
      collection: `${PREFIX}rooms`,
      field: "password",
      interface: "input",
    });

    await insertOneToManyField(insertField, {
      collection: `${PREFIX}rooms`,
      field: "room_users",
    });

    await insertOneToManyField(insertField, {
      collection: `${PREFIX}rooms`,
      field: "roles",
    });

    await insertManyToOneField(insertField, {
      collection: `${PREFIX}rooms`,
      field: "owner",
      display: "user",
      display_options: JSON.stringify({ circle: true }),
    });

    await insertReference({
      many_collection: `${PREFIX}rooms`,
      many_field: "owner",
      one_collection: "directus_users"
    });
  },
  async down(knex) {
    const removeReferencesTo = createRemoveReferencesToTable(knex);
    const removeFieldsOf = createRemoveFieldsOfTable(knex);
    const removeCollectionOf = createRemoveCollectionOfTable(knex);

    await knex.schema.dropTableIfExists(`${PREFIX}rooms`);
    await removeReferencesTo(`${PREFIX}rooms`);
    await removeFieldsOf(`${PREFIX}rooms`);
    await removeCollectionOf(`${PREFIX}rooms`);
  }
}