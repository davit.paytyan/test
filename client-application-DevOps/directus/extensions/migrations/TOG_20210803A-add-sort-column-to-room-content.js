const { createInsertField, PREFIX } = require("./utils");

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {

    const insertField = createInsertField(knex);

    await knex.schema.table(`${PREFIX}room_content`, (table) => {
      table.integer("sortOrder").unsigned().nullable();
    });

    await insertField({
      collection: `${PREFIX}room_content`,
      field: "sortOrder",
      display: "raw",
      readonly: true,
      hidden: true,
    });

    const rooms = await knex.select("id").from(`${PREFIX}rooms`);

    for (const room of rooms) {
      let sortOrder = 0;
      const roomContents = await knex
        .select("id", "date_created")
        .where({ "room": room.id })
        .from(`${PREFIX}room_content`)
        .orderBy("date_created");

      for (const roomContent of roomContents) {
        await knex(`${PREFIX}room_content`).where({ id: roomContent.id }).update({ sortOrder });
        sortOrder++;
      }
    }


  },
  async down(knex) {

    await knex.schema.table(`${PREFIX}room_content`, (table) => {
      table.dropColumn("sortOrder");
    });

    await knex("directus_fields").where({
      collection: `${PREFIX}room_content`,
      field: "sortOrder"
    }).del();

  }
}