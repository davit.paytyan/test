module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {
    await knex("directus_permissions")
      .where({ role: "5c3407a4-38f2-4f75-a872-1cc7d3417840", collection: "room_content" })
      .whereIn("action", ["create"])
      .update({
        validation: '{"type":{"_in":["directus_files", "textpad"]}}',
        permissions: '{"room":{"room_users":{"user":{"_eq":"$CURRENT_USER"}}}}'
      });
  },


  async down(knex) {
    await knex("directus_permissions")
      .where({ role: "5c3407a4-38f2-4f75-a872-1cc7d3417840", collection: "room_content" })
      .whereIn("action", ["create"])
      .update({
        validation: '{"type":{"_eq":"directus_files"}}',
        permissions: null
      });
  }
}