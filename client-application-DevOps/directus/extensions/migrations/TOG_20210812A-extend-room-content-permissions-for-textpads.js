const newPermisions = `{"_or": [{"_and": [{"type": {"_eq": "directus_files"}},{"file": {"uploaded_by": {"_eq": "$CURRENT_USER"}}}]},{"type": {"_eq": "textpad"}}],"room": {"room_users": {"user": {"_eq": "$CURRENT_USER"}}}}`;
const oldPermissions = `{"type":{"_eq":"directus_files"},"file":{"uploaded_by":{"_eq":"$CURRENT_USER"}},"room":{"room_users":{"user":{"_eq":"$CURRENT_USER"}}}}`

module.exports = {
  /**
   * @param {Knex} knex 
   */
  async up(knex) {
    await knex("directus_permissions")
      .where({ role: "5c3407a4-38f2-4f75-a872-1cc7d3417840", collection: "room_content" })
      .whereIn("action", ["read", "update", "delete"])
      .update({ permissions: newPermisions });
  },


  async down(knex) {
    await knex("directus_permissions")
      .where({ role: "5c3407a4-38f2-4f75-a872-1cc7d3417840", collection: "room_content" })
      .whereIn("action", ["read", "update", "delete"])
      .update({ permissions: oldPermissions });
  }
}

/* JSON of the new permissions
{
  "_or": [
    {
      "_and": [
        {
          "type": {
            "_eq": "directus_files"
          }
        },
        {
          "file": {
            "uploaded_by": {
              "_eq": "$CURRENT_USER"
            }
          }
        }
      ]
    },
    {
      "type": {
        "_eq": "textpad"
      }
    }
  ],
  "room": {
    "room_users": {
      "user": {
        "_eq": "$CURRENT_USER"
      }
    }
  }
}
*/