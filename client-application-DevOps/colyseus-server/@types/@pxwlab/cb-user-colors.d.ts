declare module "@pxwlab/cb-user-colors" {
  export type UserColorStruct = {
    name: string;
    userColor: string;
    userColor15: string;
    userColor30: string;
    userColor50: string;
  };

  function getUserColor(options: { userIndex: number }): Promise<UserColorStruct>;

  export default getUserColor;
}
