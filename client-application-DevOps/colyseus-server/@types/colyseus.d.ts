import "colyseus";

declare module "colyseus" {
  export interface Client {
    userData: {
      jitsiId?: string;
      sortIndex?: number;
    };
  }
}
