import { Room, Client } from "colyseus.js";

export function requestJoinOptions(this: Client, index: number) {
  return { requestNumber: index };
}

export function onJoin(this: Room) {
  console.log(this.sessionId, "joined.");

  this.onMessage("*", (type, message) => {
    console.log(this.sessionId, "received:", type, message);
  });
}

export function onLeave(this: Room) {
  console.log(this.sessionId, "left.");
}

export function onError(this: Room, error: unknown) {
  console.log(this.sessionId, "!! ERROR !!", error.message);
}

export function onStateChange(this: Room, state: unknown) {
  console.log(this.sessionId, "new state:", state);
}
