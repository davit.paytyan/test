import { MapSchema, Schema, type } from "@colyseus/schema";
import { RoomParticipantSchema } from "./room-participant-schema";
import { RoomStageItemSchema } from "./room-stage-item-schema";

export class RoomSchema extends Schema {
  @type("string") roomId!: string;
  @type("string") roomName!: string;

  @type({ map: RoomStageItemSchema })
  stage = new MapSchema<RoomStageItemSchema>();

  @type({ map: RoomParticipantSchema })
  participants = new MapSchema<RoomParticipantSchema>();

  @type({ map: "string" })
  clientUserIds = new MapSchema<string>();
}
