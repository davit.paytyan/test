import { Schema, type } from "@colyseus/schema";

export class QuickReactionSchema extends Schema {
  @type("string") type!: string;
  @type("string") value!: string;
  @type("number") createdOn?: number;
}

export class RoomParticipantSchema extends Schema {
  @type("string") userId!: string;
  @type("string") conferenceUserId!: string;
  @type("string") nickname!: string;

  @type("string") userColor!: string;
  @type("string") userRoleId!: string;
  @type("string") userRoleName!: string;

  @type("number") sortIndex!: number;

  @type("boolean") isOnStage = false;
  @type("boolean") isVideoMirrored = false;
  @type("boolean") isAudioMuted = false;
  @type("boolean") isVideoMuted = false;

  @type(QuickReactionSchema)
  quickReaction?: QuickReactionSchema;
}
