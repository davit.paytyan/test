import { Schema, type } from "@colyseus/schema";

export class RoomStageItemPayloadSchema extends Schema {
  // participant
  @type("string") userId!: string;
  @type("string") conferenceUserId!: string;
  @type("string") nickname!: string;
  @type("string") userColor!: string;

  // content
  @type("string") fileId!: string;
}

export class RoomStageItemSchema extends Schema {
  @type("string") id!: string;
  @type("string") title!: string;
  @type("string") type!: string;
  @type("string") ownerId!: string;
  @type("number") sortIndex!: number;

  @type(RoomStageItemPayloadSchema)
  payload = new RoomStageItemPayloadSchema();
}
