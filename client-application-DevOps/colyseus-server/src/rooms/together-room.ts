import { Directus, ID } from "@directus/sdk";
import { Client, Presence, Room, ServerError } from "colyseus";
import environment from "../env";
import { QuickReactionSchema, RoomParticipantSchema } from "./schema/room-participant-schema";
import { RoomSchema } from "./schema/room-schema";
import { RoomStageItemSchema } from "./schema/room-stage-item-schema";

const apiUrl = environment.INTERNAL_API_URL as string;
const apiToken = environment.INTERNAL_API_TOKEN as string;
const moderatorRoles = new Set(["owner", "moderator"]);

export class TogetherRoom extends Room<RoomSchema> {
  protected _directus!: Directus<CollectionMapping>;
  protected _clientUserIdIndex: Map<Client, string> = new Map();

  constructor(presence?: Presence) {
    super(presence);
    this._setRoomName = this._setRoomName.bind(this);
    this._putItemOnStage = this._putItemOnStage.bind(this);
    this._removeItemFromStage = this._removeItemFromStage.bind(this);
    this._renameItemOnStage = this._renameItemOnStage.bind(this);
    this._mirrorVideoTrack = this._mirrorVideoTrack.bind(this);
    this._setParticipantQuickReaction = this._setParticipantQuickReaction.bind(this);
    this._clearParticipantQuickReaction = this._clearParticipantQuickReaction.bind(this);
    this._setParticipantConferenceUserId = this._setParticipantConferenceUserId.bind(this);
  }

  async onCreate(parameters: CreateParameters) {
    this.roomId = parameters.roomId;
    this.setPrivate(true);

    this._directus = new Directus<CollectionMapping>(apiUrl);
    await this._directus.auth.static(apiToken);

    let room;

    try {
      room = await this._directus.items("rooms").readOne(this.roomId, {
        fields: ["id", "name", "owner", "room_users.user"],
      });
    } catch {
      throw new ServerError(404, "Room does not exist");
    }

    const isRoomUser = room?.room_users?.find((roomUser) => {
      return roomUser?.user === parameters.userId;
    });

    if (!isRoomUser) throw new ServerError(403, "Access Denied");

    this.setState(new RoomSchema({ roomId: this.roomId, roomName: room?.name }));

    this.onMessage("setRoomName", this._setRoomName);
    this.onMessage("putItemOnStage", this._putItemOnStage);
    this.onMessage("removeItemFromStage", this._removeItemFromStage);
    this.onMessage("renameItemOnStage", this._renameItemOnStage);
    this.onMessage("mirrorVideoTrack", this._mirrorVideoTrack);
    this.onMessage("setParticipantQuickReaction", this._setParticipantQuickReaction);
    this.onMessage("clearParticipantQuickReaction", this._clearParticipantQuickReaction);
    this.onMessage("setParticipantConferenceUserId", this._setParticipantConferenceUserId);

    // broadcasts
    this.onMessage("refreshTextboards", (client: Client) => {
      console.log("FOOO");
      this.broadcast("onRefreshTextboards", null, { except: client });
    });

    // this.onMessage("setMyNickname", not implemented yet)
  }

  onDispose() {
    console.log(`Disposing room ${this.roomId}.`);
  }

  async onJoin(client: Client, { userId, nickname, isVideoMirrored = false }: JoinParameters) {
    const { data } = await this._directus.items("room_users").readMany({
      filter: { room: this.roomId, user: userId },
      fields: ["id", "*", "role.id", "role.name", "user_colors.*"],
    });

    const [roomUser] = data ?? [];

    console.log(roomUser, nickname);

    if (!roomUser || !roomUser.id) throw new ServerError(403);

    await this._directus.items("room_users").updateOne(roomUser.id, { nickname });

    const participant = new RoomParticipantSchema();

    participant.assign({
      userId,
      nickname,
      userRoleId: roomUser.role?.id,
      userRoleName: roomUser.role?.name,
      userColor: roomUser.user_colors?.userColor,
      sortIndex: this.state.participants.size + 1,
      isVideoMirrored,
    });

    this.state.participants.set(userId, participant);
    this._clientUserIdIndex.set(client, userId);

    this.broadcast("participantJoined", { userId }, { except: client });

    // send the users room role to its client
    client.send("updateRoomUserRole", {
      id: participant.userRoleId,
      name: participant.userRoleName,
    });

    console.log(`Participant ${userId} joined.`);
  }

  async onLeave(client: Client) {
    const userId = this._clientUserIdIndex.get(client);

    if (!userId) {
      console.warn(`Missing userId for client ${client.id}!`);
      return;
    }

    for (const [itemId, item] of this.state.stage.entries()) {
      if (item.id === userId || item.ownerId === userId) this.state.stage.delete(itemId);
    }

    if (this.state.participants.has(userId)) this.state.participants.delete(userId);

    this._clientUserIdIndex.delete(client);

    this.broadcast("participantLeft", { userId }, { except: client });
    console.log(`Participant ${userId} left.`);
  }

  protected async _setRoomName(client: Client, payload: SetRoomStageNamePayload) {
    if (!this._isClientModerator(client)) {
      client.send("setRoomNameFailed", { reason: "AccessDenied" });
      return;
    }

    this.state.assign({ roomName: payload.name });
    client.send("setRoomNameCompleted");
  }

  protected _putItemOnStage(client: Client, payload: PutItemOnStagePayload) {
    //
    // CHECK IF USER IS AVAILABLE AND ITEM CAN BE PUT ON STAGE
    //
    const userId = this._clientUserIdIndex.get(client);

    if (!userId) {
      return this._putItemOnStageFailed(client, payload.id, "MissingClientUserId");
    }

    if (this.state.stage.size >= 4) {
      return this._putItemOnStageFailed(client, payload.id, "StageIsFull");
    }

    if (this.state.stage.has(payload.id)) {
      return this._putItemOnStageFailed(client, payload.id, "ItemAlreadyOnStage");
    }

    //
    // BASE ITEM
    //

    const stageItem = new RoomStageItemSchema();

    //
    // PARTICIPANT
    //

    if (payload.type === "participant") {
      const participant = this.state.participants.get(payload.id);

      if (!participant) {
        return this._putItemOnStageFailed(client, payload.id, "MissingParticipant");
      }

      stageItem.assign({
        id: payload.id,
        type: payload.type,
        ownerId: userId,
        title: participant.nickname,
        sortIndex: participant.sortIndex,
      });

      stageItem.payload.assign({
        userId: participant.userId,
        nickname: participant.nickname,
        userColor: participant.userColor,
        conferenceUserId: participant.conferenceUserId,
      });

      participant.assign({ isOnStage: true });

      this.state.stage.set(payload.id, stageItem);
      return this._putItemOnStageCompleted(client, payload);
    }

    //
    // IMAGE
    //

    if (payload.type === "directus_files") {
      const participant = this.state.participants.get(userId);

      if (!participant) {
        return this._putItemOnStageFailed(client, payload.id, "MissingParticipant");
      }

      if (!payload.payload) {
        return this._putItemOnStageFailed(client, payload.id, "FileIdMissing");
      }

      stageItem.assign({
        id: payload.id,
        type: payload.type,
        ownerId: userId,
        title: payload.title,
        sortIndex: this.state.stage.size + 1,
      });

      stageItem.payload.assign({
        fileId: payload.payload,
        nickname: participant.nickname,
      });

      this.state.stage.set(payload.id, stageItem);
      return this._putItemOnStageCompleted(client, payload);
    }

    //
    // TEXTBOARD
    //

    if (payload.type === "textpad") {
      stageItem.assign({
        id: payload.id,
        type: payload.type,
        ownerId: "__shared-in-room__", // workaround to grant permissions to all for textpads
        title: payload.title,
        sortIndex: this.state.stage.size + 1,
      });

      this.state.stage.set(payload.id, stageItem);
      return this._putItemOnStageCompleted(client, payload);
    }

    //
    // SHARED_SCREEN
    //

    if (payload.type === "shared_screen") {
      const participant = this.state.participants.get(userId);

      if (!participant) {
        return this._putItemOnStageFailed(client, payload.id, "MissingParticipant");
      }

      stageItem.assign({
        id: payload.id,
        type: payload.type,
        ownerId: userId,
        title: participant.nickname,
        sortIndex: this.state.stage.size + 1,
      });

      stageItem.payload.assign({
        conferenceUserId: payload.id,
        nickname: participant.nickname,
      });

      this.state.stage.set(payload.id, stageItem);
      return this._putItemOnStageCompleted(client, payload);
    }

    return this._putItemOnStageFailed(client, payload.id, "UnknownType");
  }

  protected _putItemOnStageCompleted(client: Client, payload: PutItemOnStagePayload) {
    console.log(`PutItemOnStageCompleted (id: ${payload.id}, type: ${payload.type})`);
    client.send("putItemOnStageCompleted", { id: payload.id });
  }

  protected _putItemOnStageFailed(client: Client, id: string, reason: string) {
    console.warn(`PutItemOnStageFailed (id: ${id}, reason: ${reason})`);
    client.send("putItemOnStageFailed", { id, reason });
  }

  protected _removeItemFromStage(client: Client, payload: RemoveItemFromStagePayload) {
    //
    // CHECK IF USER IS AVAILABLE AND ITEM CAN BE REMOVED FROM STAGE
    //
    const userId = this._clientUserIdIndex.get(client);

    if (!userId) {
      return this._removeItemFromStageFailed(client, payload.id, "MissingClientUserId");
    }

    const stageItem = this.state.stage.get(payload.id);

    if (!stageItem) {
      return this._removeItemFromStageFailed(client, payload.id, "ItemNotOnStage");
    }

    //
    // CHECK IF CLIENT HAS PERMISSIONS AND REMOVE ITEM FROM STAGE
    //
    if (
      this._isClientModerator(client) ||
      stageItem.ownerId === userId ||
      stageItem.ownerId === "__shared-in-room__"
    ) {
      if (stageItem.type === "participant") {
        const participant = this.state.participants.get(payload.id);

        if (!participant) {
          return this._putItemOnStageFailed(client, payload.id, "MissingParticipant");
        }

        participant.assign({ isOnStage: false });
      }
      this.state.stage.delete(payload.id);
      return this._removeItemFromStageCompleted(client, payload.id);
    }

    //
    // CLIENT IS NOT ALLOWED TO REMOVE ITEM FROM STAGE
    //
    return this._removeItemFromStageFailed(client, payload.id, "AccessDenied");
  }

  protected _removeItemFromStageCompleted(client: Client, id: string) {
    client.send("removeItemFromStageCompleted", { id });
  }

  protected _removeItemFromStageFailed(client: Client, id: string, reason: string) {
    console.warn(`RemoveItemFromStageFailed (id: ${id}, reason: ${reason})`);
    client.send("removeItemFromStageFailed", { id, reason });
  }

  protected async _renameItemOnStage(client: Client, payload: RenameItemOnStagePayload) {
    //
    // CHECK IF USER IS AVAILABLE AND ITEM CAN BE REMOVED FROM STAGE
    //
    const userId = this._clientUserIdIndex.get(client);

    if (!userId) {
      return this._renameItemOnStageFailed(client, payload.id, "MissingClientUserId");
    }

    const stageItem = this.state.stage.get(payload.id);

    if (!stageItem) {
      return this._renameItemOnStageFailed(client, payload.id, "ItemNotOnStage");
    }

    //
    // CHECK IF CLIENT HAS PERMISSIONS AND RENAME ITEM FROM STAGE
    //
    if (
      this._isClientModerator(client) ||
      stageItem.ownerId === userId ||
      stageItem.ownerId === "__shared-in-room__" // workaround to allow all users rename a textpad card
    ) {
      if (stageItem.type === "directus_files" || stageItem.type === "textpad") {
        try {
          await this._directus
            .items("room_content")
            .updateOne(stageItem.id, { title: payload.title });
          stageItem.assign({ title: payload.title });
        } catch (error) {
          console.error(error);
          return this._renameItemOnStageFailed(client, payload.id, "APIError");
        }
      }
      return this._renameItemOnStageCompleted(client, payload, stageItem.type);
    }

    //
    // CLIENT IS NOT ALLOWED TO REMOVE ITEM FROM STAGE
    //
    return this._renameItemOnStageFailed(client, payload.id, "AccessDenied");
  }

  protected _renameItemOnStageCompleted(
    client: Client,
    payload: RenameItemOnStagePayload,
    itemType: string
  ) {
    console.log(`RenameItemOnStageCompleted (id: ${payload.id}, title: ${payload.title})`);
    this.broadcast("renameItemOnStageCompleted", {
      id: payload.id,
      itemType,
      newItemName: payload.title,
    });
  }

  protected _renameItemOnStageFailed(client: Client, id: string, reason: string) {
    console.warn(`RenameItemOnStageFailed (id: ${id}, reason: ${reason})`);
    client.send("renameItemOnStageFailed", { id, reason });
  }

  protected _setParticipantQuickReaction(client: Client, payload: { type: string; value: string }) {
    const userId = this._clientUserIdIndex.get(client);

    if (!userId) {
      const message = `Couldn't set the quick reaction of client ${client.id}. (Reason: userId is missing)`;
      console.warn(message);
      return;
    }

    const participant = this.state.participants.get(userId);

    if (!participant) {
      const message = `Couldn't set the quick reaction of client ${client.id}. (Reason: participant missing)`;
      console.warn(message);
      return;
    }

    const quickReaction = new QuickReactionSchema({ ...payload, createdOn: Date.now() });
    participant.assign({ quickReaction });
  }

  protected _clearParticipantQuickReaction(client: Client) {
    const userId = this._clientUserIdIndex.get(client);

    if (!userId) {
      const message = `Couldn't clear the quick reaction of client ${client.id}. (Reason: userId is missing)`;
      console.warn(message);
      return;
    }

    const participant = this.state.participants.get(userId);

    if (!participant) {
      const message = `Couldn't clear the quick reaction of client ${client.id}. (Reason: participant missing)`;
      console.warn(message);
      return;
    }

    participant.assign({ quickReaction: undefined });
  }

  protected _setParticipantConferenceUserId(client: Client, payload: { conferenceUserId: string }) {
    const userId = this._clientUserIdIndex.get(client);

    if (!userId) {
      const message = `Couldn't set the conferenceUserId of client ${client.id}. (Reason: userId is missing)`;
      console.warn(message);
      return;
    }

    const participant = this.state.participants.get(userId);

    if (!participant) {
      const message = `Couldn't set the conferenceUserId of client ${client.id}. (Reason: participant is missing)`;
      console.warn(message);
      return;
    }

    participant.assign({ conferenceUserId: payload.conferenceUserId });
    console.log(`Assigned conferenceUserId ${payload.conferenceUserId} to participant ${userId}`);
  }

  protected _mirrorVideoTrack(client: Client, { mirrored }: { mirrored: boolean }) {
    const userId = this._clientUserIdIndex.get(client);

    if (!userId) {
      const message = `Couldn't set isVideoMirrored of client ${client.id}. (Reason: userId is missing)`;
      console.warn(message);
      return;
    }

    const participant = this.state.participants.get(userId);

    if (!participant) {
      const message = `Couldn't set isVideoMirrored of client ${client.id}. (Reason: participant is missing)`;
      console.warn(message);
      return;
    }

    participant.assign({ isVideoMirrored: mirrored });
    console.log(`Set isVideoMirrored to ${mirrored} of client ${client.id}!`);
  }

  protected _isClientModerator(client: Client) {
    const userId = this._clientUserIdIndex.get(client);
    if (!userId) return false;
    const participant = this.state.participants.get(userId);
    if (!participant) return false;
    return moderatorRoles.has(participant.userRoleName.toLowerCase());
  }
}

//
// TYPES
//
type JoinParameters = { userId: string; nickname?: string; isVideoMirrored?: boolean };
type CreateParameters = { roomId: string } & JoinParameters;
type SetRoomStageNamePayload = { name: string };
type PutItemOnStagePayload = { id: string; type: string; title?: string; payload?: string };
type RemoveItemFromStagePayload = { id: string };
type RenameItemOnStagePayload = { id: string; title: string };
type CollectionMapping = {
  rooms: {
    id: ID;
    name: string;
    owner: string;
    room_users: Array<{ user: string }>;
  };
  room_users: {
    id: string;
    room: string;
    user: string;
    role: {
      id: string;
      name: string;
    };
    nickname?: string;
    user_index: number;
    user_colors: {
      name: string;
      userColor: string;
      userColor15: string;
      userColor30: string;
      userColor50: string;
    };
  };
};
