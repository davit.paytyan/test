import { Directus } from "@directus/sdk";
import { Server } from "colyseus";
import express from "express";
import { createServer } from "http";
import environment from "./env";
import { TogetherRoom } from "./rooms/together-room";

async function main() {
  const port = environment.COLYSEUS_PORT ? Number(environment.COLYSEUS_PORT) : 2567;
  const apiUrl = environment.INTERNAL_API_URL as string;
  const apiToken = environment.INTERNAL_API_TOKEN as string;

  const directus = new Directus(apiUrl);

  await directus.auth.static(apiToken);

  const app = express();
  app.use(express.json());

  const gameServer = new Server({
    server: createServer(app),
  });

  // @ts-ignore the options will be merged
  gameServer.define("together_room", TogetherRoom);

  void gameServer.listen(port);
}

async function init() {
  try {
    await main();
    console.log("Started Colyseus on port 2567");
  } catch (error) {
    console.error("ERROR:", error.message);
    console.warn("Could not start server due to an error. Retry in 5000ms!");
    setTimeout(() => {
      init();
    }, 5000);
  }
}

init();
